                          TR7 Version 0.0

     "La perfection est atteinte, non pas lorsqu'il n'y a plus rien à ajouter,
                mais lorsqu'il n'y a plus rien à retirer."
                      -- Antoine de Saint-Exupéry --

        "Perfection is achieved, not when there is nothing left to add,
               but when there is nothing left to take away."

-------------------------------------------------------------------------------

     This TR7 Scheme interpreter is based on TinySCHEME Version 1.41
     TinyScheme interpreter is based on MiniSCHEME version 0.85k4

     Whose is the next?

-------------------------------------------------------------------------------

     What is TR7?
     ------------

     TR7 is a lightweight Scheme interpreter that implements the revision
     R7RS small of scheme programming language.
     It is meant to be used as an embedded scripting interpreter for
     other programs. As such, it does not offer IDEs or extensive toolkits.
     A lot of functionality in TR7 is included conditionally, to allow
     developers freedom in balancing features and footprint.

     As an embedded interpreter, it allows multiple interpreter states to
     coexist in the same program, without any interference between them.
     Programmatically, foreign functions in C can be added and values
     can be defined in the Scheme environment. Being a quite small program,
     it is easy to comprehend, get to grips with, and use.

     Download TR7 at https://gitlab.com/jobol/tr7


     Scheme Reference
     ----------------

     TR7 fully implements R7RS small (see r7rs.pdf file or
     https://small.r7rs.org/)

     Well I'm lying, very few things are still missing (see TODO), but
     will be fixed quickly:
       - null-environment
       - scheme-report-environment
       - include-library-declarations
       - string intraline spaces escape
       - immutable pairs

     If something seems to be missing or buggy, please use gitlab's tracker
     https://gitlab.com/jobol/tr7/-/issues.

     TR7 has a built-in implementation of SRFI-136 (see
     https://srfi.schemers.org/srfi-136/srfi-136.html).

     Some available extra features are listed below, note that because of the
     youth of TR7, these extra features may change in later versions.

     (environment? ANY)

        is ANY an environment?

     (defined? SYMBOL [ENV])

        is SYMBOL attached to a value in ENV (current environment by default)

     (current-environment)

        the current environment

     (load-extension STRING)

        loads the shared library of path STRING

     (oblist)

        list of known symbols

     (new-segment [NUM])

        add NUM (default is 1) new segment(s) in the memory pool

     (tr7-tracing NUM)

        activates tracing if num is not 0

     (tr7-gc [BOOL])

        perform a garbage collection if BOOL is true, emits report

     (tr7-gc-verbose [BOOL])

        without argument, return the current verbosity state of the GC
        with an argument, set the verbosity of GC and return the previous one

     (tr7-show-prompt [BOOL])

        without argument, return the current show-prompt status
        with an argument, set the show-prompt status and return the previous one

     (tr7-show-eval [BOOL])

        without argument, return the current show-eval status
        with an argument, set the show-eval status and return the previous one

     (tr7-show-result [BOOL])

        without argument, return the current show-result status
        with an argument, set the show-result status and return the previous one



     Content and building
     --------------------

     The main directory contains:
       - COPYING        BSD-0 license
       - examples/      some examples
       - Makefile       the make file
       - r7rs.pdf       R7RS small
       - README.txt     this text
       - srfi/          some SRFI available
       - tests/         some tests
       - TODO           note of what is to be done
       - tr7.c          the scheme engine
       - tr7.h          header for developpers using the scheme engine
       - tr7i.c         a standa alone scheme REPL using the engine
       - tr7i.scm       the REPL in scheme with colors

     Compilation is done using make. The targets are:
       - libtr7.a       static library containing the engine
       - libtr7.so.0    dynamic library containing the engine
       - tr7i           a tiny scheme interpreter using the engine



     Programmer's Reference
     ----------------------

     The program tr7i.c can be used as a demo of integration.

     The interpreter state is initialized with 'tr7_engine_create'.
     Custom memory allocation routines can be installed by filling
     the data of a 'tr7_config_t' structure passed to 'tr7_engine_create'.

     Files can be loaded with 'tr7_load_file'. Strings containing Scheme
     code can be loaded with 'tr7_load_string'.

     The interpreter state should be deinitialized with 'tr7_engine_destroy'

     DLLs containing foreign functions should define a function named
     'init_tr7'.  This function is called with one parameter a 'tr7_engine_t'.



     Foreign Functions
     -----------------

     The user can add foreign functions in C. For example, a function
     that squares its argument:

        tr7_t square(tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
        {
           double x = tr7_to_double(args[0]);
           return tr7_from_double(tsc, x*x);
        }

     Foreign functions can be defined as below:

        tr7_register_C_func(tsc, "square", 1, 1, TR7ARG_NUMBER, square, 0);
                                    ^      ^  ^        ^           ^    ^
         public name ---------------+      |  |        |           |    |
         minimal count of args ------------+  |        |           |    |
         maximal count of args ---------------+        |           |    |
         expected type of args ------------------------+           |    |
         address of the C function --------------------------------+    |
         closure -------------------------------------------------------+



     tr7i
     ----

     Usage: tr7i -?
     or:    tr7i [<file1> <file2> ...]
     followed by
	       -1 <file> [<arg1> <arg2> ...]
	       -c <Scheme commands> [<arg1> <arg2> ...]
     assuming that the executable is named tr7i.

     Use - in the place of a filename to denote stdin.
     The -1 flag is meant for #! usage in shell scripts. If you specify
          #! /somewhere/tr7i -1
     then tr7i will be called to process the file. For example, the
     following script echoes the Scheme list of its arguments.

	       #! /somewhere/tr7i -1
	       (display (command-line))

     The -c flag permits execution of arbitrary Scheme code.

     The following environment variables are understood by tr7i:

       TR7_PATH           path for 'load' and fallback path
       TR7_LIB_PATH       path for importing libraries
       TR7_INC_PATH       path for including files
       TR7_EXT_PATH       path for 'load-extension'
       TR7_PROMPT         string of the prompt (default is "tr7> ")



     Reader extensions
     -----------------

     When encountering an unknown character after '#', the user-specified
     procedure *sharp-hook* (if any), is called to read the expression.
     This can be used to extend the reader to handle user-defined constants
     or whatever. Currently, it should be a procedure receiving a string made
     of all the characters from the sharp (included) to the end of the line
     (excluded).

