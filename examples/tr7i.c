#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <ctype.h>
#include <time.h>
#include <wchar.h>
#include <locale.h>
#include <signal.h>
#ifdef WIN32
#  define snprintf _snprintf
#else
#  include <unistd.h>
#  include <termios.h>
#  include <sys/ioctl.h>
#endif

#include "tr7.h"

#define INITVAR        "TR7INIT"
#ifndef INITFILE
#define INITFILE       "tr7-init.scm"
#endif
#define BANNER         "Tiny R7RS interpreter TR7 0.01\n"
#define PROMPT         "tr7> "

#ifndef PATHVAR
#define PATHVAR        "TR7_PATH"        /* for load (and fallback) */
#endif
#ifndef LIBPATHVAR
#define LIBPATHVAR     "TR7_LIB_PATH"    /* for import */
#endif
#ifndef INCPATHVAR
#define INCPATHVAR     "TR7_INC_PATH"    /* for include */
#endif
#ifndef EXTPATHVAR
#define EXTPATHVAR     "TR7_EXT_PATH"    /* for load extension */
#endif
#ifndef PROMPTVAR
#define PROMPTVAR      "TR7_PROMPT"      /* for the prompt string */
#endif

static int memo_is_tty = 0;
static int memo_nrow = 0;
static int memo_ncol = 0;


static tr7_t is_tty(tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
   return memo_is_tty ? TR7_TRUE : TR7_FALSE;
}

static tr7_t tty_size(tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
   return tr7_cons(tsc, tr7_from_int(tsc, memo_nrow), tr7_from_int(tsc, memo_ncol));
}

static tr7_t shell(tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
   if (!TR7_IS_STRING(args[0]))
      return TR7_FALSE;
   if (system(tr7_string_buffer(args[0])) == 0)
      return TR7_TRUE;
   return TR7_FALSE;
}

#if defined(_WIN32)

static void get_tty_size()
{
   HANDLE handle;
   CONSOLE_SCREEN_BUFFER_INFO info;

   handle = (HANDLE)_get_osfhandle(0);
   if (GetConsoleScreenBufferInfo(handle, &info)) {
      memo_nrow = ws.ws_row;
      memo_ncol = ws.ws_col;
   }
}

/* Windows 10 built-in VT100 emulation */
#define __ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#define __ENABLE_VIRTUAL_TERMINAL_INPUT 0x0200

static void set_tty_raw()
{
   HANDLE handle;

   handle = (HANDLE)_get_osfhandle(0);
   SetConsoleMode(handle, ENABLE_WINDOW_INPUT | __ENABLE_VIRTUAL_TERMINAL_INPUT);
   handle = (HANDLE)_get_osfhandle(1); /* corresponding output */
   SetConsoleMode(handle, ENABLE_PROCESSED_OUTPUT | ENABLE_WRAP_AT_EOL_OUTPUT | __ENABLE_VIRTUAL_TERMINAL_PROCESSING);
}

#else

static void get_tty_size()
{
   struct winsize ws;
   if (ioctl(0, TIOCGWINSZ, &ws) == 0 && ws.ws_col >= 4 && ws.ws_row >= 4) {
      memo_nrow = ws.ws_row;
      memo_ncol = ws.ws_col;
   }
}

static struct termios oldtty;

static void term_exit(void)
{
    tcsetattr(0, TCSANOW, &oldtty);
}

/* XXX: should add a way to go back to normal mode */
static void set_tty_raw()
{
    struct termios tty;
    
    memset(&tty, 0, sizeof(tty));
    tcgetattr(0, &tty);
    oldtty = tty;

    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
                          |INLCR|IGNCR|ICRNL|IXON);
    tty.c_oflag |= OPOST;
    tty.c_lflag &= ~(ICANON|IEXTEN);
    tty.c_lflag &= ~(ECHO|ECHONL|ICANON|IEXTEN);
    tty.c_cflag &= ~(CSIZE|PARENB);
    tty.c_cflag |= CS8;
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 0;

    tcsetattr(0, TCSANOW, &tty);

    atexit(term_exit);
}

#endif /* !_WIN32 */

static void on_sigint(int sig)
{
   exit(0); /* TODO: send a ^C */
}

static void on_sigwinch(int sig)
{
   get_tty_size();
}

static void init(tr7_engine_t tsc)
{
   struct sigaction sa;
   memo_is_tty = isatty(0) > 0 && isatty(1) > 0;
   tr7_register_C_func(tsc, "tty?", 0, 0, 0, is_tty, 0);
   tr7_register_C_func(tsc, "tty-size", 0, 0, 0, tty_size, 0);
   tr7_register_C_func(tsc, "shell", 1, 1, TR7ARG_STRING, shell, 0);
   if (memo_is_tty) {
      setvbuf(stdin, NULL, _IONBF, 0);
//      set_tty_raw();
      get_tty_size();
      memset(&sa, 0, sizeof sa);
//      sa.sa_flags = SA_RESTART;
      sa.sa_handler = on_sigint;
      sigaction(SIGINT, &sa, 0);
      sa.sa_handler = on_sigwinch;
//      sigaction(SIGWINCH, &sa, 0);
   }
}

static void addenvstr(tr7_engine_t tsc,
                      tr7_strid_t strid,
                      const char *varname,
                      const char *defval)
{
   const char *val = getenv(varname);
   tr7_set_string(tsc, strid, val ? val : defval);
}

#if defined(__APPLE__) && !defined (OSX)
static int MacTS_main(int argc, char **argv);
int main()
{
   char **argv;
   int argc = ccommand(&argv);
   MacTS_main(argc, argv);
   return 0;
}
#undef main
#define main MacTS_main
#endif

int main(int argc, char **argv)
{
   tr7_engine_t tsc;
   int retcode;
   int isfile = 1;

   setlocale(LC_ALL, "");
   setlocale(LC_NUMERIC, "C");
   if (argc == 2 && strcmp(argv[1], "-?") == 0) {
      printf("Usage: %s -?\n", argv[0]);
      printf("or:    %s [<file1> <file2> ...]\n", argv[0]);
      printf("followed by\n");
      printf("          -1 <file> [<arg1> <arg2> ...]\n");
      printf("          -c <Scheme commands> [<arg1> <arg2> ...]\n");
      printf("Use - as filename for stdin.\n");
      return 0;
   }
   setvbuf(stdin, NULL, _IOLBF, 0);
   setvbuf(stdout, NULL, _IOLBF, 0);
   setvbuf(stderr, NULL, _IOLBF, 0);
   tsc = tr7_engine_create(NULL);
   if (!tsc) {
      fprintf(stderr, "Could not initialize!\n");
      return 2;
   }
   tr7_set_standard_ports(tsc);
   tr7_set_argv(argv);
   addenvstr(tsc, Tr7_StrID_Path, PATHVAR, NULL);
   addenvstr(tsc, Tr7_StrID_Library_Path, LIBPATHVAR, NULL);
   addenvstr(tsc, Tr7_StrID_Include_Path, INCPATHVAR, NULL);
   addenvstr(tsc, Tr7_StrID_Extension_Path, EXTPATHVAR, NULL);
   addenvstr(tsc, Tr7_StrID_Prompt, PROMPTVAR, PROMPT);
   init(tsc);
   if (argc == 1) tr7_display_string(tsc, BANNER);
   tr7_load_file(tsc, NULL, getenv(INITVAR) != NULL ? getenv(INITVAR) : INITFILE);
   if (argc == 1)
      tr7_load_file(tsc, stdin, "<stdin>");
   for (argv++ ; *argv != NULL ; argv++) {
      if (strcmp(*argv, "-") == 0)
         tr7_load_file(tsc, stdin, "<stdin>");
      else if (strcmp(*argv, "-1") != 0 && strcmp(*argv, "-c") != 0)
         tr7_load_file(tsc, NULL, *argv);
      else {
         isfile = (*argv++)[1] == '1';
         if (*argv == NULL) {
            fprintf(stderr, "no argument for %s!\n", *--argv);
            return 3;
         }
         tr7_set_argv(argv);
         if (isfile)
            tr7_load_file(tsc, NULL, *argv);
         else
            tr7_load_string(tsc, *argv);
         while (argv[1])
            argv++;
      }
   }
   retcode = 0;// TODO tsc.retcode;
   tr7_engine_destroy(tsc);

   return retcode;
}

/*
Local variables:
c-file-style: "k&r"
End:
vim: noai ts=3 sw=3 expandtab
*/
