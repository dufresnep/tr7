;; jeu de toc

;(define couleurs #(♠ ♥ ♦ ♣))
;(define cartes #(1 2 3 4 5 6 7 8 9 10 V D R))
;(define jeu (apply append (vector-map (lambda (c) (vector-map (lambda (i) (cons i c)) cartes)) couleurs)))

(define couleurs '(♠ ♥ ♦ ♣))
(define cartes '(1 2 3 4 5 6 7 8 9 10 V D R))
(define jeu (apply append (map (lambda (c) (map (lambda (i) (cons i c)) cartes)) couleurs)))

(define joueurs '(rouge bleu jaune blanc))

(define piste (make-vector (* 4 18) #f))

(define (p . x) (display x) (newline))
(define (action-std count situation)
   (p `(action-std ,count)))
(define (action-4 situation)
   (p `(action-4)))
(define (action-7 situation)
   (p `(action-7)))
(define (action-valet situation)
   (p `(action-valet)))
(define (action-as-roi count situation)
   (p `(action-as-roi ,count)))

(define carte->action
   (let ((f (lambda (fun arg1) (lambda x (apply fun arg1 x)))))
      `(
         (1 ,(f action-as-roi 1))
         (2 ,(f action-std 2))
         (3 ,(f action-std 3))
         (4 ,action-4)
         (5 ,(f action-std 5))
         (6 ,(f action-std 6))
         (7 ,action-7)
         (8 ,(f action-std 8))
         (9 ,(f action-std 9))
         (10 ,(f action-std 10))
         (V ,action-valet)
         (D ,(f action-std 12))
         (R ,(f action-as-roi 13))
       )))
jeu


