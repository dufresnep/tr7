/*
*****************************************************************************
* T R 7    0 . 0 1
*
* tiny R7RS-small scheme interpreter
* This header comes with tr7, a tiny scheme interpreter.
*
* SPDX-License-Identifier: 0BSD
* https://gitlab.com/jobol/tr7
*
* C++ programmer MUST include that file in an export "C" { ... } section!
*/
#ifndef _TR7_H
#define _TR7_H
#include <stdint.h>
#include <stddef.h>
/*
*****************************************************************************
* Defining tr7_t types
* --------------------
*
* The basic public type exposed by TR7 is tr7_t.
*
* TR7 manages many different kind of values: characters, atoms, strings,
* numbers, pairs, ... To achieve it, the most important types
* for TR7 are: tr7_t, tr7_pair_t, tr7_cell_t, tr7_double_t
*
* Within TR7, tr7_pair_t is a pointer to a pair (a pair in the common scheme
* meaning, i.e. a pair of values). A tr7_cell_t is a pointer to one of the
* internal data used by tr7. A tr7_double_t is a pointer to a double.
*/
typedef struct tr7_pair *tr7_pair_t;
typedef union  tr7_cell *tr7_cell_t;
typedef double          *tr7_double_t;
typedef intptr_t         tr7_int_t;
typedef uintptr_t        tr7_uint_t;
#ifdef _WINT_T
typedef wint_t           tr7_char_t;
#else
typedef int32_t          tr7_char_t;
#endif
/*
*****************************************************************************
* Defining tr7_t
* --------------
*
* A tr7_t value is a pointer with encoded low bits, a common technic
* to save memory and bits. And saving memory and bits a good thing no?
*
* The bad thing is that in most cases accessing the memory referenced
* directly using the pointer is just not possible!
*
* For that reason, defining tr7_t as a pointer is a bad idea. So
* tr7_t is defined as a signed integer of the size of a pointer.
*
* But using integer instead of pointers can be an issue when debugging
* internals of TR7. A tiny help in some case is to in fact declare that
* tr7_t is a pointer to a pair. Pair are of common use and, consequently,
* are the tartget of plain pointers.
* In that case, you can define TR7_WANT_DEBUG_POINTER like below:
*
#define TR7_WANT_DEBUG_POINTER 1
*
* But be aware it will makes many pointers irrevelant and unaligned.
* It is not so good in fact...
*/
#if !TR7_WANT_DEBUG_POINTER
typedef intptr_t tr7_t;     /* normal case */
#else
typedef tr7_pair_t tr7_t;
#endif
/*
* Managing to cast pointer and integer is obvious in assembly language
* and easy in C. But to enlight the code, improve its resilience and
* allow to have pointers to cells instead of integers, the below
* macros are used to cast:
*
* TR72I(t): casts a tr7_t value 't' to a intptr_t value
* I2TR7(i): casts a intptr_t value 'i' to a tr7_t value
* TR72U(t): casts a tr7_t value 't' to a uintptr_t value
* U2TR7(u): casts a uintptr_t value 'u' to a tr7_t value
* TR7EQ(a,b): returns a boolean value true if a==b
*/
#define TR72I(t)          ((intptr_t)(t))
#define I2TR7(i)          ((tr7_t)(intptr_t)(i))
#define TR72U(t)          ((uintptr_t)(t))
#define U2TR7(u)          ((tr7_t)(uintptr_t)(u))
#define TR7EQ(a,b)        ((a) == (b))
/*
* The special value TR7_VOID is intended to represent
* an invalid value of TR7. On purpose, its value is equal or
* equivalent to the C value NULL.
*
* The value TR7_VOID is used internally for indicating
* a non-value value (or an undefined value).
*/
#define TR7_VOID          I2TR7(0)
/*
* This macro test equality to TR7_VOID:
*
* TR7_IS_VOID(t): returns true if t is the predefined TR7_VOID
*/
#define TR7_IS_VOID(t)    TR7EQ(t, TR7_VOID)
/*
* As written above, tr7_t use the lower bits of a pointer compatible integer
* to encode the kind of data it represent.
*
* The count of bits used is fixed to 3.
*
* Using 3 bits is not worrying on 64 bits processors because the natural
* alignemnt of pointers is 8 bytes, implying that the 3 least significants
* bits are 0.
*
* However, on 32 bits processors, the natural alignment of 4 bytes let
* only 2 least significant bits. But tr7 takes care of that issue and
* ensures that underlying pointers are aligned on 8 bytes boundaries.
* Note that the alignement on 8 byte is natural for doubles but not guaranteed.
* But take care of it if you intend to forge your own tr7_t values.
*/
#define TR7_WIDTH_KIND     3   /* bit count for mask of kinds */
#define TR7_MASK_KIND      007 /* mask for the kinds */
/*
* (*MINOR NOTE* The above definitions are respecting a rule in naming
* for TR7: the width or the mask prefixes the name attributed. Then
* the name can be used as prefix for its values, as below)
*
* For TR7_WIDTH_KIND = 3, a tr7 value is made of:
*   - a kind on its 3 lower bits
*   - a value on its 29 or 61 upper bits
*
* The 3 least bits are representing 8 values. This values are
* attributed as below:
*/
#define TR7_KIND_PAIR      000 /* pointer to a pair or NIL */
#define TR7_KIND_EINT      001 /* even integers */
#define TR7_KIND_CELL      002 /* pointer to a cell */
#define TR7_KIND_SPECIAL   003 /* special cases */
#define TR7_KIND_DOUBLE    004 /* pointer to a double */
#define TR7_KIND_OINT      005 /* odd integers (must be 4 + TR7_KIND_EINT) */
#define TR7_KIND_SPARE1    006 /* spare pointer */
#define TR7_KIND_SPARE2    007 /* spare value */
/*
* The idea is to implement the following schema:
*
* Basic dichotomy:
*
*                   <--- 31 or 63 upper bits ---->   0
*                  +---+---+ - - - - - +---+---+---+---+
*  pointer         |   |   |           |   |   |   | 0 |
*                  +---+---+ - - - - - +---+---+---+---+
*  not pointer     |   |   |           |   |   |   | 1 |
*                  +---+---+ - - - - - +---+---+---+---+
*
* The basic dichotomy is then refined as below:
*
* Wrapped pointers:
*
*                   <--- 29 or 61 upper bits ---->   2   1   0
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_pair_t      |   |   |           |   |   |   | 0 | 0 | 0 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_cell_t      |   |   |           |   |   |   | 0 | 1 | 0 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_double_t    |   |   |           |   |   |   | 1 | 0 | 0 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*
* Wrapped integrals:
*
*                   <--- 29 or 61 upper  bits ---->  2   1   0
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_int_t even  |   |   |           |   |   |   | 0 | 0 | 1 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_int_t odd   |   |   |           |   |   |   | 1 | 0 | 1 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  specials        |   |   |           |   |   |   | 0 | 1 | 1 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*
* When interpreting the meaning of a value, it is important to distinguish
* between values that must be shifted and values that dont must not
* be shifted. Pointers must not be shifted, while integrals should
* generaly be shifted.
*
* To help manipulating TR7 values the following macro are defined.
*
* TR7_KIND(t): returns a intptr_t equals to the kind of tr7 value t
* TR7_IS_KIND(t,k): return true if kind of tr7 value t is k
* TR7_MAKE_EQ(v,k): make a tr7 of kind k and value v
*                   !WARNING! for TR7_MAKE_EQ the 3 lower bits of v
*                   MUST be zero
*/
#define TR7_KIND(t)       (TR72I(t) & TR7_MASK_KIND)
#define TR7_IS_KIND(t,k)  (TR7_KIND(t) == (k))
#define TR7_MAKE_EQ(v,k)  I2TR7((intptr_t)(v) | (intptr_t)(k))
/*
*****************************************************************************
* Partition of pointers
* ---------------------
*
* The lower bit tells if the value is a pointer or not.
*
* If lower bit is 0 this is a pointer
* If lower bit is 1 the value is not a pointer
*/
#define TR7_WIDTH_PTR        1   /* count of bit used to identify pointers */
#define TR7_MASK_PTR         001 /* mask for identifying pointers */
#define TR7_TAG_PTR          000 /* value identifying pointers */
/*
* Macros for manipulating pointers are:
*
* TR7_IS_PTR(t): returns a boolean true if the tr7 value t is a pointer
* TR7_IS_PTR_KIND(t,k): returns true if the tr7 value t is of kind k and not
*                       NULL
* TR7_TO_PTR(t,k): returns the void* given by the tr7 value t of kind k
* TR7_AS_PTR(t,k): returns the void* given by the tr7 value t if of the given
*                  kind k, or otherwise returns NULL
* TR7_FROM_PTR(p,k): returns the tr7_t value for the pointer p of kind k
*                   !WARNING! for TR7_FROM_PTR the 3 lower bits of p
*                   MUST be zero
*/
#define TR7_IS_PTR(t)        ((TR72I(t) & TR7_MASK_PTR) == TR7_TAG_PTR)
#define TR7_IS_PTR_KIND(t,k) (TR7_IS_KIND(t,k) && ((k) || (t)))
#define TR7_TO_PTR(t,k)      ((void*)(TR72I(t)-(k)))
#define TR7_AS_PTR(t,k)      (TR7_KIND(t) == (k) ? TR7_TO_PTR(t,k) : NULL)
#define TR7_FROM_PTR(p,k)    TR7_MAKE_EQ(p,k)
/*
*****************************************************************************
* Defining basic integers
* -----------------------
*
* Integers use 2 kinds, this let one more bit for encoding integer values,
* leading integer values of 30 or 62 bits depending on the target.
* For this reason, the equality 4 == TR7_KIND_OINT - TR7_KIND_EINT
* must be kept.
*
* For performance, it is needed to define a sub class of tr7_t that merge
* the two integer kinds.
*
* The idea is to implement the following schema:
*
*                   <----- 30 or 62 upper  bits ------>  1   0
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  tr7_int_t even  |   |   |           |   |   |   |   | 0 | 1 |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*/
#define TR7_WIDTH_INT      2   /* count of bit used to identify integers */
#define TR7_MASK_INT       003 /* mask for identifying integers */
#define TR7_TAG_INT        (TR7_KIND_EINT & TR7_MASK_INT)/* value identifying integers */
/*
* Macros for manipulating integers are:
*
* TR7_IS_INT(t): returns a boolean true if the tr7 value t is tr7_(u)int_t
* TR7_TO_INT(t): returns the tr7_int_t value of the tr7 value t
* TR7_FROM_INT(i): returns the tr7_t value for the tr7_int_t i
* TR7_TO_UINT(t): returns the tr7_uint_t value of the tr7 value t
* TR7_FROM_UINT(u): returns the tr7_t value for the tr7_uint_t u
*
* Note that the system doesn't provide a way to distinguish between
* signed and unsigned version of the wrapped integer. So it should
* be given by the context. Without context, signed integer is the
* default.
*/
#define TR7_IS_INT(t)     ((TR72I(t) & TR7_MASK_INT) == TR7_TAG_INT)
#define TR7_TO_INT(t)     (((tr7_int_t)TR72I(t)) >> TR7_WIDTH_INT)
#define TR7_FROM_INT(i)   TR7_MAKE_EQ((i) << TR7_WIDTH_INT, TR7_TAG_INT)
#define TR7_TO_UINT(t)    (((tr7_uint_t)TR72I(t)) >> TR7_WIDTH_INT)
#define TR7_FROM_UINT(u)  TR7_MAKE_EQ((u) << TR7_WIDTH_INT, TR7_TAG_INT)
/*
* The limits of the integer in that system are:
*/
#define TR7_INT_MAX        (INTPTR_MAX >> TR7_WIDTH_INT)
#define TR7_INT_MIN        (INTPTR_MIN >> TR7_WIDTH_INT)
#define TR7_UINT_MAX       (UINTPTR_MAX >> TR7_WIDTH_INT)
/*
*****************************************************************************
* Defining doubles
* ----------------
*
* Doubles values are presented by pointers to a double. Such pointer
* are normally aligned on 8 bytes boundaries, leading to pointer
* having their 3 lower bits nul. But take care the pointer MUST have
* its 3 lower bits nul!
*
* Macros for manipulating reals, tr7_t value wraps a pointer to a double
*
* TR7_IS_DOUBLE(t): returns a boolean true if the tr7 value t wraps a tr7_double_t
* TR7_TO_DOUBLE(t): returns the tr7_double_t of the tr7 value t
* TR7_AS_DOUBLE(t): returns the tr7_double_t of the tr7 value t if of the kind
*                 real, or otherwise, if t isn't a real, returns NULL
* TR7_FROM_DOUBLE(p): returns the tr7_t value for the tr7_double_t p
*/
#define TR7_IS_DOUBLE(t)    TR7_IS_PTR_KIND(t,TR7_KIND_DOUBLE)
#define TR7_TO_DOUBLE(t)    ((tr7_double_t)TR7_TO_PTR(t,TR7_KIND_DOUBLE))
#define TR7_AS_DOUBLE(t)    ((tr7_double_t)TR7_AS_PTR(t,TR7_KIND_DOUBLE))
#define TR7_FROM_DOUBLE(p)  TR7_FROM_PTR(p,TR7_KIND_DOUBLE)
/*
*****************************************************************************
* Defining specials
* -----------------
*
* The kind "special" (TR7_KIND_SPECIAL) is subdivided in 4 sub kinds
* called "very special cases" and noted kind of VSP.
*
* VSP are used to encode booleans, eof, oper and chars
*
* The 4 VSP are defined by taking the 2 least significant bits of the
* value of specials.
*
* The idea is to implement the following schema:
*
*                   <- 27 or 59 bits ->  4   3   2   1   0
*                  +---+---+ - - - - - +---+---+---+---+---+
*  constants       |   |   |           | 0 | 0 |  SPECIAL  |
*                  +---+---+ - - - - - +---+---+---+---+---+
*  characters      |   |   |           | 0 | 1 |  SPECIAL  |
*                  +---+---+ - - - - - +---+---+---+---+---+
*  opcodes         |   |   |           | 1 | 0 |  SPECIAL  |
*                  +---+---+ - - - - - +---+---+---+---+---+
*
* To improve computing, the 5 lower bits of tr7_t items are treated
* altogether.
*/
#define TR7_WIDTH_VSP     (2 + TR7_WIDTH_KIND)
#define TR7_MASK_VSP      (030 | TR7_MASK_KIND)
/*
* Macros for manipulating very specials are:
*
* TR7_TAG_VSP(k): returns the VSP tag of the for the VSP kind k
* TR7_VSP_TAG(t): returns the VSP tag of a tr7_t t
* TR7_IS_VSP(k,t): returns true if the tr7_t t is a VSP of kind k
* TR7_VSP_VALUE(t): returns the signed value of a special tr7_t t
* TR7_VSP_UVALUE(t): returns the unsigned value of a special tr7_t t
* TR7_MAKE_VSP(k,v): returns a special tr7_t of ind k and value v
*/
#define TR7_TAG_VSP(k)    (((k) << TR7_WIDTH_KIND) | TR7_KIND_SPECIAL)
#define TR7_VSP_TAG(t)    (TR72I(t) & TR7_MASK_VSP)
#define TR7_IS_VSP(k,t)   (TR7_VSP_TAG(t) == TR7_TAG_VSP(k))
#define TR7_VSP_VALUE(t)  (TR72I(t) >> TR7_WIDTH_VSP)
#define TR7_VSP_UVALUE(t) (TR72U(t) >> TR7_WIDTH_VSP)
#define TR7_MAKE_VSP(k,v) I2TR7(((intptr_t)(v) << TR7_WIDTH_VSP) | (intptr_t)TR7_TAG_VSP(k))
/*
* Attribution of very special numbers
*/
#define TR7_VSP_PREDEF       0    /* predefined constants */
#define TR7_VSP_CHARACTER    1    /* character */
#define TR7_VSP_OPCODE       2    /* predefined opcodes */
#define TR7_VSP_SPARE        3    /* spare */
/*
* The VSP kind VSP_PREDEF
* -----------------------
*
* It is used to encode some well known constants:
*
*  - TR7_NIL             the "NIL" value
*  - TR7_FALSE TR7_TRUE  the booleans values for False and True
*  - TR7_EOF             the EOF (end-of-file) mark
*/
#define TR7_NIL           TR7_MAKE_VSP(TR7_VSP_PREDEF,0)
#define TR7_FALSE         TR7_MAKE_VSP(TR7_VSP_PREDEF,1)
#define TR7_TRUE          TR7_MAKE_VSP(TR7_VSP_PREDEF,2)
#define TR7_EOF           TR7_MAKE_VSP(TR7_VSP_PREDEF,3)
/*
* Macros for manipulating VSP kind VSP_PREDEF are:
*
* TR7_IS_NIL(t): returns true if t is the predefined TR7_NIL
* TR7_IS_FALSE(t): returns true if t is the predefined TR7_FALSE
* TR7_IS_TRUE(t): returns true if t is the predefined TR7_TRUE
* TR7_IS_BOOLEAN(t): returns true if t is TR7_TRUE or TR7_FALSE
* TR7_IS_EOF(t): returns true if t is the predefined TR7_EOF
*/
#define TR7_IS_NIL(t)     TR7EQ(t,TR7_NIL)
#define TR7_IS_FALSE(t)   TR7EQ(t, TR7_FALSE)
#define TR7_IS_TRUE(t)    TR7EQ(t, TR7_TRUE)
#define TR7_IS_BOOLEAN(t) (TR7_IS_FALSE(t) || TR7_IS_TRUE(t))
#define TR7_IS_EOF(t)     TR7EQ(t, TR7_EOF)
/*
* The VSP kind VSP_CHARACTER
* --------------------------
*
* It is used to encode characters. The wrapped value is unsigned.
*
* Macros for manipulating VSP of this kind are:
*
* TR7_IS_CHAR(t): returns true is tr7_t t wraps a characters
* TR7_TO_CHAR(t): returns the tr7_char_t character wrapped by tr7_t t
* TR7_FROM_CHAR(c): returns a tr7_t wrapping the tr7_char_t c
*/
#define TR7_IS_CHAR(t)    TR7_IS_VSP(TR7_VSP_CHARACTER,t)
#define TR7_TO_CHAR(t)    (tr7_char_t)TR7_VSP_UVALUE(t)
#define TR7_FROM_CHAR(c)  TR7_MAKE_VSP(TR7_VSP_CHARACTER,c)
/*
* The VSP kind VSP_OPCODE
* -----------------------
*
* It is used to encode internal operations and syntaxes.
*
* It uses the least significant bit of the remaining
* value to dstinguish between operator and syntax.
*
* The idea is to implement the following schema:
*
*                   <- 26 or 58 bits ->  5   4   3   2   1   0
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  operators       |   |   |           | 0 | OPCOD |  SPECIAL  |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*  syntaxes        |   |   |           | 1 | OPCOD |  SPECIAL  |
*                  +---+---+ - - - - - +---+---+---+---+---+---+
*
* The wrapped value is unsigned.
*
* To improve computing, the 6 lower bits of tr7_t items are treated
* altogether.
*/
#define TR7_WIDTH_OPCODE  (1 + TR7_WIDTH_VSP)
#define TR7_MASK_OPCODE   (040 | TR7_MASK_VSP)
/*
* Macros for manipulating very special opcodes are:
*
* TR7_TAG_OPCODE(k): returns the VSP tag of the for the VSP kind k
* TR7_OPCODE(t): returns the VSP tag of a tr7_t t
* TR7_IS_OPCODE(k,t): returns true if the tr7_t t is an opcode of kind k
* TR7_OPCODE(t): returns the signed value of a special opcode tr7_t t
* TR7_OPCODE(t): returns the unsigned value of a special opcode tr7_t t
* TR7_MAKE_OPCODE(k,v): returns a special tr7_t of kind k and value v
*/
#define TR7_TAG_OPCODE(k)    (((k) << TR7_WIDTH_VSP) | TR7_TAG_VSP(TR7_VSP_OPCODE))
#define TR7_OPCODE_TAG(t)    (TR72I(t) & TR7_MASK_OPCODE)
#define TR7_IS_OPCODE(k,t)   (TR7_OPCODE_TAG(t) == TR7_TAG_OPCODE(k))
#define TR7_OPCODE_VALUE(t)  (TR72I(t) >> TR7_WIDTH_OPCODE)
#define TR7_OPCODE_UVALUE(t) (TR72U(t) >> TR7_WIDTH_OPCODE)
#define TR7_MAKE_OPCODE(k,v) I2TR7(((intptr_t)(v) << TR7_WIDTH_OPCODE) | (intptr_t)TR7_TAG_OPCODE(k))
/*
* Subtypes of opcodes:
*/
#define TR7_OPCODE_OPERATOR  0   /* for operators */
#define TR7_OPCODE_SYNTAX    1   /* for syntaxes */
/*
* The sub-OPCODE, VSP_OPERATOR
* ----------------------------
*
* It is used to encode operators. The wrapped value is unsigned.
*
* Macros for manipulating VSP of this kind are:
*
* TR7_IS_OPER(t): returns true is tr7_t t wraps an operator
* TR7_TO_OPER(t): returns the unsigned operator wrapped by tr7_t t
* TR7_FROM_OPER(o): returns a tr7_t wrapping the operator o
*/
#define TR7_IS_OPER(t)    TR7_IS_OPCODE(TR7_OPCODE_OPERATOR,t)
#define TR7_TO_OPER(t)    ((unsigned)TR7_OPCODE_UVALUE(t))
#define TR7_FROM_OPER(o)  TR7_MAKE_OPCODE(TR7_OPCODE_OPERATOR,o)
/*
* The sub-OPCODE, VSP_SYNTAX
* --------------------------
*
* It is used to encode syntaxes. The wrapped value is unsigned.
*
* Macros for manipulating VSP of this kind are:
*
* TR7_IS_SYNTAX(t): returns true is tr7_t t wraps a syntax
* TR7_TO_SYNTAX(t): returns the unsigned syntax wrapped by tr7_t t
* TR7_FROM_SYNTAX(s): returns a tr7_t wrapping the syntax s
*/
#define TR7_IS_SYNTAX(t)    TR7_IS_OPCODE(TR7_OPCODE_SYNTAX,t)
#define TR7_TO_SYNTAX(t)    ((unsigned)TR7_OPCODE_UVALUE(t))
#define TR7_FROM_SYNTAX(s)  TR7_MAKE_OPCODE(TR7_OPCODE_SYNTAX,s)
/*
*****************************************************************************
* Defining tr7_pair_t
* -------------------
*
* A pair is a simple structure having 2 fields: car and cdr.
*/
struct tr7_pair {
   tr7_t cdr;  /* the CDR, in first for speeding list-length */
   tr7_t car;  /* the CAR, in second */
};
/*
* Macros for manipulating pairs, tr7_t value wraps a pointer to a pair.
*
* TR7_IS_PAIR(t): returns a boolean true if the tr7 value t points a pair
* TR7_TO_PAIR(t): returns the tr_pair_t (pointer to pair) of the tr7 value t
* TR7_AS_PAIR(t): returns the tr7_pair_t of the tr7 value t if of the kind
*                 pair, or otherwise, if t isn't a pair, returns NULL
* TR7_FROM_PAIR(p): returns the tr7_t value for the pointer p
*/
#define TR7_IS_PAIR(t)    TR7_IS_PTR_KIND(t,TR7_KIND_PAIR)
#define TR7_TO_PAIR(t)    ((tr7_pair_t)TR7_TO_PTR(t,TR7_KIND_PAIR))
#define TR7_AS_PAIR(t)    ((tr7_pair_t)TR7_AS_PTR(t,TR7_KIND_PAIR))
#define TR7_FROM_PAIR(p)  TR7_FROM_PTR(p,TR7_KIND_PAIR)
/*
* Level 1 helper macros for accessing pairs.
*
* !WARNING! This macros are not checking type of the tr7_t value.
*
* TR7_PAIR_CAR(p): returns the tr7_t car of the tr7_pair_t p
* TR7_PAIR_CDR(p): returns the tr7_t cdr of the tr7_pair_t p
* TR7_CAR(t): returns the tr7_t car of the tr7_t t
* TR7_CDR(t): returns the tr7_t cdr of the tr7_t t
*/
#define TR7_PAIR_CAR(p)  (p)->car
#define TR7_PAIR_CDR(p)  (p)->cdr
#define TR7_CAR(t)       TR7_PAIR_CAR(TR7_TO_PAIR(t))
#define TR7_CDR(t)       TR7_PAIR_CDR(TR7_TO_PAIR(t))
/*
* Level 2 helpers
*
* !WARNING! This macros are not checking type of the tr7_t value.
*/
#define TR7_CAAR(t)      TR7_CAR(TR7_CAR(t))
#define TR7_CADR(t)      TR7_CAR(TR7_CDR(t))
#define TR7_CDAR(t)      TR7_CDR(TR7_CAR(t))
#define TR7_CDDR(t)      TR7_CDR(TR7_CDR(t))
/*
* Level 3 helpers
*
* !WARNING! This macros are not checking type of the tr7_t value.
*/
#define TR7_CAAAR(t)     TR7_CAR(TR7_CAAR(t))
#define TR7_CAADR(t)     TR7_CAR(TR7_CADR(t))
#define TR7_CADAR(t)     TR7_CAR(TR7_CDAR(t))
#define TR7_CADDR(t)     TR7_CAR(TR7_CDDR(t))
#define TR7_CDAAR(t)     TR7_CDR(TR7_CAAR(t))
#define TR7_CDADR(t)     TR7_CDR(TR7_CADR(t))
#define TR7_CDDAR(t)     TR7_CDR(TR7_CDAR(t))
#define TR7_CDDDR(t)     TR7_CDR(TR7_CDDR(t))
/*
* Level 4 helpers
*
* !WARNING! This macros are not checking type of the tr7_t value.
*/
#define TR7_CAAAAR(t)    TR7_CAR(TR7_CAAAR(t))
#define TR7_CAAADR(t)    TR7_CAR(TR7_CAADR(t))
#define TR7_CAADAR(t)    TR7_CAR(TR7_CADAR(t))
#define TR7_CAADDR(t)    TR7_CAR(TR7_CADDR(t))
#define TR7_CADAAR(t)    TR7_CAR(TR7_CDAAR(t))
#define TR7_CADADR(t)    TR7_CAR(TR7_CDADR(t))
#define TR7_CADDAR(t)    TR7_CAR(TR7_CDDAR(t))
#define TR7_CADDDR(t)    TR7_CAR(TR7_CDDDR(t))
#define TR7_CDAAAR(t)    TR7_CDR(TR7_CAAAR(t))
#define TR7_CDAADR(t)    TR7_CDR(TR7_CAADR(t))
#define TR7_CDADAR(t)    TR7_CDR(TR7_CADAR(t))
#define TR7_CDADDR(t)    TR7_CDR(TR7_CADDR(t))
#define TR7_CDDAAR(t)    TR7_CDR(TR7_CDAAR(t))
#define TR7_CDDADR(t)    TR7_CDR(TR7_CDADR(t))
#define TR7_CDDDAR(t)    TR7_CDR(TR7_CDDAR(t))
#define TR7_CDDDDR(t)    TR7_CDR(TR7_CDDDR(t))
/*
*****************************************************************************
* Defining tr7_head_t
* -------------------
*
* Any cell starts with a head that fits an intptr_t to be compatible
* in size and alignement with pointers being in the same heap.
*/
typedef intptr_t tr7_head_t;
/*
* The 8 least significant bits are used to hold flags.
* The remaining bits can be used to store any value.
*
* The idea is to implement the following schema:
*
*      <- 24 or 56 bits ->  7   6   5   4   3   2   1   0
*     +---+---+ - - - - - +---+---+---+---+---+---+---+---+
*     |   V A L U E       |MUT|MARKIDX|    K I N D        |
*     +---+---+ - - - - - +---+---+---+---+---+---+---+---+
*                         <············HEAD···············>
*
* Where:
* - KIND    indicates the kind of cell (string, vector, ...)
* - MARKIDX can be used internally by the garbage collector
* - MUT     if set indicates immutability of the cell (IMMUTABLE)
* - VALUE   any value having mean for the kind
*
* Values for bit field KIND
*/
#define TR7_SHIFT_HEAD_KIND        0
#define TR7_WIDTH_HEAD_KIND        5
#define TR7_MASK_HEAD_KIND         (((1 << TR7_WIDTH_HEAD_KIND) - 1) << TR7_SHIFT_HEAD_KIND)
/*
* Values for bit field MARKIDX
*/
#define TR7_SHIFT_HEAD_MARKIDX     (TR7_SHIFT_HEAD_KIND + TR7_WIDTH_HEAD_KIND)
#define TR7_WIDTH_HEAD_MARKIDX     2
#define TR7_MASK_HEAD_MARKIDX      (((1 << TR7_WIDTH_HEAD_MARKIDX) - 1) << TR7_SHIFT_HEAD_MARKIDX)
#define TR7_MAX_HEAD_MARKIDX       ((1 << TR7_WIDTH_HEAD_MARKIDX) - 1)
/*
* Values for bit field IMMUTABLE
*/
#define TR7_SHIFT_HEAD_IMMUTABLE   (TR7_SHIFT_HEAD_MARKIDX + TR7_WIDTH_HEAD_MARKIDX)
#define TR7_WIDTH_HEAD_IMMUTABLE   1
#define TR7_MASK_HEAD_IMMUTABLE    (((1 << TR7_WIDTH_HEAD_IMMUTABLE) - 1) << TR7_SHIFT_HEAD_IMMUTABLE)
/*
* Values for meta bit field FLAGS
*/
#define TR7_WIDTH_HEAD_FLAGS       (TR7_WIDTH_HEAD_KIND + TR7_WIDTH_HEAD_MARKIDX + TR7_WIDTH_HEAD_IMMUTABLE)
#define TR7_MASK_HEAD_FLAGS        (((1 << TR7_WIDTH_HEAD_FLAGS) - 1) << TR7_SHIFT_HEAD_KIND)
/*
* Extract the kind of a cell's header
*/
#define TR7_HEAD_KIND(h)           ((h) & TR7_MASK_HEAD_KIND)
/*
* Get and set the MARKIDX value of a cell's header
*/
#define TR7_HEAD_MARKIDX_GET(h)    (((h) >> TR7_SHIFT_HEAD_MARKIDX) & TR7_MAX_HEAD_MARKIDX)
#define TR7_HEAD_MARKIDX_SET(h,v)  (((h) & ~TR7_MASK_HEAD_MARKIDX) | ((v) << TR7_SHIFT_HEAD_MARKIDX))
/*
* Get and set the IMMUTABLE value of a cell's header
*/
#define TR7_HEAD_IS_IMMUTABLE(h)   ((h) & TR7_MASK_HEAD_IMMUTABLE)
#define TR7_HEAD_SET_IMMUTABLE(h)  ((h) | TR7_MASK_HEAD_IMMUTABLE)
/*
* Get the meta values of FLAGS of a cell's header
*/
#define TR7_HEAD_FLAGS(h)          ((h) & TR7_MASK_HEAD_FLAGS)
/*
* Make a cell's header from its VALUE and its FLAGS
*/
#define TR7_MAKE_HEAD(v,f)         (((v) << TR7_WIDTH_HEAD_FLAGS) | (f))
/*
* Get and set signed VALUE of a cell's header
*/
#define TR7_HEAD_VALUE(h)          (((tr7_int_t)(h)) >> TR7_WIDTH_HEAD_FLAGS)
#define TR7_HEAD_SET_VALUE(h,v)    TR7_MAKE_HEAD(v,TR7_HEAD_FLAGS(h))
/*
* Get and set unsigned VALUE of a cell's header
*/
#define TR7_HEAD_UVALUE(h)         (((tr7_uint_t)(h)) >> TR7_WIDTH_HEAD_FLAGS)
#define TR7_HEAD_SET_UVALUE(h,v)   TR7_MAKE_HEAD(v,TR7_HEAD_FLAGS(h))
/*
*****************************************************************************
* Defining tr7_cell_t
* -------------------
*
* Cell could have been a pointer to a tr7_head_t, it would be enought.
* Though defining it with the union is safer and allows more cleaner
* code.
*/
union tr7_cell {
   tr7_head_t head;  /* common case, standard head of a cell */
   tr7_cell_t link;  /* used by memory manager to link free cells */
   tr7_t      ____; /* not used, here to ensure coherency of union */
};
/*
* Macros for manipulating cells, tr7_t value wraps a pointer to a cell
*
* TR7_IS_CELL(t): returns a boolean true if the tr7 value t points a cell
* TR7_TO_CELL(t): returns the tr_cell_t (pointer to cell) of the tr7 value t
* TR7_AS_PAIR(t): returns the tr7_cell_t of the tr7 value t if of the kind
*                 cell, or otherwise, if t isn't a cell, returns NULL
* TR7_FROM_CELL(p): returns the tr7_t value for the tr7_cell_t c
*/
#define TR7_IS_CELL(t)         TR7_IS_PTR_KIND(t,TR7_KIND_CELL)
#define TR7_TO_CELL(t)         ((tr7_cell_t)TR7_TO_PTR(t,TR7_KIND_CELL))
#define TR7_AS_CELL(t)         ((tr7_cell_t)TR7_AS_PTR(t,TR7_KIND_CELL))
#define TR7_FROM_CELL(c)       TR7_FROM_PTR(c,TR7_KIND_CELL)
/*
*/
#define TR7_CELL_HEAD(c)       ((c)->head)
#define TR7_CELL_KIND(c)       TR7_HEAD_KIND(TR7_CELL_HEAD(c))
#define TR7_CELL_IS_KIND(c,k)  (TR7_CELL_KIND(c) == (k))

#define TR7_CELL_MARKIDX_HEAD(c) TR7_HEAD_MARKIDX_GET(TR7_CELL_HEAD(c))
#define TR7_CELL_SET_MARKIDX_HEAD(c, v) (TR7_CELL_HEAD(c) = TR7_HEAD_MARKIDX_SET(TR7_CELL_HEAD(c), v))

#define TR7_CELL_IS_IMMUTABLE_HEAD(c) TR7_HEAD_IS_IMMUTABLE(TR7_CELL_HEAD(c))
#define TR7_CELL_SET_IMMUTABLE_HEAD(c) (TR7_CELL_HEAD(c) = TR7_HEAD_SET_IMMUTABLE(TR7_CELL_HEAD(c)))

#define TR7_CELL_FLAGS_HEAD(c)          TR7_HEAD_FLAGS(TR7_CELL_HEAD(c))
#define TR7_CELL_VALUE_HEAD(c)          TR7_HEAD_VALUE(TR7_CELL_HEAD(c))
#define TR7_CELL_UVALUE_HEAD(c)         TR7_HEAD_UVALUE(TR7_CELL_HEAD(c))
#define TR7_CELL_SET_VALUE_HEAD(c,v)    (TR7_CELL_HEAD(c) = TR7_HEAD_SET_VALUE(TR7_CELL_HEAD(c),v))
#define TR7_CELL_SET_UVALUE_HEAD(c,v)   (TR7_CELL_HEAD(c) = TR7_HEAD_SET_UVALUE(TR7_CELL_HEAD(c),v))
/*
*/
#define TR7_HEAD_CELL(t)       TR7_CELL_HEAD(TR7_TO_CELL(t))
#define TR7_KIND_CELL_HEAD(t)  TR7_CELL_KIND(TR7_TO_CELL(t))
#define TR7_IS_CELL_KIND(t,k)  (TR7_IS_CELL(t) && (TR7_CELL_IS_KIND(TR7_TO_CELL(t), (k))))

#define TR7_MARKIDX_CELL_HEAD(t)        TR7_CELL_MARKIDX_HEAD(TR7_TO_CELL(t))
#define TR7_SET_MARKIDX_CELL_HEAD(t, v) TR7_CELL_SET_MARKIDX_HEAD(TR7_TO_CELL(t))

#define TR7_IS_IMMUTABLE_CELL_HEAD(t)   TR7_CELL_IS_IMMUTABLE_HEAD(TR7_TO_CELL(t))
#define TR7_SET_IMMUTABLE_CELL_HEAD(t)  TR7_CELL_SET_IMMUTABLE_HEAD(TR7_TO_CELL(t))

#define TR7_FLAGS_CELL_HEAD(t)          TR7_CELL_FLAGS_HEAD(TR7_TO_CELL(t))
#define TR7_VALUE_CELL_HEAD(t)          TR7_CELL_VALUE_HEAD(TR7_TO_CELL(t))
#define TR7_UVALUE_CELL_HEAD(t)         TR7_CELL_UVALUE_HEAD(TR7_TO_CELL(t))
/*
**************************************************************************
*
*
*/
#define TR7_HEAD_KIND_NONE           0
/*
* size 2
*/
#define TR7_HEAD_KIND_STRING         1
#define TR7_HEAD_KIND_SYMBOL         2
#define TR7_HEAD_KIND_BYTEVECTOR     3
#define TR7_HEAD_KIND_PORT           4
#define TR7_HEAD_KIND_FOREIGN        5
#define TR7_HEAD_KIND_CONTINUATION   6
#define TR7_HEAD_KIND_RATIONAL       7  /* reserved */
#define TR7_HEAD_KIND_COMPLEX        8  /* reserved */
/*
* size 3
*/
#define TR7_HEAD_KIND_MACRO          9
#define TR7_HEAD_KIND_LAMBDA        10
#define TR7_HEAD_KIND_CASE_LAMBDA   11
#define TR7_HEAD_KIND_PROMISE       12
#define TR7_HEAD_KIND_PARAMETER     13
#define TR7_HEAD_KIND_PARAMSAVE     14
/*
* size 4
*/
#define TR7_HEAD_KIND_GUARD         15
#define TR7_HEAD_KIND_STACK         16
/*
* size 5
*/
#define TR7_HEAD_KIND_TRANSFORM     17
/*
* size 6
*/
#define TR7_HEAD_KIND_DYNAWIND      18
/*
* size ...
*/
#define TR7_HEAD_KIND_ENVIRONMENT   19
#define TR7_HEAD_KIND_RECORD        20
#define TR7_HEAD_KIND_VECTOR        21
#define TR7_HEAD_KIND_BIGINT        22  /* reserved */
/*
**************************************************************************
*
* Definition of buffers. Buffers are used for strings, symbols
* and bytevectors
*
*/
typedef struct tr7_buffer *tr7_buffer_t;

struct tr7_buffer {
   tr7_head_t head;
   uint8_t *content;
};

#define TR7_BUFFER_HEAD(b)          (b)->head
#define TR7_BUFFER_CONTENT(b)       ((b)->content)
#define TR7_BUFFER_LENGTH(b)        TR7_HEAD_UVALUE(TR7_BUFFER_HEAD(b))
#define TR7_BUFFER_SET_LENGTH(b,v)  (TR7_BUFFER_HEAD(b) = TR7_HEAD_SET_UVALUE(TR7_BUFFER_HEAD(b),v))

#define TR7_CELL_TO_BUFFER(c)       ((tr7_buffer_t)(c))
#define TR7_CELL_CONTENT_BUFFER(c)  TR7_BUFFER_CONTENT(TR7_CELL_TO_BUFFER(c))
#define TR7_CELL_LENGTH_BUFFER(c)   TR7_BUFFER_LENGTH(TR7_CELL_TO_BUFFER(c))

#define TR7_TO_BUFFER(t)            TR7_CELL_TO_BUFFER(TR7_TO_CELL(t))
#define TR7_FROM_BUFFER(b)          TR7_FROM_CELL(b)
#define TR7_CONTENT_BUFFER(t)       TR7_BUFFER_CONTENT(TR7_TO_BUFFER(t))
#define TR7_LENGTH_BUFFER(t)        TR7_BUFFER_LENGTH(TR7_TO_BUFFER(t))
#define TR7_SET_LENGTH_BUFFER(t,v)  TR7_BUFFER_SET_LENGTH(TR7_TO_BUFFER(t),v)



#define TR7_BYTEVECTOR_LENGTH(b)    TR7_BUFFER_LENGTH(b)
#define TR7_BYTEVECTOR_CONTENT(b)   TR7_BUFFER_CONTENT(b)
#define TR7_BYTEVECTOR_SET_LENGTH(b,v)  TR7_BUFFER_SET_LENGTH(b,v)

#define TR7_CELL_IS_BYTEVECTOR(c)      TR7_CELL_IS_KIND(c, TR7_HEAD_KIND_BYTEVECTOR)
#define TR7_CELL_TO_BYTEVECTOR(c)      TR7_CELL_TO_BUFFER(c)
#define TR7_CELL_CONTENT_BYTEVECTOR(c) TR7_BYTEVECTOR_CONTENT(TR7_CELL_TO_BYTEVECTOR(c))
#define TR7_CELL_LENGTH_BYTEVECTOR(c)  TR7_BYTEVECTOR_LENGTH(TR7_CELL_TO_BYTEVECTOR(c))

#define TR7_IS_BYTEVECTOR(t)      TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_BYTEVECTOR)
#define TR7_TO_BYTEVECTOR(t)      TR7_TO_BUFFER(t)
#define TR7_AS_BYTEVECTOR(t)      (TR7_IS_BYTEVECTOR(t) ? TR7_TO_BYTEVECTOR(t) : NULL)
#define TR7_FROM_BYTEVECTOR(b)    TR7_FROM_BUFFER(b)
#define TR7_CONTENT_BYTEVECTOR(t) TR7_BYTEVECTOR_CONTENT(TR7_TO_BYTEVECTOR(t))
#define TR7_LENGTH_BYTEVECTOR(t)  TR7_BYTEVECTOR_LENGTH(TR7_TO_BYTEVECTOR(t))
#define TR7_SET_LENGTH_BYTEVECTOR(t,v)  TR7_BYTEVECTOR_SET_LENGTH(TR7_TO_BYTEVECTOR(t),v)




#define TR7_STRING_LENGTH(s)        TR7_BUFFER_LENGTH(s)
#define TR7_STRING_CONTENT(s)       TR7_BUFFER_CONTENT(s)
#define TR7_STRING_SET_LENGTH(s,v)  TR7_BUFFER_SET_LENGTH(s,v)

#define TR7_CELL_IS_STRING(c)       TR7_CELL_IS_KIND(c, TR7_HEAD_KIND_STRING)
#define TR7_CELL_TO_STRING(c)       TR7_CELL_TO_BUFFER(c)
#define TR7_CELL_CONTENT_STRING(c)  TR7_STRING_CONTENT(TR7_CELL_TO_STRING(c))
#define TR7_CELL_LENGTH_STRING(c)   TR7_STRING_LENGTH(TR7_CELL_TO_STRING(c))

#define TR7_IS_STRING(t)            TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_STRING)
#define TR7_TO_STRING(t)            TR7_TO_BUFFER(t)
#define TR7_AS_STRING(t)            (TR7_IS_STRING(t) ? TR7_TO_STRING(t) : NULL)
#define TR7_FROM_STRING(s)          TR7_FROM_BUFFER(s)
#define TR7_CONTENT_STRING(t)       TR7_STRING_CONTENT(TR7_TO_STRING(t))
#define TR7_LENGTH_STRING(t)        TR7_STRING_LENGTH(TR7_TO_STRING(t))
#define TR7_SET_LENGTH_STRING(t,v)  TR7_STRING_SET_LENGTH(TR7_TO_STRING(t),v)


#define TR7_SYMBOL_LENGTH(s)      TR7_BUFFER_LENGTH(s)
#define TR7_SYMBOL_CONTENT(s)     TR7_BUFFER_CONTENT(s)

#define TR7_CELL_IS_SYMBOL(c)      TR7_CELL_IS_KIND(c, TR7_HEAD_KIND_SYMBOL)
#define TR7_CELL_TO_SYMBOL(c)      TR7_CELL_TO_BUFFER(c)
#define TR7_CELL_CONTENT_SYMBOL(c) TR7_SYMBOL_CONTENT(TR7_CELL_TO_SYMBOL(c))
#define TR7_CELL_LENGTH_SYMBOL(c)  TR7_SYMBOL_LENGTH(TR7_CELL_TO_SYMBOL(c))

#define TR7_IS_SYMBOL(t)        TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_SYMBOL)
#define TR7_TO_SYMBOL(t)        TR7_TO_BUFFER(t)
#define TR7_AS_SYMBOL(t)        (TR7_IS_SYMBOL(t) ? TR7_TO_SYMBOL(t) : NULL)
#define TR7_FROM_SYMBOL(s)      TR7_FROM_BUFFER(s)
#define TR7_CONTENT_SYMBOL(t)   TR7_SYMBOL_CONTENT(TR7_TO_SYMBOL(t))
#define TR7_LENGTH_SYMBOL(t)    TR7_SYMBOL_LENGTH(TR7_TO_SYMBOL(t))


/*
**************************************************************************
* Definition of tr7_port_t
* ------------------------
*/
struct _port_;
typedef struct tr7_port *tr7_port_t;

struct tr7_port {
   tr7_head_t head;
   struct _port_ *_port_;
};
#define TR7_CELL_IS_PORT(c)      TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_PORT)
#define TR7_CELL_TO_PORT(c)      ((tr7_port_t)(c))
#define TR7_CELL_AS_PORT(c)      (TR7_CELL_IS_PORT(c) ? TR7_CELL_TO_PORT(c) : NULL)

#define TR7_IS_PORT(t)           TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_PORT)
#define TR7_TO_PORT(t)           ((tr7_port_t)TR7_TO_CELL(t))
#define TR7_AS_PORT(t)           (TR7_IS_PORT(t) ? TR7_TO_PORT(t) : NULL)
#define TR7_FROM_PORT(p)         TR7_FROM_CELL(p)

#define TR7_PORT__PORT_(p)       (p)->_port_
#define TR7_CELL_PORT__PORT_(c)  TR7_PORT__PORT_(TR7_CELL_TO_PORT(c))
#define TR7__PORT__PORT(c)       TR7_PORT__PORT_(TR7_TO_PORT(c))
/*
**************************************************************************
*
* Definition of continuations
*/
typedef struct tr7_continuation *tr7_continuation_t;

struct tr7_continuation {
   tr7_head_t head;
   tr7_t dump;
};
#define TR7_CELL_IS_CONTINUATION(c)  TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_CONTINUATION)
#define TR7_CELL_TO_CONTINUATION(c)  ((tr7_continuation_t)(c))
#define TR7_IS_CONTINUATION(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_CONTINUATION)
#define TR7_TO_CONTINUATION(t)       ((tr7_continuation_t)TR7_TO_CELL(t))
/*
**************************************************************************
*
* Definition of closures for lambda, case-lambda and macro
*/
typedef struct tr7_closure *tr7_closure_t;

struct tr7_closure {
   tr7_head_t head;
   tr7_t expr;
   tr7_t env;
};
#define TR7_TO_CLOSURE(t)     ((tr7_closure_t)TR7_TO_CELL(t))
#define TR7_IS_MACRO(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_MACRO)
#define TR7_IS_LAMBDA(t)      TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_LAMBDA)
#define TR7_IS_CASE_LAMBDA(t) TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_CASE_LAMBDA)
/*
**************************************************************************
*
* Definition of promises
*/
typedef struct tr7_promise *tr7_promise_t;

struct tr7_promise {
   tr7_head_t head;
   tr7_t code_or_value;
   tr7_t env;
};
#define TR7_IS_PROMISE(t)             TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_PROMISE)
#define TR7_TO_PROMISE(t)             ((tr7_promise_t)TR7_TO_CELL(t))
#define TR7_PROMISE_FLAG_FORCED       TR7_MAKE_HEAD(1,0)
#define TR7_PROMISE_FLAG_DELAY_FORCE  TR7_MAKE_HEAD(2,0)
#define TR7_PROMISE_IS_FORCED(p)      (((p)->head & TR7_PROMISE_FLAG_FORCED) != 0)
#define TR7_PROMISE_IS_DELAY_FORCE(p) (((p)->head & TR7_PROMISE_FLAG_DELAY_FORCE) != 0)
/*
**************************************************************************
*
* Definition of parameters
*
* A parameter has a current value and a converter
* The head is used only to record the kind
*/
typedef struct tr7_parameter *tr7_parameter_t;

struct tr7_parameter {
   tr7_head_t head;
   tr7_t value;
   tr7_t converter;
};

#define TR7_IS_PARAMETER(t)   TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_PARAMETER)
#define TR7_TO_PARAMETER(t)   ((tr7_parameter_t)TR7_TO_CELL(t))
#define TR7_FROM_PARAMETER(p) TR7_FROM_CELL(p)
/*
**************************************************************************
*
* Definition of syntax transforms
*/
typedef struct tr7_transform *tr7_transform_t;

struct tr7_transform {
   tr7_head_t head;
   tr7_t ellipsis;
   tr7_t literals;
   tr7_t rules;
   tr7_t env;
};
#define TR7_IS_TRANSFORM(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_TRANSFORM)
#define TR7_TO_TRANSFORM(t)       ((tr7_transform_t)TR7_TO_CELL(t))
/*
**************************************************************************
*
* Definition of dumpable item: a virtual template for STACK, GUARD and DYNAWIND
*/
typedef struct tr7_dumpable *tr7_dumpable_t;

struct tr7_dumpable {
   tr7_head_t head;         /* has op */
   tr7_t dump;
};
#define TR7_CELL_TO_DUMPABLE(c)  ((tr7_stack_t)(c))
#define TR7_TO_DUMPABLE(t)       TR7_CELL_TO_DUMPABLE(TR7_TO_CELL(t))
/*
**************************************************************************
*
* Definition of stack items
*/
typedef struct tr7_stack *tr7_stack_t;

struct tr7_stack {
   tr7_head_t head;         /* has op */
   tr7_t dump;
   tr7_t env;
   tr7_t args;
};
#define TR7_CELL_IS_STACK(c)  TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_STACK)
#define TR7_CELL_TO_STACK(c)  ((tr7_stack_t)(c))
#define TR7_IS_STACK(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_STACK)
#define TR7_TO_STACK(t)       TR7_CELL_TO_STACK(TR7_TO_CELL(t))
#define TR7_FROM_STACK(s)     TR7_FROM_CELL(s)
/*
**************************************************************************
*
* Definition of guards
*/
typedef struct tr7_guard *tr7_guard_t;

struct tr7_guard {
   tr7_head_t head;
   tr7_t dump;
   tr7_t env;
   tr7_t expr;
};

#define TR7_GUARD_KIND_GUARD     0
#define TR7_GUARD_KIND_HANDLER   1
#define TR7_GUARD_KIND_REPEAT    2
#define TR7_GUARD_KIND_ROOT      4

#define TR7_CELL_IS_GUARD(c)  TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_GUARD)
#define TR7_CELL_TO_GUARD(c)  ((tr7_guard_t)(c))
#define TR7_IS_GUARD(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_GUARD)
#define TR7_TO_GUARD(t)       TR7_CELL_TO_GUARD(TR7_TO_CELL(t))
#define TR7_FROM_GUARD(g)     TR7_FROM_CELL(g)
/*
**************************************************************************
*
* Definition of dynamic winds
*/
typedef struct tr7_dynawind *tr7_dynawind_t;

struct tr7_dynawind {
   tr7_head_t head;
   tr7_t dump;
   tr7_t env;
   tr7_t before;
   tr7_t after;
   tr7_t link;
};
#define TR7_DYNAWIND_DEPTH(dw)   TR7_HEAD_UVALUE((dw)->head)
#define TR7_CELL_IS_DYNAWIND(c)  TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_DYNAWIND)
#define TR7_CELL_TO_DYNAWIND(c)  ((tr7_dynawind_t)(c))
#define TR7_IS_DYNAWIND(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_DYNAWIND)
#define TR7_TO_DYNAWIND(t)       ((tr7_dynawind_t)TR7_TO_CELL(t))
#define TR7_FROM_DYNAWIND(dw)    TR7_FROM_CELL(dw)
/*
**************************************************************************
*
* Definition of parameter saving items
*/
typedef struct tr7_paramsave *tr7_paramsave_t;

struct tr7_paramsave {
   tr7_head_t head;
   tr7_t dump;
   tr7_t params;
};
#define TR7_CELL_IS_PARAMSAVE(c)  TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_PARAMSAVE)
#define TR7_CELL_TO_PARAMSAVE(c)  ((tr7_paramsave_t)(c))
#define TR7_IS_PARAMSAVE(t)       TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_PARAMSAVE)
#define TR7_TO_PARAMSAVE(t)       TR7_CELL_TO_PARAMSAVE(TR7_TO_CELL(t))
#define TR7_FROM_PARAMSAVE(s)     TR7_FROM_CELL(s)
/*
**************************************************************************
*
* Definition of environments
*/
typedef struct tr7_environment *tr7_environment_t;

struct tr7_environment {
   tr7_head_t head;         /* count */
   tr7_t lower;             /* lower environment */
   tr7_t items[];           /* content */
};
#define TR7_IS_ENVIRONMENT(t)    TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_ENVIRONMENT)
#define TR7_TO_ENVIRONMENT(t)    ((tr7_environment_t)TR7_TO_CELL(t))
/*
**************************************************************************
* Definition of tr7_vector_t
* --------------------------
*
* Vectors are contiguous cells held together.
*/
typedef struct tr7_vector *tr7_vector_t;

struct tr7_vector {
   tr7_head_t head;     /* the head */
   tr7_t      items[];  /* the items */
};
/*
* Macros for manipulating vectors are:
*/
#define TR7_CELL_IS_VECTOR(c)       TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_VECTOR)
#define TR7_CELL_TO_VECTOR(c)       ((tr7_vector_t)(c))
#define TR7_IS_VECTOR(t)            TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_VECTOR)
#define TR7_TO_VECTOR(t)            ((tr7_vector_t)TR7_TO_CELL(t))
#define TR7_AS_VECTOR(t)            (TR7_IS_VECTOR(t) ? TR7_TO_VECTOR(t) : NULL)
#define TR7_FROM_VECTOR(v)          TR7_FROM_CELL(v)

#define TR7_VECTOR_LENGTH(v)        TR7_HEAD_UVALUE((v)->head)
#define TR7_VECTOR_ITEMS(v)         ((v)->items)
#define TR7_VECTOR_ITEM(v,i)        (TR7_VECTOR_ITEMS(v)[i])

#define TR7_CELL_VECTOR_LENGTH(c)   TR7_VECTOR_LENGTH(TR7_CELL_TO_VECTOR(c))
#define TR7_CELL_VECTOR_ITEMS(c)    TR7_VECTOR_ITEMS(TR7_CELL_TO_VECTOR(c))
#define TR7_CELL_VECTOR_ITEM(c,i)   TR7_VECTOR_ITEM(TR7_CELL_TO_VECTOR(c),i)

#define TR7_LENGTH_VECTOR(t)         TR7_VECTOR_LENGTH(TR7_TO_VECTOR(t))
#define TR7_ITEMS_VECTOR(t)         TR7_VECTOR_ITEMS(TR7_TO_VECTOR(t))
#define TR7_ITEM_VECTOR(t,i)        TR7_VECTOR_ITEM(TR7_TO_VECTOR(t),i)
/*
**************************************************************************
* Definition of tr7_record_t
* --------------------------
*
* Record cells are just like vectors.
*/
typedef struct tr7_vector *tr7_record_t;
/*
* Macros for manipulating records are:
*/
#define TR7_CELL_IS_RECORD(c)       TR7_CELL_IS_KIND((c), TR7_HEAD_KIND_RECORD)
#define TR7_CELL_TO_RECORD(c)       ((tr7_record_t)(c))
#define TR7_IS_RECORD(t)            TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_RECORD)
#define TR7_TO_RECORD(t)            ((tr7_record_t)TR7_TO_CELL(t))
#define TR7_AS_RECORD(t)            (TR7_IS_RECORD(t) ? TR7_TO_RECORD(t) : NULL)
#define TR7_FROM_RECORD(r)          TR7_FROM_CELL(r)

#define TR7_RECORD_LENGTH(r)        TR7_HEAD_UVALUE((r)->head)
#define TR7_RECORD_ITEM(r,i)        (r)->items[i]

#define TR7_CELL_RECORD_LENGTH(c)   TR7_RECORD_LENGTH(TR7_CELL_TO_RECORD(c))
#define TR7_CELL_RECORD_ITEM(c,i)   TR7_RECORD_ITEM(TR7_CELL_TO_RECORD(c),i)

#define TR7_LENGTH_RECORD(t)        TR7_RECORD_LENGTH(TR7_TO_RECORD(t))
#define TR7_ITEM_RECORD(t,i)        TR7_RECORD_ITEM(TR7_TO_RECORD(t),i)
/*
**************************************************************************
* Defining of foreign functions
* ------------------------------
*/
typedef struct tr7_engine *tr7_engine_t;

typedef tr7_t (*tr7_C_func_t)(tr7_engine_t, int, tr7_t*, void*);

typedef struct tr7_ff *tr7_ff_t;

struct tr7_ff {
   tr7_head_t    head;
   tr7_C_func_t  func;
   void         *closure;
   const char   *typargs;
};

#define TR7_IS_FF(t)              TR7_IS_CELL_KIND((t), TR7_HEAD_KIND_FOREIGN)
#define TR7_TO_FF(t)              ((tr7_ff_t)TR7_TO_CELL(t))
#define TR7_FROM_FF(f)            TR7_TO_CELL(f)

#define TR7_FF_NARGS_WIDTH        5
#define TR7_FF_NARGS_MASK         ((1 << TR7_FF_NARGS_WIDTH) - 1)
#define TR7_FF_NARGS_MAXIMUM      TR7_FF_NARGS_MASK

#define TR7_MAKE_FF_HEAD(M,m)     TR7_MAKE_HEAD((((M) << TR7_FF_NARGS_WIDTH) | (m)), TR7_HEAD_KIND_FOREIGN)
#define TR7_FF_NARGS_MIN(f)       (TR7_HEAD_VALUE((f)->head) & TR7_FF_NARGS_MASK)
#define TR7_FF_NARGS_MAX(f)       (TR7_HEAD_VALUE((f)->head) >> TR7_FF_NARGS_WIDTH)

#define TR7_CELL_IS_FF(c)         TR7_CELL_IS_KIND(c, TR7_HEAD_KIND_FOREIGN)
#define TR7_CELL_TO_FF(c)         ((tr7_ff_t)(c))
#define TR7_CELL_FF_NARGS_MIN(c)  TR7_FF_NARGS_MIN(TR7_CELL_TO_FF(c))
#define TR7_CELL_FF_NARGS_MAX(c)  TR7_FF_NARGS_MAX(TR7_CELL_TO_FF(c))

#define TR7_NARGS_MIN_FF(t)       TR7_FF_NARGS_MIN(TR7_TO_FF(t))
#define TR7_NARGS_MAX_FF(t)       TR7_FF_NARGS_MAX(TR7_TO_FF(t))

/*
*****************************************************************************
*/
#include <stdio.h>
#include <stdbool.h>

/*
*****************************************************************************
* Default values for #define'd symbols
*/
#ifndef TR7_EXPORT
#  ifndef _MSC_VER
#    define TR7_EXPORT
#  else
#    ifdef TR7_SOURCE
#      define TR7_EXPORT __declspec(dllexport)
#    else
#      define TR7_EXPORT __declspec(dllimport)
#    endif
#  endif
#endif

/*
*****************************************************************************
*
* Definition of comparison results
*/
#define TR7_CMP_LESSER  1
#define TR7_CMP_EQUAL   2
#define TR7_CMP_GREATER 4
/*
*****************************************************************************
*/

typedef struct tr7_config tr7_config_t;
typedef void *(*tr7_malloc_t)(size_t);
typedef void (*tr7_free_t)(void *);

struct tr7_config
{
   int           main_dictionary_size;
   tr7_malloc_t  malloc;
   tr7_free_t    free;
};


TR7_EXPORT void         tr7_config_init_default(tr7_config_t *config);
TR7_EXPORT tr7_engine_t tr7_engine_create(tr7_config_t *config);
TR7_EXPORT void         tr7_engine_destroy(tr7_engine_t tsc);


TR7_EXPORT void tr7_set_argv(char **argv);
TR7_EXPORT void tr7_set_ports(tr7_engine_t tsc, FILE * fin, FILE * fout, FILE * ferr);
TR7_EXPORT void tr7_set_standard_ports(tr7_engine_t tsc);
TR7_EXPORT void tr7_set_input_port_file(tr7_engine_t tsc, FILE * fin);
TR7_EXPORT void tr7_set_input_port_string(tr7_engine_t tsc, char *start, char *end);
TR7_EXPORT void tr7_set_output_port_file(tr7_engine_t tsc, FILE * fout);
TR7_EXPORT void tr7_set_output_port_string(tr7_engine_t tsc, char *start, char *end);
TR7_EXPORT void tr7_set_error_port_file(tr7_engine_t tsc, FILE * ferr);
TR7_EXPORT void tr7_set_error_port_string(tr7_engine_t tsc, char *start, char *end);

TR7_EXPORT int  tr7_load_file(tr7_engine_t tsc, FILE * fin, const char *filename);
TR7_EXPORT void tr7_load_string(tr7_engine_t tsc, const char *cmd);



TR7_EXPORT tr7_t tr7_cons_n(tr7_engine_t tsc, int count, tr7_t cars[], tr7_t cdr);
TR7_EXPORT tr7_t tr7_cons(tr7_engine_t tsc, tr7_t a, tr7_t b);

#define TR7_CONS(tsc,a,b)               tr7_cons(tsc,a,b)
#define TR7_CONS2(tsc,a,b)              tr7_cons(tsc,a,b)
#define TR7_CONS3(tsc,a,b,c)            tr7_cons_n(tsc,2,(tr7_t[2]){a,b},c)
#define TR7_CONS4(tsc,a,b,c,d)          tr7_cons_n(tsc,3,(tr7_t[3]){a,b,c},d)
#define TR7_CONS5(tsc,a,b,c,d,e)        tr7_cons_n(tsc,4,(tr7_t[4]){a,b,c,d},e)
#define TR7_CONS6(tsc,a,b,c,d,e,f)      tr7_cons_n(tsc,5,(tr7_t[5]){a,b,c,d,e},f)
#define TR7_CONS7(tsc,a,b,c,d,e,f,g)    tr7_cons_n(tsc,6,(tr7_t[6]){a,b,c,d,e,f},g)
#define TR7_CONS8(tsc,a,b,c,d,e,f,g,h)  tr7_cons_n(tsc,7,(tr7_t[7]){a,b,c,d,e,f,g},h)

#define TR7_LIST1(tsc,a)                tr7_cons(tsc,a,TR7_NIL)
#define TR7_LIST2(tsc,a,b)              TR7_CONS3(tsc,a,b,TR7_NIL)
#define TR7_LIST3(tsc,a,b,c)            TR7_CONS4(tsc,a,b,c,TR7_NIL)
#define TR7_LIST4(tsc,a,b,c,d)          TR7_CONS5(tsc,a,b,c,d,TR7_NIL)
#define TR7_LIST5(tsc,a,b,c,d,e)        TR7_CONS6(tsc,a,b,c,d,e,TR7_NIL)
#define TR7_LIST6(tsc,a,b,c,d,e,f)      TR7_CONS7(tsc,a,b,c,d,e,f,TR7_NIL)
#define TR7_LIST7(tsc,a,b,c,d,e,f,g)    TR7_CONS8(tsc,a,b,c,d,e,f,g,TR7_NIL)

TR7_EXPORT tr7_t tr7_car_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdr_or_void(tr7_t t);

TR7_EXPORT tr7_t tr7_caar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cddr_or_void(tr7_t t);

TR7_EXPORT tr7_t tr7_caaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cadar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caddr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cddar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdddr_or_void(tr7_t t);

TR7_EXPORT tr7_t tr7_caaaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caaadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caadar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caaddr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cadaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cadadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_caddar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cadddr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdaaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdaadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdadar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdaddr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cddaar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cddadr_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cdddar_or_void(tr7_t t);
TR7_EXPORT tr7_t tr7_cddddr_or_void(tr7_t t);

TR7_EXPORT int tr7_eq(tr7_t a, tr7_t b);
TR7_EXPORT int tr7_eqv(tr7_t a, tr7_t b);
TR7_EXPORT int tr7_equal(tr7_t a, tr7_t b);

TR7_EXPORT int        tr7_list_length(tr7_t a);
TR7_EXPORT tr7_t      tr7_reverse(tr7_engine_t tsc, tr7_t head, tr7_t tail);
TR7_EXPORT tr7_t      tr7_reverse_in_place(tr7_t head, tr7_t tail);
TR7_EXPORT int        tr7_get_list_pairs(tr7_t list, int count, tr7_pair_t pairs[]);
TR7_EXPORT int        tr7_get_list_cars(tr7_t list, int count, tr7_t cars[], tr7_t *cdr);

TR7_EXPORT tr7_pair_t tr7_assoc_pair(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t));
TR7_EXPORT tr7_pair_t tr7_assq_pair(tr7_t x, tr7_t list);
TR7_EXPORT tr7_pair_t tr7_assv_pair(tr7_t x, tr7_t list);
TR7_EXPORT tr7_pair_t tr7_asse_pair(tr7_t x, tr7_t list);

TR7_EXPORT tr7_pair_t tr7_member_pair(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t));
TR7_EXPORT tr7_pair_t tr7_memq_pair(tr7_t x, tr7_t list);
TR7_EXPORT tr7_pair_t tr7_memv_pair(tr7_t x, tr7_t list);
TR7_EXPORT tr7_pair_t tr7_meme_pair(tr7_t x, tr7_t list);

TR7_EXPORT tr7_t      tr7_assoc(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t));
TR7_EXPORT tr7_t      tr7_assq(tr7_t x, tr7_t list);
TR7_EXPORT tr7_t      tr7_assv(tr7_t x, tr7_t list);
TR7_EXPORT tr7_t      tr7_asse(tr7_t x, tr7_t list);

TR7_EXPORT tr7_t      tr7_member(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t));
TR7_EXPORT tr7_t      tr7_memq(tr7_t x, tr7_t list);
TR7_EXPORT tr7_t      tr7_memv(tr7_t x, tr7_t list);
TR7_EXPORT tr7_t      tr7_meme(tr7_t x, tr7_t list);


TR7_EXPORT tr7_t tr7_make_bytevector_copy(tr7_engine_t tsc, const uint8_t *bytes, size_t len);
TR7_EXPORT tr7_t tr7_make_bytevector_fill(tr7_engine_t tsc, uint8_t byte, size_t len);
TR7_EXPORT tr7_t tr7_make_bytevector(tr7_engine_t tsc, size_t len);

TR7_EXPORT tr7_t tr7_make_string_fill(tr7_engine_t tsc, wint_t car, size_t ncars);
TR7_EXPORT tr7_t tr7_make_string_take_length(tr7_engine_t tsc, const char *sutf8, size_t length);
TR7_EXPORT tr7_t tr7_make_string_take(tr7_engine_t tsc, const char *sutf8);
TR7_EXPORT tr7_t tr7_make_string_copy_length(tr7_engine_t tsc, const char *sutf8, size_t length);
TR7_EXPORT tr7_t tr7_make_string_copy(tr7_engine_t tsc, const char *sutf8);
TR7_EXPORT tr7_t tr7_make_string_static_length(tr7_engine_t tsc, const char *sutf8, size_t length);
TR7_EXPORT tr7_t tr7_make_string_static(tr7_engine_t tsc, const char *sutf8);
TR7_EXPORT tr7_t tr7_make_string(tr7_engine_t tsc, size_t length);

TR7_EXPORT int    tr7_is_string(tr7_t item);
TR7_EXPORT size_t tr7_string_size(tr7_t string);
TR7_EXPORT size_t tr7_string_length(tr7_t string);
TR7_EXPORT wint_t tr7_string_ref(tr7_t string, size_t pos);
TR7_EXPORT int    tr7_string_set(tr7_engine_t tsc, tr7_t string, size_t pos, wint_t car);
TR7_EXPORT const char *tr7_string_buffer(tr7_t string);

TR7_EXPORT tr7_t tr7_make_symbol_copy_length(tr7_engine_t tsc, const char *sutf8, size_t length);
TR7_EXPORT tr7_t tr7_make_symbol_copy(tr7_engine_t tsc, const char *sutf8);
TR7_EXPORT tr7_t tr7_make_symbol_static_length(tr7_engine_t tsc, const char *sutf8, size_t length);
TR7_EXPORT tr7_t tr7_make_symbol_static(tr7_engine_t tsc, const char *sutf8);
TR7_EXPORT const char *tr7_symbol_string(tr7_t symbol);

TR7_EXPORT tr7_t tr7_get_symbol(tr7_engine_t tsc, const char *name, int copy);
TR7_EXPORT tr7_t tr7_get_symbol_length(tr7_engine_t tsc, const char *name, size_t length, int copy);




TR7_EXPORT tr7_t tr7_make_vector_copy(tr7_engine_t tsc, tr7_t *items, size_t len);
TR7_EXPORT tr7_t tr7_make_vector_fill(tr7_engine_t tsc, tr7_t value, size_t len);
TR7_EXPORT tr7_t tr7_make_vector(tr7_engine_t tsc, size_t len);
TR7_EXPORT tr7_t tr7_vector_ref(tr7_t vec, int ielem);
TR7_EXPORT tr7_t tr7_vector_set(tr7_t vec, int ielem, tr7_t a);





TR7_EXPORT int tr7_is_proc(tr7_t t);

#define TR7ARG_ANY_NUM         001
#define TR7ARG_STRING_NUM      002
#define TR7ARG_SYMBOL_NUM      003
#define TR7ARG_PORT_NUM        004
#define TR7ARG_INPORT_NUM      005
#define TR7ARG_OUTPORT_NUM     006
#define TR7ARG_ENVIRONMENT_NUM 007
#define TR7ARG_PAIR_NUM        010
#define TR7ARG_LIST_NUM        011
#define TR7ARG_CHAR_NUM        012
#define TR7ARG_VECTOR_NUM      013
#define TR7ARG_NUMBER_NUM      014
#define TR7ARG_INTEGER_NUM     015
#define TR7ARG_NATURAL_NUM     016
#define TR7ARG_BYTEVEC_NUM     017
#define TR7ARG_PROC_NUM        020
#define TR7ARG_ERROBJ_NUM      021
#define TR7ARG_BYTE_NUM        022
#define TR7ARG_RECORD_NUM      023
#define TR7ARG_RECORD_DESC_NUM 024

#define _TR7ARG_AUX_(x) #x
#define _TR7ARG_(x) _TR7ARG_AUX_(\x)

#define TR7ARG_ANY             _TR7ARG_(TR7ARG_ANY_NUM)
#define TR7ARG_STRING          _TR7ARG_(TR7ARG_STRING_NUM)
#define TR7ARG_SYMBOL          _TR7ARG_(TR7ARG_SYMBOL_NUM)
#define TR7ARG_PORT            _TR7ARG_(TR7ARG_PORT_NUM)
#define TR7ARG_INPORT          _TR7ARG_(TR7ARG_INPORT_NUM)
#define TR7ARG_OUTPORT         _TR7ARG_(TR7ARG_OUTPORT_NUM)
#define TR7ARG_ENVIRONMENT     _TR7ARG_(TR7ARG_ENVIRONMENT_NUM)
#define TR7ARG_PAIR            _TR7ARG_(TR7ARG_PAIR_NUM)
#define TR7ARG_LIST            _TR7ARG_(TR7ARG_LIST_NUM)
#define TR7ARG_CHAR            _TR7ARG_(TR7ARG_CHAR_NUM)
#define TR7ARG_VECTOR          _TR7ARG_(TR7ARG_VECTOR_NUM)
#define TR7ARG_NUMBER          _TR7ARG_(TR7ARG_NUMBER_NUM)
#define TR7ARG_INTEGER         _TR7ARG_(TR7ARG_INTEGER_NUM)
#define TR7ARG_NATURAL         _TR7ARG_(TR7ARG_NATURAL_NUM)
#define TR7ARG_BYTEVEC         _TR7ARG_(TR7ARG_BYTEVEC_NUM)
#define TR7ARG_PROC            _TR7ARG_(TR7ARG_PROC_NUM)
#define TR7ARG_ERROBJ          _TR7ARG_(TR7ARG_ERROBJ_NUM)
#define TR7ARG_BYTE            _TR7ARG_(TR7ARG_BYTE_NUM)
#define TR7ARG_RECORD          _TR7ARG_(TR7ARG_RECORD_NUM)
#define TR7ARG_RECORD_DESC     _TR7ARG_(TR7ARG_RECORD_DESC_NUM)



/*
* defining functions
*/
typedef struct tr7_C_func_list tr7_C_func_list_t;

struct tr7_C_func_list
{
   const char *name;
   int min_args;
   int max_args;
   const char *typargs;
   tr7_C_func_t func;
   void *closure;
};


TR7_EXPORT void tr7_register_C_func_list(tr7_engine_t tsc, const tr7_C_func_list_t *list, unsigned n);
TR7_EXPORT void tr7_register_C_func(tr7_engine_t tsc, const char *name, int min_args, int max_args, const char *typargs, tr7_C_func_t func, void *closure);

TR7_EXPORT tr7_t tr7_make_C_func(tr7_engine_t tsc, int min_args, int max_args, const char *typargs, tr7_C_func_t func, void *closure);


/*
* numbers
*/

TR7_EXPORT tr7_t tr7_from_int(tr7_engine_t tsc, tr7_int_t value);
TR7_EXPORT tr7_t tr7_from_double(tr7_engine_t tsc, double value);

TR7_EXPORT int tr7_is_number(tr7_t t);
TR7_EXPORT int tr7_is_integer(tr7_t t);
TR7_EXPORT int tr7_is_real(tr7_t t);
TR7_EXPORT int tr7_is_exact(tr7_t t);
TR7_EXPORT int tr7_is_exact_integer(tr7_t t);
TR7_EXPORT int tr7_is_NaN(tr7_t t);
TR7_EXPORT int tr7_is_finite(tr7_t t);
TR7_EXPORT int tr7_is_infinite(tr7_t t);

TR7_EXPORT tr7_int_t tr7_to_int(tr7_t p);
TR7_EXPORT double tr7_to_double(tr7_t p);

/*
* characters
*/
TR7_EXPORT tr7_t tr7_from_character(tr7_engine_t tsc, wint_t value);
TR7_EXPORT wint_t tr7_to_character(tr7_t t);
TR7_EXPORT int tr7_is_character(tr7_t t);

/*
* holding values keep them out of GC
*/
TR7_EXPORT void tr7_hold(tr7_engine_t tsc, tr7_t value);
TR7_EXPORT void tr7_unhold(tr7_engine_t tsc, tr7_t value);


TR7_EXPORT tr7_t tr7_apply0(tr7_engine_t tsc, const char *procname);
TR7_EXPORT tr7_t tr7_call(tr7_engine_t tsc, tr7_t func, tr7_t args);
TR7_EXPORT tr7_t tr7_eval(tr7_engine_t tsc, tr7_t obj);

TR7_EXPORT void tr7_define(tr7_engine_t tsc, tr7_t env, tr7_t symbol, tr7_t value);

TR7_EXPORT int tr7_is_immutable(tr7_t p);
TR7_EXPORT void tr7_set_immutable(tr7_t p);

/*
* This enumeration is for setting strings used internally.
* This strings are:
*
* - Tr7_StrID_Path:            default path and load path
* - Tr7_StrID_Library_Path:    path for libraries
* - Tr7_StrID_Include_Path:    path for includes
* - Tr7_StrID_Extension_Path:  path for extensions
* - Tr7_StrID_Prompt:          prompt string for default REPL
*/
enum tr7_strid
{
   Tr7_StrID_Path,
   Tr7_StrID_Library_Path,
   Tr7_StrID_Include_Path,
   Tr7_StrID_Extension_Path,
   Tr7_StrID_Prompt,
   __Tr7_StrID_Count__   /* DON'T USE EXCEPT FOR COUNTING STRINGS */
};
/*
* A tiny typedef for identifiers of strings
*/
typedef enum tr7_strid tr7_strid_t;
/*
* Function to set the 'value' of the string of id 'strid'
*/
TR7_EXPORT void tr7_set_string(tr7_engine_t tsc, tr7_strid_t strid, const char *value);
/*
* printing to output-port
*/
TR7_EXPORT void tr7_write(tr7_engine_t tsc, tr7_t item);
TR7_EXPORT void tr7_write_simple(tr7_engine_t tsc, tr7_t item);
TR7_EXPORT void tr7_write_shared(tr7_engine_t tsc, tr7_t item);
TR7_EXPORT void tr7_display(tr7_engine_t tsc, tr7_t item);
TR7_EXPORT int tr7_display_string(tr7_engine_t tsc, const char *s);

/*
**************************************************************************
*
*/

#endif
/*
Local variables:
c-file-style: "k&r"
End:
vim: noai ts=3 sw=3 expandtab
*/
