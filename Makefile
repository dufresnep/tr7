# Makefile for TR7

.PHONY: all test chkopt clean updtref

MAJOR = 0
MINOR = 1
VERSION = $(MAJOR).$(MINOR)

PFLAGS = -fno-asynchronous-unwind-tables -fomit-frame-pointer
WFLAGS = -Wall -Wextra -Wno-unused-parameter # -Wno-unused-function
OFLAGS = -g -O0
FFLAGS = -DDUMP_LAMBDAS=1
EFLAGS =
MFLAGS =
SYSLIBS = -lm

LIBSTA = libtr7.a
LIBSO = libtr7.so.$(MAJOR)
TR7I = tr7i

ALL = $(LIBSTA) $(TR7I) tags

ifeq ($(wchar),no)
  MFLAGS += -DNO_WCHAR_T=1
endif
ifeq ($(dynlib),no)
  MFLAGS += -DUSE_DL=0
else
  SYSLIBS += -ldl
endif
ifeq ($(shared),no)
  ALL += $(LIBSO)
endif

CFLAGS := $(CFLAGS) $(PFLAGS) $(WFLAGS) $(OFLAGS) $(FFLAGS) $(EFLAGS) $(MFLAGS)

all: $(ALL)

$(LIBSTA): tr7.sta.o
	$(AR) crs $@ $< 

$(LIBSO): tr7.dyn.o
	$(CC) -shared -o $@ $< $(SYSLIBS) -Wl,-soname,$(LIBSO)

$(TR7I): examples/tr7i.o $(LIBSTA)
	$(CC) $(CFLAGS) -o $@ $^ -Wl,--gc-sections  $(SYSLIBS)

tr7.sta.o: tr7.c tr7.h
	$(CC) $(CFLAGS) -c -o $@ $< -ffunction-sections

tr7.dyn.o: tr7.c tr7.h
	$(CC) $(CFLAGS) -c -o $@ $< -fPIC

examples/tr7i.o: examples/tr7i.c tr7.h
	$(CC) -I. $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(ALL) *.o */*.o tests/*.result

tags: tr7.h tr7.c examples/tr7i.c
	ctags $^

TAGS: tr7.h tr7.c examples/tr7i.c
	etags $^

test: $(TR7I)
	tests/test-tr7.sh

updtref:
	tests/update-references.sh

allopts = \
	$(addprefix USE_SCHEME_C, \
		CASE_LAMBDA \
		CHAR \
		COMPLEX \
		CXR \
		EVAL \
		FILE \
		INEXACT \
		LAZY \
		LOAD \
		PROCESS_CONTEXT \
		READ \
		REPL \
		TIME \
		WRITE \
		) \
	DUMP_LAMBDAS \
	SHOW_ERROR_LINE \
	SHOW_OPCODES \
	USE_ASCII_NAMES \
	USE_DL \
	USE_MATH \
	USE_NO_FEATURES \
	USE_TRACING \
	WITH_SHARP_HOOK \
	AUTO_SHARP_TO_SYMBOL \
	STACKED_GC

chkallopts = $(addsuffix .ck, $(allopts))

chkopt: $(chkallopts)

chkoptflags = -O0
#chkoptflags = -O2 -Wunused-function


%.ck:
	@echo ---------------------------------------------
	@echo ++ COMPILING FOR $(subst .ck,,$@)=0
	$(CC) -c -o tr7.ck.o tr7.c -D$(subst .ck,,$@)=0 $(chkoptflags)
	@echo ++ COMPILING FOR $(subst .ck,,$@)=1
	$(CC) -c -o tr7.ck.o tr7.c -D$(subst .ck,,$@)=1 $(chkoptflags)

