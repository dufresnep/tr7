#!/bin/bash

testdir=$(dirname $0)
cd $testdir

export PATH=..:$PATH
export TR7_LIB_PATH=$(dirname $PWD)

success=true
for t in test-*.scm; do
  echo test $t
  refe=${t%.scm}.reference
  resu=${t%.scm}.result
  tr7i $t >& $resu
  diff $refe $resu || success=false
done
if $success; then
   echo SUCCESS
   exit 0
fi
echo FAILURE
exit 1
