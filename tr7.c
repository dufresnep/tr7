/*************************************************************************
* T R 7    0 . 0 1
*
* tiny R7RS-small scheme interpreter
*
* Far successor of tinyscheme, itself successor of minischeme, itself ...
*
* SPDX-License-Identifier: 0BSD
* https://gitlab.com/jobol/tr7
*
* This is a huge file that contains everything. The good reason for that
* is that you can copy it an use it in any of your project.
* No lib, no dependency, customizable, ... many cool advantages.
* The snag is: this is a huge file.
*
* In order to make browsing of sources more easy, the code is organized
* in sections well identified by comments.
*
* Here is the summary of sections:
*
*   - FEATURING    Handles feature adapation, options of compiling
*   - INCLUDING    Includes
*   - CONSTANTS    Declaration of constants
*   - TYPING       Global main types
*   - DATA         Global variables
*   - DECLARATION  Predeclaration of functions and macros
*   - MEMORY       Memory management
*   - RECENTS      Holding of recent allocations
*   - PAIRS        Management of pairs
*   - LISTS        Management of lists
*   - CHARACTER    Management of characters
*   - IMMUTABLE    Immutable flag for cells
*   - BUFFERS      Management of buffers
*   - BYTEVECTOR   Management of bytevectors
*   - STRING       Management of strings
*   - SYMBOL       Management of symbols
*/
#ifndef ___OPER_
/*************************************************************************
* SECTION FEATURING
* -----------------
*
* The macros below define what R7RS libraries to implement.
* Macros have to be set at compile time using compiling option
* of the form -DMACRONAME=1 or -DMACRONAME=0
*
*  Macro                       Def  Description
* ----------------------------+---+----------------------------------
*  USE_SCHEME_CASE_LAMBDA     : 1 : Implement (scheme case-lambda)
*  USE_SCHEME_CHAR            : 1 : Implement (scheme char)
*  USE_SCHEME_COMPLEX         : 0 : Implement (scheme complex)
*  USE_SCHEME_CXR             : 1 : Implement (scheme cxr)
*  USE_SCHEME_EVAL            : 1 : Implement (scheme eval)
*  USE_SCHEME_FILE            : 1 : Implement (scheme file)
*  USE_SCHEME_INEXACT         : 1 : Implement (scheme inexact)
*  USE_SCHEME_LAZY            : 1 : Implement (scheme lazy)
*  USE_SCHEME_LOAD            : 1 : Implement (scheme load)
*  USE_SCHEME_PROCESS_CONTEXT : 1 : Implement (scheme process-context)
*  USE_SCHEME_READ            : 1 : Implement (scheme read)
*  USE_SCHEME_REPL            : 1 : Implement (scheme repl)
*  USE_SCHEME_TIME            : 1 : Implement (scheme time)
*  USE_SCHEME_WRITE           : 1 : Implement (scheme write)
*  USE_SCHEME_R5RS            : 1 : Implement (scheme r5rs)
*  USE_SRFI_136               : 1 : Implement (srfi 136)
*/
#ifndef USE_SCHEME_CASE_LAMBDA
#define USE_SCHEME_CASE_LAMBDA 1
#endif
#ifndef USE_SCHEME_CHAR
#define USE_SCHEME_CHAR 1
#endif
#ifndef USE_SCHEME_COMPLEX
#define USE_SCHEME_COMPLEX 0
#endif
#ifndef USE_SCHEME_CXR
#define USE_SCHEME_CXR 1
#endif
#ifndef USE_SCHEME_EVAL
#define USE_SCHEME_EVAL 1
#endif
#ifndef USE_SCHEME_FILE
#define USE_SCHEME_FILE 1
#endif
#ifndef USE_SCHEME_INEXACT
#define USE_SCHEME_INEXACT 1
#endif
#ifndef USE_SCHEME_LAZY
#define USE_SCHEME_LAZY 1
#endif
#ifndef USE_SCHEME_LOAD
#define USE_SCHEME_LOAD 1
#endif
#ifndef USE_SCHEME_PROCESS_CONTEXT
#define USE_SCHEME_PROCESS_CONTEXT 1
#endif
#ifndef USE_SCHEME_READ
#define USE_SCHEME_READ 1
#endif
#ifndef USE_SCHEME_REPL
#define USE_SCHEME_REPL 1
#endif
#ifndef USE_SCHEME_TIME
#define USE_SCHEME_TIME 1
#endif
#ifndef USE_SCHEME_WRITE
#define USE_SCHEME_WRITE 1
#endif
#ifndef USE_SCHEME_R5RS
#define USE_SCHEME_R5RS 1
#endif
#ifndef USE_SRFI_136
#define USE_SRFI_136 1
#endif
/*
*
*/
#ifndef DIR_SEP_CHAR
#define DIR_SEP_CHAR   '/'               /* directory separator */
#endif
#ifndef PATH_SEP_CHAR
#define PATH_SEP_CHAR  ':'               /* item separator */
#endif
#ifndef LIB_SEP_CHAR
#define LIB_SEP_CHAR   DIR_SEP_CHAR
#endif
#ifndef BASENAME_SIZE
#define BASENAME_SIZE  200
#endif
/*
* The macros below define tunable features:
*
*  Macro                       Def  Description
* ----------------------------+---+----------------------------------
*  DUMP_LAMBDAS               : 0 : Allows to dump code of lambdas
*  SHOW_ERROR_LINE            : 1 : Allows to show error line in files
*  SHOW_OPCODES               : 1 : Allows to show names of opcodes
*  USE_ASCII_NAMES            : 1 : Allows naming ASCII control codes
*  USE_DL                     : 1 : Allows use of loading shared library modules
*  USE_MATH                   : 1 : Allows use of functions of math.h
*  USE_NO_FEATURES            : 0 : Removes all features (as setting each to 0)
*  USE_TRACING                : 1 : Allows to trace execution (for debugging)
*  WITH_SHARP_HOOK            : 1 : Calls *sharp-hook* for processing unknown sharp expressions
*  AUTO_SHARP_TO_SYMBOL       : 1 : Translate unknown sharp expression to symbol
*/
#ifndef DUMP_LAMBDAS
#define DUMP_LAMBDAS 0
#endif
#ifndef SHOW_ERROR_LINE
#define SHOW_ERROR_LINE 1
#endif
#ifndef SHOW_OPCODES
#define SHOW_OPCODES 1
#endif
#ifndef USE_ASCII_NAMES
#define USE_ASCII_NAMES 1
#endif
#ifndef USE_DL
#define USE_DL 1
#endif
#ifndef USE_MATH
#define USE_MATH 1
#endif
#ifndef USE_TRACING
#define USE_TRACING 1
#endif
#ifndef WITH_SHARP_HOOK
#define WITH_SHARP_HOOK 1
#endif
#ifndef AUTO_SHARP_TO_SYMBOL
#define AUTO_SHARP_TO_SYMBOL 1
#endif
#ifndef USE_RATIOS
#define USE_RATIOS 0
#endif

#if USE_NO_FEATURES
#  undef DUMP_LAMBDAS
#  undef SHOW_ERROR_LINE
#  undef SHOW_OPCODES
#  undef USE_ASCII_NAMES
#  undef USE_DL
#  undef USE_MATH
#  undef USE_TRACING
#endif
/*
* The macros below define constants used internally
*
*  Macro                       Default  Description
* ----------------------------+-------+----------------------------------
*  ITEM_NSEGMENT              :    10 : Maximum count of segments
*  ITEM_SEGSIZE               : 15000 : Count of cell per segment
*  NSEGMENT_INITIAL           :     3 : Initial count of segment
*  NRECENTS                   :    30 : Count of temporary protected cells
*  STRBUFFSIZE                :   256 : Size internal buffer
*  SCRATCH_SIZE               :   256 : Default block size for scratch
*  UNREAD_COUNT               :     5 : Size of unread buffer
*  NOMEM_LEVEL                :    20 : Count of cell raising no_memory
*/
#ifndef ITEM_NSEGMENT
#define ITEM_NSEGMENT   10
#endif
#ifndef ITEM_SEGSIZE
#define ITEM_SEGSIZE    15000
#endif
#ifndef NSEGMENT_INITIAL
#define NSEGMENT_INITIAL 3
#endif
#if NSEGMENT_INITIAL > ITEM_NSEGMENT
#  undef NSEGMENT_INITIAL
#  define NSEGMENT_INITIAL ITEM_NSEGMENT
#endif
#ifndef NRECENTS
#define NRECENTS 30
#endif
#ifndef STRBUFFSIZE
#define STRBUFFSIZE 256
#endif
#ifndef SCRATCH_SIZE
#define SCRATCH_SIZE 256
#endif
#ifndef UNREAD_COUNT
#define UNREAD_COUNT 5
#endif
#ifndef NOMEM_LEVEL
#define NOMEM_LEVEL 20
#endif
/*
* The macros below define tunable code optimizations:
*
*  Macro                       Def  Description
* ----------------------------+---+----------------------------------
*  ASSUME_TAIL_CALL_OPT       : 1 : Allows naming ASCII control codes
*  OPTIMIZE_NO_CALLCC         : 1 : Assumes that call/cc is rare
*  STRESS_GC_RESILIENCE       : 0 : Enforces GC collection at each allocation
*  STACKED_GC                 : 1 : GC use the program stack to mark recursively
*
* ASSUME_TAIL_CALL_OPT avoids returning to the main loop
* of evaluation cycle if possible.
* If your compiler correctly emits tails cut-off or if you
* dont care of the stack size, let it defined to 1.
*/
#ifndef ASSUME_TAIL_CALL_OPT
#define ASSUME_TAIL_CALL_OPT 1
#endif
/*
* Should optimize case out of call/cc?
* Turn it off if you have strange result in using call/cc.
* If it solves the issue, please submit a bug report.
*/
#ifndef OPTIMIZE_NO_CALLCC
# define OPTIMIZE_NO_CALLCC 1
#endif
/*
* Normally that value must be 0.
* When set to one, a garbage collection cycle is run
* each time an allocation is required.
* This is very convenient to track bugs linked to GC.
*/
#ifndef STRESS_GC_RESILIENCE
# define STRESS_GC_RESILIENCE 0
#endif
/*
*/
#ifndef STACKED_GC
# define STACKED_GC 1
#endif
/*************************************************************************
* SECTION INCLUDING
* -----------------
*
* Set the standard, in old one works.
*/
#define _XOPEN_SOURCE 500
/*
* Get the standards headers
*/
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <ctype.h>
#include <time.h>
#ifndef NO_WCHAR_T
#  include <wchar.h>
#  include <wctype.h>
#else
#  define towlower tolower
#  define towupper toupper
#  define fgetwc fgetc
#  define fputwc fputc
#  define _WINT_T 1
#  define wint_t int32_t
#  define WEOF ((wint_t)EOF)
#endif
#if USE_MATH
#  include <math.h>
#endif
#ifdef WIN32
#  define snprintf _snprintf
#else
#  include <unistd.h>
#endif
/*
* get tr7.h
*/
#include "tr7.h"
/*************************************************************************
* SECTION CONSTANTS
* -----------------
*
* this macro defines the length of a buffer receiving utf8 encoding of a char
*/
#define UTF8BUFFSIZE 6
/*************************************************************************
* SECTION TYPING
* --------------
*
* Acces to symbols and operations by name
*
* - OP(code)      expand to the index of the operation of code
* - SY(code)      expand to the index of the symbol of code
*/
#define OP(CODE)   OP_##CODE
#define SY(CODE)   SY_##CODE
/*
*/
#if SHOW_OPCODES
#define OPCODE_PREFIX "OP_"
#endif
/*
* Definition of opidx_t, the operation codes enumeration
*/
#define _BEGIN_LIBRARY_(ID,NAME)
#define _END_LIBRARY_(ID)
#define _SYMBOL_(NAME,CODE)
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  OP(CODE),
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  OP(CODE),
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  OP(CODE),
typedef enum {
#include __FILE__
   OP(MAXDEFINED)
} opidx_t;
/*
* Definition of symidx_t, the symbol codes enumeration
*/
#define _BEGIN_LIBRARY_(ID,NAME)
#define _END_LIBRARY_(ID)
#define _SYMBOL_(NAME,CODE)                   SY(CODE),
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  SY(CODE),
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  SY(CODE),
#if SHOW_OPCODES
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  SY(CODE),
#else
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)
#endif
typedef enum {
#include __FILE__
   SY(MAXDEFINED)
} symidx_t;
/*
* Definition of eval status
* -------------------------
*/
typedef enum eval_status {
   Cycle_Continue,
   Cycle_Goto,
   Cycle_Return,
   Cycle_Error,
   Cycle_Exit
} eval_status_t;
/*
* Definition of ports
* -------------------
*
* First defining port_flag, the flags for handling ports
*/
enum port_flag {
   port_free = 0,         /* the port is neither active nor used */
   port_file = 1,         /* the port is a FILE */
   port_string = 2,       /* the port is for a string */
   port_bytevector = 4,   /* the port is for a bytevector */
   port_scratch = 8,      /* the port is a scratch buffer */
   port_input = 16,       /* the port is for input */
   port_output = 32,      /* the port is for output */
   port_saw_EOF = 64,     /* indicates that EOF was reached */
   port_closeit = 128,    /* indicates that the FILE can be closed */
   port_binary = 256,     /* indicates that the port is binary (not textual) */
   port_ownbuf = 512      /* indicates that the port holds its buffer */
};
/*
* The same structure is used for file io,
* for byte or character in-memory buffers
*/
typedef struct _port_ port_t;
struct _port_ {
   unsigned flags;        /* flags of the port */
#if SHOW_ERROR_LINE
   int line;              /* current line number */
#endif
   union {
      /* regular files */
      struct {
         FILE *file;      /* recorded filename if any */
         char *filename;  /* an utf8 encoded string */
      } stdio;

      /* string/bytearray/scratch */
      struct {
         uint8_t *curr;   /* current position if the stream */
         uint8_t *end;    /* end of byte stream */
         tr7_t    item;   /* handle to string/bytearray for gc */
         uint8_t *start;  /* start of byte stream */
      } inmem;
   } rep;
   struct {
      wint_t stack[UNREAD_COUNT];
      uint8_t count;
   } unread;
};
/*
* Definition of memory segments
* -----------------------------
*
* A memory segment holds a fixed count of cells, the list of free
* cells it has and 2 binary flags per cell.
*/
typedef struct memseg memseg_t;
struct memseg {
   unsigned       count;      /* count of cells in the segment */
   uint32_t      *flags;      /* bit array for flags (mark & final) */
   union tr7_cell cells[];    /* cells of the memory segment
                               * on 32 bits, alignment to 8 bytes boundaries
			       * is ensured if memseg is on 8 bytes itself */
};
/*
* The list of successives free cells.
*/
typedef struct freecells freecells_t;
struct freecells {
   tr7_uint_t     count;      /* count of successive free cells */
   freecells_t   *next;       /* link to next item of the list */
};
/*
* Definition of tr7_engine_t
* --------------------------
*/
struct tr7_engine {
   tr7_t args;               /* register for arguments of function */
   tr7_t envir;              /* stack register for current environment */
   tr7_t oper;               /* temporary operation when returning EXEC */
   tr7_t dump;               /* stack register for next evaluation */
   opidx_t curop;            /* current operation index */
   tr7_t value;              /* returned value */
   tr7_t values;             /* returned values */
#if OPTIMIZE_NO_CALLCC
   tr7_t savestack;          /* saved stack */
#endif
   tr7_t params;             /* head of parameters a-list */
   tr7_t error_desc;         /* record descriptor of error */
   tr7_t read_error_desc;    /* record descriptor of read error */
   tr7_t file_error_desc;    /* record descriptor of file error */
   tr7_t oblist;             /* tr7_t to symbol table */

   /* memory manager */
   memseg_t     *memsegs[ITEM_NSEGMENT];
   unsigned      nmemseg;    /* count of allocatedd memory segments */
   unsigned      nsuccfrees; /* count of successive free cells in head chunk */
   tr7_cell_t    firstfree;  /* head of free cells in head chunk */
   size_t        free_cells; /* # of free items */
   freecells_t  *freeshead;  /* head of list of free cells */
   freecells_t  *freestail;  /* tail of list of free cells */

   /* flags */
   unsigned gc_verbose: 1;   /* if gc_verbose is not zero, print gc status */
   unsigned no_memory: 1;    /* Whether mem. alloc. has failed */
   unsigned no_recent: 1;    /* set to avoid recent recording */
   unsigned tracing: 1;
#if STRESS_GC_RESILIENCE
   unsigned gc_resilience: 1;
#endif

   /* hot items */
   unsigned recent_count;    /* count of recent pushed */
   tr7_t recents[NRECENTS];  /* the recents recorded */

   /* system memory allocator */
   tr7_malloc_t  malloc;
   tr7_free_t    free;

   /* return code */
   int retcode;

   tr7_t global_env;         /* tr7_t to global environment */
   tr7_t c_nest;             /* stack for nested calls from C */
   tr7_t held;               /* list of held values */
   tr7_t features;           /* available features */
   tr7_t libraries;          /* available libraries */

   /* parameter implementation of standard ports */
#define IDX_STDIN  0
#define IDX_STDOUT 1
#define IDX_STDERR 2
   tr7_t stdports[3];

   tr7_t loadport;
   tr7_t loadenv;
   unsigned loadflags;
#define LOADF_SHOW_PROMPT        1
#define LOADF_SHOW_EVAL          2
#define LOADF_SHOW_RESULT        4
#define LOADF_SHOW_LOAD          8
#define LOADF_FOLDCASE          16
#define LOADF_NESTING           32
#define LOADF_INTERACTIVE (LOADF_SHOW_PROMPT | LOADF_SHOW_RESULT | LOADF_SHOW_LOAD)

   const char *strings[__Tr7_StrID_Count__];
   tr7_t datums;
   struct {
      unsigned length;
      unsigned size;
      char *head;
      char buffer[STRBUFFSIZE];
   } strbuff;
};
/*
* definition of a operator function
*/
typedef eval_status_t (*tr7_opfunc_t)(tr7_engine_t);
/*
* definition of operation description
*/
typedef struct {
   unsigned check:1;
   unsigned named:1;
   unsigned syntax:1;
   unsigned min_arity:5;
   unsigned max_arity:13;
#define INF_ARG 0x1fff
   unsigned hassymbol:1;
   unsigned symbolidx:9;
   char *arg_tests_encoding;
   tr7_opfunc_t func;
} opdesc_t;
/*
* definition a predefined libraries
*/
typedef struct {
   const char *name;
   const opdesc_t *head;
   unsigned count;
} libdef_t;
/*
**************************************************************************
* SECTION DATA
* -------------
*
* table of predefined symbols
*/
#define STRINGBUF(kind, string) {\
            .head = TR7_MAKE_HEAD(sizeof(string)-1,kind), \
            .content = (uint8_t*)string }

#define PRDFSYM(symbol)  STRINGBUF(TR7_HEAD_KIND_SYMBOL, symbol)

#define _BEGIN_LIBRARY_(ID,NAME)
#define _END_LIBRARY_(ID)
#define _SYMBOL_(NAME,CODE)                   PRDFSYM(NAME),
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  PRDFSYM(NAME),
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  PRDFSYM(NAME),
#if SHOW_OPCODES
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  PRDFSYM(OPCODE_PREFIX #CODE),
#else
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)
#endif

static const struct tr7_buffer predefined_symbols[] = {
#include __FILE__
};
/*
* helpers for accessing predefined symbols:
*
* - SYMBOL_AT(index)  return the tr7_t value for the symbol at index
* - SYMBOL(code)      return the tr7_t value for the symbol of code
*/
#define SYMBOL_AT(IDX)    TR7_FROM_SYMBOL(&predefined_symbols[IDX])
#define SYMBOL(CODE)      SYMBOL_AT(SY(CODE))
/*
* table/structure of predefined operations
*
* The definition is made in 3 steps:
*
* - declare the operator functions
* - declare structed array of operators
* - initialize the structured array
*/
#define _BEGIN_LIBRARY_(ID,NAME)
#define _END_LIBRARY_(ID)
#define _SYMBOL_(NAME,CODE)
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  static eval_status_t FUNC(tr7_engine_t);
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  static eval_status_t FUNC(tr7_engine_t);
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  static eval_status_t FUNC(tr7_engine_t);
#include __FILE__

static const struct {

#define _BEGIN_LIBRARY_(ID,NAME) struct {
#define _END_LIBRARY_(ID)  } lib_##ID;
#define _SYMBOL_(NAME,CODE)
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  const opdesc_t _##CODE##_;
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  const opdesc_t _##CODE##_;
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  const opdesc_t _##CODE##_;
#include __FILE__

} operations = {

#define _BEGIN_LIBRARY_(ID,NAME) {
#define _END_LIBRARY_(ID)  },
#define _SYMBOL_(NAME,CODE)
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)  {0,1,1,MIN,MAX,1,SY(CODE),TYP,FUNC},
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)  {1,1,0,MIN,MAX,1,SY(CODE),TYP,FUNC},
#if SHOW_OPCODES
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  {0,0,0,MIN,MAX,1,SY(CODE),TYP,FUNC},
#else
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)  {0,0,0,MIN,MAX,0,0,TYP,FUNC},
#endif
#include __FILE__

};
/*
* table of builtin libraries
*/
static const libdef_t builtin_libs[] = {

#define _BEGIN_LIBRARY_(ID,NAME) { NAME, (const opdesc_t *)&operations.lib_##ID, (unsigned)(sizeof operations.lib_##ID / sizeof(opdesc_t)) },
#define _END_LIBRARY_(ID)
#define _SYMBOL_(NAME,CODE)
#define _SYNTAX_(FUNC,NAME,MIN,MAX,TYP,CODE)
#define _PROC___(FUNC,NAME,MIN,MAX,TYP,CODE)
#define ___OPER_(FUNC,NAME,MIN,MAX,TYP,CODE)
#include __FILE__

};
/*
* for (scheme r5rs)
*/
#if USE_SCHEME_R5RS
static const char deflib_r5rs[] =
"(define-library (scheme r5rs)"
 " (import (only (scheme base)"
#if USE_RATIOS
    " denominator numerator rationalize"
#endif
    " < <= = > >= - / * + abs and append apply assoc assq assv begin boolean?"
    " caar cadr call-with-current-continuation call-with-values car case cdar"
    " cddr cdr ceiling char<=? char<? char=? char>=? char>? char? char->integer"
    " char-ready? close-input-port close-output-port complex? cond cons"
    " current-input-port current-output-port define define-syntax"
    " do dynamic-wind eof-object? eq? equal? eqv? even? exact exact? expt floor"
    " for-each gcd if inexact inexact? input-port? integer? integer->char lambda"
    " lcm length let let* letrec letrec-syntax let-syntax list list? list-ref"
    " list->string list-tail list->vector make-string make-vector map max member"
    " memq memv min modulo negative? newline not null? number? number->string"
    " odd? or output-port? pair? peek-char positive? procedure?"
    " quasiquote quote quotient rational? read-char real? remainder"
    " reverse round set! set-car! set-cdr! string string<=? string<? string=?"
    " string>=? string>? string? string-append string-copy string-fill!"
    " string-length string->list string->number string-ref string-set!"
    " string->symbol substring symbol? symbol->string truncate values vector"
    " vector? vector-fill! vector-length vector->list vector-ref vector-set!"
    " write-char zero?))"
  " (export"
#if USE_RATIOS
    " denominator numerator rationalize"
#endif
    " < <= = > >= - / * + abs and append apply assoc assq assv begin boolean?"
    " caar cadr call-with-current-continuation call-with-values car case cdar"
    " cddr cdr ceiling char<=? char<? char=? char>=? char>? char? char->integer"
    " char-ready? close-input-port close-output-port complex? cond cons"
    " current-input-port current-output-port define define-syntax"
    " do dynamic-wind eof-object? eq? equal? eqv? even? (rename exact inexact->exact) exact? expt floor"
    " for-each gcd if (rename inexact exact->inexact) inexact? input-port? integer? integer->char lambda"
    " lcm length let let* letrec letrec-syntax let-syntax list list? list-ref"
    " list->string list-tail list->vector make-string make-vector map max member"
    " memq memv min modulo negative? newline not null? number? number->string"
    " odd? or output-port? pair? peek-char positive? procedure?"
    " quasiquote quote quotient rational? read-char real? remainder"
    " reverse round set! set-car! set-cdr! string string<=? string<? string=?"
    " string>=? string>? string? string-append string-copy string-fill!"
    " string-length string->list string->number string-ref string-set!"
    " string->symbol substring symbol? symbol->string truncate values vector"
    " vector? vector-fill! vector-length vector->list vector-ref vector-set!"
    " write-char zero?)"
#if USE_SCHEME_CHAR
 " (import (only (scheme char)"
    " char-alphabetic? char-ci<=? char-ci<? char-ci=? char-ci>=? char-ci>?"
    " char-downcase char-lower-case? char-numeric? char-upcase char-upper-case?"
    " char-whitespace? string-ci<=? string-ci<? string-ci=? string-ci>=? string-ci>?))"
  " (export"
    " char-alphabetic? char-ci<=? char-ci<? char-ci=? char-ci>=? char-ci>?"
    " char-downcase char-lower-case? char-numeric? char-upcase char-upper-case?"
    " char-whitespace? string-ci<=? string-ci<? string-ci=? string-ci>=? string-ci>?)"
#endif
#if USE_SCHEME_COMPLEX
/*
 " (import (scheme complex))"
  " (export"
    " angle imag-part magnitude make-polar make-rectangular real-part)"
*/
#endif
#if USE_SCHEME_CXR
 " (import (scheme cxr))"
  " (export"
    " caaaar caaadr caaar caadar caaddr caadr cadaar cadadr cadar caddar cadddr caddr"
    " cdaaar cdaadr cdaar cdadar cdaddr cdadr cddaar cddadr cddar cdddar cddddr cdddr)"
#endif
#if USE_SCHEME_EVAL
 " (import (only (scheme eval) eval))"
  " (export"
    " eval)"
#endif
#if USE_SCHEME_FILE
 " (import (only (scheme file)"
    " call-with-input-file call-with-output-file open-input-file open-output-file"
    " with-input-from-file with-output-to-file))"
  " (export"
    " call-with-input-file call-with-output-file open-input-file open-output-file"
    " with-input-from-file with-output-to-file)"
#endif
#if USE_SCHEME_INEXACT
 " (import (only (scheme inexact)"
    " acos asin atan cos exp log sin sqrt tan))"
  " (export"
    " acos asin atan cos exp log sin sqrt tan)"
#endif
#if USE_SCHEME_LAZY
 " (import (only (scheme lazy)"
    " delay force))"
  " (export"
    " delay force)"
#endif
#if USE_SCHEME_LOAD
 " (import (scheme load))"
  " (export"
    " load)"
#endif
#if USE_SCHEME_READ
 " (import (scheme read))"
  " (export"
    " read)"
#endif
#if USE_SCHEME_REPL
 " (import (scheme repl))"
  " (export"
    " interaction-environment)"
#endif
#if USE_SCHEME_WRITE
 " (import (only (scheme write)"
    " display write))"
  " (export"
    " display write)"
#endif
/*
 " (import (tr7 environment))"
  " (export"
    " null-environment scheme-report-environment)"
*/
")";
#endif

/*
**************************************************************************
* SECTION DECLARATION
* -------------------
*
* The macro below are used for computing the count of cell to be
* allocated:
*
* NCELL_OF_SIZE(size): returns the count of cells needed for a given size in bytes
* NCELL_OF_TYPE(type): returns the count of cells needed for a given C type
* NCELL_OF_PTR(ptr):   returns the count of cells needed for data pointed by ptr
*/
#define NCELL_OF_SIZE(size)   (((size) + sizeof(union tr7_cell) - 1) / sizeof(union tr7_cell))
#define NCELL_OF_TYPE(type)   NCELL_OF_SIZE(sizeof(type))
#define NCELL_OF_PTR(ptr)     NCELL_OF_SIZE(sizeof(*(ptr)))
/*
* FREECELL_MIN_SUCC:   minimum count of successive pair when recording free cells
*/
#define FREECELL_MIN_SUCC     NCELL_OF_TYPE(freecells_t)
/*
* This macro is a helper for allocating cells for a typed pointer ptr
*/
#define GET_CELLS(tsc,ptr,final) get_cells(tsc,NCELL_OF_PTR(ptr),final)

static void log_str(tr7_engine_t tsc, const char *string);
static void log_item(tr7_engine_t tsc, tr7_t item);
static void log_item_string(tr7_engine_t tsc, tr7_t item);

static void finalize_buffer(tr7_engine_t tsc, tr7_cell_t a);
static void finalize_port(tr7_engine_t tsc, tr7_cell_t a);

static eval_status_t s_return_single(tr7_engine_t tsc, tr7_t a);
static int port_write_utf8_length(tr7_engine_t tsc, port_t *pt, const char *s, unsigned len);
static tr7_t parameter_get(tr7_engine_t tsc, tr7_t param);
static void parameter_set(tr7_engine_t tsc, tr7_t param, tr7_t value);
static void parameter_push(tr7_engine_t tsc, tr7_t param, tr7_t value);
static void print_item(tr7_engine_t tsc, port_t *pt, int pflags, tr7_t item, tr7_t anchors);
static eval_status_t do_raise(tr7_engine_t tsc, tr7_t errobj, int continuable);
static eval_status_t do_exec2(tr7_engine_t tsc, tr7_t oper, tr7_t args);
static eval_status_t do_begin(tr7_engine_t tsc, tr7_t exprs);
static eval_status_t do_eval(tr7_engine_t tsc, tr7_t expr);
static int eval_cond_expand_cond(tr7_engine_t tsc, tr7_t cond);
static eval_status_t do_read_with_token(tr7_engine_t tsc, port_t *pt, int funq, int tok);
static int check_args(tr7_engine_t tsc, tr7_t arglist, int min_args, int max_args, const char *tststr, tr7_t argv[TR7_FF_NARGS_MAXIMUM]);
static eval_status_t do_define_library_inner(tr7_engine_t tsc, tr7_t libexp, tr7_t aux);
static eval_status_t do_read_all_with_datum(tr7_engine_t tsc, port_t *pt);

/*
**************************************************************************
* SECTION MEMORY
* --------------
*
* allocate 'len' bytes
*/
static void *memalloc(tr7_engine_t tsc, size_t len)
{
   return tsc->malloc(len);
}
/*
* allocates 'alen' bytes and copy to it 'clen' first bytes of 'buf' (alen >= clen)
*/
static void *memalloc_copy_stringz(tr7_engine_t tsc, const void *buf, size_t len)
{
   void *mem = memalloc(tsc, len + 1);
   if (mem) {
      memcpy(mem, buf, len);
      ((char*)mem)[len] = 0;
   }
   return mem;
}
/*
* allocates 'len' bytes and copy to it len bytes of 'buf'
*/
static void *memalloc_copy(tr7_engine_t tsc, const void *buf, size_t len)
{
   void *mem = memalloc(tsc, len);
   if (mem)
      memcpy(mem, buf, len);
   return mem;
}
/*
* allocates 'len' bytes and fill it with byte
*/
static void *memalloc_fill(tr7_engine_t tsc, uint8_t fill, size_t len)
{
   void *mem = memalloc(tsc, len);
   if (mem)
      memset(mem, fill, len);
   return mem;
}
/*
* free the memory pointed by 'ptr'
*/
static void memfree(tr7_engine_t tsc, void *ptr)
{
   tsc->free(ptr);
}
/*
*  queue in free list 'count' successive cells starting at 'head'
*/
static unsigned freecells_queue(tr7_engine_t tsc, tr7_cell_t head, unsigned count)
{
   freecells_t *fc = (freecells_t*)head;
   if (count < FREECELL_MIN_SUCC)
      return 0;
   if (tsc->freestail)
      tsc->freestail->next = fc;
   else
      tsc->freeshead = fc;
   tsc->freestail = fc;
   fc->next = NULL;
   return fc->count = count;
}
/*
* dequeue the head of free list
*/
static void freecells_dequeue(tr7_engine_t tsc)
{
   freecells_t *fc = tsc->freeshead;
   if (!fc)
      tsc->nsuccfrees = 0;
   else {
      tsc->nsuccfrees = fc->count;
      tsc->firstfree = (tr7_cell_t)fc;
      tsc->freeshead = fc = fc->next;
      if (!fc)
         tsc->freestail = fc;
   }
}
/*
* searchs the memseg containing the cell 'c', return it or 0 when not found
*/
static memseg_t *search_memseg(tr7_engine_t tsc, tr7_cell_t c)
{
   unsigned low = 0, up = tsc->nmemseg;
   while (low < up) {
      unsigned iseg = (low + up) >> 1;
      memseg_t *ms = tsc->memsegs[iseg];
      if ((void*)ms->cells > (void*)c)
         up = iseg;
      else if ((void*)c >= (void*)ms->flags)
         low = iseg + 1;
      else
         return ms;
   }
   return NULL; /* not found */
}
/*
* mark as used the 'count' successives cells starting at 'icell'
* returns 1 if already marked
*/
static int markmemseg(memseg_t *ms, unsigned icell, unsigned count)
{
   unsigned imark = (icell >> 5) << 1;
   unsigned ibit  = icell & 31;
   uint32_t bits  = ((uint32_t) 1) << ibit;

   if (ms->flags[imark] & bits)
      return 1;

#if INTPTR_MAX == INT32_MAX
   count += count & 1; /* ensure oddity */
#endif

   if (ibit) {
      unsigned nbits = 32 - ibit;
      if (nbits >= count) {
         bits = (bits << count) - bits;
         ms->flags[imark] |= bits;
         return 0;
      }
      ms->flags[imark] |= -bits;
      imark += 2;
      count -= nbits;
   }
   while(count >= 32) {
      ms->flags[imark] = UINT32_MAX;
      imark += 2;
      count -= 32;
   }
   if (count) {
      bits = (((uint32_t) 1) << count) - 1;
      ms->flags[imark] |= bits;
   }
   return 0;
}
/*
* mark as used the 'count' successives cells starting at 'head'
* returns 1 if already marked
*/
static int setmarked(tr7_engine_t tsc, tr7_cell_t head, unsigned count)
{
   memseg_t *ms = search_memseg(tsc, head);
   return ms ? markmemseg(ms, (unsigned)(head - ms->cells), count) : 0;
}
#if STACKED_GC
/*
* This GC mark algorithm use the program stack
*/
static void mark(tr7_engine_t tsc, tr7_t current)
{
   tr7_cell_t cell;
   tr7_pair_t pair;
   unsigned extend; /* full extend in count of cell of a cell */
   unsigned exmark; /* extend of the cell after head to be marked */
   unsigned idx;

next:
   /* special NIL case */
   if (TR7_IS_VOID(current))
      return;

   /* inspect what to mark */
   switch (TR7_KIND(current)) {
   case TR7_KIND_PAIR:
      goto mark_pair;

   case TR7_KIND_CELL:
      goto mark_cell;

   case TR7_KIND_DOUBLE:
      setmarked(tsc, (tr7_cell_t)TR7_TO_DOUBLE(current), NCELL_OF_TYPE(double));
      return;

   default:
      return;
   }

mark_pair:
   /* mark the pair */
   pair = TR7_TO_PAIR(current);
   if (setmarked(tsc, (tr7_cell_t) pair, NCELL_OF_PTR(pair)))
      return;

   mark(tsc, pair->car);
   current = pair->cdr;
   goto next;

mark_cell:
   /* mark a cell */
   cell = TR7_TO_CELL(current);
   switch (TR7_CELL_KIND(cell)) {
   case TR7_HEAD_KIND_RATIONAL:
   case TR7_HEAD_KIND_COMPLEX:
   default:
      /* should not happen! */
      /* ns of 0 */
      return;

   case TR7_HEAD_KIND_PORT:
      if (TR7_CELL_PORT__PORT_(cell)->flags & (port_string | port_bytevector))
         mark(tsc, TR7_CELL_PORT__PORT_(cell)->rep.inmem.item);
      /*@fallthrough@*/

   case TR7_HEAD_KIND_STRING:
   case TR7_HEAD_KIND_SYMBOL:
   case TR7_HEAD_KIND_BYTEVECTOR:
      exmark = 0;
      extend = 2;
      goto mark_cell_next;

   case TR7_HEAD_KIND_FOREIGN:
      exmark = 0;
      extend = 4;
      goto mark_cell_next;

   case TR7_HEAD_KIND_CONTINUATION:
      exmark = 1;
      break;

   case TR7_HEAD_KIND_MACRO:
   case TR7_HEAD_KIND_LAMBDA:
   case TR7_HEAD_KIND_CASE_LAMBDA:
   case TR7_HEAD_KIND_PROMISE:
   case TR7_HEAD_KIND_PARAMETER:
   case TR7_HEAD_KIND_PARAMSAVE:
      exmark = 2;
      break;

   case TR7_HEAD_KIND_GUARD:
   case TR7_HEAD_KIND_STACK:
      exmark = 3;
      break;

   case TR7_HEAD_KIND_TRANSFORM:
      exmark = 4;
      break;

   case TR7_HEAD_KIND_DYNAWIND:
      exmark = 5;
      break;

   case TR7_HEAD_KIND_ENVIRONMENT:
      exmark = 1 + TR7_HEAD_VALUE(cell->head);
      break;

   case TR7_HEAD_KIND_VECTOR:
      exmark = TR7_CELL_VECTOR_LENGTH(cell);
      break;

   case TR7_HEAD_KIND_RECORD:
      exmark = TR7_CELL_RECORD_LENGTH(cell);
      break;
   }
   extend = 1 + exmark;

mark_cell_next:
   if (setmarked(tsc, cell, extend))
      return;
   if (!exmark)
      return;
   current = ((tr7_vector_t)cell)->items[0];
   for(idx = 1 ; idx < exmark ; idx++)
      mark(tsc, ((tr7_vector_t)cell)->items[idx]);
   goto next;
}
#else
/*
* mark as used the 'count' successives cells starting at 'head'
* returns 1 if already marked
*/
static int ismarked(tr7_engine_t tsc, tr7_cell_t head, memseg_t **pms, unsigned *picell)
{
   memseg_t *ms = *pms = search_memseg(tsc, head);
   if (!ms)
      return 0;
   unsigned icell = *picell = (unsigned) (head - ms->cells);
   unsigned imark = (icell >> 5) << 1;
   unsigned ibit  = icell & 31;
   uint32_t bits  = ((uint32_t) 1) << ibit;
   return ms->flags[imark] & bits;
}
/*
*  We use algorithm E (Knuth, The Art of Computer Programming Vol.1,
*  sec. 2.3.5), the Schorr-Deutsch-Waite link-inversion algorithm,
*  for marking.
*  Note that for cells other than vector, the vector structure is used
*  to access the items of the cell and flag is used to record where
*  backchaining is done.
*/
static void mark(tr7_engine_t tsc, tr7_t o)
{
   tr7_cell_t cell;
   tr7_pair_t pair;
   tr7_t current;
   tr7_t upper;
   tr7_t tmp;
   memseg_t *ms;
   unsigned icell = 0;
   unsigned extend; /* full extend in count of cell of a cell */
   unsigned exmark; /* extend of the cell after head to be marked */

   current = o;
   upper = TR7_NIL;

mark_a:
   /* special NIL case */
   if (TR7_IS_VOID(current))
      goto mark_next;

   /* inspect what to mark */
   switch (TR7_KIND(current)) {
   case TR7_KIND_PAIR:
      goto mark_pair;

   case TR7_KIND_CELL:
      goto mark_cell;

   case TR7_KIND_DOUBLE:
      setmarked(tsc, (tr7_cell_t)TR7_TO_DOUBLE(current), NCELL_OF_TYPE(double));
      goto mark_next;

   case TR7_KIND_SPECIAL:
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
   default:
      break;
   }
   goto mark_next;

mark_pair:
   /* mark the pair */
   pair = TR7_TO_PAIR(current);
   if (setmarked(tsc, (tr7_cell_t) pair, NCELL_OF_PTR(pair)))
      goto mark_next;

   /* down car */
   tmp = pair->car;
   pair->car = upper;
   upper = I2TR7(TR72I(current) ^ TR7_MASK_PTR);  /* remove the PTR mark to tell it's CAR */
   current = tmp;
   goto mark_a;

mark_cell:
   /* mark a cell */
   cell = TR7_TO_CELL(current);
   if (ismarked(tsc, cell, &ms, &icell))
      goto mark_next; /* already marked */

   switch (TR7_CELL_KIND(cell)) {
   case TR7_HEAD_KIND_RATIONAL:
   case TR7_HEAD_KIND_COMPLEX:
   default:
      /* should not happen! */
      /* ns of 0 */
      goto mark_next;

   case TR7_HEAD_KIND_PORT:
      if (TR7_CELL_PORT__PORT_(cell)->flags & (port_string | port_bytevector))
         mark(tsc, TR7_CELL_PORT__PORT_(cell)->rep.inmem.item);
      /*@fallthrough@*/

   case TR7_HEAD_KIND_STRING:
   case TR7_HEAD_KIND_SYMBOL:
   case TR7_HEAD_KIND_BYTEVECTOR:
      exmark = 0;
      extend = 2;
      goto mark_cell_next;

   case TR7_HEAD_KIND_FOREIGN:
      exmark = 0;
      extend = 4;
      goto mark_cell_next;

   case TR7_HEAD_KIND_CONTINUATION:
      exmark = 1;
      break;

   case TR7_HEAD_KIND_MACRO:
   case TR7_HEAD_KIND_LAMBDA:
   case TR7_HEAD_KIND_CASE_LAMBDA:
   case TR7_HEAD_KIND_PROMISE:
   case TR7_HEAD_KIND_PARAMETER:
   case TR7_HEAD_KIND_PARAMSAVE:
      exmark = 2;
      break;

   case TR7_HEAD_KIND_GUARD:
   case TR7_HEAD_KIND_STACK:
      exmark = 3;
      break;

   case TR7_HEAD_KIND_TRANSFORM:
      exmark = 4;
      break;

   case TR7_HEAD_KIND_DYNAWIND:
      exmark = 5;
      break;

   case TR7_HEAD_KIND_ENVIRONMENT:
      exmark = 1 + TR7_HEAD_VALUE(cell->head);
      break;

   case TR7_HEAD_KIND_VECTOR:
      exmark = TR7_CELL_VECTOR_LENGTH(cell);
      break;

   case TR7_HEAD_KIND_RECORD:
      exmark = TR7_CELL_RECORD_LENGTH(cell);
      break;
   }
   extend = 1 + exmark;

mark_cell_next:
   if (ms)
      markmemseg(ms, icell, extend);
   if (exmark == 0)
      goto mark_next;

   /* be partially recursive if too much items */
   while (exmark > TR7_MAX_HEAD_MARKIDX + 1)
      mark(tsc, ((tr7_vector_t)cell)->items[--exmark]);

   /* backtrack algo E */
   exmark--;
   TR7_CELL_HEAD(cell) = TR7_HEAD_MARKIDX_SET(TR7_CELL_HEAD(cell), exmark);
   tmp = ((tr7_vector_t)cell)->items[exmark];
   ((tr7_vector_t)cell)->items[exmark] = upper;
   upper = current;
   current = tmp;
   goto mark_a;

mark_next:                    /* up.  Undo the link switching from steps E4 and E5. */
   if (TR7_IS_NIL(upper))
      return; /* terminated */

   if (!TR7_IS_PTR(upper)) {
      /* restore car and down cdr */
      upper = I2TR7(TR72I(upper) ^ TR7_MASK_PTR); /* restore the PTR mark */
      pair = TR7_TO_PAIR(upper);
      /* stored upper moves from car to cdr while current is set to cdr */
      tmp = pair->car;
      pair->car = current;
      current = pair->cdr;
      pair->cdr = tmp;
      /* mark the cdr */
      goto mark_a;
   }

   if (TR7_IS_PAIR(upper)) {
      pair = TR7_TO_PAIR(upper);
      /* restore cdr */
      tmp = pair->cdr;
      pair->cdr = current;
      /* move upper */
      current = upper;
      upper = tmp;
      /* upper continue to mark */
      goto mark_next;
   }

   /* for sure it is a cell */
   cell = TR7_TO_CELL(upper);
   /* restore the item for current */
   exmark = TR7_HEAD_MARKIDX_GET(TR7_CELL_HEAD(cell));
   tmp = ((tr7_vector_t)cell)->items[exmark];
   ((tr7_vector_t)cell)->items[exmark] = current;
   if (exmark == 0) {
      /* move upper */
      current = upper;
      upper = tmp;
      /* upper continue to mark */
      goto mark_next;
   }

   /* enter the previous sibling in the cell */
   exmark--;
   TR7_CELL_HEAD(cell) = TR7_HEAD_MARKIDX_SET(TR7_CELL_HEAD(cell), exmark);
   current = ((tr7_vector_t)cell)->items[exmark];
   ((tr7_vector_t)cell)->items[exmark] = tmp;
   /* mark sibling */
   goto mark_a;
}
#endif
/*
*
*/
static void gc_prepare(tr7_engine_t tsc)
{
   if (tsc->gc_verbose)
      log_str(tsc, "gc...");
}
/*
*
*/
static void gc_collect(tr7_engine_t tsc)
{
   memseg_t *ms;
   tr7_cell_t c, hc;
   unsigned i, j, ji, n, nfc;
   uint32_t wm, jm, wf;
   long prev;

   /* garbage collect */
   prev = tsc->free_cells;
   tsc->free_cells = 0;
   /* free-list is kept sorted by address so as to maintain consecutive
      ranges, if possible, for use with vectors. Here we scan the cells
      (which are also kept sorted by address) to build the free-list.  */
   tsc->freeshead = tsc->freestail = NULL;
   i = tsc->nmemseg;
   while (i > 0) {
      ms = tsc->memsegs[--i];
      c = hc = ms->cells;
      for (j = n = nfc = ji = 0 ; j < ms->count ; ji += 2) {
         wm = ms->flags[ji];
         ms->flags[ji] = 0;/*reset the mark now*/
         if (!~wm) {/* fully hmarked */
            if (nfc)
               n += freecells_queue(tsc, hc, nfc);
            nfc = 0;
            j += 32;
            c += 32;
         }
         else {
            wf = ms->flags[ji + 1];
            if (!(wm | wf)) {/*nomark, no finalizer*/
               if (!nfc)
                  hc = c;
               nfc += 32;
               c += 32;
               j += 32;
            }
            else {
               for (jm = 1 ; jm && j < ms->count ; j++, jm <<= 1, c++) {
                  if (jm & wm) {/*marked*/
                     if (nfc)
                        n += freecells_queue(tsc, hc, nfc);
                     nfc = 0;
                  }
                  else {/*not marked*/
                     if (!nfc++)
                        hc = c;
                     if (wf & jm) {/*finalizer*/
                        wf ^= jm;
                        /* add cases here for finalizing */
                        switch (TR7_CELL_KIND(c)) {
                        case TR7_HEAD_KIND_STRING:
                        case TR7_HEAD_KIND_SYMBOL:
                        case TR7_HEAD_KIND_BYTEVECTOR:
                           finalize_buffer(tsc, c);
                           break;
                        case TR7_HEAD_KIND_PORT:
                           finalize_port(tsc, c);
                           break;
                        default:
                           break;
                        }
                     }
                  }
               }
               /* update final flags */
               ms->flags[ji + 1] = wf;
            }
         }
      }
      if (nfc)
         n += freecells_queue(tsc, hc, nfc);
      tsc->free_cells += n;
   }
   freecells_dequeue(tsc);

   /* log if required */
   if (tsc->gc_verbose) {
#if 0
      char msg[80];
      snprintf(msg, sizeof msg, "done: %ld items were recovered (%ld).\n", (tsc->free_cells - prev), (long) ((sizeof(union tr7_cell)) * (tsc->free_cells - prev))
          );
#else
      long count = ITEM_SEGSIZE;
      char msg[120];
      if (count & 31) count += 32 - (count & 31);
      snprintf(msg, sizeof msg, "done: %ld items were recovered (%ld), %ld free now (%ld/%ld).\n",
               (long) (tsc->free_cells - prev),
               (long) ((sizeof(union tr7_cell)) * (tsc->free_cells - prev)),
               (long) tsc->free_cells,
	       (long) (tsc->free_cells * (long) sizeof(union tr7_cell)),
	       (long) (tsc->nmemseg * (long) sizeof(union tr7_cell) * count)
          );
#endif
      log_str(tsc, msg);
   }
}
/*
* allocate 'n' new memory segments
*/
static int memseg_multi_alloc(tr7_engine_t tsc, unsigned n, unsigned count)
{
   size_t nru32, size, szflags;
   unsigned i, k;

   /* adjust count to align of 32 */
   if (count & 31)
      count += 32 - (count & 31);

   /* compute sizes */
   nru32 = count >> 5;   /* count / 32) */
   szflags = nru32 << (2 + 1);    /* 2 * nru32 * sizeof(uint32_t) */
   size = sizeof(memseg_t) + szflags + count * sizeof(union tr7_cell);

   /* iterated allocation */
   for (k = 0; tsc->nmemseg < ITEM_NSEGMENT && k < n; k++) {
      /* allocation */
      memseg_t *ms = memalloc(tsc, size);
      if (ms == NULL)
         break;

      /* init fields */
      ms->count = count;
      ms->flags = (uint32_t *)(&ms->cells[count]);
      memset(ms->flags, 0, szflags);

      /* keep segments sorted */
      i = tsc->nmemseg++;
      while (i > 0 && tsc->memsegs[i - 1] > ms) {
         tsc->memsegs[i] = tsc->memsegs[i - 1];
         i = i - 1;
      }
      tsc->memsegs[i] = ms;

      /* make it available */
      tsc->free_cells += ms->count;
      freecells_queue(tsc, ms->cells, ms->count);
   }
   return k;
}
/*
* find 'n' consecutive cells and set its head final flag if 'final' is not nul
* returns the head found or 0
*/
static tr7_cell_t find_consecutive_cells(tr7_engine_t tsc, unsigned n, int final)
{
   memseg_t *ms;
   tr7_cell_t res;
   freecells_t **pfc, *fc;
   unsigned rnf; /* remaining number of successive free cells */
   unsigned i;

   if (tsc->nsuccfrees >= n) {
      res = tsc->firstfree;
      rnf = tsc->nsuccfrees - n;
   }
   else {
      pfc = &tsc->freeshead;
      while ((fc = *pfc) && fc->count < n)
         pfc = &fc->next;
      if (!fc)
         return NULL;
      freecells_queue(tsc, tsc->firstfree, tsc->nsuccfrees);
      rnf = (unsigned)fc->count - n;
      *pfc = fc->next;
      res = (tr7_cell_t)fc;
   }
   if (rnf < FREECELL_MIN_SUCC)
      freecells_dequeue(tsc);
   else {
      tsc->nsuccfrees = rnf;
      tsc->firstfree = &res[n];
   }
   tsc->free_cells -= n;
   if (final) {
      ms = search_memseg(tsc, res);
      i = (unsigned) (res - ms->cells);
      ms->flags[((i >> 5) << 1) + 1] |= ((uint32_t) 1) << (i & 31);
   }
   return res;
}
/*
* garbage collection. parameter a is marked.
*/
static void collect_garbage(tr7_engine_t tsc)
{
   unsigned i;

   /* prepare marking */
   gc_prepare(tsc);

   /* mark system globals */
   mark(tsc, tsc->oblist);
   mark(tsc, tsc->global_env);
   mark(tsc, tsc->libraries);

   /* mark current registers */
#if OPTIMIZE_NO_CALLCC
   tsc->savestack = TR7_NIL;
#endif
   mark(tsc, tsc->args);
   mark(tsc, tsc->envir);
   mark(tsc, tsc->oper);
   mark(tsc, tsc->dump);
   mark(tsc, tsc->value);
   mark(tsc, tsc->params);
   mark(tsc, tsc->features);
   mark(tsc, tsc->loadport);
   mark(tsc, tsc->loadenv);
   mark(tsc, tsc->datums);
   mark(tsc, tsc->error_desc);
   mark(tsc, tsc->read_error_desc);
   mark(tsc, tsc->file_error_desc);
   for (i = 0; i < (unsigned)(sizeof tsc->stdports / sizeof *tsc->stdports); i++)
      mark(tsc, tsc->stdports[i]);

   /* Mark recent objects the interpreter doesn't know about yet. */
   for (i = tsc->recent_count ; i ;)
      mark(tsc, tsc->recents[--i]);
   /* Mark any older stuff above nested C calls */
   mark(tsc, tsc->c_nest);
   mark(tsc, tsc->held);

   /* garbage collect */
   gc_collect(tsc);
}
/*
* Allocation of n consecutive items in the heap of 'tsc'.
* Returns a pointer to the first item or NULL on memory depletion.
* The boolean 'final' tells to set of not the finalize bit for the
* allocated bloc.
*/
static void *get_cells(tr7_engine_t tsc, unsigned n, int final)
{
   tr7_cell_t x;

#if STRESS_GC_RESILIENCE
   if (tsc->gc_resilience)
      collect_garbage(tsc);
#endif

#if INTPTR_MAX == INT32_MAX
   n += n & 1; /* ensure oddity */
#endif

   /* Are there any cells available? */
   if (n < tsc->free_cells) {
      x = find_consecutive_cells(tsc, n, final);
      if (x != NULL)
         return x;
   }

   /* If not, try gc'ing some */
   collect_garbage(tsc);
   x = find_consecutive_cells(tsc, n, final);
   if (x != NULL)
      return x;

   /* If there still aren't, try getting more heap */
   if (memseg_multi_alloc(tsc, 1, n > ITEM_SEGSIZE ? n : ITEM_SEGSIZE)) {
      /* got more place */
      x = find_consecutive_cells(tsc, n, final);
      if (x != NULL)
         return x;
   }

   /* If all fail, report failure */
   tsc->no_memory = tsc->free_cells < NOMEM_LEVEL;
   return NULL;
}
/*
**************************************************************************
* SECTION RECENTS
* ---------------
*
* To retain recent allocs before interpreter knows about them - Tehom
*/
static void ok_to_freely_gc(tr7_engine_t tsc)
{
   tsc->recent_count = 0;
}
/*
* returns an array holding the recent allocations
*/
static tr7_t wrap_recent_allocs(tr7_engine_t tsc)
{
   unsigned i, nrec = tsc->recent_count;
   if (nrec) {
      tr7_vector_t vec = get_cells(tsc, 1 + nrec, 0);
      if (vec) {
         vec->head = TR7_MAKE_HEAD(nrec, TR7_HEAD_KIND_VECTOR);
         for(i = 0 ; i < nrec ; i++)
            vec->items[i] = tsc->recents[i];
         return TR7_FROM_VECTOR(vec);
      }
   }
   return TR7_FALSE;
}
/*
* add 'last' to the list of recently allocated values and return it
*/
static tr7_t push_recent_alloc(tr7_engine_t tsc, tr7_t last)
{
   if (!tsc->no_recent) {
      unsigned i = tsc->recent_count++;
      tsc->recents[i] = last;
      if (tsc->recent_count == NRECENTS) {
         //log_str(tsc, "RECENT OVERFLOW!!\n");
         tr7_t repl = wrap_recent_allocs(tsc);
         if (!TR7_IS_FALSE(repl)) {
            tsc->recents[0] = repl;
            tsc->recent_count = 1;
         }
         else {
            /* older values are maybe already accessible to GC */
            for(i = 0 ; i < NRECENTS - 1 ; i++)
               tsc->recents[i] = tsc->recents[i + 1];
            tsc->recent_count = NRECENTS - 1;
         }
      }
   }
   return last;
}
/*
* add any cell pointer 'last' to the list of recently allocated values
* returns its tr7_t value
*/
static tr7_t push_recent_cell(tr7_engine_t tsc, void *last)
{
   return push_recent_alloc(tsc, TR7_FROM_CELL(last));
}
/*
**************************************************************************
* SECTION PAIRS
* -------------
*
* creation of count pairs linked together with the given cars and the given cdr
* returns the head or TR7_NIL on failure
*/
tr7_t tr7_cons_n(tr7_engine_t tsc, int count, tr7_t cars[], tr7_t cdr)
{
   tr7_t result;
   tr7_pair_t pairs;
   unsigned npair, ncell = count * NCELL_OF_TYPE(struct tr7_pair);
   if (ncell <= tsc->nsuccfrees) {
oneblock:
      pairs = get_cells(tsc, ncell, 0);
      result = TR7_FROM_PAIR(pairs);
      while (--count) {
         pairs->car = *cars++;
         pairs->cdr = TR7_FROM_PAIR(&pairs[1]);
         pairs++;
      }
      pairs->car = *cars;
      pairs->cdr = cdr;
      return push_recent_alloc(tsc, result);
   }
   if (!tsc->nsuccfrees && !tsc->no_memory) {
      collect_garbage(tsc);
      if (tsc->free_cells < NCELL_OF_TYPE(struct tr7_pair))
         return TR7_NIL;
      if (ncell <= tsc->nsuccfrees)
         goto oneblock;
   }
   npair = tsc->nsuccfrees / NCELL_OF_TYPE(struct tr7_pair);
   npair = npair ? npair : 1;
   result = tr7_cons_n(tsc, npair, &cars[count - npair], cdr);
   return TR7_IS_NIL(result) ? result : tr7_cons_n(tsc, count - npair, cars, result);
}
/*
* get new pair with the given car and cdr
* returns TR7_NIL on failure
*/
tr7_t tr7_cons(tr7_engine_t tsc, tr7_t car, tr7_t cdr)
{
   tr7_pair_t pair = get_cells(tsc, NCELL_OF_TYPE(struct tr7_pair), 0);
   if (!pair)
      return TR7_NIL;
   pair->car = car;
   pair->cdr = cdr;
   return push_recent_alloc(tsc, TR7_FROM_PAIR(pair));
}
/*
* CxR helpers of level 1
*/
tr7_t tr7_car_or_void(tr7_t t) { return TR7_IS_PAIR(t) ? TR7_CAR(t) : TR7_VOID; }
tr7_t tr7_cdr_or_void(tr7_t t) { return TR7_IS_PAIR(t) ? TR7_CDR(t) : TR7_VOID; }
/*
* CxR helpers of level 2
*/
tr7_t tr7_caar_or_void(tr7_t t) { return tr7_car_or_void(tr7_car_or_void(t)); }
tr7_t tr7_cadr_or_void(tr7_t t) { return tr7_car_or_void(tr7_cdr_or_void(t)); }
tr7_t tr7_cdar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_car_or_void(t)); }
tr7_t tr7_cddr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cdr_or_void(t)); }
/*
* CxR helpers of level 3
*/
tr7_t tr7_caaar_or_void(tr7_t t) { return tr7_car_or_void(tr7_caar_or_void(t)); }
tr7_t tr7_caadr_or_void(tr7_t t) { return tr7_car_or_void(tr7_cadr_or_void(t)); }
tr7_t tr7_cadar_or_void(tr7_t t) { return tr7_car_or_void(tr7_cdar_or_void(t)); }
tr7_t tr7_caddr_or_void(tr7_t t) { return tr7_car_or_void(tr7_cddr_or_void(t)); }
tr7_t tr7_cdaar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_caar_or_void(t)); }
tr7_t tr7_cdadr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cadr_or_void(t)); }
tr7_t tr7_cddar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cdar_or_void(t)); }
tr7_t tr7_cdddr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cddr_or_void(t)); }
/*
* CxR helpers of level 4
*/
tr7_t tr7_caaaar_or_void(tr7_t t) { return tr7_car_or_void(tr7_caaar_or_void(t)); }
tr7_t tr7_caaadr_or_void(tr7_t t) { return tr7_car_or_void(tr7_caadr_or_void(t)); }
tr7_t tr7_caadar_or_void(tr7_t t) { return tr7_car_or_void(tr7_cadar_or_void(t)); }
tr7_t tr7_caaddr_or_void(tr7_t t) { return tr7_car_or_void(tr7_caddr_or_void(t)); }
tr7_t tr7_cadaar_or_void(tr7_t t) { return tr7_car_or_void(tr7_cdaar_or_void(t)); }
tr7_t tr7_cadadr_or_void(tr7_t t) { return tr7_car_or_void(tr7_cdadr_or_void(t)); }
tr7_t tr7_caddar_or_void(tr7_t t) { return tr7_car_or_void(tr7_cddar_or_void(t)); }
tr7_t tr7_cadddr_or_void(tr7_t t) { return tr7_car_or_void(tr7_cdddr_or_void(t)); }
tr7_t tr7_cdaaar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_caaar_or_void(t)); }
tr7_t tr7_cdaadr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_caadr_or_void(t)); }
tr7_t tr7_cdadar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cadar_or_void(t)); }
tr7_t tr7_cdaddr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_caddr_or_void(t)); }
tr7_t tr7_cddaar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cdaar_or_void(t)); }
tr7_t tr7_cddadr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cdadr_or_void(t)); }
tr7_t tr7_cdddar_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cddar_or_void(t)); }
tr7_t tr7_cddddr_or_void(tr7_t t) { return tr7_cdr_or_void(tr7_cdddr_or_void(t)); }
/*
**************************************************************************
* SECTION LISTS
* -------------
*
* Compute the length of the list 'a'
* Result is:
*  proper list: length
*  circular list: -1
*  not even a pair: -2
*  dotted list: -2 minus length before dot
*/
int tr7_list_length(tr7_t a)
{
   int i = 0;
   tr7_t slow = a, fast = a;
   for (;;) {
      if (TR7_IS_NIL(fast))
         return i;
      if (!TR7_IS_PAIR(fast))
         return -2 - i;
      fast = TR7_CDR(fast);
      if ((++i & 1) == 0) {
         slow = TR7_CDR(slow);
         if (TR7EQ(fast, slow))
            return -1; /* circular */
      }
   }
}
/*
* Copy 'a' that MUST be a pair.
* Result in head+tail and return 1 if ok or 0 if improper list
*/
static int list_copy(tr7_engine_t tsc, tr7_t a, tr7_t *head, tr7_t *tail)
{
   int i = 0;
   tr7_t cdr, first = TR7_NIL;
   tr7_pair_t slow, iter, copy, prev = NULL;

   slow = iter = TR7_TO_PAIR(a);
   for (;;) {
      /* copy if possible */
      copy = GET_CELLS(tsc, copy, 0);
      if (copy == NULL)
         break;
      copy->car = iter->car;
      if (prev != NULL)
         prev->cdr =  TR7_FROM_PAIR(copy);
      else {
         first =  TR7_FROM_PAIR(copy);
         push_recent_alloc(tsc, first);
      }

      /* next */
      prev = copy;
      cdr = iter->cdr;
      if (!TR7_IS_PAIR(cdr)) {
         copy->cdr = cdr;
         break;
      }
      iter = TR7_TO_PAIR(cdr);
      copy->cdr = TR7_NIL;

      /* check loop */
      i++;
      if ((i & 1) == 0) {
         slow = TR7_TO_PAIR(slow->cdr);
         if (iter == slow) {
            *head = *tail = TR7_NIL;
            return 0;
         }
      }
   }
   if (prev == NULL) {
      *tail = *head = TR7_NIL;
      return 0;
   }
   *head = first;
   *tail = TR7_FROM_PAIR(prev);
   return TR7_IS_NIL(cdr);
}
/*
* new list from transforming (a b ... x y) to (a b ... x . y)
*/
static tr7_t list_star(tr7_engine_t tsc, tr7_t d)
{
   tr7_t r, *prev = &r;

   while (TR7_IS_PAIR(d) && !TR7_IS_NIL(TR7_CDR(d))) {
      *prev = tr7_cons(tsc, TR7_CAR(d), TR7_NIL);
      prev = &TR7_CDR(*prev);
      d = TR7_CDR(d);
   }
   *prev = TR7_IS_PAIR(d) ? TR7_CAR(d) : d;
   return r;
}
/*
* reverse list 'head' and set 'tail' at its end -- produce new list
*/
tr7_t tr7_reverse(tr7_engine_t tsc, tr7_t head, tr7_t tail)
{
   for ( ; TR7_IS_PAIR(head) ; head = TR7_CDR(head))
      tail = tr7_cons(tsc, TR7_CAR(head), tail);
   return TR7_IS_NIL(head) ? tail : TR7_VOID;   /* signal an error if improper  list */
}
/*
* reverse list 'head' and set 'tail' at its end -- in-place
*/
tr7_t tr7_reverse_in_place(tr7_t head, tr7_t tail)
{
   while (TR7_IS_PAIR(head)) {
      tr7_t tmp = TR7_CDR(head);
      TR7_CDR(head) = tail;
      tail = head;
      head = tmp;
   }
   return TR7_IS_NIL(head) ? tail : TR7_VOID;   /* signal an error if improper  list */
}
/*
* Get, at max, 'count' pairs of the 'list' in 'pairs' and return the found count
*/
int tr7_get_list_pairs(tr7_t list, int count, tr7_pair_t pairs[])
{
   tr7_pair_t p;
   int i = 0;
   while (i < count && TR7_IS_PAIR(list)) {
      pairs[i++] = p = TR7_TO_PAIR(list);
      list = p->cdr;
   }
   return i;
}
/*
* Get 'count' cars of the 'list' in 'cars' and return the found count
*/
int tr7_get_list_cars(tr7_t list, int count, tr7_t cars[], tr7_t *cdr)
{
   tr7_pair_t p;
   int i = 0;
   while (i < count && TR7_IS_PAIR(list)) {
      p = TR7_TO_PAIR(list);
      cars[i++] = p->car;
      list = p->cdr;
   }
   if (cdr != NULL)
      *cdr = list;
   return i;
}
/*
* Like scheme's assoc but returns a pair pointer or NULL
*/
tr7_pair_t tr7_assoc_pair(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t))
{
   int i = 0;
   tr7_pair_t plist, pitem, slow;

   slow = plist = TR7_AS_PAIR(list);
   while (plist != NULL) {
      pitem = TR7_AS_PAIR(plist->car);
      if (pitem != NULL && eq(x, pitem->car))
         return pitem;
      plist = TR7_AS_PAIR(plist->cdr);
      i ^= 1;
      if (i == 0) {
         slow = TR7_TO_PAIR(slow->cdr);
         if (slow == plist)
            break;
      }
   }
   return NULL;
}
/*
* Like scheme's assq but returns a pair pointer or NULL
*/
tr7_pair_t tr7_assq_pair(tr7_t x, tr7_t list)
{
   return tr7_assoc_pair(x, list, tr7_eq);
}
/*
* Like scheme's assv but returns a pair pointer or NULL
*/
tr7_pair_t tr7_assv_pair(tr7_t x, tr7_t list)
{
   return tr7_assoc_pair(x, list, tr7_eqv);
}
/*
* Like scheme's assoc for equal but returns a pair pointer or NULL
*/
tr7_pair_t tr7_asse_pair(tr7_t x, tr7_t list)
{
   return tr7_assoc_pair(x, list, tr7_equal);
}
/*
* Like scheme's member but returns a pair pointer or NULL
*/
tr7_pair_t tr7_member_pair(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t))
{
   int i = 0;
   tr7_pair_t plist, slow;

   slow = plist = TR7_AS_PAIR(list);
   while (plist != NULL) {
      if (eq(x, plist->car))
         return plist;
      plist = TR7_AS_PAIR(plist->cdr);
      i ^= 1;
      if (i == 0) {
         slow = TR7_TO_PAIR(slow->cdr);
         if (slow == plist)
            break;
      }
   }
   return NULL;
}
/*
* Like scheme's memq but returns a pair pointer or NULL
*/
tr7_pair_t tr7_memq_pair(tr7_t x, tr7_t list)
{
   return tr7_member_pair(x, list, tr7_eq);
}
/*
* Like scheme's memv but returns a pair pointer or NULL
*/
tr7_pair_t tr7_memv_pair(tr7_t x, tr7_t list)
{
   return tr7_member_pair(x, list, tr7_eqv);
}
/*
* Like scheme's member for equal but returns a pair pointer or NULL
*/
tr7_pair_t tr7_meme_pair(tr7_t x, tr7_t list)
{
   return tr7_member_pair(x, list, tr7_equal);
}
/*
* Call the function 'fun' for searching 'x' in 'list'
* Returns the result of TR7_FALE if not found
*/
static tr7_t wrap_pair_search(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t), tr7_pair_t (*fun)(tr7_t, tr7_t, int (*)(tr7_t, tr7_t)))
{
   tr7_pair_t pair = fun(x, list, eq);
   return pair ? TR7_FROM_PAIR(pair) : TR7_FALSE;
}
/*
* Like scheme's assoc
*/
tr7_t tr7_assoc(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t))
{
   return wrap_pair_search(x,  list, eq, tr7_assoc_pair);
}
/*
* Like scheme's assq
*/
tr7_t tr7_assq(tr7_t x, tr7_t list)
{
   return tr7_assoc(x,  list, tr7_eq);
}
/*
* Like scheme's assv
*/
tr7_t tr7_assv(tr7_t x, tr7_t list)
{
   return tr7_assoc(x,  list, tr7_eqv);
}
/*
* Like scheme's assoc but for equal
*/
tr7_t tr7_asse(tr7_t x, tr7_t list)
{
   return tr7_assoc(x,  list, tr7_equal);
}
/*
* Like scheme's member
*/
tr7_t tr7_member(tr7_t x, tr7_t list, int (*eq)(tr7_t, tr7_t))
{
   return wrap_pair_search(x,  list, eq, tr7_member_pair);
}
/*
* Like scheme's memq
*/
tr7_t tr7_memq(tr7_t x, tr7_t list)
{
   return tr7_member(x,  list, tr7_eq);
}
/*
* Like scheme's memv
*/
tr7_t tr7_memv(tr7_t x, tr7_t list)
{
   return tr7_member(x,  list, tr7_eqv);
}
/*
* Like scheme's member but for equal
*/
tr7_t tr7_meme(tr7_t x, tr7_t list)
{
   return tr7_member(x,  list, tr7_equal);
}
/*
**************************************************************************
* SECTION CHARACTER
* -----------------
*
* crete a character
*/
tr7_t tr7_from_character(tr7_engine_t tsc, wint_t value)
{
   return TR7_FROM_CHAR(value);
}
/*
* get a character
*/
wint_t tr7_to_character(tr7_t t)
{
   return TR7_TO_CHAR(t);
}
/*
* is character?
*/
int tr7_is_character(tr7_t t)
{
   return TR7_IS_CHAR(t);
}
/*
* is c alphabetic and ASCII?
*/
static int Cisalpha(wint_t c)
{
   return isascii(c) && isalpha(c);
}
/*
* is c digit and ASCII?
*/
static int Cisdigit(wint_t c)
{
   return isascii(c) && isdigit(c);
}
/*
* is c space and ASCII?
*/
static int Cisspace(wint_t c)
{
   return isascii(c) && isspace(c);
}
/*
* is c upper letter and ASCII?
*/
static int Cisupper(wint_t c)
{
   return isascii(c) && isupper(c);
}
/*
* is c lower letter and ASCII?
*/
static int Cislower(wint_t c)
{
   return isascii(c) && islower(c);
}
/*
* compare characters
*/
static int char_cmp(wint_t a, wint_t b)
{
   return a < b ? TR7_CMP_LESSER : a == b ? TR7_CMP_EQUAL : TR7_CMP_GREATER;
}
/*
* compare characters, ignoring case
*/
static int char_cmp_ci(wint_t a, wint_t b)
{
   return char_cmp(towupper(a), towupper(b));
}
/*
* handle utf8 internal encoding
* even if not optimal in most cases, internal utf8 encoding was
* chosen for its natural compatibility with english language
*/
static unsigned char_to_utf8(wint_t car, uint8_t *utf8)
{
   if (car <= 0x7F) { /* 0xxxxxxx */
      utf8[0] = (uint8_t)car;
      return 1;
   }
   if (car <= 0x7FF) { /* 110xxxxx 10xxxxxx */
      utf8[0] = (uint8_t)(192 + ((car >> 6) & 31));
      utf8[1] = (uint8_t)(128 + (car & 63));
      return 2;
   }
   if (car <= 0xFFFF) { /* 1110xxxx 10xxxxxx 10xxxxxx */
      utf8[0] = (uint8_t)(224 + ((car >> 12) & 15));
      utf8[1] = (uint8_t)(128 + ((car >> 6) & 63));
      utf8[2] = (uint8_t)(128 + (car & 63));
      return 3;
   }
   if (car <= 0x1FFFFF) { /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
      utf8[0] = (uint8_t)(240 + ((car >> 18) & 7));
      utf8[1] = (uint8_t)(128 + ((car >> 12) & 63));
      utf8[2] = (uint8_t)(128 + ((car >> 6) & 63));
      utf8[3] = (uint8_t)(128 + (car & 63));
      return 4;
   }
   if (car <= 0x3FFFFFF) { /* 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
      utf8[0] = (uint8_t)(248 + ((car >> 24) & 3));
      utf8[1] = (uint8_t)(128 + ((car >> 18) & 63));
      utf8[2] = (uint8_t)(128 + ((car >> 12) & 63));
      utf8[3] = (uint8_t)(128 + ((car >> 6) & 63));
      utf8[4] = (uint8_t)(128 + (car & 63));
      return 5;
   }
   if (car <= 0x7FFFFFFF) { /* 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
      utf8[0] = (uint8_t)(252 + ((car >> 30) & 1));
      utf8[1] = (uint8_t)(128 + ((car >> 24) & 63));
      utf8[2] = (uint8_t)(128 + ((car >> 18) & 63));
      utf8[3] = (uint8_t)(128 + ((car >> 12) & 63));
      utf8[4] = (uint8_t)(128 + ((car >> 6) & 63));
      utf8[5] = (uint8_t)(128 + (car & 63));
      return 6;
   }
   return 0;
}
/*
* convert an 'utf8' sequence to character 'car'
* returns the length of the byte sequence or 0 on error
*/
static unsigned utf8_to_char(const uint8_t *utf8, wint_t *car)
{
   uint8_t x = utf8[0];
   if (x < 128) { /* 0xxxxxxx */
      *car = (wint_t)x;
      return 1;
   }
   if (x < 192) { /* 10xxxxxx */
      *car = 0;
      return 0;
   }
   if (x < 224) { /* 110xxxxx 10xxxxxx */
      *car = (((wint_t)(x & 31)) << 6)
           | ((wint_t)(utf8[1] & 63));
      return 2;
   }
   if (x < 240) { /* 1110xxxx 10xxxxxx 10xxxxxx */
      *car = (((wint_t)(x & 15)) << 12)
           | (((wint_t)(utf8[1] & 63)) << 6)
           | ((wint_t)(utf8[2] & 63));
      return 3;
   }
   if (x < 248) { /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
      *car = (((wint_t)(x & 7)) << 18)
           | (((wint_t)(utf8[1] & 63)) << 12)
           | (((wint_t)(utf8[2] & 63)) << 6)
           | ((wint_t)(utf8[3] & 63));
      return 4;
   }
   if (x < 252) { /* 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
      *car = (((wint_t)(x & 3)) << 24)
           | (((wint_t)(utf8[1] & 63)) << 18)
           | (((wint_t)(utf8[2] & 63)) << 12)
           | (((wint_t)(utf8[3] & 63)) << 6)
           | ((wint_t)(utf8[4] & 63));
      return 5;
   }
   if (x < 254) { /* 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
      *car = (((wint_t)(x & 1)) << 30)
           | (((wint_t)(utf8[1] & 63)) << 24)
           | (((wint_t)(utf8[2] & 63)) << 18)
           | (((wint_t)(utf8[3] & 63)) << 12)
           | (((wint_t)(utf8[4] & 63)) << 6)
           | ((wint_t)(utf8[5] & 63));
      return 6;
   }
   *car = 0;
   return 0;
}
/*
* get the length in bytes of the utf8 representation of 'var'
*/
static unsigned char_length(wint_t car)
{
   if (car <= 0x7F)
      return 1;
   if (car <= 0x7FF)
      return 2;
   if (car <= 0xFFFF)
      return 3;
   if (car <= 0x1FFFFF)
      return 4;
   if (car <= 0x3FFFFFF)
      return 5;
   if (car <= 0x7FFFFFFF)
      return 6;
   return 0;
}
/*
* get the length in byte of the utf8 sequence whose first
* byt is 'utf8' or return 0 if the byte is invalid utf8 header
*/
static unsigned utf8_length(uint8_t utf8)
{
   if (utf8 < 128)
      return 1;
   if (utf8 < 192)
      return 0;
   if (utf8 < 224)
      return 2;
   if (utf8 < 240)
      return 3;
   if (utf8 < 248)
      return 4;
   if (utf8 < 252)
      return 5;
   if (utf8 < 254)
      return 6;
   return 0;
}
/*
* get the count of characters of the utf8 string of 'length' bytes
*/
static size_t utf8str_nchars(const uint8_t *str, size_t length)
{
   unsigned nb;
   size_t nchars = 0, index = 0;
   while (index < length) {
      nb = utf8_length((uint8_t)str[index]);
      if (!nb)
         break;
      index += nb;
      nchars += index <= length;
   }
   return nchars;
}
/*
* get the byte offset of the 'pos'th character of the utf8 string of 'length' bytes
*/
static ssize_t utf8str_offset(const uint8_t *sutf8, size_t length, size_t pos)
{
   size_t nb, index = 0;
   while (pos--) {
      if (index >= length)
         return -1;
      nb = utf8_length(sutf8[index]);
      if (!nb)
         return -1;
      index += nb;
   }
   return (ssize_t)index;
}
/*
* Validate the utf8 sequence of length
*/
static int utf8str_is_valid(const uint8_t *sutf8, size_t length)
{
   while (length) {
      unsigned len = utf8_length(*sutf8);
      if (!len || len > length)
         return 0;
      sutf8 += len;
      length -= len;
   }
   return 1;
}
/*
* Computes a hashing code for the key
* the returned value is wrapped to be in the
* interval 0 .. table_size - 1
*
* There is no case folding because the effect of the directive
* #!fold-case is to translate to lowercases before hashing.
*/
static unsigned int utf8str_hash(const uint8_t *sutf8, size_t length)
{
   unsigned int hashed = (unsigned int)length;
   const uint8_t *end = &sutf8[length];
   while (sutf8 != end)
      hashed = (hashed << 5)
             + (hashed >> (sizeof(unsigned int) * CHAR_BIT - 5))
             + (unsigned int)*sutf8++;
   return hashed;
}
/*
*/
static const char *fold(char low[STRBUFFSIZE], const char *p)
{
   int i;
   for (i = 0 ; p[i] && i < STRBUFFSIZE - 1 ; i++)
      low[i] = towlower(p[i]);
   low[i] = 0;
   return low;
}
/*
**************************************************************************
*
*/
#define is_control_code(c)  ((c) == 127 || ((unsigned)(c)) < 32)
#if USE_ASCII_NAMES
static const char charnames[33][4] = {
/* 0.. 7*/   "nul", "soh", "stx", "etx", "eot", "enq", "ack", "bel",
/* 8..15*/   "bs",  "ht",  "lf",  "vt",  "ff",  "cr",  "so",  "si",
/*16..23*/   "dle", "dc1", "dc2", "dc3", "dc4", "nak", "syn", "etb",
/*24..31*/   "can", "em",  "sub", "esc", "fs",  "gs",  "rs",  "us",
/*!127!!*/   "del"
};

/*
 * Put in *'pc' the code for the character of given 'name'
 * and return 1
 * If 'name' is not for a known character, returns 0
 */
static int get_control_code(const char *name, wint_t *pc)
{
   int i;
   for (i = 0; i <= 32; i++) {
      if (strcmp(name, charnames[i]) == 0) {
         *pc = (wint_t)(i == 32 ? 127 : i);
         return 1;
      }
   }
   return 0;
}

/*
 * Return the name of the character or NULL is unknown
 */
static int get_control_name(char *buf, size_t size, wint_t c)
{
   const char *name = charnames[c < 32 ? (int)(c & 31) : 32];
   buf[0] = '#';
   buf[1] = '\\';
   buf[2] = name[0];
   buf[3] = name[1];
   buf[4] = name[2];
   buf[5] = 0;
   return 5 - !name[2];
}
#else
#define get_control_code(name,pc)     (0)
#define get_control_name(buf,size,c)  snprintf(buf,size,"#\\x%x",c)
#endif

/*
**************************************************************************
* SECTION IMMUTABLE
* -----------------
*
* TODO change that
*/
int tr7_is_immutable(tr7_t t)
{
   return TR7_IS_CELL(t) && TR7_IS_IMMUTABLE_CELL_HEAD(t);
}
/*
*/
void tr7_set_immutable(tr7_t t)
{
   if (TR7_IS_CELL(t))
      TR7_SET_IMMUTABLE_CELL_HEAD(t);
}
/*
**************************************************************************
* SECTION BUFFERS
* ---------------
*
* Create a tr7_t holding a buffer of 'kind' pinting the buffer 'buf' of allocated 'len' bytes
*/
static tr7_t create_buffer(tr7_engine_t tsc, uint8_t *buf, size_t len, int final, unsigned kind)
{
   tr7_buffer_t buffer = GET_CELLS(tsc, buffer, final);
   if (!buffer) {
      if (final)
         memfree(tsc, buf);
      return TR7_NIL;
   }
   buffer->head = TR7_MAKE_HEAD(len, kind);
   buffer->content = buf;
   return push_recent_cell(tsc, buffer);
}
/*
* finalize the buffer
*/
static void finalize_buffer(tr7_engine_t tsc, tr7_cell_t a)
{
   memfree(tsc, TR7_CELL_CONTENT_BUFFER(a));
}
/*
**************************************************************************
* SECTION BYTEVECTOR
* ------------------
*
* create a new bytevector pointing a 'mem'ory of 'len'gth bytes
*/
static tr7_t make_bytevector(tr7_engine_t tsc, uint8_t *mem, size_t len)
{
   return mem ? create_buffer(tsc, mem, len, 1, TR7_HEAD_KIND_BYTEVECTOR) : TR7_NIL;
}
/*
* get a new bytevector holding a copy of 'len' 'bytes'
*/
tr7_t tr7_make_bytevector_copy(tr7_engine_t tsc, const uint8_t *bytes, size_t len)
{
   return make_bytevector(tsc, memalloc_copy(tsc, bytes, len), len);
}
/*
* get a new bytevector holding a 'len' bytes set to 'byte'
*/
tr7_t tr7_make_bytevector_fill(tr7_engine_t tsc, uint8_t byte, size_t len)
{
   return make_bytevector(tsc, memalloc_fill(tsc, byte, len), len);
}
/*
* get a new bytevector holding a 'len' bytes unintialized (unsafe)
*/
tr7_t tr7_make_bytevector(tr7_engine_t tsc, size_t len)
{
   return make_bytevector(tsc, memalloc(tsc, len), len);
}
/*
**************************************************************************
* SECTION STRING
* --------------
*
* Management of strings
*/
static tr7_t make_string(tr7_engine_t tsc, char *mem, size_t length, int final)
{
   unsigned kind = final ? TR7_HEAD_KIND_STRING : TR7_HEAD_KIND_STRING|TR7_MASK_HEAD_IMMUTABLE;
   return create_buffer(tsc, (uint8_t*)mem, length, final, kind);
}
/*
* get a string of 'ncars' times the character 'car'
*/
tr7_t tr7_make_string_fill(tr7_engine_t tsc, wint_t car, size_t ncars)
{
   uint8_t utf8[UTF8BUFFSIZE];
   unsigned clen = char_to_utf8(car, utf8); /* convert to utf8 */
   size_t i, length = ncars * clen;
   char *mem = memalloc(tsc, 1 + length);
   if (!mem)
      return TR7_NIL;
   for (i = 0 ; i < length ; i += clen)
      memcpy(&mem[i], utf8, clen);
   mem[length] = 0;
   return make_string(tsc, mem, length, 1);
}
/*
* get a string being a 'sutf8' of 'length'. sutf8 is freed on dispose
*/
tr7_t tr7_make_string_take_length(tr7_engine_t tsc, const char *sutf8, size_t length)
{
   return sutf8 ? make_string(tsc, (char*)sutf8, length, 1) : TR7_NIL;
}
/*
* get a string being a 'sutf8'. sutf8 is freed on dispose
*/
tr7_t tr7_make_string_take(tr7_engine_t tsc, const char *sutf8)
{
   return sutf8 ? make_string(tsc, (char*)sutf8, strlen(sutf8), 1) : TR7_NIL;
}
/*
* get a string being a copy of 'sutf8' of 'length'
*/
tr7_t tr7_make_string_copy_length(tr7_engine_t tsc, const char *sutf8, size_t length)
{
   void *mem = memalloc_copy_stringz(tsc, sutf8, length);
   return mem ? make_string(tsc, mem, length, 1) : TR7_NIL;
}
/*
* get a string being a copy of 'sutf8' terminated with a zero
*/
tr7_t tr7_make_string_copy(tr7_engine_t tsc, const char *sutf8)
{
   return tr7_make_string_copy_length(tsc, sutf8, strlen(sutf8));
}
/*
* get a string referencing 'sutf8' of 'length'
*/
tr7_t tr7_make_string_static_length(tr7_engine_t tsc, const char *sutf8, size_t length)
{
   return make_string(tsc, (void*)sutf8, length, 0);
}
/*
* get a string referencing 'sutf8' terminated with a zero
*/
tr7_t tr7_make_string_static(tr7_engine_t tsc, const char *sutf8)
{
   return tr7_make_string_static_length(tsc, sutf8, strlen(sutf8));
}
/*
* get a new string holding a 'len' bytes unintialized (unsafe)
*/
tr7_t tr7_make_string(tr7_engine_t tsc, size_t length)
{
   char *mem = memalloc(tsc, length + 1);
   if (!mem)
      return TR7_NIL;
   return make_string(tsc, mem, length, 1);
}
/*
* check if 'item' is a string
*/
int tr7_is_string(tr7_t item)
{
   return TR7_IS_STRING(item);
}
/*
* get the utf8 string buffer
*/
const char *tr7_string_buffer(tr7_t string)
{
   return (const char*)TR7_CONTENT_STRING(string);
}
/*
* get the string length (byte length without the terminating zero)
*/
size_t tr7_string_size(tr7_t string)
{
   return TR7_LENGTH_STRING(string);
}
/*
* get length (in chars) of 'string'
*/
size_t tr7_string_length(tr7_t string)
{
   const uint8_t *str = TR7_CONTENT_STRING(string);
   size_t length = TR7_LENGTH_STRING(string);
   return utf8str_nchars(str, length);
}
/*
* get the character of the 'string' at the position 'pos' (in characters)
*/
wint_t tr7_string_ref(tr7_t string, size_t pos)
{
   wint_t car;
   const uint8_t *str = (uint8_t*)TR7_CONTENT_STRING(string);
   size_t length = TR7_LENGTH_STRING(string);
   ssize_t offset = utf8str_offset(str, length, pos);
   if (offset >= 0 && utf8_to_char(&str[offset], &car))
      return car;
   return (wint_t)WEOF;
}
/*
* set the character of the 'string' at the position 'pos' (in characters)
* to the character value 'car'
* returns 1 on success or else 0 on memory depletion
*/
int tr7_string_set(tr7_engine_t tsc, tr7_t string, size_t pos, wint_t car)
{
   unsigned nbef, naft, dif;
   uint8_t *cpy, *str = (uint8_t*)TR7_CONTENT_STRING(string);
   size_t length = TR7_LENGTH_STRING(string);
   ssize_t offset = utf8str_offset(str, length, pos);
   if (offset < 0)
      return 0; /* invalid position */
   nbef = utf8_length(str[offset]); /* utf8 length before */
   naft = char_length(car); /* utf8 length after */
   if (naft <= nbef) {
      char_to_utf8(car, &str[offset]);
      if (naft < nbef) {
         dif = nbef - naft;
         TR7_SET_LENGTH_STRING(string, length - dif);
         str += offset + naft;
         length -= offset + naft;
         while(length) {
            *str = str[dif];
            str++;
            length--;
         }
         *str = 0;
      }
   }
   else {
      dif = naft - nbef;
      cpy = memalloc(tsc, length + dif + 1);
      if (!cpy)
         return 0;
      TR7_SET_LENGTH_STRING(string, length + dif);
      TR7_CONTENT_STRING(string) = cpy;
      memcpy(cpy, str, offset);
      memcpy(&cpy[offset+naft], &str[offset+nbef], length - offset - nbef + 1);
      memfree(tsc, str);
      str = cpy;
      char_to_utf8(car, &str[offset]);
   }
   return 1;
}
/*
**************************************************************************
* SECTION SYMBOL
* --------------
*
* Management of symbols
*/
static tr7_t make_symbol(tr7_engine_t tsc, char *mem, size_t length, int final)
{
   return create_buffer(tsc, (uint8_t*)mem, length, final, TR7_HEAD_KIND_SYMBOL|TR7_MASK_HEAD_IMMUTABLE);
}
/*
*/
tr7_t tr7_make_symbol_copy_length(tr7_engine_t tsc, const char *sutf8, size_t length)
{
   void *mem = memalloc_copy_stringz(tsc, sutf8, length);
   return mem ? make_symbol(tsc, mem, length, 1) : TR7_NIL;
}
/*
*/
tr7_t tr7_make_symbol_copy(tr7_engine_t tsc, const char *sutf8)
{
   return tr7_make_symbol_copy_length(tsc, sutf8, strlen(sutf8));
}
/*
*/
tr7_t tr7_make_symbol_static_length(tr7_engine_t tsc, const char *sutf8, size_t length)
{
   return make_symbol(tsc, (void*)sutf8, length, 0);
}
/*
*/
tr7_t tr7_make_symbol_static(tr7_engine_t tsc, const char *sutf8)
{
   return tr7_make_symbol_static_length(tsc, sutf8, strlen(sutf8));
}
/*
*/
const char *tr7_symbol_string(tr7_t symbol)
{
   return (const char*)TR7_CONTENT_SYMBOL(symbol);
}
/*
**************************************************************************
* SECTION VECTOR
* --------------
*
*/
static tr7_t alloc_vector(tr7_engine_t tsc, int len)
{
   tr7_vector_t vec = get_cells(tsc, 1 + len, 0);
   if (vec == NULL)
      return TR7_NIL;
   vec->head = TR7_MAKE_HEAD(len, TR7_HEAD_KIND_VECTOR);
   return push_recent_cell(tsc, vec);
}
/*
*/
tr7_t tr7_make_vector_copy(tr7_engine_t tsc, tr7_t *items, size_t len)
{
   tr7_t res = alloc_vector(tsc, len);
   if (!TR7_IS_NIL(res)) {
      tr7_t *to = TR7_ITEMS_VECTOR(res);
      memcpy(to, items, len * sizeof *to);
   }
   return res;
}
/*
*/
tr7_t tr7_make_vector_fill(tr7_engine_t tsc, tr7_t value, size_t len)
{
   tr7_t res = alloc_vector(tsc, len);
   if (!TR7_IS_NIL(res)) {
      tr7_t *to = TR7_ITEMS_VECTOR(res);
      while(len--)
         *to++ = value;
   }
   return res;
}
/*
*/
tr7_t tr7_make_vector(tr7_engine_t tsc, size_t len)
{
   return alloc_vector(tsc, len);
}
tr7_t tr7_vector_ref(tr7_t vec, int ielem)
{
   return TR7_ITEM_VECTOR(vec, ielem);
}
/*
*/
tr7_t tr7_vector_set(tr7_t vec, int ielem, tr7_t a)
{
   return TR7_ITEM_VECTOR(vec, ielem) = a;
}
/*
**************************************************************************
*
* File search
*/

/*
**************************************************************************
*
*/

/* possible extensions when search a scheme file */
static const char *suffixes_scheme[] = { ".scm", "/init.scm" };

/* locate a file based on a dirname of dirlength,
 * a basename of baselength and set of possible suffixes
 * return the opened file for read or NULL
 * on success, path holds the filename */
static int search_access_file_in(
   const char *dirname, int dirlength,
   const char *basename, int baselength,
   char path[], int pathsize,
   const char **suffixes, int nrsuffixes)
{
   int len, idx, slen;

   /* prepare prefix */
   if (dirname == NULL) {
      len = baselength;
      if (len >= pathsize)
         return 0;

      memcpy(path, basename, baselength);
   }
   else {
      len = dirlength + baselength + 1;
      if (len >= pathsize)
         return 0;

      memcpy(path, dirname, dirlength);
      path[dirlength] = DIR_SEP_CHAR;
      memcpy(&path[dirlength + 1], basename, baselength);
   }
   path[len] = 0;

   /* try with suffixes */
   for (idx = 0 ;; idx++) {
      if (access(path, R_OK) == 0)
         return 1;

      /* search next possible suffix */
      for ( ;; idx++) {
         if (idx >= nrsuffixes)
            return 0;
         slen = 1 + strlen(suffixes[idx]);
         if (len + slen <= pathsize) {
            memcpy(&path[len], suffixes[idx], slen);
            break;
         }
      }
   }
}

/* locate a file based basename of baselength, current context and envar
 * return the opened file for read or NULL
 * on success, path holds the filename */
static int search_access_file(
   tr7_engine_t tsc,
   const char *basename, int baselength,
   char path[], int pathsize,
   const char **suffixes, int nrsuffixes,
   const char *defpath)
{
   int len, found;
   const char *envpath;

   found = search_access_file_in(NULL, 0, basename, baselength, path, pathsize, suffixes, nrsuffixes);

   if (!found && TR7_IS_PORT(tsc->loadport)) {
      port_t *pt = TR7__PORT__PORT(tsc->loadport);
      if (pt && (pt->flags & port_file) && pt->rep.stdio.filename != NULL) {
         envpath = pt->rep.stdio.filename;
         for (len = strlen(envpath) ; len > 0 && envpath[len] != DIR_SEP_CHAR ; len--);
         if (len > 0)
            found = search_access_file_in(envpath, len, basename, baselength, path, pathsize, suffixes, nrsuffixes);
      }
   }

   if (!found) {
      /* get search path from environment variable with fallback */
      envpath = defpath ? defpath : tsc->strings[Tr7_StrID_Path];
      /* search in environment path if found */
      if (envpath != NULL) {
         while(!found && *envpath) {
            while (*envpath == PATH_SEP_CHAR)
               envpath++;
            if (*envpath) {
               /* get next prefix directory */
               for(len = 1 ; envpath[len] && envpath[len] != PATH_SEP_CHAR ; len++);
               found = search_access_file_in(envpath, len, basename, baselength, path, pathsize, suffixes, nrsuffixes);
               envpath = &envpath[len];
            }
         }
      }
   }
   return found;
}

/*
**************************************************************************
*
* Handle shared library for load-extension
*/
#if USE_DL

#ifdef _WIN32

#include <windows.h>

#define EXTSTDEXT ".dll"

static void display_w32_error_msg(const char *additional_message)
{
   LPVOID msg_buf;

   FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, NULL, GetLastError(), 0, (LPTSTR) & msg_buf, 0, NULL);
   fprintf(stderr, "scheme load-extension: %s: %s", additional_message, msg_buf);
   LocalFree(msg_buf);
}

static HMODULE dl_attach(const char *module)
{
   HMODULE dll = LoadLibrary(module);
   if (!dll)
      display_w32_error_msg(module);
   return dll;
}

static FARPROC dl_proc(HMODULE mo, const char *proc)
{
   FARPROC procedure = GetProcAddress(mo, proc);
   if (!procedure)
      display_w32_error_msg(proc);
   return procedure;
}

static void dl_detach(HMODULE mo)
{
   (void) FreeLibrary(mo);
}

#else

#include <dlfcn.h>

#define EXTSTDEXT ".so"

typedef void *HMODULE;
typedef void (*FARPROC)();

static HMODULE dl_attach(const char *module)
{
   HMODULE so = dlopen(module, RTLD_LAZY);
   if (!so) {
      fprintf(stderr, "Error loading scheme extension \"%s\": %s\n", module, dlerror());
   }
   return so;
}

static FARPROC dl_proc(HMODULE mo, const char *proc)
{
   const char *errmsg;
   FARPROC fp = (FARPROC) dlsym(mo, proc);
   if ((errmsg = dlerror()) == NULL) {
      return fp;
   }
   fprintf(stderr, "Error initializing scheme module \"%s\": %s\n", proc, errmsg);
   return NULL;
}

static void dl_detach(HMODULE mo)
{
   (void) dlclose(mo);
}
#endif

static const char *suffixes_extension[] = { EXTSTDEXT };

static const char dl_init_function_name[] = "init_tr7";

static int dl_load_ext(tr7_engine_t tsc, const char *name)
{
   char filename[PATH_MAX + 1];
   HMODULE handle;
   void (*module_init)(tr7_engine_t tsc);
   int found;

   found = search_access_file(tsc, name, strlen(name), filename, sizeof filename,
               suffixes_extension, (int)(sizeof suffixes_extension / sizeof *suffixes_extension),
               tsc->strings[Tr7_StrID_Extension_Path]);
   if (found) {
      handle = dl_attach(filename);
      if (handle != NULL) {
         module_init = (void (*)(tr7_engine_t )) dl_proc(handle, dl_init_function_name);
         if (module_init == NULL)
            dl_detach(handle);
         else {
            (*module_init) (tsc);
            return 1;
         }
      }
   }
   return 0;
}

#endif


/*
**************************************************************************
* SECTION
* --------------
*
*/
/*
**************************************************************************
*
*/
#if !USE_MATH
/************* CAUTION this is a hack for compiling with USE_MATH = 0 */
/************* CAUTION don't compile with USE_MATH = 0 at the moment */
/************* support of double should be removed if USE_MATH = 0 */
/************* so name USE_MATH is not the expected one in that case */
union id {
   uint64_t i;
   double d;
};
#define _I2D_(x) (((union id){.i = (x)}).d)
#define _D2I_(x) (((union id){.d = (x)}).i)
#define _NAN_SI_ (((uint64_t)0x1)<<63)
#define _NAN_EI_ (((uint64_t)0x7ff)<<52)
#define _NAN_MI_ ((((uint64_t)0x1)<<52) - 1)
#define _NAN_(x) (((union id){.i = (uint64_t)(x) | _NAN_EI_}).d)
#define INFINITY _NAN_(0)
#define NAN      _NAN_(1)
#define isnan(x) (((_D2I_(x) & _NAN_EI_) == _NAN_EI_) && ((_D2I_(x) & _NAN_MI_) != 0))
#define isinf(x) (((_D2I_(x) & _NAN_EI_) == _NAN_EI_) && ((_D2I_(x) & _NAN_MI_) == 0))
#define isfinite(x) ((_D2I_(x) & _NAN_EI_) != _NAN_EI_)

static double trunc(double x) { return (double)(tr7_int_t)x; }
static double round(double x) { return x < 0 ? -round(-x) : trunc(x+0.5); }
static double floor(double x) { double r = trunc(x); return r == x ? r : x > 0 ? r : r - 1.0; }
static double fmod(double x, double y) { return x - y * trunc(x / y); }
static double sqrt(double x) {
   if (x < 0 || isnan(x)) return NAN;
   if (isinf(x)) return x;
   double i = 1;
   int j = 20;
   while(j--) i = 0.5 * (i + x / i);
   return i;
}
#endif


/* get number atom (integer) */
tr7_t tr7_from_int(tr7_engine_t tsc, tr7_int_t num)
{
   /* TODO shift to long integer if overflow */
   return TR7_FROM_INT(num);
}

tr7_t tr7_from_double(tr7_engine_t tsc, double n)
{
   tr7_double_t x = GET_CELLS(tsc, x, 0);
   *x = n;
   push_recent_cell(tsc, x);
   return TR7_FROM_DOUBLE(x);
}

int tr7_is_number(tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
   case TR7_KIND_DOUBLE:
      return 1;
   default:
      return 0;
   }
}

int tr7_is_integer(tr7_t t)
{
   double x;
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return 1;
   case TR7_KIND_DOUBLE:
      x = *TR7_TO_DOUBLE(t);
      return isfinite(x) && round(x) == x;
   default:
      return 0;
   }
}

int tr7_is_real(tr7_t t)
{
   return TR7_IS_DOUBLE(t);
}

int tr7_is_exact(tr7_t t)
{
   return TR7_IS_INT(t);
}

int tr7_is_exact_integer(tr7_t t)
{
   return TR7_IS_INT(t);
}

int tr7_is_NaN(tr7_t t)
{
   return TR7_IS_DOUBLE(t) && isnan(*TR7_TO_DOUBLE(t));
}

int tr7_is_finite(tr7_t t)
{
   return !TR7_IS_DOUBLE(t) || isfinite(*TR7_TO_DOUBLE(t));
}

int tr7_is_infinite(tr7_t t)
{
   return TR7_IS_DOUBLE(t) && isinf(*TR7_TO_DOUBLE(t));
}

tr7_int_t tr7_to_int(tr7_t t)
{
   if (TR7_KIND(t) == TR7_KIND_DOUBLE)
      return (tr7_int_t)*TR7_TO_DOUBLE(t);
   return TR7_TO_INT(t);
}

double tr7_to_double(tr7_t t)
{
   if (TR7_KIND(t) == TR7_KIND_DOUBLE)
      return *TR7_TO_DOUBLE(t);
   return (double)TR7_TO_INT(t);
}

/*
**************************************************************************
*
*/

#ifdef EMULATE_RINT
/* Round to nearest. Round to even if midway */
static double rint(double x)
{
   double fl = floor(x);
   double ce = ceil(x);
   double dfl = x - fl;
   double dce = ce - x;
   if (dfl > dce) {
      return ce;
   }
   else if (dfl < dce) {
      return fl;
   }
   else {
      if (fmod(fl, 2.0) == 0.0) {       /* I imagine this holds */
         return fl;
      }
      else {
         return ce;
      }
   }
}
#endif

static int is_zero_double(double x)
{
   return x < DBL_MIN && x > -DBL_MIN;
}

/*
**************************************************************************
*
*/

/* compares a and b */
int tr7_cmp_num(tr7_t a, tr7_t b)
{
   tr7_int_t ia, ib;
   double da, db;
   switch (TR7_KIND(a)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      ia = TR7_TO_INT(a);
      switch (TR7_KIND(b)) {
      case TR7_KIND_EINT:
      case TR7_KIND_OINT:
         ib = TR7_TO_INT(b);
         return ia == ib ? TR7_CMP_EQUAL : ia < ib ? TR7_CMP_LESSER : TR7_CMP_GREATER;
      case TR7_KIND_DOUBLE:
         da = (double)ia;
         db = *TR7_TO_DOUBLE(b);
         break;
      default:
         return 0;
      }
      break;
   case TR7_KIND_DOUBLE:
      da = *TR7_TO_DOUBLE(a);
      switch (TR7_KIND(b)) {
      case TR7_KIND_EINT:
      case TR7_KIND_OINT:
         db = (double)TR7_TO_INT(b);
         break;
      case TR7_KIND_DOUBLE:
         db = *TR7_TO_DOUBLE(b);
         break;
      default:
         return 0;
      }
      break;
   default:
      return 0;
   }
   return da == db ? TR7_CMP_EQUAL : da < db ? TR7_CMP_LESSER : da > db ? TR7_CMP_GREATER : 0;
}

/*
**************************************************************************
*
*/

typedef struct tr7_num tr7_num_t;

struct tr7_num {
   unsigned is_int: 1;
   union {
      tr7_int_t _int;
      double    _double;
   } value;
};

/* n := i */
void tr7_num_set_int(tr7_num_t *n, tr7_int_t i)
{
   n->is_int = 1;
   n->value._int = i;
}

/* n := i */
void tr7_num_set_double(tr7_num_t *n, double d)
{
   n->is_int = 0;
   n->value._double = d;
}

/* n := t */
void tr7_num_set(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_set_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_set_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      tr7_num_set_double(n, NAN);
      break;
   }
}

/* num(t) */
tr7_num_t tr7_num(tr7_t t)
{
   tr7_num_t n;
   tr7_num_set(&n, t);
   return n;
}

/* get the tr7_t for the number n */
tr7_t tr7_num_get(tr7_engine_t tsc, tr7_num_t *n)
{
   if (n->is_int)
      return tr7_from_int(tsc, n->value._int);
   return tr7_from_double(tsc, n->value._double);
}

/* n = min(n, i) */
void tr7_num_min_int(tr7_num_t *n, tr7_int_t i)
{
   double di;
   if (n->is_int) {
      if (i < n->value._int)
         n->value._int = i;
   }
   else {
      di = (double)i;
      if (di < n->value._double)
         n->value._double = di;
   }
}

/* n = min(n, d) */
void tr7_num_min_double(tr7_num_t *n, double d)
{
   double di;
   if (!n->is_int) {
      if (d < n->value._double)
         n->value._double = d;
   }
   else {
      di = (double)n->value._int;
      n->is_int = 0;
      n->value._double = d < di ? d : di;
   }
}

/* n = min(n, t) */
void tr7_num_min(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_min_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_min_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

/* n = max(n, i) */
void tr7_num_max_int(tr7_num_t *n, tr7_int_t i)
{
   double di;
   if (n->is_int) {
      if (i > n->value._int)
         n->value._int = i;
   }
   else {
      di = (double)i;
      if (di > n->value._double)
         n->value._double = di;
   }
}

/* n = max(n, d) */
void tr7_num_max_double(tr7_num_t *n, double d)
{
   double di;
   if (!n->is_int) {
      if (d > n->value._double)
         n->value._double = d;
   }
   else {
      di = (double)n->value._int;
      n->is_int = 0;
      n->value._double = d > di ? d : di;
   }
}

/* n = max(n, t) */
void tr7_num_max(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_max_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_max_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

/* n += i */
void tr7_num_add_int(tr7_num_t *n, tr7_int_t i)
{
   if (n->is_int)
      n->value._int += i;
   else
      n->value._double += (double)i;
}

/* n += d */
void tr7_num_add_double(tr7_num_t *n, double d)
{
   if (!n->is_int)
      n->value._double += d;
   else
      tr7_num_set_double(n, (double)n->value._int + d);
}

/* n += t */
void tr7_num_add(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_add_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_add_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

/* n -= i */
void tr7_num_sub_int(tr7_num_t *n, tr7_int_t i)
{
   if (n->is_int)
      n->value._int -= i;
   else
      n->value._double -= (double)i;
}

/* n -= d */
void tr7_num_sub_double(tr7_num_t *n, double d)
{
   if (!n->is_int)
      n->value._double -= d;
   else
      tr7_num_set_double(n, (double)n->value._int - d);
}

/* n -= t */
void tr7_num_sub(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_sub_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_sub_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

/* n *= i */
void tr7_num_mul_int(tr7_num_t *n, tr7_int_t i)
{
   if (n->is_int)
      n->value._int *= i;
   else
      n->value._double *= (double)i;
}

/* n *= d */
void tr7_num_mul_double(tr7_num_t *n, double d)
{
   if (!n->is_int)
      n->value._double *= d;
   else
      tr7_num_set_double(n, (double)n->value._int * d);
}

/* n *= t */
void tr7_num_mul(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_mul_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_mul_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

/* n /= d */
int tr7_num_div_double(tr7_num_t *n, double d)
{
   if (is_zero_double(d))
      return 0;
   if (n->is_int)
      tr7_num_set_double(n, (double)n->value._int / d);
   else
      n->value._double /= d;
   return 1;
}

/* n /= i */
int tr7_num_div_int(tr7_num_t *n, tr7_int_t i)
{
   return tr7_num_div_double(n, (double)i);
}

/* n /= t */
int tr7_num_div(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return tr7_num_div_int(n, TR7_TO_INT(t));
   case TR7_KIND_DOUBLE:
      return tr7_num_div_double(n, *TR7_TO_DOUBLE(t));
   default:
      return 0;
   }
}

/* (n, nn) := n truncate/ d */
int tr7_num_div_trunc_double(tr7_num_t *n, tr7_num_t *nn, double d)
{
   double v, q, r;
   if (is_zero_double(d))
      return 0;
   v = n->is_int ? (double)n->value._int : n->value._double;
   q = v / d;
   q = trunc(q);
   tr7_num_set_double(n, q);
   r = v - q * d;
   tr7_num_set_double(nn, r);
   return 1;
}

/* (n, nn) := n truncate/ i */
int tr7_num_div_trunc_int(tr7_num_t *n, tr7_num_t *nn, tr7_int_t i)
{
   tr7_int_t q, r;
   if (i == 0)
      return 0;
   if (!n->is_int)
      return tr7_num_div_trunc_double(n, nn, (double)i);
   q = n->value._int / i;
   r = n->value._int % i;
   n->value._int = q;
   tr7_num_set_int(nn, r);
   return 1;
}

/* (n, nn) := n truncate/ t */
int tr7_num_div_trunc(tr7_num_t *n, tr7_num_t *nn, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return tr7_num_div_trunc_int(n, nn, TR7_TO_INT(t));
   case TR7_KIND_DOUBLE:
      return tr7_num_div_trunc_double(n, nn, *TR7_TO_DOUBLE(t));
   default:
      return 0;
   }
}

/* (n, nn) := n floor/ d */
int tr7_num_div_floor_double(tr7_num_t *n, tr7_num_t *nn, double d)
{
   double v, q, r;
   if (is_zero_double(d))
      return 0;
   v = n->is_int ? (double)n->value._int : n->value._double;
   q = v / d;
   q = floor(q);
   tr7_num_set_double(n, q);
   r = v - q * d;
   tr7_num_set_double(nn, r);
   return 1;
}

/* (n, nn) := n floor/ i */
int tr7_num_div_floor_int(tr7_num_t *n, tr7_num_t *nn, tr7_int_t i)
{
   tr7_int_t q, r;
   if (i == 0)
      return 0;
   if (!n->is_int)
      return tr7_num_div_floor_double(n, nn, (double)i);
   q = n->value._int / i;
   r = n->value._int % i;
   if ((r ^ i) < 0) {
      r += i;
      q--;
   }
   n->value._int = q;
   tr7_num_set_int(nn, r);
   return 1;
}

/* (n, nn) := n floor/ t */
int tr7_num_div_floor(tr7_num_t *n, tr7_num_t *nn, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return tr7_num_div_floor_int(n, nn, TR7_TO_INT(t));
   case TR7_KIND_DOUBLE:
      return tr7_num_div_floor_double(n, nn, *TR7_TO_DOUBLE(t));
   default:
      return 0;
   }
}

static tr7_int_t gcd_int(tr7_int_t a, tr7_int_t b)
{
   tr7_int_t x;

   if (a < 0)
      a = -a;
   if (b < 0)
      b = -b;

   if (a != b) {
      if (a < b) {
         x = b;
         b = a;
         a = x;
      }

      while (b) {
         x = a % b;
         a = b;
         b = x;
      }
   }
   return a;
}

static double gcd_double(double a, double b)
{
   double x;

   if (a < 0)
      a = -a;
   if (b < 0)
      b = -b;

   if (a != b) {
      if (a < b) {
         x = b;
         b = a;
         a = x;
      }

      while (b) {
         x = fmod(a, b);
         a = b;
         b = x;
      }
   }
   return a;
}

/* n := gcd(n, i) */
void tr7_num_gcd_int(tr7_num_t *n, tr7_int_t i)
{
   if (n->is_int)
      n->value._int = gcd_int(i, n->value._int);
   else
      n->value._double = gcd_double((double)i, n->value._double);
}

/* n := gcd(n, d) */
void tr7_num_gcd_double(tr7_num_t *n, double d)
{
   if (!n->is_int)
      n->value._double = gcd_double(d, n->value._double);
   else
      tr7_num_set_double(n, gcd_double(d, (double)n->value._int));
}

/* n := gcd(n, t) */
void tr7_num_gcd(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_gcd_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_gcd_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

static tr7_int_t lcm_int(tr7_int_t a, tr7_int_t b)
{
   if (a == 0 || b == 0)
      return 0;

   if (a < 0)
      a = -a;
   if (b < 0)
      b = -b;

   return (a / gcd_int(a, b)) * b;
}

static double lcm_double(double a, double b)
{
   if (a == 0 || b == 0)
      return 0;

   if (a < 0)
      a = -a;
   if (b < 0)
      b = -b;

   return (a / gcd_double(a, b)) * b;
}

/* n := lcm(n, i) */
void tr7_num_lcm_int(tr7_num_t *n, tr7_int_t i)
{
   if (n->is_int)
      n->value._int = lcm_int(i, n->value._int);
   else
      n->value._double = lcm_double((double)i, n->value._double);
}

/* n := lcm(n, d) */
void tr7_num_lcm_double(tr7_num_t *n, double d)
{
   if (!n->is_int)
      n->value._double = lcm_double(d, n->value._double);
   else
      tr7_num_set_double(n, lcm_double(d, (double)n->value._int));
}

/* n := lcm(n, t) */
void tr7_num_lcm(tr7_num_t *n, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      tr7_num_lcm_int(n, TR7_TO_INT(t));
      break;
   case TR7_KIND_DOUBLE:
      tr7_num_lcm_double(n, *TR7_TO_DOUBLE(t));
      break;
   default:
      break;
   }
}

static tr7_int_t exsqrt_int(tr7_int_t x, tr7_int_t *pr)
{
   tr7_int_t i, i2, r, ii;

   i = 1 + (x >> 1);
   for (;;) {
      i2 = i * i;
      r = x - i2;
      ii = i << 1;
      if (0 <= r && r <= ii) {
         *pr = r;
         return i;
      }
      i = (i2 + x) / ii;
   }
}

static double exsqrt_double(double x, double *pr)
{
   double r = floor(sqrt(x));
   *pr = x - r * r;
   return r;
}

/* (n, nn) := exact sqrt(i) */
int tr7_num_exact_sqrt_int(tr7_num_t *n, tr7_num_t *nn, tr7_int_t i)
{
   tr7_int_t a, b;
   if (i < 0)
      return 0;
   a = exsqrt_int(i, &b);
   tr7_num_set_int(n, a);
   tr7_num_set_int(nn, b);
   return 1;
}

/* (n, nn) := exact sqrt(t) */
int tr7_num_exact_sqrt_double(tr7_num_t *n, tr7_num_t *nn, double d)
{
   double a, b;
   if (d < 0)
      return 0;
   a = exsqrt_double(d, &b);
   tr7_num_set_double(n, a);
   tr7_num_set_double(nn, b);
   return 1;
}


/* (n, nn) := exact sqrt(t) */
int tr7_num_exact_sqrt(tr7_num_t *n, tr7_num_t *nn, tr7_t t)
{
   switch (TR7_KIND(t)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return tr7_num_exact_sqrt_int(n, nn, TR7_TO_INT(t));
   case TR7_KIND_DOUBLE:
      return tr7_num_exact_sqrt_double(n, nn, *TR7_TO_DOUBLE(t));
   default:
      return 0;
   }
   return 0;
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

static tr7_t oblist_initial_value(tr7_engine_t tsc, int size)
{
   return tr7_make_vector_fill(tsc, TR7_NIL, size);
}

static void oblist_add_one_predefined_symbol(tr7_engine_t tsc, tr7_t symbol)
{
   const uint8_t *string = TR7_CONTENT_SYMBOL(symbol);
   size_t length = TR7_LENGTH_SYMBOL(symbol);
   int location = utf8str_hash(string, length) % TR7_LENGTH_VECTOR(tsc->oblist);
   tr7_t x = TR7_ITEM_VECTOR(tsc->oblist, location);
   TR7_ITEM_VECTOR(tsc->oblist, location) = tr7_cons(tsc, symbol, x);
}

/* init the list of predefined symbols */
static void oblist_add_predefined_symbols(tr7_engine_t tsc)
{
   int i, n = sizeof predefined_symbols / sizeof *predefined_symbols;
   for(i = 0 ; i < n ; i++) {
#if SHOW_OPCODES
      if (memcmp(predefined_symbols[i].content, OPCODE_PREFIX, strlen(OPCODE_PREFIX)))
#endif
         oblist_add_one_predefined_symbol(tsc, TR7_FROM_SYMBOL(&predefined_symbols[i]));
   }
}

static tr7_t oblist_get_length(tr7_engine_t tsc, const char *name, size_t length, int copy, int create)
{
   int location;
   tr7_t x, h, s;

   location = utf8str_hash((const uint8_t*)name, length) % TR7_LENGTH_VECTOR(tsc->oblist);
   h = x = TR7_ITEM_VECTOR(tsc->oblist, location);
   for ( ; !TR7_IS_NIL(x) ; x = TR7_CDR(x)) {
      s = TR7_CAR(x);
      if (strcmp(name, tr7_symbol_string(s)) == 0)
         return s;
   }
   if (!create)
      s = TR7_NIL;
   else {
      s = (copy ? tr7_make_symbol_copy_length : tr7_make_symbol_static_length)(tsc, name, length);
      TR7_ITEM_VECTOR(tsc->oblist, location) = tr7_cons(tsc, s, h);
   }
   return s;
}

/* get new symbol */
tr7_t tr7_get_symbol(tr7_engine_t tsc, const char *name, int copy)
{
   return tr7_get_symbol_length(tsc, name, strlen(name), copy);
}

tr7_t tr7_get_symbol_length(tr7_engine_t tsc, const char *name, size_t length, int copy)
{
   return oblist_get_length(tsc, name, length, copy, 1);
}

static tr7_t oblist_all_symbols(tr7_engine_t tsc)
{
   tr7_uint_t i;
   tr7_t x;
   tr7_t ob_list = TR7_NIL;

   for (i = 0; i < TR7_LENGTH_VECTOR(tsc->oblist); i++) {
      for (x = TR7_ITEM_VECTOR(tsc->oblist, i); !TR7_IS_NIL(x); x = TR7_CDR(x)) {
         ob_list = tr7_cons(tsc, TR7_CAR(x), ob_list);
      }
   }
   return ob_list;
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
* ========== Environment implementation  ==========
*
* In this implementation, each frame of the environment may be
* a hash table: a vector of alists hashed by variable name.
* In practice, we use a vector only for the initial frame;
* subsequent frames are too small and transient for the lookup
* speed to out-weigh the cost of making a new vector.
*/

tr7_t mk_environment(tr7_engine_t tsc, tr7_t lower, int len)
{
   tr7_environment_t env = get_cells(tsc, 2 + (len > 0 ? len : 1), 0);
   env->head = TR7_MAKE_HEAD(len, TR7_HEAD_KIND_ENVIRONMENT);
   env->lower = lower;
   while (len)
      env->items[--len] = TR7_NIL;
   return push_recent_cell(tsc, env);
}

static void new_environment(tr7_engine_t tsc, tr7_t lower)
{
   tsc->envir = mk_environment(tsc, lower, 1);
}

#define hashptr(p)  ((int)((((uintptr_t)(p))>>4) & 0x7ffffff))

static int environment_get(tr7_t env, tr7_t symbol, tr7_t *value)
{
   tr7_t y;
   int count, location;
   tr7_environment_t e;
   tr7_vector_t vec;

   for ( ; !TR7_IS_NIL(env) ; env = e->lower) {
      e = TR7_TO_ENVIRONMENT(env);
      count = (int)TR7_HEAD_VALUE(e->head);
      location = count == 1 ? 0 : hashptr(symbol) % count;
      y = e->items[location];
      while (!TR7_IS_NIL(y)) {
         vec = TR7_TO_VECTOR(y);
         if (TR7EQ(vec->items[1], symbol)) {
            *value = vec->items[2];
            return 1;
         }
         y = vec->items[0];
      }
   }
   return 0;
}

static int environment_set(tr7_engine_t tsc, tr7_t symbol, tr7_t value, tr7_t env, int create)
{
   tr7_t h, y;
   int count, location;
   tr7_environment_t e;
   tr7_vector_t vec;

   for ( ; !TR7_IS_NIL(env) ; env = e->lower) {
      e = TR7_TO_ENVIRONMENT(env);
      count = (int)TR7_HEAD_VALUE(e->head);
      location = count == 1 ? 0 : hashptr(symbol) % count;
      /* TODO use assq_pair */
      y = h = e->items[location];
      while (!TR7_IS_NIL(y)) {
         vec = TR7_TO_VECTOR(y);
         if (TR7EQ(vec->items[1], symbol)) {
            vec->items[2] = value;
            return 1;
         }
         y = vec->items[0];
      }
      if (create) {
         vec = get_cells(tsc, 4, 0);
         if (vec == NULL)
            break;
         vec->head = TR7_MAKE_HEAD(3, TR7_HEAD_KIND_VECTOR);
         vec->items[0] = h;
         vec->items[1] = symbol;
         vec->items[2] = value;
         e->items[location] = TR7_FROM_VECTOR(vec);
         return 1;
      }
   }
   return 0;
}

/**
* enumerate all the values of the given environment
* and call the function 'fun' with closure, name and value
* until it returns a non zero value.
*/
static int environment_enumerate(tr7_t env, int fun(void*, tr7_t, tr7_t), void *closure)
{
   tr7_t y;
   int count, idx, sts;
   tr7_environment_t e;
   tr7_vector_t vec;

   for (sts = 0 ; sts == 0 && !TR7_IS_NIL(env) ; env = e->lower) {
      e = TR7_TO_ENVIRONMENT(env);
      count = (int)TR7_HEAD_VALUE(e->head);
      for (idx = 0 ; !sts && idx < count ; idx++) {
         y = e->items[idx];
         while (sts == 0 && !TR7_IS_NIL(y)) {
            vec = TR7_TO_VECTOR(y);
            sts = fun(closure, vec->items[1], vec->items[2]);
            y = vec->items[0];
         }
      }
   }
   return sts;
}

static void new_slot_in_env(tr7_engine_t tsc, tr7_t variable, tr7_t value)
{
   environment_set(tsc, variable, value, tsc->envir, 1);
}
/*
**************************************************************************
* SECTION
* --------------
*
*/

static opidx_t index_of_opdesc(const opdesc_t *op)
{
   return (opidx_t)(op - ((opdesc_t*)&operations));
}

static const opdesc_t *opdesc_of_index(opidx_t index)
{
   return ((opdesc_t*)&operations) + index;
}

static const char *get_opname(tr7_uint_t index, const char *defname)
{
   const opdesc_t *ifo = opdesc_of_index((unsigned)index);
   return ifo->hassymbol ? (const char*)predefined_symbols[ifo->symbolidx].content : defname;
}

static void add_builtin_op(tr7_engine_t tsc, symidx_t symbolidx, opidx_t opnum, int issyntax)
{
   tr7_t op = issyntax ? TR7_FROM_SYNTAX(opnum) : TR7_FROM_OPER(opnum);
   tr7_t sb = SYMBOL_AT(symbolidx);
   new_slot_in_env(tsc, sb, op);
}
/*
* search the builtin library of name
*/
static const libdef_t *search_builtin_lib(const char *name)
{
   const libdef_t *it = builtin_libs;
   const libdef_t *end = &builtin_libs[sizeof builtin_libs / sizeof builtin_libs[0]];
   for( ; it != end ; it++)
      if (0 == strcmp(name, it->name))
         return it;
   return 0;
}
/*
* record operators of a builtin library
*/
static void add_builtin_lib(tr7_engine_t tsc, const libdef_t *slib)
{
   const opdesc_t *cur = slib->head;
   const opdesc_t *end = &cur[slib->count];
   opidx_t opidx = index_of_opdesc(cur);

   for ( ; cur != end ; opidx++, cur++)
      if (cur->named)
         add_builtin_op(tsc, (symidx_t)cur->symbolidx, opidx, cur->syntax);

   /* aliases for (scheme base) */
   if (slib->head == (const opdesc_t*)&operations.lib_base) {
      add_builtin_op(tsc, SY(CALL_CC), OP(CALLCC), 0);
      add_builtin_op(tsc, SY(LAMBDA_CHAR), OP(LAMBDA), 1);
   }
}
/*
* record the predefined operators
*/
static void init_opers(tr7_engine_t tsc)
{
   const libdef_t *it = builtin_libs;
   const libdef_t *end = &builtin_libs[sizeof builtin_libs / sizeof builtin_libs[0]];
   for( ; it != end ; it++)
      add_builtin_lib(tsc, it);
}
/*
**************************************************************************
* SECTION
* --------------
*
*/


/* ========== handling bindings ========== */

static int is_formals_spec_ok(tr7_t formals)
{
   while (TR7_IS_PAIR(formals)) {
      if (!TR7_IS_SYMBOL(TR7_CAR(formals)))
         return 0;
      formals = TR7_CDR(formals);
   }
   return TR7_IS_NIL(formals) || TR7_IS_SYMBOL(formals);
}

static int is_binding_spec_ok(tr7_t binding, int values)
{
   tr7_t x, y;

   if (!TR7_IS_PAIR(binding))
      return 0;

   x = TR7_CAR(binding);
   while (TR7_IS_PAIR(x)) {
      y = TR7_CAR(x);
      if (!TR7_IS_PAIR(y) || !TR7_IS_PAIR(TR7_CDR(y)) || !TR7_IS_NIL(TR7_CDDR(y)))
         return 0;
      y = TR7_CAR(y);
      if (!(values ? is_formals_spec_ok(y) : TR7_IS_SYMBOL(y)))
         return 0;
      x = TR7_CDR(x);
   }
   if (!TR7_IS_NIL(x))
      return 0;

   return 1;
}

static void predeclare_binding_rec(tr7_engine_t tsc, tr7_t binding, tr7_t env)
{
   tr7_t y = binding;
   while (TR7_IS_PAIR(y)) {
      environment_set(tsc, TR7_CAAR(y), TR7_FALSE, env, 1);
      y = TR7_CDR(y);
   }
}

enum bind_list_status {
   BIND_LIST_OK,
   BIND_LIST_MORE,
   BIND_LIST_MISS,
   BIND_LIST_BAD
};

static enum bind_list_status bind_list(tr7_engine_t tsc, tr7_t names, tr7_t values)
{
   while (TR7_IS_PAIR(names)) {
      if (!TR7_IS_PAIR(values))
         return BIND_LIST_MISS;
      new_slot_in_env(tsc, TR7_CAR(names), TR7_CAR(values));
      names = TR7_CDR(names);
      values = TR7_CDR(values);
   }
   if (TR7_IS_SYMBOL(names))
      new_slot_in_env(tsc, names, values);
   else if (!TR7_IS_NIL(names)) /* || !TR7_IS_NIL(values)) */
      return BIND_LIST_BAD;
   else if (!TR7_IS_NIL(values))
      return BIND_LIST_MORE;
   return BIND_LIST_OK;
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
*/
tr7_t tr7_make_C_func(tr7_engine_t tsc, int min_args, int max_args, const char *typargs, tr7_C_func_t func, void *closure)
{
   tr7_ff_t ff;

   if (min_args < 0 || min_args > TR7_FF_NARGS_MAXIMUM
    || (max_args >= 0 && (max_args < min_args || max_args > TR7_FF_NARGS_MAXIMUM))
    || (max_args < 0 && (max_args + min_args < -1 || max_args + min_args > 0 || -max_args > TR7_FF_NARGS_MAXIMUM)))
      return TR7_NIL;

   ff = GET_CELLS(tsc, ff, 0);
   if (!ff)
      return TR7_NIL;

   ff->head = TR7_MAKE_FF_HEAD(max_args, min_args);
   ff->func = func;
   ff->closure = closure;
   ff->typargs = typargs;
   return push_recent_cell(tsc, ff);
}


static eval_status_t exec_ff(tr7_engine_t tsc, tr7_t args, tr7_ff_t ff)
{
   tr7_t r, argv[TR7_FF_NARGS_MAXIMUM];
   int i, min_args, max_args;

   min_args = TR7_FF_NARGS_MIN(ff);
   max_args = TR7_FF_NARGS_MAX(ff);
   i = check_args(tsc, args, min_args, max_args, ff->typargs, argv);
   if (i < 0)
      return do_raise(tsc, tsc->value, 0);
   while (i < max_args)
       argv[i++] = TR7_VOID;
   r = ff->func(tsc, i, argv, ff->closure);
   return s_return_single(tsc, r);
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
*/

/* make closure. */
static tr7_t mk_closure(tr7_engine_t tsc, tr7_t expr, tr7_t env, int kind)
{
   tr7_closure_t x = GET_CELLS(tsc, x, 0);

   x->head = kind;
   x->expr = expr;
   x->env = env;
   return push_recent_cell(tsc, x);
}

/* make lambda. */
static tr7_t mk_lambda(tr7_engine_t tsc, tr7_t expr, tr7_t env)
{
   return mk_closure(tsc, expr, env, TR7_HEAD_KIND_LAMBDA);
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
*/

tr7_t mk_macro(tr7_engine_t tsc, tr7_t p)
{
   if (TR7_IS_LAMBDA(p))
      TR7_TO_CLOSURE(p)->head = TR7_HEAD_KIND_MACRO;
   return p;
}

int tr7_is_proc(tr7_t t)
{
     /*--
      * continuation should be procedure by the example
      * (call-with-current-continuation procedure?) ==> #t
      * in R^3 report sec. 6.9
      */
      return TR7_IS_OPER(t)
          || TR7_IS_LAMBDA(t)
          || TR7_IS_CONTINUATION(t)
          || TR7_IS_FF(t);
}

/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
* Create a parameter object
*/
static tr7_t mk_parameter(tr7_engine_t tsc, tr7_t init, tr7_t converter)
{
   tr7_parameter_t p = GET_CELLS(tsc, p, 0);

   p->head = TR7_HEAD_KIND_PARAMETER;
   p->value = init;
   p->converter = converter;
   return push_recent_cell(tsc, p);
}
/*
**************************************************************************
* SECTION
* --------------
*
*/

/*
**************************************************************************
*
* make continuation
*/

static tr7_t mk_continuation(tr7_engine_t tsc, tr7_t dump)
{
   tr7_continuation_t x;
#if OPTIMIZE_NO_CALLCC
   tr7_t d;
   tr7_cell_t c;
   tr7_uint_t op;

   d = dump;
   while (!TR7_IS_NIL(d)) {
      c = TR7_TO_CELL(d);
      d = TR7_CELL_TO_DUMPABLE(c)->dump;
      if (TR7_CELL_IS_STACK(c)) {
         if (TR7_CELL_IS_IMMUTABLE_HEAD(c))
            break;
         op = TR7_CELL_UVALUE_HEAD(c);
         switch(op) {
         case OP(E1ARGS_NOCC): op = OP(E1ARGS); break;
         case OP(E2ARGS_NOCC): op = OP(E2ARGS); break;
         default: break;
         }
         TR7_CELL_HEAD(c) = TR7_HEAD_SET_IMMUTABLE(TR7_MAKE_HEAD(op, TR7_HEAD_KIND_STACK));
      }
   }
#endif
   x = GET_CELLS(tsc, x, 0);
   x->head = TR7_HEAD_KIND_CONTINUATION;
   x->dump = dump;
   return push_recent_cell(tsc, x);
}

/*
**************************************************************************
* SECTION PROMISE
* ---------------
*
*/
static int promise_was_forced(tr7_t p)
{
   return TR7_TO_PROMISE(p)->head & TR7_PROMISE_FLAG_FORCED;
}

static int promise_is_delay_force(tr7_t p)
{
   return TR7_TO_PROMISE(p)->head & TR7_PROMISE_FLAG_DELAY_FORCE;
}

tr7_t tr7_promise_value(tr7_t p)
{
   return TR7_TO_PROMISE(p)->code_or_value;
}

void tr7_promise_set_value(tr7_t p, tr7_t value)
{
   tr7_promise_t x = TR7_TO_PROMISE(p);
   x->head |= TR7_PROMISE_FLAG_FORCED;
   x->code_or_value = value;
   x->env = TR7_NIL;
}

static tr7_t promise_code(tr7_t p)
{
   return TR7_TO_PROMISE(p)->code_or_value;
}

static tr7_t promise_env(tr7_t p)
{
   return TR7_TO_PROMISE(p)->env;
}

static tr7_t mk_promise(tr7_engine_t tsc, int flags, tr7_t expr, tr7_t env)
{
   tr7_promise_t x = GET_CELLS(tsc, x, 0);

   x->head = TR7_HEAD_KIND_PROMISE | flags;
   x->code_or_value = expr;
   x->env = env;
   return push_recent_cell(tsc, x);
}

/*
**************************************************************************
* SECTION RECORD
* --------------
* The records are internally handled accordingly to the SRFI 136.
* A record descriptor is a record whose recid is itself.
*/
#define RECORD_RECID              0
#define RECORD_FIELD_OFFSET       1
#define RECORD_DESC_SELF          0
#define RECORD_DESC_NAME          1
#define RECORD_DESC_PARENT        2
#define RECORD_DESC_FIELD_COUNT   3
#define RECORD_DESC_FIELD_OFFSET  4
/*
* Return the record pointer if 'item' is a record descriptor, or otherwise NULL
*/
tr7_record_t tr7_as_record_desc(tr7_t item)
{
   tr7_record_t desc = TR7_AS_RECORD(item);
   return (desc && TR7_RECORD_ITEM(desc, RECORD_DESC_SELF) == item) ? desc : NULL;
}
/*
* Is 'item' a record descriptor?
*/
int tr7_is_record_desc(tr7_t item)
{
   return tr7_as_record_desc(item) != NULL;
}
/*
*/
static unsigned record_desc_field_count(tr7_t recdesc)
{
   return (unsigned)TR7_TO_INT(TR7_ITEM_RECORD(recdesc, RECORD_DESC_FIELD_COUNT));
}
/*
* Create a record descriptor of name and count fields, return it
*/
static tr7_t mk_record_desc(tr7_engine_t tsc, tr7_t name, tr7_t parent, unsigned count)
{
   unsigned idx, nrfields = count;
   unsigned len = RECORD_DESC_FIELD_OFFSET + 2 * count;
   tr7_record_t par, desc = get_cells(tsc, 1 + len, 0);
   if (!desc)
      return TR7_VOID;
   par = tr7_as_record_desc(parent);
   if (par != NULL)
      nrfields += record_desc_field_count(parent);
   else
      parent = TR7_FALSE;
   desc->head = TR7_MAKE_HEAD(len, TR7_HEAD_KIND_RECORD);
   TR7_RECORD_ITEM(desc, RECORD_DESC_SELF) = TR7_FROM_RECORD(desc);
   TR7_RECORD_ITEM(desc, RECORD_DESC_NAME) = name;
   TR7_RECORD_ITEM(desc, RECORD_DESC_PARENT) = parent;
   TR7_RECORD_ITEM(desc, RECORD_DESC_FIELD_COUNT) = TR7_FROM_INT(nrfields);
   for (idx = RECORD_DESC_FIELD_OFFSET ; idx < len ; idx++)
      TR7_RECORD_ITEM(desc, idx) = TR7_FALSE;

   return push_recent_cell(tsc, desc);
}
/*
* add a field to the record desciptor
*/
static int record_desc_put_field(tr7_engine_t tsc, tr7_t recdesc, unsigned index, tr7_t name, int mutable)
{
   tr7_record_t desc = TR7_TO_RECORD(recdesc);
   unsigned idx = RECORD_DESC_FIELD_OFFSET + 2 * index;
   if (idx >= TR7_RECORD_LENGTH(desc))
      return 0;
   TR7_RECORD_ITEM(desc, idx) = name;
   TR7_RECORD_ITEM(desc, idx + 1) = mutable ? TR7_TRUE : TR7_FALSE;
   return 1;
}
/*
* create an instance of the record type 'recdesc' and init it with 'init'
*/
static tr7_t mk_record_instance(tr7_engine_t tsc, tr7_t recdesc, tr7_t init)
{
   tr7_vector_t vec;
   unsigned idx;
   unsigned nrfld = record_desc_field_count(recdesc);
   unsigned count = nrfld + RECORD_FIELD_OFFSET;
   tr7_record_t rec = get_cells(tsc, 1 + count, 0);
   if (rec == NULL)
      return TR7_VOID;

   /* Record it as a record so that gc understands it. */
   rec->head = TR7_MAKE_HEAD(count, TR7_HEAD_KIND_RECORD);
   TR7_RECORD_ITEM(rec, RECORD_RECID) = recdesc;

   vec = TR7_AS_VECTOR(init);
   if (vec) {
      if (TR7_VECTOR_LENGTH(vec) != nrfld)
         return TR7_VOID;
      for (idx = RECORD_FIELD_OFFSET ; idx < count ; idx++)
         TR7_RECORD_ITEM(rec, idx) = TR7_VECTOR_ITEM(vec, idx - RECORD_FIELD_OFFSET);
   }
   else {
      for (idx = RECORD_FIELD_OFFSET ; idx < count && TR7_IS_PAIR(init) ; idx++, init = TR7_CDR(init))
         TR7_RECORD_ITEM(rec, idx) = TR7_CAR(init);
      if (idx < count || !TR7_IS_NIL(init))
         return TR7_VOID;
   }
   return push_recent_cell(tsc, rec);
}

static tr7_t mk_record_constructor(tr7_engine_t tsc, tr7_t recdesc)
{
   return mk_lambda(tsc, TR7_LIST2(tsc, SYMBOL(UNDERSCORE), TR7_CONS3(tsc, TR7_FROM_OPER(OP(TYPCON)), recdesc, SYMBOL(UNDERSCORE))), tsc->envir);
}

static tr7_t mk_record_predicate(tr7_engine_t tsc, tr7_t recdesc)
{
   return mk_lambda(tsc, TR7_LIST2(tsc, SYMBOL(UNDERSCORE), TR7_CONS3(tsc, TR7_FROM_OPER(OP(TYPP)), recdesc, SYMBOL(UNDERSCORE))), tsc->envir);
}

static tr7_t mk_record_accessor(tr7_engine_t tsc, tr7_t recdesc, int idx)
{
   return mk_lambda(tsc, TR7_LIST2(tsc, SYMBOL(UNDERSCORE), TR7_CONS4(tsc, TR7_FROM_OPER(OP(TYPACC)), recdesc, TR7_FROM_INT(idx + RECORD_FIELD_OFFSET), SYMBOL(UNDERSCORE))), tsc->envir);
}

static tr7_t mk_record_modifier(tr7_engine_t tsc, tr7_t recdesc, int idx)
{
   return mk_lambda(tsc, TR7_LIST2(tsc, SYMBOL(UNDERSCORE), TR7_CONS4(tsc, TR7_FROM_OPER(OP(TYPMOD)), recdesc, TR7_FROM_INT(idx + RECORD_FIELD_OFFSET), SYMBOL(UNDERSCORE))), tsc->envir);
}
/*
* Is 'item' a record?
*/
int tr7_is_record(tr7_t item)
{
   tr7_record_t rec = TR7_AS_RECORD(item);
   return rec && TR7_RECORD_ITEM(rec, RECORD_RECID) != item;
}
/*
* Get the record descriptor of the record 'item' or TR7_VOID when not a record
*/
tr7_t tr7_record_desc(tr7_t item)
{
   tr7_record_t rec = TR7_AS_RECORD(item);
   return (rec && TR7_RECORD_ITEM(rec, RECORD_RECID) != item) ? TR7_RECORD_ITEM(rec, RECORD_RECID) : TR7_VOID;
}
/*
* Returns the record structure of item or 0 if its type doesn't match recdesc
*/
tr7_record_t tr7_as_record_cond(tr7_t item, tr7_t recdesc)
{
   tr7_t desc;
   tr7_record_t recdsc, rec = TR7_AS_RECORD(item);
   if (item == recdesc || rec == NULL)
      return NULL;
   desc = TR7_RECORD_ITEM(rec, RECORD_RECID);
   if (desc == item)
      return NULL; /* a record descriptor */
   while (desc != recdesc) {
      recdsc = TR7_AS_RECORD(desc);
      if (!recdsc)
         return NULL;
      desc = TR7_RECORD_ITEM(recdsc, RECORD_DESC_PARENT);
   }
   return rec;
}
/*
* Returns 1 if record structure of item match the type recdesc or 0 otherwise
*/
int tr7_is_record_type(tr7_t item, tr7_t recdesc)
{
   return tr7_as_record_cond(item, recdesc) != NULL;
}

tr7_t tr7_record_desc_name(tr7_t item)
{
   tr7_record_t rec = TR7_AS_RECORD(item);
   return (rec && TR7_RECORD_ITEM(rec, RECORD_RECID) == item) ? TR7_RECORD_ITEM(rec, RECORD_DESC_NAME) : TR7_VOID;
}

tr7_t tr7_record_desc_parent(tr7_t item)
{
   tr7_record_t rec = TR7_AS_RECORD(item);
   return (rec && TR7_RECORD_ITEM(rec, RECORD_RECID) == item) ? TR7_RECORD_ITEM(rec, RECORD_DESC_PARENT) : TR7_VOID;
}

const char *tr7_record_desc_name_string(tr7_t item)
{
   tr7_t name = tr7_record_desc_name(item);
   return TR7_IS_VOID(name) ? "?" : tr7_symbol_string(name);
}

const char *tr7_record_typename_string(tr7_t item)
{
   tr7_record_t rec = TR7_AS_RECORD(item);
   if (rec && TR7_RECORD_ITEM(rec, RECORD_RECID) != item)
      return tr7_record_desc_name_string(TR7_RECORD_ITEM(rec, RECORD_RECID));
   return "?";
}

#if USE_SRFI_136
static tr7_t record_desc_fields_srfi136(tr7_engine_t tsc, tr7_t recdesc)
{
   unsigned idx, fld, parfld;
   tr7_t nam, mut, acc, mod, par, resu = TR7_NIL;
   tr7_record_t rec = tr7_as_record_desc(recdesc);
   if (rec != NULL) {
      fld = (unsigned)TR7_TO_INT(TR7_RECORD_ITEM(rec, RECORD_DESC_FIELD_COUNT));
      par = TR7_RECORD_ITEM(rec, RECORD_DESC_PARENT);
      parfld = TR7_IS_FALSE(par) ? 0 : record_desc_field_count(par);
      while (fld > parfld) {
         idx = RECORD_DESC_FIELD_OFFSET + 2 * (--fld - parfld);
         nam = TR7_RECORD_ITEM(rec, idx);
         mut = TR7_RECORD_ITEM(rec, idx + 1);
         acc = mk_record_accessor(tsc, recdesc, fld);
         mod = TR7_IS_FALSE(mut) ? mut : mk_record_modifier(tsc, recdesc, fld);
         resu = tr7_cons(tsc, TR7_LIST3(tsc, nam, acc, mod), resu);
      }
   }
   return resu;
}

/* implementation of (make-record-type-descriptor ...) */
static tr7_t make_record_type_srfi136(tr7_engine_t tsc, tr7_t type, tr7_t parent, tr7_t fields)
{
   int mutable;
   int field_count;   /* count of fields */
   int idx;
   tr7_t recdesc;
   tr7_t iter, mut;
   tr7_t field_name;  /* name of a field */

   /* check fields */
   field_count = tr7_list_length(fields);
   if (field_count < 0) {
      /* invalid field list */
      return TR7_VOID;
   }

   /* create the record descriptor */
   recdesc = mk_record_desc(tsc, type, parent, field_count);

   /* iterate over fields */
   for (idx = 0 ; !TR7_IS_NIL(fields) ; idx++, fields = TR7_CDR(fields)) {
      iter = TR7_CAR(fields);
      if (TR7_IS_SYMBOL(iter)) {
         field_name = iter;
         mutable = 1;
      }
      else {
         mut = TR7_CAR(iter);
         iter = TR7_CDR(iter);
         if (!TR7_IS_PAIR(iter)) {
            /* invalid mutability specifier */
            return TR7_VOID;
         }
         field_name = TR7_CAR(iter);
         if (!TR7_IS_SYMBOL(field_name)) {
            /* bad field spec */
            return TR7_VOID;
         }
         if (TR7EQ(mut, SYMBOL(MUTABLE)))
            mutable = 1;
         else if (TR7EQ(mut, SYMBOL(IMMUTABLE)))
            mutable = 0;
         else {
            /* invalid mutability specifier */
            return TR7_VOID;
         }
      }
      if (!record_desc_put_field(tsc, recdesc, idx, field_name, mutable)) {
         /* unexpected error */
         return TR7_VOID;
      }
   }
   return recdesc;
}
#endif

/* implementation of (define-record-type ...) */
static int define_record_type(tr7_engine_t tsc, tr7_t type, tr7_t constructor, tr7_t predicate, tr7_t fields)
{
   int rc;
   int cnt;
   int parent_field_count;   /* count of fields */
   int field_count;   /* count of fields */
   int idx;
   tr7_t recdesc;
   tr7_t iter;
   tr7_t type_name;   /* name of the type */
   tr7_t parent_name; /* name of the parent type */
   tr7_t parent;      /* parent type */
   tr7_t constr_flds; /* head of constructor fields */
   tr7_t field_name;  /* name of a field */
   tr7_t access_name; /* accessor name */
   tr7_t modif_name;  /* modifier name */
   tr7_pair_t pairs[3];

   rc = 0;

   /* scan the type */
   if (TR7_IS_SYMBOL(type)) {
      type_name = type;
      parent = TR7_FALSE;
      parent_field_count = 0;
   }
   else if (tr7_get_list_pairs(type, 3, pairs) != 2
    || !TR7_IS_SYMBOL(pairs[0]->car)
    || !(TR7_IS_SYMBOL(pairs[1]->car) || TR7_IS_FALSE(pairs[1]->car))
    || !TR7_IS_NIL(pairs[1]->cdr)) {
      /* wrong type spec */
      goto end;
   }
   else {
      type_name = pairs[0]->car;
      parent_name = pairs[1]->car;
      if (TR7_IS_FALSE(parent_name)) {
         parent = TR7_FALSE;
         parent_field_count = 0;
      }
      else if (environment_get(tsc->envir, parent_name, &parent)
       && tr7_is_record_desc(parent))
         parent_field_count = record_desc_field_count(parent);
      else {
         /* wrong parent */
         goto end;
      }
   }

   /* check fields */
   field_count = tr7_list_length(fields);
   if (field_count < 0) {
      /* invalid field list */
      goto end;
   }

   /* scan the predicate */
   if (!TR7_IS_SYMBOL(predicate) && !TR7_IS_FALSE(predicate))
      goto end;

   /* scan the constructor */
   constr_flds = TR7_NIL;
   if (!TR7_IS_FALSE(constructor) && !TR7_IS_SYMBOL(constructor)) {
      if (!TR7_IS_PAIR(constructor)
         || !TR7_IS_SYMBOL(TR7_CAR(constructor))) {
         /* invalid constructor spec */
         goto end;
      }
      else {
         constr_flds = TR7_CDR(constructor);
         constructor = TR7_CAR(constructor);
         /* check that constructor arguments are symbols */
         iter = constr_flds;
         idx = 0;
         while (idx < parent_field_count && TR7_IS_PAIR(iter)
            && TR7_IS_SYMBOL(TR7_CAR(iter))
            && !tr7_memq_pair(TR7_CAR(iter), TR7_CDR(iter))) {
            iter = TR7_CDR(iter);
            idx++;
         }
         if (idx != parent_field_count) {
            /* not enough fields or duplicated one */
            goto end;
         }
         constr_flds = iter;
      }
   }

   /* create the record descriptor */
   recdesc = mk_record_desc(tsc, type_name, parent, field_count);
   new_slot_in_env(tsc, type_name, recdesc);

   /* create the constructor */
   if (!TR7_IS_FALSE(constructor))
      new_slot_in_env(tsc, constructor, mk_record_constructor(tsc, recdesc));

   /* create the predicate */
   if (!TR7_IS_FALSE(predicate))
      new_slot_in_env(tsc, predicate, mk_record_predicate(tsc, recdesc));

   /* iterate over fields */
   for (idx = 0 ; idx < field_count ; idx++) {
      if (TR7_IS_PAIR(constr_flds)) {
         field_name = TR7_CAR(constr_flds);
         constr_flds = TR7_CDR(constr_flds);
         if (tr7_memq_pair(field_name, constr_flds)) {
            /* duplicated field */
            goto end;
         }
         iter = tr7_assq(field_name, fields);
         if (TR7_IS_FALSE(iter)) {
            /* no field of name */
            goto end;
         }
      }
      else if (TR7_IS_PAIR(fields)) {
         iter = TR7_CAR(fields);
         fields = TR7_CDR(fields);
         if (!TR7_IS_PAIR(iter)) {
            /* bad field spec */
            goto end;
         }
         field_name = TR7_CAR(iter);
         if (!TR7_IS_SYMBOL(field_name)
          || tr7_assq_pair(field_name, fields)) {
            /* bad field spec */
            goto end;
         }
      }
      else {
         /* missing field in constructor */
         goto end;
      }

      cnt = tr7_get_list_pairs(TR7_CDR(iter), 3, pairs);
      if (cnt == 0 || cnt == 3 || !TR7_IS_NIL(pairs[cnt - 1]->cdr)) {
         /* wrong field spec */
         goto end;
      }

      if (!record_desc_put_field(tsc, recdesc, idx, field_name, cnt == 2)) {
         /* unexpected error */
         goto end;
      }

      access_name = pairs[0]->car;
      if (!TR7_IS_SYMBOL(access_name)) {
         /* accessor not a symbol */
         goto end;
      }
      new_slot_in_env(tsc, access_name, mk_record_accessor(tsc, recdesc, idx + parent_field_count));

      /* optionaly creates the modifier */
      if (cnt == 2) {
         modif_name = pairs[1]->car;
         if (!TR7_IS_SYMBOL(modif_name)) {
            /* modifier not a symbol */
            goto end;
         }
         new_slot_in_env(tsc, modif_name, mk_record_modifier(tsc, recdesc, idx + parent_field_count));
      }
   }
   rc = 1;
end:
   return rc;
}

/*
**************************************************************************
*
*/

#define transform_name(t) tr7_symbol_string(TR7_CAR(TR7_TO_TRANSFORM(t)->literals))

static tr7_t mk_transform(tr7_engine_t tsc, tr7_t name, tr7_t ellipsis, tr7_t literals, tr7_t rules, tr7_t envir)
{

   /* record the rule */
   tr7_t litt = tr7_cons(tsc, name, literals);
   tr7_transform_t t = GET_CELLS(tsc, t, 0);
   if (t == NULL)
      return TR7_NIL;

   t->head = TR7_HEAD_KIND_TRANSFORM;
   t->ellipsis = TR7_IS_FALSE(tr7_memq(ellipsis, literals)) ? ellipsis : TR7_NIL;
   t->literals = litt;
   t->rules = rules;
   t->env = envir;

   return push_recent_cell(tsc, t);
}

/*
**************************************************************************
*
*/

static void port_rep_finalize(tr7_engine_t tsc, port_t *pt)
{
   if (pt->flags & port_file) {
      if (pt->flags & port_closeit)
         fclose(pt->rep.stdio.file);
      if (pt->rep.stdio.filename)
         tsc->free(pt->rep.stdio.filename);
   }
   else if (pt->flags & port_ownbuf)
      tsc->free(pt->rep.inmem.start);
}

static void port_rep_close(tr7_engine_t tsc, port_t *pt, int flag)
{
   pt->flags &= ~flag;
   if ((pt->flags & (port_input | port_output)) == 0) {
      if (pt->flags & port_file) {
         if (pt->flags & port_closeit)
            fclose(pt->rep.stdio.file);
         if (pt->rep.stdio.filename)
            tsc->free(pt->rep.stdio.filename);
      }
      pt->flags = port_free;
   }
}

static port_t *port_rep_for_file(tr7_engine_t tsc, FILE * f, unsigned prop, const char *fn)
{
   port_t *pt;
   char mode[4];
   int idx;

   if (f == NULL) {
      if (prop & port_output) {
         if (prop & port_input) {
            mode[0] = 'a';
            mode[1] = '+';
            idx = 2;
         }
         else {
            mode[0] = 'w';
            idx = 1;
         }
      }
      else {
         mode[0] = 'r';
         idx = 1;
      }
      if (prop & port_binary)
         mode[idx++] = 'b';
      mode[idx] = 0;
      f = fopen(fn, mode);
      if (f == NULL)
         return NULL;
      prop |= port_closeit;
   }
   pt = (port_t *) memalloc(tsc, sizeof *pt);
   if (pt != NULL) {
      pt->flags = port_file | prop;
      pt->rep.stdio.file = f;
      pt->rep.stdio.filename = fn == NULL ? NULL :  memalloc_copy_stringz(tsc, fn, strlen(fn));
#if SHOW_ERROR_LINE
      pt->line = 1;
#endif
      pt->unread.count = 0;
   }
   else if (prop & port_closeit)
      fclose(f);
   return pt;
}

static port_t *port_rep_from_file(tr7_engine_t tsc, FILE * f, int prop)
{
   return port_rep_for_file(tsc, f, prop, NULL);
}

static port_t *port_rep_from_filename(tr7_engine_t tsc, const char *fn, int prop)
{
   return port_rep_for_file(tsc, NULL, prop, fn);
}

static void port_rep_init(port_t *pt, tr7_t item, uint8_t *start, uint8_t *end, int flags)
{
   pt->flags = flags;
#if SHOW_ERROR_LINE
   pt->line = 1;
#endif
   pt->rep.inmem.item = item;
   pt->rep.inmem.start = start;
   pt->rep.inmem.curr = start;
   pt->rep.inmem.end = end;
   pt->unread.count = 0;
}

static port_t *port_rep_from_string(tr7_engine_t tsc, tr7_t string, uint8_t *start, uint8_t *end)
{
   port_t *pt = (port_t*)memalloc(tsc, sizeof(port_t));
   if (pt)
      port_rep_init(pt, string, start, end ? end : start + strlen((char*)start), port_string | port_input);
   return pt;
}

static port_t *port_rep_from_bytevector(tr7_engine_t tsc, tr7_t bytevec)
{
   port_t *pt = (port_t*)memalloc(tsc, sizeof(port_t));
   if (pt) {
      tr7_buffer_t bv = TR7_TO_BYTEVECTOR(bytevec);
      port_rep_init(pt, bytevec, bv->content, &bv->content[TR7_BUFFER_LENGTH(bv)],
                   port_bytevector | port_input | port_binary);
   }
   return pt;
}

static void port_rep_init_scratch(tr7_engine_t tsc, port_t *pt, uint8_t *start, int size, int flags)
{
   port_rep_init(pt, TR7_NIL, start, start + size - 1, flags);
   memset(start, 0, size);
}

static port_t *port_rep_from_scratch(tr7_engine_t tsc, int prop)
{
   port_t *pt = (port_t*)memalloc(tsc, sizeof(port_t));
   if (pt) {
      uint8_t *start = memalloc(tsc, SCRATCH_SIZE);
      if (start)
         port_rep_init_scratch(tsc, pt, start, SCRATCH_SIZE,
                 prop | port_output | port_scratch | port_ownbuf);
      else {
         memfree(tsc, pt);
         pt = NULL;
      }
   }
   return pt;
}

static int port_rep_realloc_scratch_size(tr7_engine_t tsc, port_t * pt, size_t new_size)
{
   uint8_t *start = pt->rep.inmem.start;
   size_t old_size = pt->rep.inmem.end - start;
   if (new_size > old_size) {
      uint8_t *str = memalloc(tsc, new_size + 1);
      if (!str)
         return 0;
      memcpy(str, start, old_size);
      memset(&str[old_size], ' ', new_size - old_size);
      str[new_size] = '\0';
      pt->rep.inmem.start = str;
      pt->rep.inmem.end = str + new_size;
      pt->rep.inmem.curr = &str[pt->rep.inmem.curr - start];
      if (pt->flags & port_ownbuf)
         tsc->free(start);
      else
         pt->flags |= port_ownbuf;
   }
   return 1;
}

/*
static int port_rep_realloc_scratch(tr7_engine_t tsc, port_t * pt)
{
   size_t size = pt->rep.inmem.end - pt->rep.inmem.start;
   return port_rep_realloc_scratch_size(tsc, pt, size + size - 1);
}
*/

static tr7_t port_rep_get_string(tr7_engine_t tsc, port_t * pt)
{
   if ((pt->flags & (port_string | port_scratch)) == (port_string | port_scratch))
      return tr7_make_string_copy_length(tsc, (const char*)pt->rep.inmem.start,
                         pt->rep.inmem.curr - pt->rep.inmem.start);
   return TR7_FALSE;
}

static tr7_t port_rep_get_bytevector(tr7_engine_t tsc, port_t * pt)
{
   if ((pt->flags & (port_bytevector | port_scratch)) == (port_bytevector | port_scratch))
      return tr7_make_bytevector_copy(tsc, (uint8_t*)pt->rep.inmem.start,
                         pt->rep.inmem.curr - pt->rep.inmem.start);
   return TR7_FALSE;
}

/*
**************************************************************************
*
*/


int tr7_is_input_port(tr7_t p)
{
   return TR7_IS_PORT(p) && (TR7__PORT__PORT(p)->flags & port_input);
}

int tr7_is_output_port(tr7_t p)
{
   return TR7_IS_PORT(p) && (TR7__PORT__PORT(p)->flags & port_output);
}

int tr7_is_textual_port(tr7_t p)
{
   return TR7_IS_PORT(p) && !(TR7__PORT__PORT(p)->flags & port_binary);
}

int tr7_is_binary_port(tr7_t p)
{
   return TR7_IS_PORT(p) && (TR7__PORT__PORT(p)->flags & port_binary);
}

static tr7_t mk_port(tr7_engine_t tsc, port_t * p)
{
   tr7_port_t x = GET_CELLS(tsc, x, 1);

   x->head = TR7_HEAD_KIND_PORT;
   x->_port_ = p;
   return push_recent_cell(tsc, x);
}

static void port_close(tr7_engine_t tsc, tr7_t p, int flag)
{
   port_rep_close(tsc, TR7__PORT__PORT(p), flag);
}

static void finalize_port(tr7_engine_t tsc, tr7_cell_t a)
{
   port_t *p = TR7_CELL_PORT__PORT_(a);
   port_rep_finalize(tsc, p);
   tsc->free(p);
}

static tr7_t mk_port_cond(tr7_engine_t tsc, port_t * pt)
{
   return pt ? mk_port(tsc, pt) : TR7_FALSE;
}

static tr7_t port_from_filename(tr7_engine_t tsc, const char *fn, int prop)
{
   return mk_port_cond(tsc, port_rep_from_filename(tsc, fn, prop));
}

static tr7_t port_from_file(tr7_engine_t tsc, FILE * f, int prop)
{
   return mk_port_cond(tsc, port_rep_from_file(tsc, f, prop));
}

static tr7_t port_for_file(tr7_engine_t tsc, FILE * f, const char *fn, int prop)
{
   return mk_port_cond(tsc, port_rep_for_file(tsc, f, prop, fn));
}

static tr7_t port_from_string(tr7_engine_t tsc, tr7_t string, uint8_t *start, uint8_t *end)
{
   return mk_port_cond(tsc, port_rep_from_string(tsc, string, start, end));
}

static tr7_t port_from_bytevector(tr7_engine_t tsc, tr7_t bytevec)
{
   return mk_port_cond(tsc, port_rep_from_bytevector(tsc, bytevec));
}

static tr7_t port_from_scratch(tr7_engine_t tsc, int prop)
{
   return mk_port_cond(tsc, port_rep_from_scratch(tsc, prop));
}

static tr7_t port_get_string(tr7_engine_t tsc, tr7_t p)
{
   port_t *pt = TR7__PORT__PORT(p);
   return port_rep_get_string(tsc, pt);
}

static tr7_t port_get_bytevector(tr7_engine_t tsc, tr7_t p)
{
   port_t *pt = TR7__PORT__PORT(p);
   return port_rep_get_bytevector(tsc, pt);
}

/*
**************************************************************************
*
* Routines for loading
*/

static void load_set(tr7_engine_t tsc, tr7_t port, unsigned loadflags, tr7_t loadenv)
{
   tsc->loadenv = loadenv;
   tsc->loadport = port;
   tsc->loadflags = loadflags;
}

static int load_enter(tr7_engine_t tsc, tr7_t port, unsigned loadflags)
{
   tr7_t e = TR7_CONS3(tsc, tsc->loadport, TR7_FROM_UINT(tsc->loadflags), tsc->loadenv);
   load_set(tsc, port, loadflags % LOADF_NESTING, e);
   return 1;
}

static int load_leave(tr7_engine_t tsc)
{
   unsigned loadflags;
   tr7_t p1, p2;

   p1 = tsc->loadenv;
   if (!TR7_IS_PAIR(p1))
      return 0;
   p2 = TR7_CDR(p1);
   if (!TR7_IS_PAIR(p2))
      return 0;

   loadflags = (unsigned)TR7_TO_UINT(TR7_CAR(p2));
   load_set(tsc, TR7_CAR(p1), loadflags, TR7_CDR(p2));
   return 1;
}

static int load_enter_for_file(tr7_engine_t tsc, FILE * f, const char *fn, unsigned loadflags)
{
   tr7_t p = port_for_file(tsc, f, fn, port_input);
   return TR7_IS_FALSE(p) ? 0 : load_enter(tsc, p, loadflags);
}

static int load_enter_string(tr7_engine_t tsc, uint8_t *start, uint8_t *end, unsigned loadflags)
{
   tr7_t p = port_from_string(tsc, TR7_NIL, start, end);
   return TR7_IS_FALSE(p) ? 0 : load_enter(tsc, p, loadflags);
}

static int load_enter_search(
   tr7_engine_t tsc,
   const char *basename, int baselength,
   tr7_strid_t idpath, unsigned loadflags)
{
   char path[PATH_MAX + 1];
   int found = search_access_file(tsc,
            basename, baselength ? baselength : (int)strlen(basename),
            path, sizeof path,
            suffixes_scheme, (int)(sizeof suffixes_scheme / sizeof *suffixes_scheme),
            tsc->strings[idpath]);
   return found ? load_enter_for_file(tsc, NULL, path, loadflags) : 0;
}

static int load_enter_search_import(tr7_engine_t tsc, const char *basename, int baselength)
{
   return load_enter_search(tsc, basename, baselength, Tr7_StrID_Library_Path, 0);
}

static int load_enter_search_load(tr7_engine_t tsc, FILE * f, const char *fn, unsigned loadflags)
{
   return f == NULL
             ? load_enter_search(tsc, fn, 0, Tr7_StrID_Path, loadflags)
             : load_enter_for_file(tsc, f, fn, loadflags);
}

static int load_enter_search_include(tr7_engine_t tsc, const char *basename, unsigned loadflags)
{
   return load_enter_search(tsc, basename, 0, Tr7_StrID_Include_Path, loadflags);
}

/*
**************************************************************************
*
* Routines for reading
*/

static wint_t port_read_char(tr7_engine_t tsc, port_t *pt)
{
   wint_t car;
   if (pt->unread.count)
      car = pt->unread.stack[--pt->unread.count];
   else if (pt->flags & port_file) {
      car = fgetwc(pt->rep.stdio.file);
      if (car == WEOF)
         pt->flags |= port_saw_EOF;
   }
   else if (pt->rep.inmem.curr >= pt->rep.inmem.end) {
      pt->flags |= port_saw_EOF;
      car = WEOF;
   }
   else
      pt->rep.inmem.curr += utf8_to_char((uint8_t*)pt->rep.inmem.curr, &car);
   return car;
}

/* back character to input buffer */
static void port_unread_char(tr7_engine_t tsc, port_t *pt, wint_t car)
{
   if (car != WEOF) {
      pt->flags &= ~(unsigned)port_saw_EOF;
      pt->unread.stack[pt->unread.count++] = car;
   }
}

/* check if a char is ready */
static int port_has_char(tr7_engine_t tsc, port_t *pt)
{
   /* TODO improve */
   return pt->unread.count || (pt->flags & (port_saw_EOF | port_string));
}

static int port_read_bytes(tr7_engine_t tsc, port_t *pt, uint8_t *buffer, unsigned length)
{
   unsigned nread = 0;
   size_t sz;

   while (pt->unread.count && nread < length)
      buffer[nread++] = (uint8_t)pt->unread.stack[--pt->unread.count];

   if (nread < length) {
      if (pt->flags & port_file) {
         sz = (unsigned)fread(&buffer[nread], 1, length - nread, pt->rep.stdio.file);
         if (sz > 0)
            nread += (unsigned)sz;
         else
            pt->flags |= port_saw_EOF;
      }
      else {
         sz = pt->rep.inmem.end - pt->rep.inmem.curr;
         if (sz == 0)
            pt->flags |= port_saw_EOF;
         else {
            if (sz < length)
               length = (unsigned)sz;
            memcpy(&buffer[nread], pt->rep.inmem.curr, length);
            pt->rep.inmem.curr += length;
         }
      }
   }
   return nread || !(pt->flags & port_saw_EOF) ? (int)nread : EOF;
}

static int port_read_byte(tr7_engine_t tsc, port_t *pt)
{
   uint8_t byte;
   int res = port_read_bytes(tsc, pt, &byte, 1);
   return res == 1 ? (int)byte : EOF;
}

/* back character to input buffer */
static void port_unread_byte(tr7_engine_t tsc, port_t *pt, int byte)
{
   if (byte != EOF) {
      pt->flags &= ~(unsigned)port_saw_EOF;
      pt->unread.stack[pt->unread.count++] = (wint_t)byte;
   }
}

/*  */
static int port_has_byte(tr7_engine_t tsc, port_t *pt)
{
   /* TODO improve */
   return pt->unread.count || (pt->flags & (port_saw_EOF | port_bytevector));
}

/*
**************************************************************************
*
* Routines for writing
*/


static void port_flush(tr7_engine_t tsc, port_t *pt)
{
   if (pt->flags & port_file)
      fflush(pt->rep.stdio.file);
}

static int port_write_chars_length(tr7_engine_t tsc, port_t *pt, const wint_t *s, unsigned len)
{
   uint8_t buffer[UTF8BUFFSIZE];
   unsigned idx;
   if (pt->flags & port_file) {
      for (idx = 0 ; idx < len ; idx++)
         if (fputwc(s[idx], pt->rep.stdio.file) == WEOF)
            return 0;
   }
   else {
      for (idx = 0 ; idx < len ; idx++)
         if (!port_write_utf8_length(tsc, pt, (char*)buffer, char_to_utf8(s[idx], buffer)))
            return 0;
   }
   return 1;
}

static int port_write_char(tr7_engine_t tsc, port_t *pt, wint_t c)
{
   return port_write_chars_length(tsc, pt, &c, 1);
}

static int port_write_utf8_length(tr7_engine_t tsc, port_t *pt, const char *s, unsigned len)
{
   if (pt->flags & port_file) {
      wint_t car;
      unsigned idx;
      for (idx = 0 ; idx < len ; ) {
         idx += utf8_to_char((uint8_t*)&s[idx], &car);
         if (!port_write_char(tsc, pt, car))
            return 0;
      }
   }
   else {
      size_t size = (size_t)(pt->rep.inmem.end - pt->rep.inmem.curr);
      if (size < (size_t)len) {
         if (!(pt->flags & port_scratch))
            return 0;
         size = (size_t)(len + ((pt->rep.inmem.curr - pt->rep.inmem.start) << 1));
         if (!port_rep_realloc_scratch_size(tsc, pt, size))
            return 0;
      }
      memcpy(pt->rep.inmem.curr, s, (size_t)len);
      pt->rep.inmem.curr += len;
   }
   return 1;
}

static int port_write_utf8(tr7_engine_t tsc, port_t *pt, const char *s)
{
   return port_write_utf8_length(tsc, pt, s, (unsigned)strlen(s));
}

static int port_write_bytes(tr7_engine_t tsc, port_t *pt, const uint8_t *bytes, unsigned len)
{
   if (pt->flags & port_file) {
      if (fwrite(bytes, 1, len, pt->rep.stdio.file) != len)
            return 0;
   }
   else {
      size_t size = (size_t)(pt->rep.inmem.end - pt->rep.inmem.curr);
      if (size < (size_t)len) {
         if (!(pt->flags & port_scratch))
            return 0;
         size = (size_t)(len + ((pt->rep.inmem.curr - pt->rep.inmem.start) << 1));
         if (!port_rep_realloc_scratch_size(tsc, pt, size))
            return 0;
      }
      memcpy(pt->rep.inmem.curr, bytes, len);
      pt->rep.inmem.curr += len;
   }
   return 1;
}

/*
**************************************************************************
*
*/
static tr7_t get_stdport(tr7_engine_t tsc, int num)
{
   return parameter_get(tsc, tsc->stdports[num]);
}

static void set_stdport(tr7_engine_t tsc, tr7_t value, int num)
{
   parameter_set(tsc, tsc->stdports[num], value);
}

static void push_stdport(tr7_engine_t tsc, tr7_t value, int num)
{
   parameter_push(tsc, tsc->stdports[num], value);
}

static void make_stdport(tr7_engine_t tsc, tr7_t symbol, int idx)
{
   tr7_t param = mk_parameter(tsc, TR7_NIL, TR7_NIL);
   tsc->stdports[idx] = param;
   new_slot_in_env(tsc, symbol, param);
}

static void init_stdports(tr7_engine_t tsc)
{
   make_stdport(tsc, SYMBOL(CURR_INPORT), IDX_STDIN);
   make_stdport(tsc, SYMBOL(CURR_OUTPORT), IDX_STDOUT);
   make_stdport(tsc, SYMBOL(CURR_ERRPORT), IDX_STDERR);
}

/*
**************************************************************************
*
*/
/* ========== atoms implementation  ========== */

static tr7_int_t decode_binary(const char *s)
{
   signed char c;
   tr7_int_t x = 0;
   for (;;)
      if ((c = *s++) == '_')
         ;
      else if ((c -= '0'), (0 <= c && c <= 1))
         x = (x << 1) + c;
      else
         return x;
}

static tr7_int_t decode_octal(const char *s)
{
   signed char c;
   tr7_int_t x = 0;
   for (;;)
      if ((c = *s++) == '_')
         ;
      else if ((c -= '0'), (0 <= c && c <= 7))
         x = (x << 3) + c;
      else
         return x;
   return x;
}

static tr7_int_t decode_decimal(const char *s)
{
   signed char c;
   tr7_int_t x = 0;
   for (;;)
      if ((c = *s++) == '_')
         ;
      else if ((c -= '0'), (0 <= c && c <= 9))
         x = (x * 10) + c;
      else
         return x;
}

static tr7_int_t decode_hexa(const char *s)
{
   signed char c;
   tr7_int_t x = 0;
   for (;;)
      if ((c = *s++) == '_')
         ;
      else if (((c -= '0'),             (0 <= c && c <= 9))
            || ((c += '0' - 'a' + 10),  (10 <= c && c <= 15))
            || ((c += 'a' - 'A'),       (10 <= c && c <= 15)))
         x = (x << 4) + c;
      else
         return x;
   return x;
}

static wint_t decode_character(const char *s)
{
   wint_t c;
   if (s[utf8_to_char((uint8_t*)s, &c)]) {
      c = WEOF;
      switch(*s) {
      case 'a':
         if (!strcmp(s, "alarm"))
            c = '\x07';
         break;
      case 'b':
         if (!strcmp(s, "backspace"))
            c = '\x08';
         break;
      case 'd':
         if (!strcmp(s, "delete"))
            c = '\x7f';
         break;
      case 'e':
         if (!strcmp(s, "escape"))
            c = '\x1b';
         break;
      case 'n':
         if (!strcmp(s, "null"))
            c = 0;
         else if (!strcmp(s, "newline"))
            c = '\n';
         break;
      case 'r':
         if (!strcmp(s, "return"))
            c = '\r';
         break;
      case 's':
         if (!strcmp(s, "space"))
            c = ' ';
         break;
      case 't':
         if (!strcmp(s, "tab"))
            c = '\t';
         break;
      case 'x':
         c = (wint_t)decode_hexa(s + 1);
         if (c > UCHAR_MAX)
            c = WEOF;
         break;
      default:
         get_control_code(s, &c);
         break;
      }
   }
   return c;
}

/* make symbol or number atom from string */
static tr7_t mk_atom(tr7_engine_t tsc, const char *q, size_t length)
{
   char c;
   const char *p;
   int has_dec_point = 0;
   int has_fp_exp = 0;
   char low[STRBUFFSIZE];

   if (tsc->loadflags & LOADF_FOLDCASE)
      q = fold(low, q);
   else
      q = q;

   p = q;
   c = *p++;
   if ((c == '+') || (c == '-')) {
      if (strcmp(p, "nan.0") == 0)
         return tr7_from_double(tsc, NAN);
      if (strcmp(p, "inf.0") == 0)
         return tr7_from_double(tsc, c == '-' ? -INFINITY : INFINITY);
      c = *p++;
      if (c == '.') {
         has_dec_point = 1;
         c = *p++;
      }
      if (!isdigit(c))
         return tr7_get_symbol_length(tsc, q, length, 1);
   }
   else if (c == '.') {
      has_dec_point = 1;
      c = *p++;
      if (!isdigit(c))
         return tr7_get_symbol_length(tsc, q, length, 1);
   }
   else if (!isdigit(c))
      return tr7_get_symbol_length(tsc, q, length, 1);

   for (; (c = *p) != 0; ++p) {
      if (!isdigit(c)) {
         if (c == '.') {
            if (!has_dec_point) {
               has_dec_point = 1;
               continue;
            }
         }
         else if ((c == 'e') || (c == 'E')) {
            if (!has_fp_exp) {
               has_dec_point = 1;       /* decimal point illegal
                                           from now on */
               p++;
               if ((*p == '-') || (*p == '+') || isdigit(*p)) {
                  continue;
               }
            }
         }
         return tr7_get_symbol_length(tsc, q, length, 1);
      }
   }
   if (has_dec_point)
      return tr7_from_double(tsc, atof(q));
   return tr7_from_int(tsc, atol(q));
}

/* make constant */
static tr7_t mk_sharp_const(tr7_engine_t tsc, const char *name, size_t length)
{
   wint_t c;
   long x;
   char low[STRBUFFSIZE];

   if (tsc->loadflags & LOADF_FOLDCASE)
      name = fold(low, name);

   switch (name[0]) {
   case 't':
      if (length != 1 && strcmp(name, "true"))
         break;
      return TR7_TRUE;
   case 'f':
      if (length != 1 && strcmp(name, "false"))
         break;
      return TR7_FALSE;
   case 'o': /* #o (octal) */
      x = decode_octal(name + 1);
      return tr7_from_int(tsc, x);
   case 'd': /* #d (decimal) */
      x = decode_decimal(name + 1);
      return tr7_from_int(tsc, x);
   case 'x': /* #x (hexadecimal) */
      x = decode_hexa(name + 1);
      return tr7_from_int(tsc, x);
   case 'b': /* #b (binary) */
      x = decode_binary(name + 1);
      return tr7_from_int(tsc, x);
   case '\\': /* #\ (character) */
      c = decode_character(name + 1);
      if (c == WEOF)
         break;
      return TR7_FROM_CHAR(c);
   default:
      break;
   }
   return TR7_NIL;
}
/*
**************************************************************************
* SECTION STRBUFF - strbuff is used for buffering characters in UTF8
* ---------------
*
* start buffering in strbuff
*/
static void strbuff_start(tr7_engine_t tsc)
{
   tsc->strbuff.length = 0;
   if (tsc->strbuff.head != tsc->strbuff.buffer) {
      free(tsc->strbuff.head);
      tsc->strbuff.head = tsc->strbuff.buffer;
      tsc->strbuff.size = (unsigned)sizeof tsc->strbuff.buffer;
   }
}
/*
* stop buffering of strbuff
* appends a zero at tail without increasing the length
* returns 1 if okay or zero otherwise
*/
static int strbuff_ensure(tr7_engine_t tsc, unsigned count)
{
   void *newhead;
   unsigned req = tsc->strbuff.length + count;
   unsigned sz = tsc->strbuff.size;
   if (req > sz) {
      do { sz <<= 1; } while (req > sz);
      newhead = memalloc(tsc, sz);
      if (newhead == NULL)
         return 0;
      memcpy(newhead, tsc->strbuff.head, tsc->strbuff.length);
      if (tsc->strbuff.head != tsc->strbuff.buffer)
         memfree(tsc, tsc->strbuff.head);
      tsc->strbuff.head = newhead;
      tsc->strbuff.size = sz;
   }
   return 1;
}
/*
* stop buffering of strbuff
* appends a zero at tail without increasing the length
* returns 1 if okay or zero otherwise
*/
static int strbuff_stop(tr7_engine_t tsc)
{
   int ret = strbuff_ensure(tsc, 1);
   if (ret)
      tsc->strbuff.head[tsc->strbuff.length] = 0;
   return ret;
}
/*
* removes the spaces at tail and then stops (see strbuff_stop)
*/
static int strbuff_stop_trim(tr7_engine_t tsc)
{
   while (tsc->strbuff.length && isspace(tsc->strbuff.head[tsc->strbuff.length - 1]))
      tsc->strbuff.length--;
   return strbuff_stop(tsc);
}
/*
* add the character 'car' to the current strbuff
* returns 1 if okay or zero otherwise
*/
static int strbuff_add(tr7_engine_t tsc, wint_t car)
{
   int ret = strbuff_ensure(tsc, UTF8BUFFSIZE);
   if (ret) {
      unsigned len = tsc->strbuff.length;
      len += char_to_utf8(car, (uint8_t*)&tsc->strbuff.head[len]);
      tsc->strbuff.length = len;
   }
   return ret;
}
/*
* get head of the current strbuff
*/
static const char *strbuff_head(tr7_engine_t tsc)
{
   return tsc->strbuff.head;
}
/*
* get length in bytes (utf8) of the current strbuff (without terminating zero)
*/
static unsigned strbuff_length(tr7_engine_t tsc)
{
   return tsc->strbuff.length;
}
/*
* get a new TR7 string for the current strbuff
*/
static tr7_t strbuff_string(tr7_engine_t tsc)
{
   tr7_t res;
   if (tsc->strbuff.head == tsc->strbuff.buffer)
      res = tr7_make_string_copy_length(tsc, tsc->strbuff.head, tsc->strbuff.length);
   else {
      res = tr7_make_string_take_length(tsc, tsc->strbuff.head, tsc->strbuff.length);
      tsc->strbuff.head = tsc->strbuff.buffer;
      tsc->strbuff.size = (unsigned)sizeof tsc->strbuff.buffer;
   }
   return res;
}

/*
**************************************************************************
*
*/
/* read string like expression (")xxx...xxx" or (|)xxx...xxx| */
static int read_str_like_exp_port(tr7_engine_t tsc, port_t *pt, wint_t term)
{
   wint_t car;
   int num;

   strbuff_start(tsc);
   for (;;) {
      car = port_read_char(tsc, pt);
      if (car == WEOF)
         return 0;
      if (car == term)
         return strbuff_stop(tsc);
      if (car == '\\') {
escaping:
         car = port_read_char(tsc, pt);
         if (car == WEOF)
            return 0;
         switch (car) {
         case ' ':
         case '\t':
         case '\r':
            do {
               car = port_read_char(tsc, pt);
            } while(car == ' ' || car == '\t' || car == '\r');
            if (car != '\n')
               return 0;
            /*@fallthrough@*/
         case '\n':
            do {
               car = port_read_char(tsc, pt);
            } while(car == ' ' || car == '\t' || car == '\r');
            if (car == WEOF)
               return 0;
            if (car == term)
               return strbuff_stop(tsc);
            if (car == '\\')
               goto escaping;
            break;
         case 'n':
            car = '\n';
            break;
         case 't':
            car = '\t';
            break;
         case 'r':
            car = '\r';
            break;
         case 'x':
         case 'X':
            num = 0;
            do {
               car = port_read_char(tsc, pt);
               if (car <= '9' && car >= '0')
                  num = (num << 4) + car - '0';
               else if (car <= 'F' && car >= 'A')
                  num = (num << 4) + car - 'A' + 10;
               else if (car <= 'f' && car >= 'a')
                  num = (num << 4) + car - 'a' + 10;
               else if (car != ';')
                  return 0;
            } while(car != ';');
            car = (wint_t)num;
            break;
         default:
            break;
         }
      }
      if (!strbuff_add(tsc, car))
         return 0;
   }
}

/*
 * read started (#!) directives
 * return -1 if error or if OK, 1 if terminated with '\n' or 0 if terminated with EOF
 */
static int read_directive(tr7_engine_t tsc, port_t *pt)
{
   wint_t car;

   strbuff_start(tsc);
   if (strbuff_add(tsc, '#') && strbuff_add(tsc, '!'))
      do {
         car = port_read_char(tsc, pt);
         if (car == '\n' || car == WEOF) {
            if (!strbuff_stop_trim(tsc))
               break;
#if SHOW_ERROR_LINE
            if (car == '\n')
               pt->line++;
#endif
            return car == '\n';
         }
      } while(strbuff_add(tsc, car));
   return -1;
}

/* read string expression "xxx...xxx" */

static int skip_block_comment_port(tr7_engine_t tsc, port_t *pt)
{
   wint_t car;
   int state = 0, nest = 1;
   for(;;) {
      car = port_read_char(tsc, pt);
      switch (car) {
      case WEOF:
         return 1;
      case '|':
         if (state != '#')
            state = car;
         else {
            nest++;
            state = 0;
         }
         break;
      case '#':
         if (state != '|')
            state = car;
         else {
            if (0 == --nest)
               return 0;
            state = 0;
         }
         break;
      default:
#if SHOW_ERROR_LINE
         if (car == '\n')
            pt->line++;
#endif
         state = 0;
         break;
      }
   }
}

#define TOK_EOF     0   /* END OF INPUT */
#define TOK_LPAREN  1   /* ( left perenthesis */
#define TOK_RPAREN  2   /* ) right parenthesis */
#define TOK_DOT     3   /* . dot */
#define TOK_QUOTE   4   /* ' quote */
#define TOK_BQUOTE  5   /* ` backquote = quasi-quote */
#define TOK_COMMA   6   /* , comma = unquote */
#define TOK_ATMARK  7   /* @, unquote-splicing */
#define TOK_SHARP   8   /* # */
#define TOK_VEC     9   /* #( */
#define TOK_VEC_U8  10  /* #u8( */
#define TOK_ERROR   11  /* ERROR */
#define TOK_VALUE   12  /* VALUE */
#define TOK_DATSET  13  /* #N= define data */
#define TOK_DATREF  14  /* #N# reference data */
#define TOK_COMDAT  15  /* #; comment data */
#define TOK_COMMENT 16  /* COMMENT */

static int is_a_delimiter(wint_t car)
{
   switch (car) {
   case '(':
   case ')':
   case '"':
   case ';':
   case '\f':
   case '\t':
   case '\v':
   case '\n':
   case '\r':
   case ' ':
      return 1;
   default:
      return 0;
   }
}

/*
* read non standard sharp expressions
*/
static int read_other_sharp(tr7_engine_t tsc, port_t *pt, wint_t car)
{
   strbuff_start(tsc);
   if (strbuff_add(tsc, '#'))
      while(strbuff_add(tsc, car)) {
         car = port_read_char(tsc, pt);
         if (car == WEOF || is_a_delimiter(car)) {
            if (!strbuff_stop(tsc))
               break;
            if (car != WEOF)
               port_unread_char(tsc, pt, car);
            tsc->value = strbuff_string(tsc);
            return TOK_SHARP;
         }
      }
   return TOK_ERROR; /* oom */
}

static int skip_directive_port(tr7_engine_t tsc, port_t *pt)
{
   static const char nfc[] = "no-fold-case";
   int sts = read_directive(tsc, pt);
   if (sts < 0)
      return TOK_ERROR;
   if (!strcmp(&strbuff_head(tsc)[2], nfc))
      tsc->loadflags &= ~LOADF_FOLDCASE;
   else if (!strcmp(&strbuff_head(tsc)[2], &nfc[3]))
      tsc->loadflags |= LOADF_FOLDCASE;
   else {
      tsc->value = strbuff_string(tsc);
      return TOK_SHARP;
   }
   return sts ? TOK_COMMENT : TOK_EOF;
}

/* get token */
static int read_token(tr7_engine_t tsc, port_t *pt)
{
   wint_t car;
   int idx, res;
   for(;;) {
      car = port_read_char(tsc, pt);
      switch (car) {
      case WEOF:
         return TOK_EOF;
      case '\n':
#if SHOW_ERROR_LINE
         pt->line++;
         /*@fallthrough@*/
#endif
      case '\r':
      case '\t':
      case ' ':
         break;
      case ';':
         while ((car = port_read_char(tsc, pt)) != '\n')
            if (car == WEOF)
               return TOK_EOF;
#if SHOW_ERROR_LINE
         pt->line++;
#endif
         break;
      case '(':
         return TOK_LPAREN;
      case ')':
         return TOK_RPAREN;
      case '\'':
         return TOK_QUOTE;
      case '`':
         return TOK_BQUOTE;
      case ',':
         if ((car = port_read_char(tsc, pt)) == '@')
            return TOK_ATMARK;
         port_unread_char(tsc, pt, car);
         return TOK_COMMA;
      case '|':
      case '"':
         if (!read_str_like_exp_port(tsc, pt, car))
            return TOK_ERROR;
         if (car == '"')
            tsc->value = strbuff_string(tsc);
         else
            tsc->value = tr7_get_symbol_length(tsc, strbuff_head(tsc), strbuff_length(tsc), 1);
         return TOK_VALUE;
      case '#':
         car = port_read_char(tsc, pt);
         switch (car) {
         case ';':
            return TOK_COMDAT;
         case '(':
            return TOK_VEC;
         case '|':
            if (skip_block_comment_port(tsc, pt))
               return TOK_EOF;
            break;
         case '!':
            res = skip_directive_port(tsc, pt);
            if (res != TOK_COMMENT)
               return res;
            break;
         case 'u':
            car = port_read_char(tsc, pt);
            if (car == '8') {
               car = port_read_char(tsc, pt);
               if (car == '(')
                  return TOK_VEC_U8;
               port_unread_char(tsc, pt, car);
               car = '8';
            }
            port_unread_char(tsc, pt, car);
            return read_other_sharp(tsc, pt, 'u');
         case 't':
         case 'f':
         case 'o':
         case 'd':
         case 'x':
         case 'b':
         case '\\':
            strbuff_start(tsc);
            while (car != WEOF && !is_a_delimiter(car)) {
               if (!strbuff_add(tsc, car))
                  return TOK_ERROR;
               car = port_read_char(tsc, pt);
            }
            if (strbuff_head(tsc)[0] != '\\' || strbuff_length(tsc) != 1)
               port_unread_char(tsc, pt, car);
            else if (car == WEOF || !strbuff_add(tsc, car))
               return TOK_ERROR;
            if (!strbuff_stop(tsc))
               return TOK_ERROR;
            tsc->value = mk_sharp_const(tsc, strbuff_head(tsc), strbuff_length(tsc));
            return TR7_IS_NIL(tsc->value) ? TOK_ERROR : TOK_VALUE;
         default:
            if (car < '0' || car > '9')
               return read_other_sharp(tsc, pt, car);
            idx = (int)(car - '0');
            for(;;) {
               car = port_read_char(tsc, pt);
               if (car >= '0' && car <= '9')
                  idx = 10 * idx + (int)(car - '0');
               else if (car != '=' && car != '#')
                  return TOK_ERROR;
               else if (car == '=' || car == '#') {
                  tsc->value = TR7_FROM_INT(idx);
                  return car == '=' ? TOK_DATSET : TOK_DATREF;
               }
            }
         }
         break;
      default:
         strbuff_start(tsc);
         do {
            if (!strbuff_add(tsc, car))
               return TOK_ERROR;
            car = port_read_char(tsc, pt);
         } while (car != WEOF && !is_a_delimiter(car));
         port_unread_char(tsc, pt, car);
         if (!strbuff_stop(tsc))
            return TOK_ERROR;
         if (strbuff_length(tsc) == 1 && *strbuff_head(tsc) == '.')
            return TOK_DOT;
         tsc->value = mk_atom(tsc, strbuff_head(tsc), strbuff_length(tsc));
         return TOK_VALUE;
      }
   }
}
/*
**************************************************************************
* Basic number formatting to be improved if needed (avoid copy, big nums)
*/

static int format_int(tr7_engine_t tsc, char *buffer, int length, int fbase, tr7_int_t num)
{
   char car;
   char buf[140]; /* enough for 128 bits */
   char *str;
   int sig, lenstr, dig, base;
   tr7_uint_t val;

   base = fbase < 2 || fbase > 36 ? 10 : fbase;
   if (num < 0) {
      sig = 1;
      val = (tr7_uint_t)-num;
   }
   else {
      sig = 0;
      val = (tr7_uint_t)num;
   }
   lenstr = 0;
   str = &buf[sizeof buf];
   *--str = 0;
   switch(base) {
   case 2:
      do {
         car = (char)('0' + (int)(val & 1));
         val >>= 1;
         *--str = car;
      } while(val);
      break;
   case 8:
      do {
         car = (char)('0' + (int)(val & 7));
         val >>= 3;
         *--str = car;
      } while(val);
      break;
   case 16:
      do {
         dig = (int)(val & 15);
         val >>= 4;
         car = (char)(dig < 10 ? '0' + dig : 'a' + dig - 10);
         *--str = car;
      } while(val);
      break;
   default:
      do {
         dig = (int)(val % base);
         val /= base;
         car = (char)(dig < 10 ? '0' + dig : 'a' + dig - 10);
         *--str = car;
      } while(val);
      break;
   }
   if (sig)
         *--str = '-';
   lenstr = (int)(sizeof buf - 1 - (str - buf));
   if (length)
      memcpy(buffer, str, lenstr < length ? lenstr + 1 : length);
   return lenstr;
}

static int format_double(tr7_engine_t tsc, char *buffer, int length, int fbase, double num)
{
   char buf[20];
   const char *str;
   int lenstr;

   if (fbase != 10 && fbase > 1 && fbase <= 36)
      return format_int(tsc, buffer, length, fbase, (tr7_int_t)num); /* hum, TODO checks */

   if (isnan(num)) {
      str = "+nan.0";
      lenstr = 6;
   }
   else if (!isfinite(num)) {
      str = num < 0 ? "-inf.0" : "+inf.0";
      lenstr = 6;
   }
   else {
      str = buf;
      lenstr = snprintf(buf, sizeof buf, "%.10g", num);
      /* r5rs says there must be a '.' (unless 'e'?) */
      if (lenstr == (int)strcspn(buf, ".e")) {
         buf[lenstr++] = '.';      /* not found, so add '.0' at the end */
         buf[lenstr++] = '0';
         buf[lenstr] = 0;
      }
   }
   if (length)
      memcpy(buffer, str, lenstr < length ? lenstr + 1 : length);
   return lenstr;
}

static int format_number(tr7_engine_t tsc, char *buffer, int length, int fbase, tr7_t num)
{
   switch (TR7_KIND(num)) {
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return format_int(tsc, buffer, length, fbase, TR7_TO_INT(num));
   case TR7_KIND_DOUBLE:
      return format_double(tsc, buffer, length, fbase, *TR7_TO_DOUBLE(num));
   default:
      return 0;
   }
}
/*
**************************************************************************
* SECTION WRITING
* ===============
*
* scan items for its loops and fulfil the set of anchored values
* accordingly to the exploration trail
*/
static tr7_t do_scan_loops(tr7_engine_t tsc, tr7_t item, tr7_t anchors, tr7_t trail)
{
   tr7_vector_t vec;
   tr7_uint_t idx, cnt;

   /* is item to be checked? */
   if (TR7_IS_PAIR(item)
#if DUMP_LAMBDAS
    || TR7_IS_LAMBDA(item)
#endif
    || TR7_IS_VECTOR(item)) {
      /* yes, to be checked.
       * is it in the trail? */
      if (tr7_memq_pair(item, trail) != NULL) {
         /* in the trail.
          * add to detected anchors if not already done */
         if (tr7_assq_pair(item, anchors) == NULL)
            anchors = tr7_cons(tsc, tr7_cons(tsc, item, TR7_FROM_INT(0)), anchors);
      }
      else {
         /* not in the trail.
          * update the trail to include this item */
#if INTPTR_MAX == INT32_MAX
         tr7_t here[3];
         tr7_pair_t hpair = (tr7_pair_t)&here[0];
         if (!TR7_IS_PAIR(TR7_FROM_PAIR(hpair)))
            hpair = (tr7_pair_t)&here[1];
#else
         struct tr7_pair here;
         tr7_pair_t hpair = &here;
#endif
         hpair->car = item;
         hpair->cdr = trail;
         trail = TR7_FROM_PAIR(hpair);
         /* recursive scan of content of item with current trail */
         if (TR7_IS_PAIR(item)) {
            anchors = do_scan_loops(tsc, TR7_CAR(item), anchors, trail);
            anchors = do_scan_loops(tsc, TR7_CDR(item), anchors, trail);
         }
#if DUMP_LAMBDAS
         else if (TR7_IS_LAMBDA(item)) {
            anchors = do_scan_loops(tsc, TR7_TO_CLOSURE(item)->expr, anchors, trail);
         }
#endif
         else {
            vec = TR7_TO_VECTOR(item);
            cnt = TR7_HEAD_UVALUE(vec->head);
            for(idx = 0 ; idx < cnt ; idx++)
               anchors = do_scan_loops(tsc, vec->items[idx], anchors, trail);
         }
      }
   }
   return anchors;
}
/*
* scan the the content of item and return a list made
* of pairs (x . 0) where x is a detected loops value.
*/
static tr7_t scan_loops(tr7_engine_t tsc, tr7_t item)
{
   return do_scan_loops(tsc, item, TR7_NIL, TR7_NIL);
}
/*
* scan the the content of item and fulfil the set of detected
* values as pairs of (x . occur) where x is a detected value
* and occur its count of detected occurence minus one
*/
static tr7_t do_scan_shareds(tr7_engine_t tsc, tr7_t item, tr7_t set)
{
   tr7_vector_t vec;
   tr7_uint_t idx, cnt;

   /* is item to be checked? */
   if (TR7_IS_PAIR(item)
#if DUMP_LAMBDAS
    || TR7_IS_LAMBDA(item)
#endif
    || TR7_IS_VECTOR(item)) {
      /* yes, to be checked.
       * search it in the set */
      tr7_pair_t p = tr7_assq_pair(item, set);
      if (p != NULL)
         /* found in the set, increase its count */
         p->cdr = TR7_FROM_INT(TR7_TO_INT(p->cdr) + 1);
      else {
         /* not found, add to the set */
         set = tr7_cons(tsc, tr7_cons(tsc, item, TR7_FROM_INT(0)), set);
         /* recursive scan of content of item with current set */
         if (TR7_IS_PAIR(item)) {
            set = do_scan_shareds(tsc, TR7_CAR(item), set);
            set = do_scan_shareds(tsc, TR7_CDR(item), set);
         }
#if DUMP_LAMBDAS
         else if (TR7_IS_LAMBDA(item)) {
            set = do_scan_shareds(tsc, TR7_TO_CLOSURE(item)->expr, set);
         }
#endif
         else if (TR7_IS_VECTOR(item)) {
            vec = TR7_TO_VECTOR(item);
            cnt = TR7_HEAD_UVALUE(vec->head);
            for(idx = 0 ; idx < cnt ; idx++)
               set = do_scan_shareds(tsc, vec->items[idx], set);
         }
      }
   }
   return set;
}
/*
* scan the the content of item and return a list made
* of pairs (x . 0) where x is a detected shared value.
*/
static tr7_t scan_shareds(tr7_engine_t tsc, tr7_t item)
{
   tr7_t entries = do_scan_shareds(tsc, item, TR7_NIL);
   tr7_t anchors = TR7_NIL;
   /* removes the entries with single occurency */
   while (!TR7_IS_NIL(entries)) {
      tr7_t next = TR7_CDR(entries);
      if (TR7_TO_INT(TR7_CDAR(entries)) != 0) {
         TR7_CDAR(entries) = TR7_FROM_INT(0);
         TR7_CDR(entries) = anchors;
         anchors = entries;
      }
      entries = next;
   }
   return anchors;
}

static int is_simple_symbol(const char *str)
{
   int sta = 0;
   char c = *str;
   while(c) {
      if (Cisalpha(c))
         sta = 2;
      else if (c == '.')
         sta++;
      else if (!Cisdigit(c)) {
         if (!strchr("!$%&*+-/:<=>?@^_~", c))
            return 0;
         sta = 2;
      }
      c = *++str;
   }
   return sta > 1;
}

static void print_esc_string(tr7_engine_t tsc, port_t *pt, const char *p, int len, int delim)
{
   int i, n, d;
   char buf[5];
   unsigned char c;

   buf[0] = '\\';
   port_write_char(tsc, pt, (char)delim);
   while (len) {
      n = i = 0;
      while(i < len && n == 0) {
         c = (unsigned char)p[i];
         switch (c) {
         case '\n':
            buf[1] = 'n';
            n = 2;
            break;
         case '\t':
            buf[1] = 't';
            n = 2;
            break;
         case '\r':
            buf[1] = 'r';
            n = 2;
            break;
         default:
            if (delim != (int)c) {
               if (c >= ' ' && (c & 0x7f) != 0x7f)
                  i++;
               else {
                  buf[1] = 'x';
                  d = (c >> 4) & 15;
                  buf[2] = (char)((d > 9 ? ('A' - 10) : '0') + d);
                  d = c & 15;
                  buf[3] = (char)((d > 9 ? ('A' - 10) : '0') + d);
                  buf[4] = ';';
                  n = 5;
               }
               break;
            }
            /*@fallthrough@*/
         case '\\':
            buf[1] = c;
            n = 2;
            break;
         }
      }
      if (i)
         port_write_utf8_length(tsc, pt, p, i);
      if (n) {
         port_write_utf8_length(tsc, pt, buf, n);
         i++;
      }
      p += i;
      len -= i;
   }
   port_write_char(tsc, pt, (char)delim);
}

static void write_character(tr7_engine_t tsc, port_t *pt, wint_t car)
{
   char buf[UTF8BUFFSIZE + 2];
   const char *str;
   int lenstr;
   switch (car) {
   case ' ':
      str = "#\\space";
      lenstr = 7;
      break;
   case '\n':
      str = "#\\newline";
      lenstr = 9;
      break;
   case '\r':
      str = "#\\return";
      lenstr = 8;
      break;
   case '\t':
      str = "#\\tab";
      lenstr = 5;
      break;
   case '\x07':
      str = "#\\alarm";
      lenstr = 7;
      break;
   case '\x08':
      str = "#\\backspace";
      lenstr = 11;
      break;
   case '\x7f':
      str = "#\\delete";
      lenstr = 8;
      break;
   case '\x1b':
      str = "#\\escape";
      lenstr = 8;
      break;
   case 0:
      str = "#\\null";
      lenstr = 6;
      break;
   default:
      str = buf;
      if (is_control_code(car))
         lenstr = get_control_name(buf, sizeof buf, car);
      else {
         buf[0] = '#';
         buf[1] = '\\';
         lenstr = 2 + char_to_utf8(car, (uint8_t*)&buf[2]);
      }
      break;
   }
   port_write_utf8_length(tsc, pt, str, lenstr);
}

static void print_int(tr7_engine_t tsc, port_t *pt, int pflags, tr7_int_t num)
{
   char buf[140]; /* enough for 128 bits */
   int lenstr;

   lenstr = format_int(tsc, buf, sizeof buf, pflags, num);
   port_write_utf8_length(tsc, pt, buf, lenstr);
}

static void print_double(tr7_engine_t tsc, port_t *pt, int pflags, double num)
{
   char buf[140]; /* enough for 128 bits */
   int lenstr;

   lenstr = format_double(tsc, buf, sizeof buf, pflags, num);
   port_write_utf8_length(tsc, pt, buf, lenstr);
}


static void print_vector(tr7_engine_t tsc, port_t *pt, int pflags, tr7_vector_t vector, tr7_t anchors)
{
   tr7_uint_t idx, cnt = TR7_HEAD_UVALUE(vector->head); /* TODO: MACRO */
   port_write_utf8_length(tsc, pt, "#(", 2);
   for(idx = 0 ; idx < cnt ; idx++) {
      if (idx)
         port_write_utf8_length(tsc, pt, " ", 1);
      print_item(tsc, pt, pflags, vector->items[idx], anchors);
   }
   port_write_utf8_length(tsc, pt, ")", 1);
}

static void print_bytevector(tr7_engine_t tsc, port_t *pt, tr7_buffer_t bytevector)
{
   int off = 1, cnt;
   char buf[4];
   uint8_t val;
   uint8_t *ptr = bytevector->content;
   uint8_t *end = ptr + TR7_BUFFER_LENGTH(bytevector);
   port_write_utf8_length(tsc, pt, "#u8(", 4);
   buf[0] = ' ';
   for(; ptr != end; off = 0) {
      val = *ptr++;
      if (val > 99) {
         buf[1] = (char)('0' + (val / 100));
         buf[2] = (char)('0' + ((val % 100) / 10));
         buf[3] = (char)('0' + (val % 10));
         cnt = 4;
      }
      else if (val > 9) {
         buf[1] = (char)('0' + (val / 10));
         buf[2] = (char)('0' + (val % 10));
         cnt = 3;
      }
      else {
         buf[1] = (char)('0' + val);
         cnt = 2;
      }
      port_write_utf8_length(tsc, pt, &buf[off], cnt - off);
   }
   port_write_utf8_length(tsc, pt, ")", 1);
}

static int print_anchor(tr7_engine_t tsc, port_t *pt, tr7_t item, tr7_t anchors)
{
   char buf[60]; /* enougth */
   int len, num, idx = 1;
   while (!TR7_IS_NIL(anchors)) {
      if (TR7EQ(item, TR7_CAAR(anchors))) {
         buf[0] = '#';
         len = format_int(tsc, &buf[1], sizeof buf - 2, 10, idx);
         num = (int)TR7_TO_INT(TR7_CDAR(anchors));
         if (num)
            buf[len + 1] = '#';
         else {
            TR7_CDAR(anchors) = TR7_FROM_INT(idx);
            buf[len + 1] = '=';
         }
         port_write_utf8_length(tsc, pt, buf, len + 2);
         return num;
      }
      anchors = TR7_CDR(anchors);
      idx++;
   }
   return 0;
}

static void print_pair(tr7_engine_t tsc, port_t *pt, int pflags, tr7_pair_t head, tr7_t anchors)
{
   tr7_t car = head->car;
   tr7_t cdr = head->cdr;

#define PRINT_ABBREV(sym, bref, len) \
   if (TR7EQ(car, SYMBOL(sym)) && TR7_IS_PAIR(cdr) && TR7_IS_NIL(TR7_CDR(cdr))) { \
      port_write_utf8_length(tsc, pt, bref, len); \
      return print_item(tsc, pt, pflags, TR7_CAR(cdr), anchors); \
   }
   PRINT_ABBREV(QUOTE, "'", 1)
   PRINT_ABBREV(QUASIQUOTE, "`", 1)
   PRINT_ABBREV(UNQUOTE, ",", 1)
   PRINT_ABBREV(UNQUOTE_SPLICING, ",@", 2)
#undef PRINT_ABBREV

   port_write_utf8_length(tsc, pt, "(", 1);
   print_item(tsc, pt, pflags, car, anchors);
   while (TR7_IS_PAIR(cdr) && !tr7_assq_pair(cdr, anchors)) {
      car = TR7_CAR(cdr);
      cdr = TR7_CDR(cdr);
      port_write_utf8_length(tsc, pt, " ", 1);
      print_item(tsc, pt, pflags, car, anchors);
   }
   if (!TR7_IS_NIL(cdr)) {
      port_write_utf8_length(tsc, pt, " . ", 3);
      print_item(tsc, pt, pflags, cdr, anchors);
   }
   port_write_utf8_length(tsc, pt, ")", 1);
}

static void print_item(tr7_engine_t tsc, port_t *pt, int pflags, tr7_t item, tr7_t anchors)
{
   const char *str = "#<?>";
   tr7_cell_t cell;
   int lenstr;

   if (TR7_IS_VOID(item))
      return;

   switch (TR7_KIND(item)) {

   /* case of integers */
   case TR7_KIND_EINT:
   case TR7_KIND_OINT:
      return print_int(tsc, pt, 10, TR7_TO_INT(item));

   /* operation */
   case TR7_KIND_DOUBLE:
      return print_double(tsc, pt, 10, *TR7_TO_DOUBLE(item));

   /* case of specials */
   case TR7_KIND_SPECIAL:
      switch (TR7_VSP_TAG(item)) {

      /* predefined */
      case TR7_TAG_VSP(TR7_VSP_PREDEF):
         switch (item) {
         case TR7_NIL:   str = "()"; break;
         case TR7_FALSE: str = "#f"; break;
         case TR7_TRUE:  str = "#t"; break;
         case TR7_EOF:   str = "#<EOF>";  break;
         case TR7_VOID:  str = "#<VOID>"; break;
         default: /*TODO*/ break;
         }
         break;

      /* operator */
      case TR7_TAG_VSP(TR7_VSP_OPCODE):
         port_write_utf8(tsc, pt, "#<");
         if (TR7_IS_OPER(item)) {
            /* operator */
            const char *opname = get_opname(TR7_TO_OPER(item), NULL);
            if (opname != NULL)
               port_write_utf8(tsc, pt, opname);
            else {
               port_write_utf8(tsc, pt, "PROC ");
               print_int(tsc, pt, 10, TR7_TO_OPER(item));
            }
         }
         else {
            /* syntax */
            const char *syname = get_opname(TR7_TO_SYNTAX(item), NULL);
            if (syname != NULL)
               port_write_utf8(tsc, pt, syname);
            else {
               port_write_utf8(tsc, pt, "SYNTAX ");
               print_int(tsc, pt, 10, TR7_TO_SYNTAX(item));
            }
         }
         port_write_utf8(tsc, pt, ">");
         return;

      /* character */
      case TR7_TAG_VSP(TR7_VSP_CHARACTER):
         if (pflags)
            write_character(tsc, pt, TR7_TO_CHAR(item));
         else
            port_write_char(tsc, pt, TR7_TO_CHAR(item));
         return;

      default:
         break;
      }
      break;

   /* case of pairs */
   case TR7_KIND_PAIR:
      if (print_anchor(tsc, pt, item, anchors))
         return;
      print_pair(tsc, pt, pflags, TR7_TO_PAIR(item), anchors);
      return;

   /* case of cells */
   case TR7_KIND_CELL:
      if (print_anchor(tsc, pt, item, anchors))
         return;
      cell = TR7_TO_CELL(item);
      switch (TR7_CELL_KIND(cell)) {

      /* strings */
      case TR7_HEAD_KIND_STRING:
         str = (char*)TR7_CELL_CONTENT_STRING(cell);
         lenstr = TR7_CELL_LENGTH_STRING(cell);
         if (pflags)
            print_esc_string(tsc, pt, str, lenstr, '"');
         else
            port_write_utf8_length(tsc, pt, str, lenstr);
         return;

      /* symbols */
      case TR7_HEAD_KIND_SYMBOL:
         str = (char*)TR7_CELL_CONTENT_SYMBOL(cell);
         lenstr = TR7_CELL_LENGTH_SYMBOL(cell);
         if (!is_simple_symbol(str))
            print_esc_string(tsc, pt, str, lenstr, '|');
         else
            port_write_utf8_length(tsc, pt, str, lenstr);
         return;

      /* bytevector */
      case TR7_HEAD_KIND_BYTEVECTOR:
         print_bytevector(tsc, pt, (tr7_buffer_t)cell);
         return;

      case TR7_HEAD_KIND_PORT:
         str = "#<PORT>";
         break;

      case TR7_HEAD_KIND_FOREIGN:
         port_write_utf8(tsc, pt, "#<FOREIGN PROCEDURE ");
         print_int(tsc, pt, 16, (intptr_t)((tr7_ff_t)cell)->func);
         port_write_utf8(tsc, pt, ">");
         return;

      case TR7_HEAD_KIND_CONTINUATION:
         str = "#<CONTINUATION>";
         break;

      case TR7_HEAD_KIND_RATIONAL:
         break;

      case TR7_HEAD_KIND_COMPLEX:
         break;

      case TR7_HEAD_KIND_MACRO:
         str = "#<MACRO>";
         break;

      case TR7_HEAD_KIND_LAMBDA:
#if DUMP_LAMBDAS
         port_write_utf8(tsc, pt, "#<LAMBDA ");
         print_item(tsc, pt, 1, TR7_TO_CLOSURE(item)->expr, anchors);
         port_write_utf8(tsc, pt, ">");
         return;
#else
         str = "#<LAMBDA>";
         break;
#endif

      case TR7_HEAD_KIND_CASE_LAMBDA:
         str = "#<CASE-LAMBDA>";
         break;

      case TR7_HEAD_KIND_PROMISE:
         str = "#<PROMISE>";
         break;

      case TR7_HEAD_KIND_PARAMETER:
         str = "#<PARAMETER>";
         break;

      case TR7_HEAD_KIND_PARAMSAVE:
         break;

      case TR7_HEAD_KIND_GUARD:
         break;

      case TR7_HEAD_KIND_STACK:
         break;

      case TR7_HEAD_KIND_TRANSFORM:
         port_write_utf8(tsc, pt, "#<TRANSFORM ");
         port_write_utf8(tsc, pt, transform_name(item));
         port_write_utf8(tsc, pt, ">");
         return;

      case TR7_HEAD_KIND_DYNAWIND:
         break;

      case TR7_HEAD_KIND_ENVIRONMENT:
         str = "#<ENVIRONMENT>";
         break;

      case TR7_HEAD_KIND_RECORD:
         if (tr7_is_record_desc(item)) {
            port_write_utf8(tsc, pt, "#<RECORD-DESC ");
            port_write_utf8(tsc, pt, tr7_record_desc_name_string(item));
         }
         else {
            port_write_utf8(tsc, pt, "#<RECORD ");
            port_write_utf8(tsc, pt, tr7_record_typename_string(item));
         }
         port_write_utf8(tsc, pt, ">");
         return;

      case TR7_HEAD_KIND_VECTOR:
         print_vector(tsc, pt, pflags, (tr7_vector_t)cell, anchors);
         return;

      case TR7_HEAD_KIND_BIGINT:
         break;

      default: /*TODO*/
         break;
      }
      break;

   default:
      break;
   }
   port_write_utf8(tsc, pt, str);
}

#define PRTFLG_ESCAPE   1
#define PRTFLG_LOOPS    2
#define PRTFLG_SHAREDS  4

static void do_print(tr7_engine_t tsc, port_t *pt, int pflags, tr7_t obj)
{
   tr7_t  anchors;
   if (pflags & PRTFLG_SHAREDS)
      anchors = scan_shareds(tsc, obj);
   else if (pflags & PRTFLG_LOOPS)
      anchors = scan_loops(tsc, obj);
   else
      anchors = TR7_NIL;
   print_item(tsc, pt, pflags & PRTFLG_ESCAPE, obj, anchors);
}

/* print items */
int tr7_display_string(tr7_engine_t tsc, const char *s)
{
   port_t *pt = TR7__PORT__PORT(get_stdport(tsc,IDX_STDOUT));
   return port_write_utf8(tsc, pt, s);
}

static void print_to(tr7_engine_t tsc, tr7_t item, int pflags, int idxport)
{
   port_t *pt = TR7__PORT__PORT(get_stdport(tsc, idxport));
   do_print(tsc, pt, pflags, item);
}

void tr7_write(tr7_engine_t tsc, tr7_t item)
{
   print_to(tsc, item, PRTFLG_ESCAPE | PRTFLG_LOOPS, IDX_STDOUT);
}

void tr7_write_simple(tr7_engine_t tsc, tr7_t item)
{
   print_to(tsc, item, PRTFLG_ESCAPE, IDX_STDOUT);
}

void tr7_write_shared(tr7_engine_t tsc, tr7_t item)
{
   print_to(tsc, item, PRTFLG_ESCAPE | PRTFLG_SHAREDS, IDX_STDOUT);
}

void tr7_display(tr7_engine_t tsc, tr7_t item)
{
   print_to(tsc, item, PRTFLG_LOOPS, IDX_STDOUT);
}

static void log_str(tr7_engine_t tsc, const char *string)
{
   port_t *pt = TR7__PORT__PORT(get_stdport(tsc, IDX_STDERR));
   port_write_utf8(tsc, pt, string);
}

static void log_item(tr7_engine_t tsc, tr7_t item)
{
   print_to(tsc, item, PRTFLG_ESCAPE | PRTFLG_LOOPS, IDX_STDERR);
}

static void log_item_string(tr7_engine_t tsc, tr7_t item)
{
   int pflags = TR7_IS_STRING(item) ? 0 : PRTFLG_ESCAPE | PRTFLG_LOOPS;
   port_t *pt = TR7__PORT__PORT(get_stdport(tsc, IDX_STDERR));
   do_print(tsc, pt, pflags, item);
}

/*
**************************************************************************
*
*/

/* equivalence of atoms */
int tr7_eq(tr7_t a, tr7_t b)
{
   return TR7EQ(a, b);
}

int tr7_eqv(tr7_t a, tr7_t b)
{
   if (TR7EQ(a, b))
      return 1; /* symbols, integers, specials, identity */

   return tr7_cmp_num(a, b) == TR7_CMP_EQUAL; /* TODO: avoid equality if inexacts */
}

/* protection against infinite equality search */
struct eqguard { tr7_t a, b; struct eqguard *prv; };
static int equal_unchecked(tr7_t a, tr7_t b, struct eqguard *prv);

/* compares 2 lists for equality */
static int listequal_checked(tr7_t a, tr7_t b, struct eqguard *prv)
{
   int i = 0;
   tr7_t slowa, fasta;
   tr7_t slowb, fastb;

   slowa = fasta = a;
   slowb = fastb = b;
   for (;;) {
      if (!equal_unchecked(TR7_CAR(fasta), TR7_CAR(fastb), prv))
         return 0;
      fasta = TR7_CDR(fasta);
      fastb = TR7_CDR(fastb);
      if (TR7_IS_NIL(fasta))
         return TR7_IS_NIL(fastb);
      if (!TR7_IS_PAIR(fasta))
         return tr7_equal(fasta, fastb);
      if (!TR7_IS_PAIR(fastb))
         return 0;
      i = i ^ 1;
      if ((i & 1) == 0) {
         slowa = TR7_CDR(slowa);
         slowb = TR7_CDR(slowb);
         if (TR7EQ(fasta, slowa))
            i |= 2;
         if (TR7EQ(fastb, slowb))
            i |= 4;
         if (i == 8 + 4 + 2)
            return 1;
         if (i == 4 + 2) {
            i = 8;
            slowa = fasta;
            slowb = fastb;
         }
      }
   }
}

/* compares 2 cells for equality */
static int cellequal_checked(tr7_t a, tr7_t b, struct eqguard *prv)
{
   tr7_cell_t cella, cellb;
   tr7_head_t heada, headb;
   tr7_uint_t length;
   tr7_t *itemsa, *itemsb;

   /* CAUTION: next is optimized!!! be aware!!! */
   cella = TR7_TO_CELL(a);
   cellb = TR7_TO_CELL(b);
   heada = TR7_CELL_HEAD(cella);
   headb = TR7_CELL_HEAD(cellb);

   if (TR7_HEAD_KIND(heada ^ headb) == 0)
      switch (TR7_HEAD_KIND(heada)) {
      case TR7_HEAD_KIND_STRING:
      case TR7_HEAD_KIND_BYTEVECTOR:
         length = TR7_HEAD_UVALUE(heada);
         return length == TR7_HEAD_UVALUE(headb)
            && !memcmp(((tr7_buffer_t)cella)->content, ((tr7_buffer_t)cellb)->content, length);
      case TR7_HEAD_KIND_RECORD:
         if (heada != headb)
            return 0;
         length = TR7_CELL_RECORD_LENGTH(cella);
         goto itemseq;
      case TR7_HEAD_KIND_VECTOR:
         length = TR7_HEAD_UVALUE(heada);
         if (length != TR7_HEAD_UVALUE(headb))
            return 0;
itemseq:
         itemsa = (tr7_t*)cella;
         itemsb = (tr7_t*)cellb;
         while (length) {
            if (!equal_unchecked(*++itemsa, *++itemsb, prv))
               return 0;
            length--;
         }
         return 1;
      }

   return 0;
}

/* unchecked recursive equality */
static int equal_unchecked(tr7_t a, tr7_t b, struct eqguard *prv)
{
   struct eqguard guard;

   /* check pure equality */
   if (TR7EQ(a, b))
      return 1; /* symbols, integers, specials, identity */

   /* init the guard */
   if ((intptr_t)a < (intptr_t)b) {
      guard.a = a;
      guard.b = b;
   } else {
      guard.a = b;
      guard.b = a;
   }
   guard.prv = prv;
   while (prv != NULL) {
      if (prv->a == guard.a && prv->b == guard.b)
         return 1; /* equals? yes, doesn't avoid to fail later */
      prv = prv->prv;
   }

   /* recurse safely (if needed) now */
   if (TR7_IS_PAIR(guard.a))
      return TR7_IS_PAIR(guard.b) && listequal_checked(guard.a, guard.b, &guard);

   if (TR7_IS_CELL(guard.a))
      return TR7_IS_CELL(guard.b) && cellequal_checked(guard.a, guard.b, &guard);

   return tr7_cmp_num(guard.a, guard.b) == TR7_CMP_EQUAL; /* TODO: avoid equality if inexacts */
}

int tr7_equal(tr7_t a, tr7_t b)
{
   return equal_unchecked(a, b, NULL);
}

/* ========== Evaluation Cycle: handling tarnsformers ========== */

static int declare_transform(tr7_engine_t tsc, tr7_t envir, tr7_t trfspec)
{
   tr7_t name;
   tr7_t ellipsis;
   tr7_t literals;
   tr7_t rules;
   tr7_t x, y;

   /* get the name */
   if (!TR7_IS_PAIR(trfspec))
      goto error;
   name = TR7_CAR(trfspec);
   if (!TR7_IS_SYMBOL(name))
      goto error;

   /* enter the syntax-rules */
   trfspec = TR7_CDR(trfspec);
   if (!TR7_IS_PAIR(trfspec))
      goto error;
   rules = TR7_CAR(trfspec);
   trfspec = TR7_CDR(trfspec);
   if (!TR7_IS_NIL(trfspec) || !TR7_IS_PAIR(rules))
      goto error;
   x = TR7_CAR(rules);
   if (!TR7_IS_SYMBOL(x) || strcmp(tr7_symbol_string(x), "syntax-rules"))     /* TODO: adjust/optimize */
      goto error;

   /* get the ellipsis and the literals */
   rules = TR7_CDR(rules);
   if (!TR7_IS_PAIR(rules))
      goto error;
   ellipsis = TR7_CAR(rules);
   if (!TR7_IS_SYMBOL(ellipsis)) {
      literals = ellipsis;
      ellipsis = SYMBOL(ELLIPSIS);
   }
   else {
      rules = TR7_CDR(rules);
      if (!TR7_IS_PAIR(rules))
         goto error;
      literals = TR7_CAR(rules);
   }

   /* check the literals */
   x = literals;
   while (TR7_IS_PAIR(x)) {
      if (!TR7_IS_SYMBOL(TR7_CAR(x)))
         goto error;
      x = TR7_CDR(x);
   }
   if (!TR7_IS_NIL(x))
      goto error;

   /* get the rules */
   rules = TR7_CDR(rules);
   if (TR7_IS_NIL(rules))
      goto error;

   /* check the rules */
   x = rules;
   while (TR7_IS_PAIR(x)) {
      y = TR7_CAR(x);
      if (!TR7_IS_PAIR(y) || !TR7_IS_PAIR(TR7_CAR(y)))
         goto error;
      /* commented: testing good name in pattern
      if (!TR7EQ(TR7_CAAR(y), name))
         goto error;
      */
      y = TR7_CDR(y);
      if (!TR7_IS_PAIR(y))
         goto error;
      y = TR7_CDR(y);
      if (!TR7_IS_NIL(y))
         goto error;
      x = TR7_CDR(x);
   }
   if (!TR7_IS_NIL(x))
      goto error;

   /* record the rule */
   y = mk_transform(tsc, name, ellipsis, literals, rules, tsc->envir);
   if (!TR7_IS_NIL(y))
      return environment_set(tsc, name, y, envir, 1);

 error:
   return 0;
}




















struct trf {
   tr7_engine_t tsc;            /* the main scheme */
   tr7_t ellipsis;              /* given ellipsis */
   tr7_t underscore;            /* given underscore */
   tr7_t literals;              /* given literals */
   tr7_t envir;                 /* saved environment */
   tr7_t nonlitval;             /*  */
   tr7_t locals;                /* local bindings */
   int capture;                 /* capture the matching variables */
   int depth;                   /* depth of indexes */
   int indexes[8];              /* indexes */
};

#if 0
static int true_matching_transform(struct trf *trf, tr7_t pattern, tr7_t scan);
static int matching_transform(struct trf *trf, tr7_t pattern, tr7_t scan)
{
   int r;
   r = true_matching_transform(trf, pattern, scan);
      log_str(trf->tsc, "\n---------------------   matching\n");
      log_str(trf->tsc, "      matching expr:    ");
      log_item(trf->tsc, scan);
      log_str(trf->tsc, "\n");
      log_str(trf->tsc, "      matching pattern: ");
      log_item(trf->tsc, pattern);
      log_str(trf->tsc, "\n");
      log_str(trf->tsc, r ? "      YES\n" : "      NO\n");
return r;
}
static int true_matching_transform(struct trf *trf, tr7_t pattern, tr7_t scan)
#else
static int matching_transform(struct trf *trf, tr7_t pattern, tr7_t scan)
#endif
{
   int capture, depth, found;
   tr7_t x, y, i, pat, match;

   /* match symbol */
   if (TR7_IS_SYMBOL(pattern)) {
      /* literal ? */
      match = tr7_memq(pattern, trf->literals); /* search in literals */
      if (!TR7_IS_FALSE(match)) /* if found, is a literal */
         return TR7EQ(pattern, scan);   /* TODO conformity */

      /* matches if it where the question */
      if (!trf->capture)
         return 1;

      /* don't capture underscore */
      if (TR7EQ(pattern, trf->underscore))
         return 1;

      /* get the capture data */
      match = tr7_assq(pattern, trf->nonlitval);
      if (TR7_IS_FALSE(match)) {
         /* not found, add it */
         match = TR7_LIST2(trf->tsc, pattern, TR7_FROM_INT(trf->depth));
         trf->nonlitval = tr7_cons(trf->tsc, match, trf->nonlitval);
      }
      x = TR7_CDR(match); /* skip recorded depth */

      /* capture the value */
      depth = 0;
      while (depth + 1 < trf->depth) {
         i = TR7_FROM_INT(trf->indexes[depth++]);
         y = tr7_assq(TR7_CDR(x), i);
         if (TR7_IS_FALSE(y)) {
            y = TR7_LIST1(trf->tsc, i);
            TR7_CDR(x) = tr7_cons(trf->tsc, y, TR7_CDR(x));
         }
         x = y;
      }
      /* append at end */
      while(!TR7_IS_NIL(TR7_CDR(x)))
         x = TR7_CDR(x);
      TR7_CDR(x) = TR7_LIST1(trf->tsc, scan);
      return 1;
   }

   /* match list */
   if (TR7_IS_PAIR(pattern)) {
      do {
         /* get the pattern head */
         pat = TR7_CAR(pattern);
         pattern = TR7_CDR(pattern);
         /* check if ellipsis follows */
         if (!TR7_IS_PAIR(pattern) || !TR7EQ(TR7_CAR(pattern), trf->ellipsis) || TR7_IS_NIL(trf->ellipsis)) {
            /* no ellipsis, check if pair matching */
            if (!TR7_IS_PAIR(scan) || !matching_transform(trf, pat, TR7_CAR(scan)))
               return 0;
            scan = TR7_CDR(scan);
         } else {
            /* check depth */
            depth = trf->depth++;
            if (depth >= (int)(sizeof trf->indexes / sizeof *trf->indexes))
               return 0;
            /* ellipsis... advance pattern */
            pattern = TR7_CDR(pattern);
            /* match longuest prefix, without capturing */
            capture = trf->capture;
            trf->capture = 0;
            i = y = scan;
            found = matching_transform(trf, pattern, i);
            while (TR7_IS_PAIR(i) && matching_transform(trf, pat, TR7_CAR(i))) {
               i = TR7_CDR(i);
               if (matching_transform(trf, pattern, i)) {
                  found = 1;
                  scan = i;
               }
            }
            if (!found)
               return 0;
            if (capture) {
               /* capturing... */
               trf->capture = 1;
               trf->indexes[depth] = 0;
               i = y;
               while (!TR7EQ(i, scan)) {
                  matching_transform(trf, pat, TR7_CAR(i));
                  i = TR7_CDR(i);
                  trf->indexes[depth]++;
               }
            }
            trf->depth--;
         }
      } while (TR7_IS_PAIR(pattern));
      return matching_transform(trf, pattern, scan);
   }

   /* match vector */
   /* TODO */

   /* match others */
#if 0
      log_str(trf->tsc, "      EQV{ ");
      log_item(trf->tsc, pattern);
      log_str(trf->tsc, " }{ ");
      log_item(trf->tsc, scan);
      log_str(trf->tsc, tr7_equal(pattern, scan) ? " } = yes\n" : " } = NO\n");
#endif
   return tr7_equal(pattern, scan);
}

#if 0
static tr7_t true_apply_transform(struct trf *trf, tr7_t template);
static tr7_t apply_transform(struct trf *trf, tr7_t template)
{
   tr7_t r;
   r = true_apply_transform(trf, template);
      log_str(trf->tsc, "\n---------------------   transform\n");
      log_str(trf->tsc, "      from: ");
      log_item(trf->tsc, template);
      log_str(trf->tsc, "\n");
      log_str(trf->tsc, "      to:   ");
      log_item(trf->tsc, r);
      log_str(trf->tsc, "\n");
      log_str(trf->tsc, trf->capture ? "      CAPTURE\n" : "      STOPPED\n");
return r;
}
static tr7_t true_apply_transform(struct trf *trf, tr7_t template)
#else
static tr7_t apply_transform(struct trf *trf, tr7_t template)
#endif
{
   int d, j, dlow, dupp, depth, hascap;
   tr7_t match, x, y, t, head, tail;

   /* transform symbols */
   if (TR7_IS_SYMBOL(template)) {
      /* literal ? */
      match = tr7_memq(template, trf->literals); /* search in literals */
      if (!TR7_IS_FALSE(match))
         return template; /* yes -> the literal */

      /* captured item? */
      match = tr7_assq(template, trf->nonlitval);
      if (TR7_IS_FALSE(match)) {
         /* value of the environement of transform ? */
         if (environment_get(trf->envir, template, &x))
            return x; /* yes -> its value */
         /* no, a local binding */
         match = tr7_assq(template, trf->locals);
         if (TR7_IS_FALSE(match)) {
            /* create a local symbol for the given name */
            x = tr7_make_symbol_copy(trf->tsc, (char*)TR7_CONTENT_SYMBOL(template));
            match = tr7_cons(trf->tsc, template, x);
            trf->locals = tr7_cons(trf->tsc, match, trf->locals);
         }
         return TR7_CDR(match);
      }

      /* the captured item */
      x = TR7_CDR(match);
      depth = TR7_TO_INT(TR7_CAR(x));
      x = TR7_CDR(x);
      d = 0;
      while (d + 1 < depth) {
         if (d >= trf->depth)
            return TR7_NIL;
         j = trf->indexes[d++];
         y = tr7_assq(TR7_FROM_INT(j), x);
         if (TR7_IS_FALSE(y))
            return TR7_NIL;
         x = TR7_CDR(y);
      }
      j = depth ? trf->indexes[d] : 0;
      while (j && TR7_IS_PAIR(x)) {
         x = TR7_CDR(x);
         j--;
      }
      if (!TR7_IS_PAIR(x))
         return TR7_NIL;
      trf->capture = 1;
      return TR7_CAR(x);
   }

   /* transform list */
   if (TR7_IS_PAIR(template)) {
      /* special case of an ellipsis at start */
      t = TR7_CAR(template);
      template = TR7_CDR(template);
      if (TR7EQ(t, trf->ellipsis) && !TR7_IS_NIL(t)) {
         return TR7_IS_PAIR(template) ? TR7_CAR(template) : template;
      }
      /* make a list */
      dlow = trf->depth;
      head = tail = TR7_NIL;
      for (;;) {
         /* yes, how many ellipsis ? */
         dupp = dlow;
         while (TR7_IS_PAIR(template) && (x = TR7_CAR(template), !TR7_IS_NIL(x)) && TR7EQ(x, trf->ellipsis)) {
            dupp++;
            template = TR7_CDR(template);
         }
         /* followed by ellipsis ? */
         if (dlow == dupp) {
            /* no, not followed by ellipsis */
            x = apply_transform(trf, t);
            y = tr7_cons(trf->tsc, x, TR7_NIL);
            if (TR7_IS_NIL(tail))
               head = y;
            else
               TR7_CDR(tail) = y;
            tail = y;
         }
         else {
            /* init */
            trf->depth = dupp--;
            for (d = dlow ; d <= dupp ; d++)
               trf->indexes[d] = 0;
            hascap = 0;
            /* loop */
            for (;;) {
               /* get a template instance */
               trf->capture = 0;
               x = apply_transform(trf, t);
               if (trf->capture) {
                  /* got one, record it */
                  y = tr7_cons(trf->tsc, x, TR7_NIL);
                  if (TR7_IS_NIL(tail))
                     head = y;
                  else
                     TR7_CDR(tail) = y;
                  tail = y;
                  hascap = 1;
                  /* next */
                  trf->indexes[dupp]++;
               }
               else {
                  /* got none, continue next depths */
                  d = dupp;
                  while (d >= dlow && trf->indexes[d] == 0)
                     d--;
                  if (d <= dlow)
                     break; /* next, continue */
                  trf->indexes[d] = 0;
                  trf->indexes[d - 1]++;
               }
            }
            trf->capture = hascap;

            /* terminate */
            trf->depth = dlow;
         }
         if (!TR7_IS_PAIR(template))
            break;
         t = TR7_CAR(template);
         template = TR7_CDR(template);
      }
      if (!TR7_IS_NIL(template)) {
         x = apply_transform(trf, template);
         if (TR7_IS_NIL(tail))
            head = x;
         else
            TR7_CDR(tail) = x;
      }
      return head;
   }

   /* transform vector */
   /* TODO */

   /* transform others */
   return template;
}

static tr7_t eval_transform(tr7_engine_t tsc, tr7_t transform, tr7_t expr)
{
   tr7_transform_t t;
   tr7_t pattern, rules, scanned, template, result;
   struct trf trf;

   /* retrieve the rules */
   t = TR7_TO_TRANSFORM(transform);
   trf.tsc = tsc;
   trf.ellipsis = t->ellipsis;
   trf.underscore = SYMBOL(UNDERSCORE);
   trf.literals = t->literals;
   trf.envir = t->env;
   rules = t->rules;
   scanned = TR7_CDR(expr);
   while (!TR7_IS_NIL(rules)) {
      /* inspect the rule */
      pattern = TR7_CAAR(rules);
      /* skip heads */
#define DEBUG_SYNTAX 0
#if DEBUG_SYNTAX
      log_str(tsc, "\nmatching\n");
      log_str(tsc, "      syntax match expr:    ");
      log_item(tsc, expr);
      log_str(tsc, "\n");
      log_str(tsc, "      syntax match pattern: ");
      log_item(tsc, pattern);
      log_str(tsc, "\n");
#endif
      pattern = TR7_CDR(pattern);
      /* init match */
      trf.nonlitval = TR7_NIL;
      trf.locals = TR7_NIL;
      trf.capture = 0;
      trf.depth = 0;
      if (matching_transform(&trf, pattern, scanned)) {
#if DEBUG_SYNTAX
         log_str(tsc, "      MATCHES\n");
#endif
         trf.capture = 1;
         trf.depth = 0;
         matching_transform(&trf, pattern, scanned);
#if DEBUG_SYNTAX
         log_str(tsc, "\n      CAPTURES: ");
         log_item(tsc, trf.nonlitval);
         log_str(tsc, "\n\n");
#endif
         template = TR7_CADAR(rules);
#if DEBUG_SYNTAX
         log_str(tsc, "      transform from ");
         log_item(tsc, template);
         log_str(tsc, "\n");
#endif
         result = apply_transform(&trf, template);
#if DEBUG_SYNTAX
         log_str(tsc, "      transform to ");
         log_item(tsc, result);
         log_str(tsc, "\n\n");
#endif
         return result;
      }
#if DEBUG_SYNTAX
      log_str(tsc, "      NO MATCH\n");
#endif
      /* no match try next */
      rules = TR7_CDR(rules);
   }
   return TR7_NIL;
}
#undef DEBUG_SYNTAX

/* ========== Management of libraries ======================== */

/* test if libname is valid */
static int is_valid_libname(tr7_t libname)
{
   while (TR7_IS_PAIR(libname)) {
      tr7_t k = TR7_CAR(libname);
      if (TR7_IS_SYMBOL(k) || (TR7_IS_INT(k) && TR7_TO_INT(k) >= 0)) {
         libname = TR7_CDR(libname);
         if (TR7_IS_NIL(libname))
            return 1;
      }
   }
   return 0;
}

/* translate a valid libname to filename ex: (hi guy 1) -> hi/guy/1 */
static int get_libname(tr7_t libname, char buffer[], int size)
{
   char bufint[30];
   const char *ptr;
   tr7_t x;
   int pos = 0;

   do {
      /* get the car, should be a symbol or a non negative integer */
      x = TR7_CAR(libname);
      if (TR7_IS_SYMBOL(x))
         ptr = (char*)TR7_CONTENT_SYMBOL(x);
      else {
         sprintf(bufint, "%lld", (long long)TR7_TO_INT(x));
         ptr = bufint;
      }

      /* append the value found */
      if (pos) {
         if (pos < size) buffer[pos] = LIB_SEP_CHAR;
         pos++;
      }
      while(*ptr) {
         if (pos < size) buffer[pos] = *ptr;
         pos++;
         ptr++;
      }

      /* next of the list */
      libname = TR7_CDR(libname);
   } while(!TR7_IS_NIL(libname));

   /* return the length excluding 0 */
   return pos;
}

/* ========== Evaluation Cycle: handling stack ========== */

static eval_status_t s_goto(tr7_engine_t tsc, opidx_t op)
{
   tsc->curop = op;
   return Cycle_Goto;
}

static eval_status_t s_exec(tr7_engine_t tsc, tr7_t oper, tr7_t args)
{
   tsc->oper = oper;
   tsc->args = args;
#if ASSUME_TAIL_CALL_OPT
   return do_exec2(tsc, oper, args);
#else
   return s_goto(tsc, OP(EXEC));
#endif
}

static eval_status_t s_exec_0(tr7_engine_t tsc, tr7_t oper)
{
   return s_exec(tsc, oper, TR7_NIL);
}

static eval_status_t s_exec_1(tr7_engine_t tsc, tr7_t oper, tr7_t arg1)
{
   return s_exec(tsc, oper, TR7_LIST1(tsc, arg1));
}

static eval_status_t s_eval(tr7_engine_t tsc, tr7_t expr)
{
#if ASSUME_TAIL_CALL_OPT
   return do_eval(tsc, expr);
#else
   tsc->args = expr;
   return s_goto(tsc, OP(IEVAL));
#endif
}

/* exit the current frame */
static eval_status_t s_return(tr7_engine_t tsc)
{
   tr7_cell_t x;
   tr7_t d;
   tr7_stack_t s;
   tr7_dynawind_t dw;
   tr7_paramsave_t ps;

   d = tsc->dump;
   while(!TR7_IS_NIL(d)) {
      x = TR7_TO_CELL(d);
      switch(TR7_CELL_KIND(x)) {
      case TR7_HEAD_KIND_STACK:
         s = TR7_CELL_TO_STACK(x);
         tsc->curop = (opidx_t)TR7_HEAD_VALUE(s->head);
         tsc->args = s->args;
         tsc->envir = s->env;
         tsc->dump = s->dump;
#if OPTIMIZE_NO_CALLCC
         if (!TR7_HEAD_IS_IMMUTABLE(s->head)) {
            s->dump = tsc->savestack;
            tsc->savestack = d;
         }
#endif
         return Cycle_Return;

      case TR7_HEAD_KIND_DYNAWIND:
         dw = TR7_CELL_TO_DYNAWIND(x);
         tsc->curop = OP(THUNK);
         tsc->args = dw->after;
         tsc->envir = dw->env;
         tsc->dump = dw->dump;
         return Cycle_Return;

      case TR7_HEAD_KIND_PARAMSAVE:
         ps = TR7_CELL_TO_PARAMSAVE(x);
         tsc->params = ps->params;
         d = ps->dump;
         break;

      default:
         /* skip guards */
         d = TR7_CELL_TO_DUMPABLE(x)->dump;
         break;
      }
   }
   tsc->dump = TR7_NIL;
   return Cycle_Exit;
}

static eval_status_t s_return_single(tr7_engine_t tsc, tr7_t a)
{
   tsc->value = a;
   tsc->values = TR7_NIL;
   return s_return(tsc);
}

static eval_status_t s_return_integer(tr7_engine_t tsc, tr7_int_t i)
{
   return s_return_single(tsc, tr7_from_int(tsc, i));
}

static eval_status_t s_return_double(tr7_engine_t tsc, double d)
{
   return s_return_single(tsc, tr7_from_double(tsc, d));
}

static eval_status_t s_return_void(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_VOID);
}

static eval_status_t s_return_nil(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_NIL);
}

static eval_status_t s_return_false(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_FALSE);
}

static eval_status_t s_return_true(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_TRUE);
}

static eval_status_t s_return_EOF(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_EOF);
}

static eval_status_t s_return_boolean(tr7_engine_t tsc, int value)
{
   return value ? s_return_true(tsc) : s_return_false(tsc);
}

static eval_status_t s_return_values(tr7_engine_t tsc, tr7_t a)
{
   tsc->values = a;
   tsc->value = TR7_IS_PAIR(a) ? TR7_CAR(a) : a;
   return s_return(tsc);
}

static tr7_t get_values(tr7_engine_t tsc)
{
   return TR7_IS_NIL(tsc->values) ? TR7_LIST1(tsc, tsc->value) : tsc->values;
}

static tr7_t mk_stack(tr7_engine_t tsc, opidx_t op, tr7_t args, tr7_t env, tr7_t dump)
{
#if OPTIMIZE_NO_CALLCC
   tr7_stack_t s;
   tr7_t h = tsc->savestack;
   if (TR7_IS_NIL(h))
      s = GET_CELLS(tsc, s, 0);
   else {
      s = TR7_TO_STACK(h);
      tsc->savestack = s->dump;
   }
#else
   tr7_stack_t s = GET_CELLS(tsc, s, 0);
#endif
   if (s == NULL) {
      fprintf(stderr, "stack overflow!\n");
      exit(1); /* TODO: quiece gently */
   }
   s->head = TR7_MAKE_HEAD(op, TR7_HEAD_KIND_STACK);
   s->dump = dump;
   s->args = args;
   s->env = env;
   return TR7_FROM_STACK(s);
}

static void s_push(tr7_engine_t tsc, opidx_t op, tr7_t args, tr7_t env)
{
   tsc->dump = mk_stack(tsc, op, args, env, tsc->dump);
}

static void s_save(tr7_engine_t tsc, opidx_t op, tr7_t args)
{
   s_push(tsc, op, args, tsc->envir);
}

static void s_save_nil(tr7_engine_t tsc, opidx_t op)
{
   s_save(tsc, op, TR7_NIL);
}

static void s_save_cons(tr7_engine_t tsc, opidx_t op, tr7_t car, tr7_t cdr)
{
   s_save(tsc, op, tr7_cons(tsc, car, cdr));
}

static void s_save_arg1(tr7_engine_t tsc, opidx_t op, tr7_t arg1)
{
   s_save_cons(tsc, op, arg1, TR7_NIL);
}

static eval_status_t s_begin(tr7_engine_t tsc, tr7_t exprs)
{
#if ASSUME_TAIL_CALL_OPT
   if (!TR7_IS_PAIR(exprs))
      return s_return(tsc);
   return do_begin(tsc, exprs);
#else
   tsc->args = exprs;
   return s_goto(tsc, OP(BEGIN));
#endif
}

/* ========== Evaluation Cycle: handling exceptions ========== */

static tr7_guard_t s_get_guard(tr7_engine_t tsc, tr7_t dump)
{
   tr7_cell_t x;

   /* skip stack until guard */
   while (!TR7_IS_NIL(dump)) {
      x = TR7_TO_CELL(dump);
      switch(TR7_CELL_KIND(x)) {
      case TR7_HEAD_KIND_GUARD:
         return TR7_CELL_TO_GUARD(x);
      case TR7_HEAD_KIND_PARAMSAVE:
         tsc->params = TR7_CELL_TO_PARAMSAVE(x)->params;
         /*@fallthrough@*/
      default:
         dump = TR7_CELL_TO_STACK(x)->dump;
         break;
      }
   }
   return NULL;
}

static void s_guard_kind(tr7_engine_t tsc, tr7_t expr, unsigned kind, tr7_t env)
{
   tr7_guard_t g = GET_CELLS(tsc, g, 0);
   g->head = TR7_MAKE_HEAD(kind, TR7_HEAD_KIND_GUARD);
   g->dump = tsc->dump;
   g->expr = expr;
   g->env = env;
   tsc->dump = TR7_FROM_GUARD(g);
}

static void s_guard(tr7_engine_t tsc, tr7_t expr, unsigned kind)
{
   s_guard_kind(tsc, expr, kind, tsc->envir);
}

/* ========== Evaluation Cycle: handling dynamic wind ========== */

static tr7_t dynawind_create(tr7_engine_t tsc, tr7_t before, tr7_t after, tr7_t link, tr7_t dump, tr7_t env)
{
   tr7_uint_t depth = TR7_IS_NIL(link) ? 0 : TR7_DYNAWIND_DEPTH(TR7_TO_DYNAWIND(link));
   tr7_dynawind_t dw = GET_CELLS(tsc, dw, 0);
   dw->head = TR7_MAKE_HEAD(depth + 1, TR7_HEAD_KIND_DYNAWIND);
   dw->dump = dump;
   dw->env = env;
   dw->before = before;
   dw->after = after;
   dw->link = link;
   return TR7_FROM_DYNAWIND(dw);
}

/* nearest dynamic-wind pushed on stack or TR7_NIL */
static tr7_t dynawind_search_in_dump(tr7_t dump)
{
   tr7_cell_t x;
   /* skip stack until dynawind */
   while (!TR7_IS_NIL(dump)) {
      x = TR7_TO_CELL(dump);
      if (TR7_CELL_IS_DYNAWIND(x))
         break;
      dump = TR7_CELL_TO_DUMPABLE(x)->dump;
   }
   return dump;
}

/* pushes the dynamic wind having before and after */
static void dynawind_push(tr7_engine_t tsc, tr7_t before, tr7_t after)
{
   tr7_t prvdw = dynawind_search_in_dump(tsc->dump);
   tsc->dump = dynawind_create(tsc, before, after, prvdw, tsc->dump, tsc->envir);
}

/* pushes action to perform when changing of dynamic environment */
static tr7_t dynawind_compute_actions(tr7_engine_t tsc, tr7_t from_dump, tr7_t to_dump)
{
   tr7_t tmp;
   tr7_t fromit = dynawind_search_in_dump(from_dump);
   tr7_t toit = dynawind_search_in_dump(to_dump);
   tr7_uint_t fromdepth = TR7_IS_NIL(fromit) ? 0 : TR7_DYNAWIND_DEPTH(TR7_TO_DYNAWIND(fromit));
   tr7_uint_t todepth = TR7_IS_NIL(toit) ? 0 : TR7_DYNAWIND_DEPTH(TR7_TO_DYNAWIND(toit));
   tr7_t afters = TR7_NIL;
   tr7_t result = to_dump;
   while (!TR7EQ(fromit, toit)) {
      if (fromdepth >= todepth) {
         afters = mk_stack(tsc, OP(THUNK), TR7_TO_DYNAWIND(fromit)->after, TR7_TO_DYNAWIND(fromit)->env, afters);
         push_recent_alloc(tsc, afters);
         fromit = TR7_TO_DYNAWIND(fromit)->link;
         fromdepth--;
      }
      if (todepth > fromdepth) {
         result = mk_stack(tsc, OP(THUNK), TR7_TO_DYNAWIND(toit)->before, TR7_TO_DYNAWIND(toit)->env, result);
         push_recent_alloc(tsc, result);
         toit = TR7_TO_DYNAWIND(toit)->link;
         todepth--;
      }
   }
   while (!TR7_IS_NIL(afters)) {
      tmp = TR7_TO_STACK(afters)->dump;
      TR7_TO_STACK(afters)->dump = result;
      result = afters;
      afters = tmp;
   }
   return result;
}

/* ========== Evaluation Cycle: saving parameters ========== */

static void parameters_save(tr7_engine_t tsc)
{
   tr7_paramsave_t ps = GET_CELLS(tsc, ps, 0);

   ps->head = TR7_HEAD_KIND_PARAMSAVE;
   ps->dump = tsc->dump;
   ps->params = tsc->params;
   tsc->dump = TR7_FROM_PARAMSAVE(ps);
}

static tr7_t parameter_get(tr7_engine_t tsc, tr7_t param)
{
   tr7_pair_t latest = tr7_assq_pair(param, tsc->params);
   return latest ? latest->cdr : TR7_TO_PARAMETER(param)->value;
}

static void parameter_set(tr7_engine_t tsc, tr7_t param, tr7_t value)
{
   tr7_pair_t latest = tr7_assq_pair(param, tsc->params);
   if (latest)
      latest->cdr = value;
   else
      TR7_TO_PARAMETER(param)->value = value;
}

static void parameter_push(tr7_engine_t tsc, tr7_t param, tr7_t value)
{
   parameters_save(tsc);
   tsc->params = tr7_cons(tsc, tr7_cons(tsc, param, value), tsc->params);
}

/* ========== Evaluation Cycle: handling errors ========== */


/* test if item is an error */
int tr7_is_error(tr7_engine_t tsc, tr7_t item)
{
   return tr7_is_record_type(item, tsc->error_desc);
}

/* test if item is a read error */
int tr7_is_read_error(tr7_engine_t tsc, tr7_t item)
{
   return tr7_is_record_type(item, tsc->read_error_desc);
}

/* test if item is a file error */
int tr7_is_file_error(tr7_engine_t tsc, tr7_t item)
{
   return tr7_is_record_type(item, tsc->file_error_desc);
}

/* helper for accessing error-object items */
static tr7_t error_item(tr7_engine_t tsc, tr7_t errobj, int index)
{
   tr7_record_t rec = tr7_as_record_cond(errobj, tsc->error_desc);
   return rec->items[index];
}

/* access error message */
tr7_t tr7_error_message(tr7_engine_t tsc, tr7_t errobj)
{
   return error_item(tsc, errobj, RECORD_FIELD_OFFSET + 0);
}

/* access error irritants */
tr7_t tr7_error_irritants(tr7_engine_t tsc, tr7_t errobj)
{
   return error_item(tsc, errobj, RECORD_FIELD_OFFSET + 1);
}

/* create an instance of error */
static tr7_t mk_error_instance(tr7_engine_t tsc, tr7_t errdsc, tr7_t msg, tr7_t irritants)
{
   return mk_record_instance(tsc, errdsc, TR7_LIST2(tsc, msg, irritants));
}

static tr7_t mk_error_1(tr7_engine_t tsc, const char *s, tr7_t a, tr7_t errdsc)
{
   char sbuf[STRBUFFSIZE];
   const char *str = s, *opn = get_opname(tsc->curop, NULL);
   int sts, offset = 0;
   tr7_t t;

#if SHOW_ERROR_LINE
   /* make sure error is not in REPL */
   if (TR7_IS_PORT(tsc->loadport)) {
      port_t *pt = TR7__PORT__PORT(tsc->loadport);
      int ln = pt->line;
      const char *fname = pt->flags & port_file ? pt->rep.stdio.filename : NULL;

      if (fname) {
         sts = snprintf(sbuf, sizeof sbuf, "(%s : %i) ", fname, ln);
         if (sts > 0 && sts < (int)sizeof sbuf)
            offset = sts;
      }
   }
#endif
   if (opn) {
      sts = snprintf(&sbuf[offset], (sizeof sbuf) - offset, "%s: ", opn);
      if (sts > 0 && offset + sts < (int)sizeof sbuf)
            offset += sts;
   }
   if (offset) {
      sts = snprintf(&sbuf[offset], (sizeof sbuf) - offset, "%s", str);
      if (sts > 0 && offset + sts < (int)sizeof sbuf)
            str = sbuf;
   }

   t = tr7_make_string_copy(tsc, str);
   return mk_error_instance(tsc, errdsc, t, a);
}

static eval_status_t Error_1(tr7_engine_t tsc, const char *s, tr7_t a)
{
   tr7_t e = mk_error_1(tsc, s, a, tsc->error_desc);
   return do_raise(tsc, e, 0);
}

static eval_status_t Error_0(tr7_engine_t tsc, const char *s)
{
   return Error_1(tsc, s, TR7_NIL);
}

static eval_status_t error_out_of_memory(tr7_engine_t tsc)
{
   return Error_0(tsc, "out of memory");
}

static eval_status_t error_out_of_bound(tr7_engine_t tsc)
{
   return Error_1(tsc, "out of bound", tsc->args);
}

static eval_status_t error_invalid_argument(tr7_engine_t tsc)
{
   return Error_1(tsc, "invalid argument", tsc->args);
}

static eval_status_t error_unbound(tr7_engine_t tsc, tr7_t varname)
{
   return Error_1(tsc, "unbound variable", varname);
}

static eval_status_t error_immutable(tr7_engine_t tsc)
{
   return Error_0(tsc, "can not set immutable");
}

static eval_status_t error_division_by_zero(tr7_engine_t tsc)
{
   return Error_0(tsc, "division by zero");
}

static eval_status_t s_return_single_alloc(tr7_engine_t tsc, tr7_t value)
{
   return TR7_IS_NIL(value) ? error_out_of_memory(tsc) : s_return_single(tsc, value);
}

#define Error_unavailable_feature(tsc, feat) Error_0(tsc, "unavailable feature (" feat "=0)")
#define Error_not_yet_implemented(tsc) Error_0(tsc, "not yet implemented")

static eval_status_t read_error_1(tr7_engine_t tsc, const char *s, tr7_t a)
{
   tr7_t e = mk_error_1(tsc, s, a, tsc->read_error_desc);
   return do_raise(tsc, e, 0);
}

static eval_status_t file_error_1(tr7_engine_t tsc, const char *s, tr7_t a)
{
   tr7_t e = mk_error_1(tsc, s, a, tsc->file_error_desc);
   return do_raise(tsc, e, 0);
}

/*************************************************************************
* SECTION SCHEME_BASE
* -------------------
*/
static eval_status_t do_begin(tr7_engine_t tsc, tr7_t exprs)
{
   tr7_t next = TR7_CDR(exprs);
   if (!TR7_IS_NIL(next))
      s_save(tsc, OP(BEGIN), next);
   return s_eval(tsc, TR7_CAR(exprs));
}

static eval_status_t do_xreturn(tr7_engine_t tsc, tr7_t args)
{
   if (TR7_IS_NIL(args))
      return s_return_nil(tsc);
   if (TR7_IS_NIL(TR7_CDR(args)))
      return s_return_single(tsc, TR7_CAR(args));
   return s_return_values(tsc, args);
}

/* realize exec of (oper . args) */
static eval_status_t do_exec2(tr7_engine_t tsc, tr7_t oper, tr7_t args)
{
   tr7_t x, z, zz;
   int alen, blen;
#if USE_TRACING
   if (tsc->tracing) {
      log_str(tsc, "\nApply ");
      log_item(tsc, oper);
      log_str(tsc, " to: ");
      log_item(tsc, args);
   }
#endif
   /* PROCEDURE ? */
   if (TR7_IS_OPER(oper))
      return s_goto(tsc, TR7_TO_OPER(oper));

   /* LAMBDA or MACRO? */
   if (TR7_IS_LAMBDA(oper) || TR7_IS_MACRO(oper)) {
      /* make environment */
      new_environment(tsc, TR7_TO_CLOSURE(oper)->env);
      z = TR7_TO_CLOSURE(oper)->expr;
      x = TR7_CAR(z);
      switch (bind_list(tsc, x, args)) {
      case BIND_LIST_OK:
         break;
      case BIND_LIST_MORE:
         /* return Error_0(tsc,"too many arguments"); */
         break;
      case BIND_LIST_MISS:
         return Error_0(tsc, "not enough arguments");
      case BIND_LIST_BAD:
         return Error_1(tsc, "bad parameter list: ", x);
      }
      return s_begin(tsc, TR7_CDR(z));
   }

   /* LAMBDA or MACRO? */
   if (TR7_IS_CASE_LAMBDA(oper)) {
      /* make environment */
      new_environment(tsc, TR7_TO_CLOSURE(oper)->env);
      z = TR7_TO_CLOSURE(oper)->expr;
      alen = tr7_list_length(args);
      while(TR7_IS_PAIR(z)) {
         x = TR7_CAR(z);
         blen = tr7_list_length(TR7_CAR(x)); /*TODO: (pair? x) */
         if (blen == alen || (blen < 0 && blen >= 2 - alen)) {
            bind_list(tsc, TR7_CAR(x), args);
            return s_begin(tsc, TR7_CDR(x));
         }
         z = TR7_CDR(z);
      }
      return Error_0(tsc, "unbound case-lambda");
   }

   /* FOREIGN FUNCTION ? */
   if (TR7_IS_FF(oper))
      return exec_ff(tsc, args,  TR7_TO_FF(oper));

   /* CONTINUATION ? */
   if (TR7_IS_CONTINUATION(oper)) {
      x = tsc->dump;
      tsc->dump = TR7_TO_CONTINUATION(oper)->dump;
      s_save(tsc, OP(XRETURN), args);
      tsc->dump = dynawind_compute_actions(tsc, x, tsc->dump);
      return s_return(tsc);
   }

   /* PARAMETER ? */
   if (TR7_IS_PARAMETER(oper)) {
      x = args;
      if (TR7_IS_NIL(x))
         return s_return_single(tsc, parameter_get(tsc, oper));
      if (!TR7_IS_PAIR(x) || !TR7_IS_NIL(TR7_CDR(x)))
         return Error_0(tsc, "wrong parameter argument");
      zz = TR7_TO_PARAMETER(oper)->converter;
      if (TR7_IS_NIL(zz)) {
         x = TR7_CAR(x);
         parameter_set(tsc, oper, x);
         return s_return_single(tsc, x);
      }
      s_save(tsc, OP(PARAMCVT), oper);
      return s_exec(tsc, zz, x);
   }
   return Error_0(tsc, "illegal function");
}

/* operate OP(E2ARGS): eval arguments end */
static eval_status_t do_e2args(tr7_engine_t tsc, tr7_t done, tr7_t last)
{
   tr7_t x = tr7_reverse(tsc, done, last);
   return s_exec(tsc, TR7_CAR(x), TR7_CDR(x));
}

/* operate OP(E1ARGS): eval arguments */
static eval_status_t do_e1args(tr7_engine_t tsc, tr7_t done, tr7_t todo)
{
   if (TR7_IS_PAIR(todo)) {  /* continue */
      s_save_cons(tsc, OP(E1ARGS),  TR7_CDR(todo), done);
      return s_eval(tsc, TR7_CAR(todo));
   }
   if (!TR7_IS_NIL(todo)) {
      s_save(tsc, OP(E2ARGS), done);
      return s_eval(tsc, todo);
   }
#if ASSUME_TAIL_CALL_OPT
   return do_e2args(tsc, done, TR7_NIL);
#else
   tsc->value = TR7_NIL;
   tsc->args = done;
   return s_goto(tsc, OP(E2ARGS));
#endif
}

#if OPTIMIZE_NO_CALLCC
/* operate OP(E2ARGS): eval arguments end */
static eval_status_t do_e2args_nocc(tr7_engine_t tsc, tr7_t done, tr7_t last)
{
   tr7_t x = tr7_reverse_in_place(done, last);
   return s_exec(tsc, TR7_CAR(x), TR7_CDR(x));
}

/* operate OP(E1ARGS): eval arguments */
static eval_status_t do_e1args_nocc(tr7_engine_t tsc, tr7_t done, tr7_t todo)
{
   if (TR7_IS_PAIR(todo)) {  /* continue */
      s_save_cons(tsc, OP(E1ARGS_NOCC), TR7_CDR(todo), done);
      return s_eval(tsc, TR7_CAR(todo));
   }
   if (!TR7_IS_NIL(todo)) {
      s_save(tsc, OP(E2ARGS_NOCC), done);
      return s_eval(tsc, todo);
   }
#if ASSUME_TAIL_CALL_OPT
   return do_e2args_nocc(tsc, done, TR7_NIL);
#else
   tsc->value = TR7_NIL;
   tsc->args = done;
   return s_goto(tsc, OP(E2ARGS_NOCC));
#endif
}
#endif

/* operate OP(E0ARGS): eval action */
static eval_status_t do_e0args(tr7_engine_t tsc, tr7_t head_value, tr7_t expr)
{
   tr7_t y;
   if (TR7_IS_SYNTAX(head_value)) {
      tsc->args = TR7_CDR(expr);
      return s_goto(tsc, TR7_TO_SYNTAX(head_value));
   }
   if (TR7_IS_TRANSFORM(head_value)) {    /* syntax rule */
      y = eval_transform(tsc, head_value, expr);
      if (TR7_IS_NIL(y))
         return Error_1(tsc, "unmatched syntax:", expr);
      return s_eval(tsc, y);
   }
   if (TR7_IS_MACRO(head_value)) {        /* macro expansion */
      s_save_nil(tsc, OP(XEVAL));
      return s_exec_1(tsc, head_value, expr);
   }
#if ASSUME_TAIL_CALL_OPT
   y = tr7_cons(tsc, head_value, TR7_NIL);
#if OPTIMIZE_NO_CALLCC
   return do_e1args_nocc(tsc, y, TR7_CDR(expr));
#else
   return do_e1args(tsc, y, TR7_CDR(expr));
#endif
#else
   tsc->args = tr7_cons(tsc, TR7_CDR(expr), TR7_NIL);
   tsc->value = head_value;
#if OPTIMIZE_NO_CALLCC
   return s_goto(tsc, OP(E1ARGS_NOCC));
#else
   return s_goto(tsc, OP(E1ARGS));
#endif
#endif
}

/* operate OP(IEVAL) */
static eval_status_t do_eval(tr7_engine_t tsc, tr7_t expr)
{
   tr7_t value, head;
#if USE_TRACING
   if (tsc->tracing) {
      log_str(tsc, "\nEval: ");
      log_item(tsc, expr);
   }
#endif
   if (!TR7_IS_PAIR(expr)) {
      if (!TR7_IS_SYMBOL(expr))
         return s_return_single(tsc, expr);
      /* symbol */
      if (environment_get(tsc->envir, expr, &value))
         return s_return_single(tsc, value);
      return error_unbound(tsc, expr);
   }
   /* pair */
   head = TR7_CAR(expr);
   if (TR7_IS_SYMBOL(head)) {
#if USE_TRACING
      if (tsc->tracing) {
         log_str(tsc, "\nEval: ");
         log_item(tsc, head);
      }
#endif
      if (!environment_get(tsc->envir, head, &value))
         return error_unbound(tsc, head);
#if USE_TRACING
      if (tsc->tracing) {
         log_str(tsc, "\nReturns: ");
         log_item(tsc, value);
      }
#endif
#if ASSUME_TAIL_CALL_OPT
      return do_e0args(tsc, value, expr);
#else
      tsc->value = value;
      tsc->args = expr;
      return s_goto(tsc, OP(E0ARGS));
#endif
   }
   s_save(tsc, OP(E0ARGS), expr);
   return s_eval(tsc, head);
}

/* implement OP(EXEC) */
static eval_status_t op_exec(tr7_engine_t tsc)
{
   return do_exec2(tsc, tsc->oper, tsc->args);
}

/* implement OP(THUNK) */
static eval_status_t op_thunk(tr7_engine_t tsc)
{
   return do_exec2(tsc, tsc->args, TR7_NIL);
}

/* implement OP(IEVAL) */
static eval_status_t op_ieval(tr7_engine_t tsc)
{
   return do_eval(tsc, tsc->args);
}

/* implement OP(E0ARGS): eval action */
static eval_status_t op_e0args(tr7_engine_t tsc)
{
   return do_e0args(tsc, tsc->value, tsc->args);
}

/* implement OP(E1ARGS): eval arguments */
static eval_status_t op_e1args(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t todo = TR7_CAR(args);
   tr7_t done = tr7_cons(tsc, tsc->value, TR7_CDR(args));
   return do_e1args(tsc, done, todo);
}

/* implement OP(E2ARGS): eval arguments end */
static eval_status_t op_e2args(tr7_engine_t tsc)
{
   return do_e2args(tsc, tsc->args, tsc->value);
}

#if OPTIMIZE_NO_CALLCC
/* implement OP(E1ARGS_NOCC): eval arguments */
static eval_status_t op_e1args_nocc(tr7_engine_t tsc)
{
   tr7_t done = tsc->args;
   tr7_t todo = TR7_CAR(done);
   TR7_CAR(done) = tsc->value;
   tsc->value = todo;/*save todo for gc!*/
   return do_e1args_nocc(tsc, done, todo);
}

/* implement OP(E2ARGS_NOCC): eval arguments end */
static eval_status_t op_e2args_nocc(tr7_engine_t tsc)
{
   return do_e2args_nocc(tsc, tsc->args, tsc->value);
}
#endif

/* ========== Evaluation Cycle ========== */

/* implement 'quote' */
static eval_status_t op_quote(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_CAR(tsc->args));
}

/* operate quasiquote */
static eval_status_t do_quasiquote(tr7_engine_t tsc, int depth, tr7_t item)
{
   opidx_t op;
   tr7_t head, tail;
   for(;;) {
      if (!TR7_IS_PAIR(item)) /* TODO: what for arrays? */
         return s_return_single(tsc, item);
      head = TR7_CAR(item);
      tail = TR7_CDR(item);
      if (TR7EQ(head, SYMBOL(QUASIQUOTE))) {
         s_save(tsc, OP(QQ_CONSIF), item);
         depth++;
         item = tail;
      }
      else if (TR7EQ(head, SYMBOL(UNQUOTE))) {
         if (depth == 0)
            return s_eval(tsc, TR7_CAR(tail));
         s_save(tsc, OP(QQ_CONSIF), item);
         depth--;
         item = tail;
      }
      else if (TR7EQ(head, SYMBOL(UNQUOTE_SPLICING))) {
         if (depth == 0)
            return Error_1(tsc, "Unquote-splicing isn't in list", item);
         s_save(tsc, OP(QQ_CONSIF), item);
         depth--;
         item = tail;
      }
      else if (depth == 0
            && TR7_IS_PAIR(head)
            && TR7EQ(TR7_CAR(head), SYMBOL(UNQUOTE_SPLICING))) {
         s_save(tsc, OP(QQ_APPEND), TR7_CADR(head));
         item = tail;
      }
      else {
         switch(depth) {
         case 0: op = OP(QQ_0); break;
         case 1: op = OP(QQ_1); break;
         case 2: op = OP(QQ_2); break;
         default: op = OP(QQ_N); item = tr7_cons(tsc, TR7_FROM_INT(depth), item); break;
         }
         s_save(tsc, op, item);
         item = head;
      }
   }
}

static eval_status_t do_qq(tr7_engine_t tsc, int depth, tr7_t item, tr7_t value)
{
   if (TR7EQ(TR7_CAR(item), value))
      s_save(tsc, OP(QQ_CONSIF), item);
   else
      s_save(tsc, OP(QQ_CONS), value);
   return do_quasiquote(tsc, depth, TR7_CDR(item));
}

/* implement 'quasiquote' */
static eval_status_t op_quasiquote(tr7_engine_t tsc)
{
   return do_quasiquote(tsc, 0, TR7_CAR(tsc->args));
}

/* implement 'quasiquote' depth 0 */
static eval_status_t op_qq_0(tr7_engine_t tsc)
{
   return do_qq(tsc, 0, tsc->args, tsc->value);
}

/* implement 'quasiquote' depth 1 */
static eval_status_t op_qq_1(tr7_engine_t tsc)
{
   return do_qq(tsc, 1, tsc->args, tsc->value);
}

/* implement 'quasiquote' depth 2 */
static eval_status_t op_qq_2(tr7_engine_t tsc)
{
   return do_qq(tsc, 2, tsc->args, tsc->value);
}

/* implement 'quasiquote' depth n */
static eval_status_t op_qq_n(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   return do_qq(tsc, TR7_TO_INT(TR7_CAR(args)), TR7_CDR(args), tsc->value);
}

/* implement 'quasiquote' append */
static eval_status_t op_qq_append(tr7_engine_t tsc)
{
   s_save(tsc, OP(QQ_APPEND_NEXT), tsc->value);
   return s_eval(tsc, tsc->args);
}

/* implement 'quasiquote' append next */
static eval_status_t op_qq_append_next(tr7_engine_t tsc)
{
   /* TODO: optimize */
   tr7_t rev = tr7_reverse(tsc, tsc->value, TR7_NIL);
   tr7_t value = tr7_reverse_in_place(rev, tsc->args);
   return s_return_single(tsc, value);
}

/* implement 'quasiquote' cons */
static eval_status_t op_qq_cons(tr7_engine_t tsc)
{
   tr7_t value = tr7_cons(tsc, tsc->args, tsc->value);
   return s_return_single(tsc, value);
}

/* implement 'quasiquote' consif */
static eval_status_t op_qq_consif(tr7_engine_t tsc)
{
   tr7_t value, args = tsc->args;
   if (TR7EQ(TR7_CDR(args), tsc->value))
      value = args;
   else
      value = tr7_cons(tsc, TR7_CAR(args), tsc->value);
   return s_return_single(tsc, value);
}

/* operate 'lambda' */
static eval_status_t do_lambda(tr7_engine_t tsc, tr7_t expr, tr7_t env)
{
   return s_return_single(tsc, mk_lambda(tsc, expr, env));
}

/* implement 'lambda' */
static eval_status_t op_lambda(tr7_engine_t tsc)
{
   tr7_t x, expr = tsc->args, env = tsc->envir;
   if (!environment_get(env, SYMBOL(COMPILE_HOOK), &x))
      return do_lambda(tsc, expr, env);
   /* If the hook is defined, apply it to tsc->args */
   s_save_nil(tsc, OP(LAMBDA_NEXT));
   return s_exec_1(tsc, x, expr);
}

/* implement 'lambda' next */
static eval_status_t op_lambda_next(tr7_engine_t tsc)
{
   return do_lambda(tsc, tsc->value, tsc->envir);
}

/*************************************************************************
* SECTION SCHEME_CASE_LAMBDA
* --------------------------
*/
#if USE_SCHEME_CASE_LAMBDA
/* operate 'case-lambda' */
static eval_status_t do_case_lambda(tr7_engine_t tsc, tr7_t expr, tr7_t env)
{
   return s_return_single(tsc, mk_closure(tsc, expr, env, TR7_HEAD_KIND_CASE_LAMBDA));
}

/* implement 'case-lambda' */
static eval_status_t op_case_lambda(tr7_engine_t tsc)
{
   tr7_t x, expr = tsc->args, env = tsc->envir;
   if (!environment_get(env, SYMBOL(COMPILE_HOOK), &x))
      return do_case_lambda(tsc, expr, env);
   /* If the hook is defined, apply it */
   s_save_arg1(tsc, OP(CASE_LAMBDA_NEXT), TR7_CDR(expr));
   return s_exec_1(tsc, x, TR7_CAR(expr));
}

/* implement 'case-lambda' next */
static eval_status_t op_case_lambda_next(tr7_engine_t tsc)
{
   tr7_t x, expr = tsc->args, env = tsc->envir;
   tr7_t next = TR7_CAR(expr);
   TR7_CAR(expr) = tsc->value;
   if (TR7_IS_NIL(next) || !environment_get(env, SYMBOL(COMPILE_HOOK), &x))
      return do_case_lambda(tsc, tr7_reverse_in_place(expr, next), env);
   s_save_cons(tsc, OP(CASE_LAMBDA_NEXT), TR7_CDR(next), expr);
   return s_exec_1(tsc, x, TR7_CAR(next));
}
#endif

/* implement 'if' */
static eval_status_t op_if(tr7_engine_t tsc)
{
   tr7_t next, expr = tsc->args;
   if (!TR7_IS_PAIR(expr))
      return Error_0(tsc, "without <test>");
   next = TR7_CDR(expr);
   if (TR7_IS_NIL(next))
      return Error_0(tsc, "without <consequent>");
   s_save(tsc, OP(IF_NEXT), next);
   return s_eval(tsc, TR7_CAR(expr));
}

/* implement 'if' next */
static eval_status_t op_if_next(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   if (TR7_IS_FALSE(tsc->value)) {
      expr = TR7_CDR(expr);
      if (TR7_IS_NIL(expr))
         return s_return(tsc);
   }
   return s_eval(tsc, TR7_CAR(expr));
}

/* implement 'set!' */
static eval_status_t op_set(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   s_save(tsc, OP(SET_NEXT), TR7_CAR(expr));
   return s_eval(tsc, TR7_CADR(expr));

}

/* implement 'set!' next */
static eval_status_t op_set_next(tr7_engine_t tsc)
{
   if (!environment_set(tsc, tsc->args, tsc->value, tsc->envir, 0))
      return error_unbound(tsc, tsc->args);
   return s_return(tsc);

}

/* implement 'begin' */
static eval_status_t op_begin(tr7_engine_t tsc)
{
   return TR7_IS_NIL(tsc->args) ? s_return_void(tsc) : do_begin(tsc, tsc->args);
}

/* operate 'cond' */
static eval_status_t do_cond(tr7_engine_t tsc, tr7_t clauses)
{
   if (TR7_IS_NIL(clauses))
      return s_return_void(tsc);
   if (TR7_IS_PAIR(clauses)) {
      tr7_t cl = TR7_CAR(clauses);
      if (TR7_IS_PAIR(cl)) {
         s_save(tsc, OP(COND_NEXT), clauses);
         return s_eval(tsc, TR7_CAR(cl));
      }
   }
   return Error_1(tsc, "syntax error in cond", clauses);
}

/* calls returned value with given args */
static eval_status_t op_xapply(tr7_engine_t tsc)
{
   return s_exec(tsc, tsc->value, tsc->args);
}

/* evaluate returned value */
static eval_status_t op_xeval(tr7_engine_t tsc)
{
   return s_eval(tsc, tsc->value);
}

/* calls returned value with given args */
static eval_status_t op_xreturn(tr7_engine_t tsc)
{
   return do_xreturn(tsc, tsc->args);
}

/* operate cond and case expressions (use tsc->value for feeding) */
static eval_status_t do_begin_or_feed_to(tr7_engine_t tsc, tr7_t exprs)
{
   if (TR7_IS_NIL(exprs))
      return s_return(tsc);
   if (TR7_IS_PAIR(exprs)) {
      if (!TR7EQ(TR7_CAR(exprs), SYMBOL(FEED_TO)))
         return s_begin(tsc, exprs);
      exprs = TR7_CDR(exprs);
      if (TR7_IS_PAIR(exprs)) {
         s_save_arg1(tsc, OP(XAPPLY), tsc->value);
         return s_eval(tsc, TR7_CAR(exprs));
      }
   }
   return Error_1(tsc, "syntax error in cond or case", exprs);
}

/* implement 'cond' */
static eval_status_t op_cond(tr7_engine_t tsc)
{
   return do_cond(tsc, tsc->args);
}

/* implement 'cond' next */
static eval_status_t op_cond_next(tr7_engine_t tsc)
{
   tr7_t clauses = tsc->args;
   if (TR7_IS_FALSE(tsc->value))
      return do_cond(tsc, TR7_CDR(clauses));
   return do_begin_or_feed_to(tsc, TR7_CDAR(clauses));
}

/* implement 'case' */
static eval_status_t op_case(tr7_engine_t tsc)
{
   s_save(tsc, OP(CASE_NEXT), TR7_CDR(tsc->args));
   return s_eval(tsc, TR7_CAR(tsc->args));
}

/* implement 'case' next */
static eval_status_t op_case_next(tr7_engine_t tsc)
{
   tr7_t cl, datums, clauses = tsc->args;
   for (; TR7_IS_PAIR(clauses) ; clauses = TR7_CDR(clauses)) {
      cl = TR7_CAR(clauses);
      if (!TR7_IS_PAIR(cl))
         return Error_1(tsc, "case clause is not a list", cl);
      datums = TR7_CAR(cl);
      if (TR7EQ(datums, SYMBOL(ELSE)))
         return do_begin_or_feed_to(tsc, TR7_CDR(cl));
      if (!TR7_IS_PAIR(datums))
         return Error_1(tsc, "case clause is not valid", cl);
      do {
         if (tr7_eqv(TR7_CAR(datums), tsc->value))
            return do_begin_or_feed_to(tsc, TR7_CDR(cl));
         datums = TR7_CDR(datums);
      } while (TR7_IS_PAIR(datums));
   }
   return s_return_nil(tsc);
}

/* operate 'and' or 'or' */
static eval_status_t do_and_or(tr7_engine_t tsc, tr7_t exprs, opidx_t op)
{
   tr7_t next = TR7_CDR(exprs);
   if (!TR7_IS_NIL(next))
      s_save(tsc, op, next);
   return s_eval(tsc, TR7_CAR(exprs));
}

/* operate 'and' */
static eval_status_t do_and(tr7_engine_t tsc, tr7_t exprs)
{
   return do_and_or(tsc, exprs, OP(AND_NEXT));
}

/* implement 'and' */
static eval_status_t op_and(tr7_engine_t tsc)
{
   if (TR7_IS_NIL(tsc->args))
      return s_return_true(tsc);
   return do_and(tsc, tsc->args);
}

/* implement 'and' next */
static eval_status_t op_and_next(tr7_engine_t tsc)
{
   if (TR7_IS_FALSE(tsc->value))
      return s_return(tsc);
   return do_and(tsc, tsc->args);
}

/* operate 'or' */
static eval_status_t do_or(tr7_engine_t tsc, tr7_t exprs)
{
   return do_and_or(tsc, exprs, OP(OR_NEXT));
}

/* implement 'or' */
static eval_status_t op_or(tr7_engine_t tsc)
{
   if (TR7_IS_NIL(tsc->args))
      return s_return_false(tsc);
   return do_or(tsc, tsc->args);
}

/* implement 'or' next */
static eval_status_t op_or_next(tr7_engine_t tsc)
{
   if (!TR7_IS_FALSE(tsc->value))
      return s_return(tsc);
   return do_or(tsc, tsc->args);
}

/* implement 'when' */
static eval_status_t op_when(tr7_engine_t tsc)
{
   tr7_t test, exprs = tsc->args;
   if (!TR7_IS_PAIR(exprs))
      return Error_0(tsc, "without <test>");
   test = TR7_CAR(exprs);
   exprs = TR7_CDR(exprs);
   if (TR7_IS_NIL(exprs))
      return Error_0(tsc, "without <consequent>");
   s_save(tsc, OP(WHEN_NEXT), exprs);
   return s_eval(tsc, test);
}

/* implement 'when' next */
static eval_status_t op_when_next(tr7_engine_t tsc)
{
   return TR7_IS_FALSE(tsc->value) ? s_return(tsc) : s_begin(tsc, tsc->args);
}

/* implement 'unless' */
static eval_status_t op_unless(tr7_engine_t tsc)
{
   tr7_t test, exprs = tsc->args;
   if (!TR7_IS_PAIR(exprs))
      return Error_0(tsc, "without <test>");
   test = TR7_CAR(exprs);
   exprs = TR7_CDR(exprs);
   if (TR7_IS_NIL(exprs))
      return Error_0(tsc, "without <consequent>");
   s_save(tsc, OP(UNLESS_NEXT), exprs);
   return s_eval(tsc, test);
}

/* implement 'unless' next */
static eval_status_t op_unless_next(tr7_engine_t tsc)
{
   return TR7_IS_FALSE(tsc->value) ? s_begin(tsc, tsc->args) : s_return(tsc);
}

/* ========== binding ========== */

static eval_status_t do_let(tr7_engine_t tsc, opidx_t op, tr7_t todo, tr7_t done)
{
   if (!TR7_IS_PAIR(todo))
      return s_return_single(tsc, done);
   s_save_cons(tsc, op, todo, done);
   return s_eval(tsc, TR7_CADAR(todo));
}

static eval_status_t do_let1(tr7_engine_t tsc, opidx_t op, tr7_t value)
{
   tr7_t todo = TR7_CAR(tsc->args);
   tr7_t done = TR7_CONS3(tsc, value, TR7_CAAR(todo), TR7_CDR(tsc->args));
   return do_let(tsc, op, TR7_CDR(todo), done);
}

/* implement 'let' */
static eval_status_t op_let(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   tr7_t env = mk_environment(tsc, tsc->envir, 1);
   if (!TR7_IS_PAIR(expr) || !TR7_IS_SYMBOL(TR7_CAR(expr)))
      s_push(tsc, OP(LET2), expr, env);
   else {
      /* named let, section 4.2.4 */
      s_push(tsc, OP(LET3), expr, env);
      expr = TR7_CDR(expr);
   }
   if (!is_binding_spec_ok(expr, 0))
      return Error_1(tsc, "Bad syntax of binding:", expr);
   expr = TR7_CAR(expr);
   return do_let(tsc, OP(LET1), expr, TR7_NIL);
}

static eval_status_t op_let1(tr7_engine_t tsc)
{
   return do_let1(tsc, OP(LET1), tsc->value);
}

static eval_status_t op_let2(tr7_engine_t tsc)
{
   tr7_t name, value, list = tsc->value;
   while (!TR7_IS_NIL(list)) {
      value = TR7_CAR(list);
      list = TR7_CDR(list);
      name = TR7_CAR(list);
      list = TR7_CDR(list);
      new_slot_in_env(tsc, name, value);
   }
   return s_begin(tsc, TR7_CDR(tsc->args));
}

/* implement 'letrec' */
static eval_status_t op_letrec(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   tr7_t env = mk_environment(tsc, tsc->envir, 1);
   if (!is_binding_spec_ok(expr, 0))
      return Error_1(tsc, "Bad syntax of binding:", expr);
   s_push(tsc, OP(LET2), expr, env);
   expr = TR7_CAR(expr);
   tsc->envir = env;
   predeclare_binding_rec(tsc, expr, env);
   return do_let(tsc, OP(LET1), expr, TR7_NIL);
}

static eval_status_t op_let1val(tr7_engine_t tsc)
{
   return do_let1(tsc, OP(LET1VAL), get_values(tsc));
}

/* implement 'let-values' */
static eval_status_t op_let_values(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   tr7_t env = mk_environment(tsc, tsc->envir, 1);
   if (!is_binding_spec_ok(expr, 1))
      return Error_1(tsc, "Bad syntax of binding:", expr);
   s_push(tsc, OP(LET2VAL), expr, env);
   expr = TR7_CAR(expr);
   return do_let(tsc, OP(LET1VAL), expr, TR7_NIL);
}

static eval_status_t op_let2val(tr7_engine_t tsc)
{
   tr7_t names, values, list = tsc->value;
   while (!TR7_IS_NIL(list)) {
      values = TR7_CAR(list);
      list = TR7_CDR(list);
      names = TR7_CAR(list);
      list = TR7_CDR(list);
      bind_list(tsc, names, values);
   }
   return s_begin(tsc, TR7_CDR(tsc->args));
}

static eval_status_t op_let3(tr7_engine_t tsc)
{
   tr7_t name, value, lambda;
   tr7_t list = tsc->value;
   tr7_t expr = tsc->args;
   tr7_t loop = TR7_CAR(expr);
   tr7_t vars = TR7_NIL;

   while (!TR7_IS_NIL(list)) {
      value = TR7_CAR(list);
      list = TR7_CDR(list);
      name = TR7_CAR(list);
      list = TR7_CDR(list);
      new_slot_in_env(tsc, name, value);
      vars = tr7_cons(tsc, name, vars);
   }

   expr = TR7_CDDR(expr);
   lambda = mk_lambda(tsc, tr7_cons(tsc, vars, expr), tsc->envir);
   new_slot_in_env(tsc, loop, lambda);
   return s_begin(tsc, expr);
}

/* operate let* (calculate parameters) */
static eval_status_t do_let1star(tr7_engine_t tsc, opidx_t op, tr7_t binds)
{
   if (TR7_IS_NIL(binds))
      return s_return(tsc);
   s_save(tsc, op, binds);
   return s_eval(tsc, TR7_CADAR(binds));
}

/* implement 'let*' */
static eval_status_t op_letstar(tr7_engine_t tsc)
{
   tr7_t binds, expr = tsc->args;

   if (!is_binding_spec_ok(expr, 0))
      return Error_1(tsc, "Bad syntax of binding:", expr);

   binds = TR7_CAR(expr);
   expr = TR7_CDR(expr);
   tsc->envir = mk_environment(tsc, tsc->envir, 1);
   s_save(tsc, OP(BEGIN), expr);
   return do_let1star(tsc, OP(LET1STAR), binds);
}

/* let* (calculate parameters) */
static eval_status_t op_let1star(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   new_slot_in_env(tsc, TR7_CAAR(expr), tsc->value);
   return do_let1star(tsc, OP(LET1STAR), TR7_CDR(expr));
}

/* implement 'letrec*' */
static eval_status_t op_letrecstar(tr7_engine_t tsc)
{
   tr7_t binds, expr = tsc->args;

   if (!is_binding_spec_ok(expr, 0))
      return Error_1(tsc, "Bad syntax of binding:", expr);

   binds = TR7_CAR(expr);
   expr = TR7_CDR(expr);
   tsc->envir = mk_environment(tsc, tsc->envir, 1);
   predeclare_binding_rec(tsc, binds, tsc->envir);
   s_save(tsc, OP(BEGIN), expr);
   return do_let1star(tsc, OP(LET1STAR), binds);
}

/* implement 'let-values*' */
static eval_status_t op_letstarval(tr7_engine_t tsc)
{
   tr7_t binds, expr = tsc->args;

   if (!is_binding_spec_ok(expr, 1))
      return Error_1(tsc, "Bad syntax of binding:", expr);

   binds = TR7_CAR(expr);
   expr = TR7_CDR(expr);
   tsc->envir = mk_environment(tsc, tsc->envir, 1);
   s_save(tsc, OP(BEGIN), expr);
   return do_let1star(tsc, OP(LET1STARVAL), binds);
}

/* let* (calculate parameters) */
static eval_status_t op_let1starval(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   tr7_t values = get_values(tsc);
   bind_list(tsc, TR7_CAAR(expr), values);
   return do_let1star(tsc, OP(LET1STARVAL), TR7_CDR(expr));
}

/* ========== parameter ========== */

/* implement 'make-parameter' */
static eval_status_t op_mkparam(tr7_engine_t tsc)
{
   tr7_t args, param, ini, cvt;

   args = tsc->args;
   ini = TR7_CAR(args); /* initial value 'z */
   args = TR7_CDR(args);
   param = mk_parameter(tsc, TR7_NIL, TR7_NIL);
   if (TR7_IS_NIL(args)) {
      /* no converter, fast path */
      TR7_TO_PARAMETER(param)->value = ini;
      return s_return_single(tsc, param);
   }
   /* with converter */
   cvt = TR7_CAR(args);
   TR7_TO_PARAMETER(param)->converter = cvt;
   s_save(tsc, OP(MKPARAMCVT), param);
   return s_exec_1(tsc, cvt, ini);
}

static eval_status_t op_mkparamcvt(tr7_engine_t tsc)
{
   TR7_TO_PARAMETER(tsc->args)->value = tsc->value;
   return s_return_single(tsc, tsc->args);
}

static eval_status_t op_paramcvt(tr7_engine_t tsc)
{
   parameter_set(tsc, tsc->args, tsc->value);
   return s_return(tsc);
}

static eval_status_t do_param(tr7_engine_t tsc, tr7_t list_params)
{
   tr7_t param, value, head, next;

   head = TR7_CAR(list_params);
   if (!TR7_IS_PAIR(head))
      return Error_0(tsc, "parameterize expect list");
   param = TR7_CAR(head);
   value = TR7_CDR(head);
   if (!TR7_IS_PAIR(value) || !TR7_IS_NIL(TR7_CDR(value)))
      return Error_0(tsc, "wrong parameterize value setting");

   next = TR7_CDR(list_params);
   if (TR7_IS_PAIR(next))
      s_save(tsc, OP(PARAM1), next);

   s_save(tsc, OP(PARAM2), head);
   return s_eval(tsc, param);
}

/* implement 'parameterize' */
static eval_status_t op_parameterize(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t params = TR7_CAR(args); /* list of params */
   tr7_t body = TR7_CDR(args);

   if (!TR7_IS_PAIR(params))
      return Error_0(tsc, "parameterize expects parameter list");

   if (!TR7_IS_PAIR(body))
      return Error_0(tsc, "parameterize expects actions");

   /* okay save parameters and prepare body */
   parameters_save(tsc);
   s_save(tsc, OP(BEGIN), body);

   return do_param(tsc, params);
}

static eval_status_t op_param1(tr7_engine_t tsc)
{
   return do_param(tsc, tsc->args);
}

static eval_status_t op_param2(tr7_engine_t tsc)
{
   tr7_t param = tsc->value;
   tsc->params = tr7_cons(tsc, tr7_cons(tsc, param, TR7_NIL), tsc->params);
   return do_e0args(tsc, param, tsc->args);
}

/* ========== do operation ========== */

static tr7_t trf_do(tr7_engine_t tsc, tr7_t desc)
{
   tr7_t vars, inits, steps, test, exprs, commands, it, sub, lambda, eif, etrue, efalse, estep, sym;

   /* identify ((it...) (test exprs...) commands...) */
   if (!TR7_IS_PAIR(desc))
      goto error;
   it = TR7_CAR(desc);
   commands = TR7_CDR(desc);
   if (!TR7_IS_PAIR(commands))
      goto error;
   exprs = TR7_CAR(commands);
   commands = TR7_CDR(commands);
   if (!TR7_IS_PAIR(exprs))
      goto error;
   test = TR7_CAR(exprs);
   exprs = TR7_CDR(exprs);

   /* split it into (var init step)... */
   vars = inits = steps = TR7_NIL;
   while(!TR7_IS_NIL(it)) {
      if (!TR7_IS_PAIR(it))
         goto error;
      sub = TR7_CAR(it);
      it = TR7_CDR(it);

      if (!TR7_IS_PAIR(sub))
         goto error;
      sym = TR7_CAR(sub);
      if (!TR7_IS_SYMBOL(sym))
         goto error;
      vars = tr7_cons(tsc, sym, vars);
      sub = TR7_CDR(sub);
      if (!TR7_IS_PAIR(sub))
         goto error;
      inits = tr7_cons(tsc, TR7_CAR(sub), inits);
      sub = TR7_CDR(sub);
      if (TR7_IS_PAIR(sub))
         sub = TR7_CAR(sub);
      else if (TR7_IS_NIL(sub))
         sub = sym;
      else
         goto error;
      steps = tr7_cons(tsc, sub, steps);
   }

   /* (LAMBDA vars (IF test (BEGIN exprs...) (BEGIN (BEGIN commands...) (<SELF> steps)))) */
   lambda = tr7_cons(tsc, SYMBOL(LAMBDA), TR7_NIL);
   estep = tr7_cons(tsc, lambda, steps);
   if (TR7_IS_NIL(commands))
      efalse = estep;
   else {
      efalse = tr7_cons(tsc, SYMBOL(BEGIN), commands);
      efalse = TR7_LIST3(tsc, SYMBOL(BEGIN), efalse, estep);
   }
   if (TR7_IS_NIL(exprs))
      etrue = TR7_TRUE;
   else
      etrue = tr7_cons(tsc, SYMBOL(BEGIN), exprs);
   eif = TR7_LIST4(tsc, SYMBOL(IF), test, etrue, efalse);
   TR7_CDR(lambda) = TR7_LIST2(tsc, vars, eif);
   /* TODO self calling is not printable */

   /* call with inits */
   return tr7_cons(tsc, lambda, inits);

error:
   Error_1(tsc, "invalid do:", desc);
   return TR7_NIL;
}

static eval_status_t op_do(tr7_engine_t tsc)
{
   tr7_t t = trf_do(tsc, tsc->args);
   return TR7_IS_NIL(t) ? Cycle_Error : s_eval(tsc, t);
}

/* ========== exception ========== */

/* implement 'guard' */
static eval_status_t op_guard(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   if (TR7_IS_PAIR(expr)) {
      tr7_t guard = TR7_CAR(expr);
      tr7_t body = TR7_CDR(expr);
      if (TR7_IS_PAIR(guard) && TR7_IS_SYMBOL(TR7_CAR(guard)) && TR7_IS_PAIR(body)) {
         s_guard(tsc, guard, TR7_GUARD_KIND_GUARD);
         return do_begin(tsc, body);
      }
   }
   return Error_1(tsc, "invalid guard:", expr);
}

static eval_status_t do_guard_cond(tr7_engine_t tsc, tr7_t clauses)
{
   if (TR7_IS_PAIR(clauses)) {
      tr7_t cl = TR7_CAR(clauses);
      if (TR7_IS_PAIR(cl)) {
         s_save(tsc, OP(GUARD_COND), clauses);
         return s_eval(tsc, TR7_CAR(cl));
      }
   }
   return do_raise(tsc, TR7_FALSE, 1); /* TODO: FALSE? tsc->value */
}

static eval_status_t op_guard_cond(tr7_engine_t tsc)
{
   tr7_t clauses = tsc->args;
   if (TR7_IS_FALSE(tsc->value))
      return do_guard_cond(tsc, TR7_CDR(clauses));
   tr7_guard_t guard = s_get_guard(tsc, tsc->dump);
   tsc->dump = guard->expr;
   return do_begin_or_feed_to(tsc, TR7_CDAR(clauses));
}

static eval_status_t op_with_exception_handler(tr7_engine_t tsc)
{
   tr7_t expr = tsc->args;
   tr7_t handler = TR7_CAR(expr);
   tr7_t thunk = TR7_CADR(expr);
   s_guard(tsc, handler, TR7_GUARD_KIND_HANDLER);
   return s_exec_0(tsc, thunk);
}

static eval_status_t do_raise(tr7_engine_t tsc, tr7_t errobj, int continuable)
{
   unsigned gkind;
   tr7_t expr;

   /* retrieves the current guard */
   tr7_guard_t guard = s_get_guard(tsc, tsc->dump);
   while(guard != NULL) {
      /* inspect the found guard */
      gkind = TR7_HEAD_UVALUE(guard->head);
      if (gkind & TR7_GUARD_KIND_REPEAT)
         guard = s_get_guard(tsc, guard->env);
      else if (gkind & TR7_GUARD_KIND_ROOT) {
         tsc->envir = guard->env;
         tsc->dump = guard->dump;
         return s_exec_1(tsc, guard->expr, errobj);
      }
      else {
         s_guard_kind(tsc, TR7_FROM_GUARD(guard), TR7_GUARD_KIND_REPEAT, guard->dump);
         tsc->envir = mk_environment(tsc, guard->env, 1);

         if (gkind & TR7_GUARD_KIND_HANDLER) {
            if (!continuable)
               s_save(tsc, OP(RERAISE), errobj);
            return s_exec_1(tsc, guard->expr, errobj);
         }

         expr = guard->expr;
         new_slot_in_env(tsc, TR7_CAR(expr), errobj);
         return do_guard_cond(tsc, TR7_CDR(expr));
      }
   }
   tsc->value = errobj;
   return Cycle_Exit;
}

/* implement 'raise' */
static eval_status_t op_raise(tr7_engine_t tsc)
{
   return do_raise(tsc, TR7_CAR(tsc->args), 0);
}

/* implement 'raise-continuable' */
static eval_status_t op_raise_continuable(tr7_engine_t tsc)
{
   return do_raise(tsc, TR7_CAR(tsc->args), 1);
}

/* implement OP(RERAISE) */
static eval_status_t op_reraise(tr7_engine_t tsc)
{
   return do_raise(tsc, tsc->args, 0);
}

/* implement 'error' */
static eval_status_t op_error(tr7_engine_t tsc)
{
   tr7_t err = mk_error_instance(tsc, tsc->error_desc, TR7_CAR(tsc->args), TR7_CDR(tsc->args));
   return do_raise(tsc, err, 0);
}

/* implement 'error-object?' */
static eval_status_t op_is_error_object(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_error(tsc, TR7_CAR(tsc->args)));
}

/* implement 'error-object-message' */
static eval_status_t op_error_msg(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_error_message(tsc, TR7_CAR(tsc->args)));
}

/* implement 'error-object-irritants' */
static eval_status_t op_error_irritants(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_error_irritants(tsc, TR7_CAR(tsc->args)));
}

/* implement 'read-error?' */
static eval_status_t op_is_read_error(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_read_error(tsc, TR7_CAR(tsc->args)));
}

/* implement 'file-error?' */
static eval_status_t op_is_file_error(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_file_error(tsc, TR7_CAR(tsc->args)));
}

/* implement 'dynamic-wind' */
static eval_status_t op_dynamic_wind(tr7_engine_t tsc)
{
   tr7_t cars[3];
   tr7_get_list_cars(tsc->args, 3, cars, NULL);
   dynawind_push(tsc, cars[0], cars[2]);
   s_save(tsc, OP(THUNK), cars[1]);
   return s_exec_0(tsc, cars[0]);
}

/*************************************************************************
* SECTION SCHEME_LAZY
* -------------------
*/
#if USE_SCHEME_LAZY
/* implement 'delay' */
static eval_status_t op_delay(tr7_engine_t tsc)
{
   tr7_t x = mk_promise(tsc, 0, tsc->args, tsc->envir);
   return s_return_single(tsc, x);
}

/* implement 'delay-force' */
static eval_status_t op_delay_force(tr7_engine_t tsc)
{
   tr7_t x = mk_promise(tsc, TR7_PROMISE_FLAG_DELAY_FORCE, tsc->args, tsc->envir);
   return s_return_single(tsc, x);
}

/* implement 'promise?' */
static eval_status_t op_is_promise(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_PROMISE(TR7_CAR(tsc->args)));
}

static eval_status_t do_force(tr7_engine_t tsc, tr7_t promise)
{
   opidx_t op = promise_is_delay_force(promise)
                               ? OP(FORCE_DELAYED) : OP(SAVE_FORCED);
   s_save(tsc, op, promise);
   new_environment(tsc, promise_env(promise));
   return s_begin(tsc, promise_code(promise));
}


/* implement 'force' */
static eval_status_t op_force(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (!TR7_IS_PROMISE(x))
      return s_return_single(tsc, x);
   if (promise_was_forced(x))
      return s_return_single(tsc, tr7_promise_value(x));
   return do_force(tsc, x);
}

/* implement 'force' next */
static eval_status_t op_save_forced(tr7_engine_t tsc)
{
   tr7_promise_set_value(tsc->args, tsc->value);
   return s_return(tsc);
}

/* implement 'delay-force' next */
static eval_status_t op_force_delayed(tr7_engine_t tsc)
{
   tr7_t y = tsc->args;
   tr7_t x = tsc->value;
   if (TR7_IS_PROMISE(x)) {
      if (promise_was_forced(x))
         tsc->value = x = tr7_promise_value(x);
      else {
         s_save(tsc, OP(SAVE_FORCED), y);
         return do_force(tsc, x);
      }
   }
   tr7_promise_set_value(y, x);
   return s_return(tsc);
}

/* implement 'make-promise' */
static eval_status_t op_make_promise(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (!TR7_IS_PROMISE(x))
      x = mk_promise(tsc, TR7_PROMISE_FLAG_FORCED, x, TR7_NIL);
   return s_return_single(tsc, x);
}
#endif
/*================= macro (4.3) ====================*/

/* implement 'let-syntax' & 'letrec-syntax' */
static eval_status_t do_let_syntax(tr7_engine_t tsc, int isrec)
{
   tr7_t x, y, env;

   x = tsc->args;
   env = mk_environment(tsc, tsc->envir, 1);
   if (!is_binding_spec_ok(x, 0))
      return Error_1(tsc, "Bad syntax of binding:", x);
   y = TR7_CDR(x);
   x = TR7_CAR(x);
   if (isrec) {
      tsc->envir = env;
      predeclare_binding_rec(tsc, x, env);
   }
   while (TR7_IS_PAIR(x)) {
      if (!declare_transform(tsc, env, TR7_CAR(x)))
         return Error_1(tsc, "bad transformer spec", TR7_CAR(x));
      x = TR7_CDR(x);
   }
   tsc->envir = env;
   return s_begin(tsc, y);
}

/* implement 'let-syntax' */
static eval_status_t op_let_syntax(tr7_engine_t tsc)
{
   return do_let_syntax(tsc, 0);
}

/* implement 'letrec-syntax' */
static eval_status_t op_letrec_syntax(tr7_engine_t tsc)
{
   return do_let_syntax(tsc, 1);
}

/* operate 'define' or 'macro' */
static eval_status_t do_define_or_macro(tr7_engine_t tsc, opidx_t nextop)
{
   tr7_t name, expr, formals;

   expr = tsc->args;
   if (!TR7_IS_PAIR(expr))
      return  Error_0(tsc, "no content");
   name = TR7_CAR(expr);
   expr = TR7_CDR(expr);
   if (!TR7_IS_PAIR(expr))
      return  Error_0(tsc, "no value");

   if (!TR7_IS_PAIR(name))
      expr = TR7_CAR(expr);
   else {
      formals = TR7_CDR(name);
      if (!is_formals_spec_ok(formals))
         return Error_0(tsc, "invalid formals");
      expr = TR7_CONS3(tsc, SYMBOL(LAMBDA), formals, expr);
      name = TR7_CAR(name);
   }
   if (!TR7_IS_SYMBOL(name))
      return Error_0(tsc, "not a symbol");

   s_save(tsc, nextop, name);
   return s_eval(tsc, expr);
}

/* implement 'define' */
static eval_status_t op_define(tr7_engine_t tsc)
{
   return do_define_or_macro(tsc, OP(DEFINE_NEXT));
}

/* implement 'define' next */
static eval_status_t op_define_next(tr7_engine_t tsc)
{
   new_slot_in_env(tsc, tsc->args, tsc->value);
   return s_return_single(tsc, tsc->args);
}

/* implement 'macro' */
static eval_status_t op_macro(tr7_engine_t tsc)
{
   return do_define_or_macro(tsc, OP(MACRO_NEXT));
}

/* implement 'macro' next */
static eval_status_t op_macro_next(tr7_engine_t tsc)
{
   new_slot_in_env(tsc, tsc->args, mk_macro(tsc, tsc->value));
   return s_return_single(tsc, tsc->args);
}

/* implement 'define-values' */
static eval_status_t op_define_values(tr7_engine_t tsc)
{
   tr7_t x, y;

   x = tsc->args;
   if (!TR7_IS_PAIR(x))
      return Error_0(tsc, "no def");
   y = TR7_CAR(x);
   x = TR7_CDR(x);
   if (!is_formals_spec_ok(y))
      return Error_0(tsc, "invalid def");
   if (!TR7_IS_PAIR(x))
      return Error_0(tsc, "no val");
   s_save(tsc, OP(DEFVAL_NEXT), y);
   return s_eval(tsc, TR7_CAR(x));
}

/* implement 'define-values' next */
static eval_status_t op_define_values_next(tr7_engine_t tsc)
{
   bind_list(tsc, tsc->args, get_values(tsc));
   return s_return_single(tsc, tsc->args);
}

/* implement 'define-syntax' */
static eval_status_t op_define_syntax(tr7_engine_t tsc)
{
   if (!declare_transform(tsc, tsc->envir, tsc->args))
      return Error_1(tsc, "bad transformer spec", tsc->args);
   return s_return_true(tsc);
}

/*================= record type ====================*/

/* implement 'define-record-type' */
static eval_status_t op_define_record_type(tr7_engine_t tsc)
{
   tr7_pair_t pairs[3];
   int cnt = tr7_get_list_pairs(tsc->args, 3, pairs);
   if (cnt != 3)
      return Error_0(tsc, "error type definition");

   if (!define_record_type(tsc, pairs[0]->car, pairs[1]->car, pairs[2]->car, pairs[2]->cdr))
      return Error_0(tsc, "error type definition");
   return s_return_void(tsc);
}

/* implement record-type predicate (OP(TYPP) TYPE TARGET) */
static eval_status_t op_is_type(tr7_engine_t tsc)
{
   tr7_t recid = TR7_CAR(tsc->args);
   tr7_t target = TR7_CADR(tsc->args);
   return s_return_boolean(tsc, tr7_is_record_type(target, recid));
}

/* implement record-type constructor (OP(TYPCON) TYPE VALUES...) */
static eval_status_t op_type_constructor(tr7_engine_t tsc)
{
   tr7_t recid = TR7_CAR(tsc->args);
   tr7_t values = TR7_CDR(tsc->args);
   tr7_t result = mk_record_instance(tsc, recid, values);
   if (TR7_IS_NIL(result))
      return Error_0(tsc, "error type constructor");
   return s_return_single(tsc, result);
}

/* implement record-type accessor (OP(TYPACC) TYPE INDEX TARGET) */
static eval_status_t op_type_accessor(tr7_engine_t tsc)
{
   tr7_record_t rec;
   tr7_t index, target, args = tsc->args;
   tr7_t recid = TR7_CAR(args);
   args = TR7_CDR(args);
   index = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return Error_0(tsc, "missing type instance");
   target = TR7_CAR(args);
   rec = tr7_as_record_cond(target, recid);
   if (!rec)
      return Error_1(tsc, "bad type", target);
   return s_return_single(tsc, rec->items[TR7_TO_INT(index)]);
}

/* implement record-type modifier (OP(TYPMOD) TYPE INDEX TARGET VALUE) */
static eval_status_t op_type_modifier(tr7_engine_t tsc)
{
   tr7_record_t rec;
   tr7_t index, target, value, args = tsc->args;
   tr7_t recid = TR7_CAR(args);
   args = TR7_CDR(args);
   index = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return Error_0(tsc, "missing type instance");
   target = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return Error_0(tsc, "missing type set value");
   value = TR7_CAR(args);
   rec = tr7_as_record_cond(target, recid);
   if (!rec)
      return Error_0(tsc, "bad type");
   rec->items[TR7_TO_INT(index)] = value;
   return s_return_single(tsc, value);
}

#if USE_SRFI_136
/* implement 'record?' */
static eval_status_t op_is_record(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_record(TR7_CAR(tsc->args)));
}

/* implement 'record-type-descriptor?' */
static eval_status_t op_is_record_desc(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_record_desc(TR7_CAR(tsc->args)));
}

/* implement 'record-type-descriptor' */
static eval_status_t op_record_desc(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_record_desc(TR7_CAR(tsc->args)));
}

/* implement 'record-type-predicate' */
static eval_status_t op_record_desc_pred(tr7_engine_t tsc)
{
   return s_return_single(tsc, mk_record_predicate(tsc, TR7_CAR(tsc->args)));
}

/* implement 'record-type-name' */
static eval_status_t op_record_desc_name(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_record_desc_name(TR7_CAR(tsc->args)));
}

/* implement 'record-type-parent' */
static eval_status_t op_record_desc_parent(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_record_desc_parent(TR7_CAR(tsc->args)));
}

/* implement 'record-type-fields' */
static eval_status_t op_record_desc_fields(tr7_engine_t tsc)
{
   return s_return_single(tsc, record_desc_fields_srfi136(tsc, TR7_CAR(tsc->args)));
}

/* implement 'make-record-type-descriptor' */
static eval_status_t op_make_record_desc(tr7_engine_t tsc)
{
   tr7_t resu, fields, parent, name, args = tsc->args;
   name = TR7_CAR(args);
   args = TR7_CDR(args);
   fields = TR7_CAR(args);
   args = TR7_CDR(args);
   parent = TR7_IS_NIL(args) ? TR7_FALSE : TR7_CAR(args);
   if (!TR7_IS_FALSE(parent) && !tr7_is_record_desc(parent))
      return Error_1(tsc, "bad parent", parent);

   resu = make_record_type_srfi136(tsc, name, parent, fields);
   if (TR7_IS_VOID(resu))
      return Error_0(tsc, "error type definition");
   return s_return_single(tsc, resu);
}

/* implement 'make-record' */
static eval_status_t op_make_record(tr7_engine_t tsc)
{
   tr7_t re = mk_record_instance(tsc, TR7_CAR(tsc->args), TR7_CADR(tsc->args));
   if (TR7_IS_VOID(re))
      return Error_0(tsc, "creation of record failed");
   return s_return_single(tsc, re);
}
#endif

/* ========== equivalence ========== */

/* implement 'eq?' */
static eval_status_t op_eq(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7EQ(TR7_CAR(tsc->args), TR7_CADR(tsc->args)));
}

/* implement 'eqv?' */
static eval_status_t op_eqv(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_eqv(TR7_CAR(tsc->args), TR7_CADR(tsc->args)));
}

/* implement 'equal?' */
static eval_status_t op_equal(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_equal(TR7_CAR(tsc->args), TR7_CADR(tsc->args)));
}

/* ========== number ========== */

/* implement 'number?' */
static eval_status_t op_is_number(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_number(TR7_CAR(tsc->args)));
}

/* implement 'integer?' */
static eval_status_t op_is_integer(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_integer(TR7_CAR(tsc->args)));
}

/* implement 'real?' */
static eval_status_t op_is_real(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_number(TR7_CAR(tsc->args)));      /* All numbers are real */
}

/* implement 'complex?' */
static eval_status_t op_is_complex(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_number(TR7_CAR(tsc->args)));
}

/* implement 'rational?' */
static eval_status_t op_is_rational(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_integer(TR7_CAR(tsc->args)));
}

/* implement 'exact?' */
static eval_status_t op_is_exact(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_exact(TR7_CAR(tsc->args)));
}

/* implement 'inexact?' */
static eval_status_t op_is_inexact(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, !tr7_is_exact(TR7_CAR(tsc->args)));
}

/* implement 'exact-integer?' */
static eval_status_t op_is_exact_int(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_exact_integer(TR7_CAR(tsc->args)));
}

/* implement 'zero?' */
static eval_status_t op_is_zero(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x))
      return s_return_boolean(tsc, TR7_TO_INT(x) == 0);
   return s_return_boolean(tsc, *TR7_TO_DOUBLE(x) == 0);
}

/* implement 'positive?' */
static eval_status_t op_is_positive(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x))
      return s_return_boolean(tsc, TR7_TO_INT(x) > 0);
   return s_return_boolean(tsc, *TR7_TO_DOUBLE(x) > 0);
}

/* implement 'negative?' */
static eval_status_t op_is_negative(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x))
      return s_return_boolean(tsc, TR7_TO_INT(x) < 0);
   return s_return_boolean(tsc, *TR7_TO_DOUBLE(x) < 0);
}

/* implement 'odd?' */
static eval_status_t op_is_odd(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_boolean(tsc, 0 != (TR7_TO_INT(x) & 1));
}

/* implement 'even?' */
static eval_status_t op_is_even(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_boolean(tsc, 0 == (TR7_TO_INT(x) & 1));
}

/* operate number comparison */
static eval_status_t do_number_compare(tr7_engine_t tsc, int comp)
{
   tr7_t a, b, x = tsc->args;
   b = TR7_CAR(x);
   x = TR7_CDR(x);
   while (TR7_IS_PAIR(x)) {
      a = b;
      b = TR7_CAR(x);
      x = TR7_CDR(x);
      if (0 == (comp & tr7_cmp_num(a, b)))
         return s_return_false(tsc);
   }
   return s_return_true(tsc);
}

/* implement '=' */
static eval_status_t op_number_eq(tr7_engine_t tsc)
{
   return do_number_compare(tsc, TR7_CMP_EQUAL);
}

/* implement '<' */
static eval_status_t op_number_lt(tr7_engine_t tsc)
{
   return do_number_compare(tsc, TR7_CMP_LESSER);
}

/* implement '>' */
static eval_status_t op_number_gt(tr7_engine_t tsc)
{
   return do_number_compare(tsc, TR7_CMP_GREATER);
}

/* implement '<=' */
static eval_status_t op_number_le(tr7_engine_t tsc)
{
   return do_number_compare(tsc, TR7_CMP_LESSER | TR7_CMP_EQUAL);
}

/* implement '>=' */
static eval_status_t op_number_ge(tr7_engine_t tsc)
{
   return do_number_compare(tsc, TR7_CMP_GREATER | TR7_CMP_EQUAL);
}

static eval_status_t do_max_min(tr7_engine_t tsc, void (*func)(tr7_num_t*, tr7_t))
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   for(x = TR7_CDR(x) ; TR7_IS_PAIR(x) ; x = TR7_CDR(x))
      func(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'max' */
static eval_status_t op_max(tr7_engine_t tsc)
{
   return do_max_min(tsc, tr7_num_max);
}

/* implement 'min' */
static eval_status_t op_min(tr7_engine_t tsc)
{
   return do_max_min(tsc, tr7_num_min);
}

/* implement '+' */
static eval_status_t op_add(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(x))
      return s_return_single(tsc, TR7_FROM_INT(0));
   tr7_num_set(&n, TR7_CAR(x));
   for (x = TR7_CDR(x); !TR7_IS_NIL(x); x = TR7_CDR(x))
      tr7_num_add(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement '-' */
static eval_status_t op_sub(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(TR7_CDR(x)))
      tr7_num_set_int(&n, 0);
   else {
      tr7_num_set(&n, TR7_CAR(x));
      x = TR7_CDR(x);
   }
   for (; !TR7_IS_NIL(x) ; x = TR7_CDR(x))
      tr7_num_sub(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement '*' */
static eval_status_t op_mul(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(x))
      return s_return_single(tsc, TR7_FROM_INT(1));
   tr7_num_set(&n, TR7_CAR(x));
   for (x = TR7_CDR(x); !TR7_IS_NIL(x); x = TR7_CDR(x))
      tr7_num_mul(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement '/' */
static eval_status_t op_div(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(TR7_CDR(x)))
      tr7_num_set_int(&n, 1);
   else {
      tr7_num_set(&n, TR7_CAR(x));
      x = TR7_CDR(x);
   }
   for (; !TR7_IS_NIL(x); x = TR7_CDR(x)) {
      if (!tr7_num_div(&n, TR7_CAR(x)))
         return error_division_by_zero(tsc);
   }
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'abs' */
static eval_status_t op_abs(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x)) {
      if (TR7_TO_INT(x) < 0)
         x = TR7_FROM_INT(-TR7_TO_INT(x));
   }
   else {
      if (*TR7_TO_DOUBLE(x) < 0)
         x = tr7_from_double(tsc, -*TR7_TO_DOUBLE(x));
   }
   return s_return_single(tsc, x);
}

/* implement 'floor/' */
static eval_status_t op_floor_div(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_floor(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   x = TR7_LIST2(tsc, tr7_num_get(tsc, &n), tr7_num_get(tsc, &nn));
   return s_return_values(tsc, x);
}

/* implement 'floor-quotient' */
static eval_status_t op_floor_quotient(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_floor(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'floor-remainder' / 'modulo' */
static eval_status_t op_floor_rem(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_floor(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   return s_return_single(tsc, tr7_num_get(tsc, &nn));
}

/* implement 'truncate/' */
static eval_status_t op_truncate_div(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_trunc(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   x = TR7_LIST2(tsc, tr7_num_get(tsc, &n), tr7_num_get(tsc, &nn));
   return s_return_values(tsc, x);
}

/* implement 'truncate-quotient' / 'quotient' */
static eval_status_t op_truncate_quotient(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_trunc(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'truncate-remainder' / 'remainder' */
static eval_status_t op_truncate_rem(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   tr7_num_set(&n, TR7_CAR(x));
   if (!tr7_num_div_trunc(&n, &nn, TR7_CADR(x)))
      return error_division_by_zero(tsc);
   return s_return_single(tsc, tr7_num_get(tsc, &nn));
}

/* implement 'gcd' */
static eval_status_t op_gcd(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(x))
      return s_return_single(tsc, TR7_FROM_INT(0));
   tr7_num_set(&n, TR7_CAR(x));
   for (; !TR7_IS_NIL(x); x = TR7_CDR(x))
      tr7_num_gcd(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'lcm' */
static eval_status_t op_lcm(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(x))
      return s_return_single(tsc, TR7_FROM_INT(1));
   tr7_num_set(&n, TR7_CAR(x));
   for (; !TR7_IS_NIL(x); x = TR7_CDR(x))
      tr7_num_lcm(&n, TR7_CAR(x));
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

/* implement 'number->string' */
static eval_status_t op_num2str(tr7_engine_t tsc)
{
   char buf[140]; /* enough for 128 bits */
   int len;
   tr7_int_t pf = 10;
   tr7_t x = TR7_CAR(tsc->args);
   if (!TR7_IS_NIL(TR7_CDR(tsc->args))) {
      pf = tr7_to_int(TR7_CADR(tsc->args));
      if (pf != 16 && pf != 10 && pf != 8 && pf != 2)
         return Error_1(tsc, "bad radix", TR7_CADR(tsc->args));
   }
   len = format_number(tsc, buf, sizeof buf, (int)pf, x);
   return s_return_single(tsc, tr7_make_string_copy_length(tsc, buf, len));
}

/* implement 'string->number' */
static eval_status_t op_str2num(tr7_engine_t tsc)
{
   tr7_t r;
   long long iv;
   tr7_int_t pf = 0;
   char *ep, *s = (char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args));
   size_t length = TR7_LENGTH_STRING(TR7_CAR(tsc->args));
   if (!TR7_IS_NIL(TR7_CDR(tsc->args))) {
      pf = tr7_to_int(TR7_CADR(tsc->args));
      if (pf != 16 && pf != 10 && pf != 8 && pf != 2)
         return Error_1(tsc, "bad radix", TR7_CADR(tsc->args));
   }
   if (*s == '#')   /* no use of base! assume zero terminated string */
      r = mk_sharp_const(tsc, s + 1, length - 1);
   else if (pf == 0 || pf == 10)
      r = mk_atom(tsc, s, length);
   else {
      iv = strtoll(s, &ep, (int) pf);
      if (*ep == 0) /* TODO check limits */
         return s_return_integer(tsc, (tr7_int_t)iv);
      r = TR7_NIL;
   }
   return tr7_is_number(r) ? s_return_single(tsc, r) : s_return_false(tsc);
}

/* implement 'exact-integer-sqrt' */
static eval_status_t op_int_sqrt(tr7_engine_t tsc)
{
   tr7_num_t n, nn;
   tr7_t x = tsc->args;
   if (!tr7_num_exact_sqrt(&n, &nn, TR7_CAR(x)))
      return Error_0(tsc, "imaginary");
   x = TR7_LIST2(tsc, tr7_num_get(tsc, &n), tr7_num_get(tsc, &nn));
   return s_return_values(tsc, x);
}

/* implement 'square' */
static eval_status_t op_square(tr7_engine_t tsc)
{
   tr7_num_t n;
   tr7_t x = TR7_CAR(tsc->args);
   tr7_num_set(&n, x);
   tr7_num_mul(&n, x);
   return s_return_single(tsc, tr7_num_get(tsc, &n));
}

#if USE_MATH

/* implement 'exact' */
static eval_status_t op_exact(tr7_engine_t tsc)
{
   double dd;
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_DOUBLE(x)) {
      dd = *TR7_TO_DOUBLE(x);
      if (modf(dd, &dd) != 0.0)
         return Error_1(tsc, "not integral:", x);
      if (dd > TR7_INT_MAX || dd < TR7_INT_MIN)
         return Error_1(tsc, "out of range:", x);
      x = TR7_FROM_INT((tr7_int_t)dd);
   }
   return s_return_single(tsc, x);
}

/* implement 'inexact' */
static eval_status_t op_inexact(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x))
      x = tr7_from_double(tsc, TR7_TO_INT(x));
   return s_return_single(tsc, x);
}

/* implement 'expt' */
static eval_status_t op_expt(tr7_engine_t tsc)
{
   double result;
   int real_result = 1;
   tr7_t y = TR7_CADR(tsc->args);
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x) && TR7_IS_INT(y))
      real_result = 0;
   /* This 'if' is an R5RS compatibility fix. */
   /* NOTE: Remove this 'if' fix for R6RS.    */
   if (tr7_to_double(x) == 0 && tr7_to_double(y) < 0) {
      result = 0.0;
   }
   else {
      result = pow(tr7_to_double(x), tr7_to_double(y));
   }
   /* Before returning integer result make sure we can. */
   /* If the test fails, result is too big for integer. */
   if (!real_result) {
      tr7_int_t result_as_int = (tr7_int_t) result;
      if (result == (double) result_as_int)
         return s_return_integer(tsc, result);
   }
   return s_return_double(tsc, result);
}

/* implement 'floor' */
static eval_status_t op_floor(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, floor(tr7_to_double(x)));
}

/* implement 'ceiling' */
static eval_status_t op_ceiling(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, ceil(tr7_to_double(x)));
}

/* implement 'truncate' */
static eval_status_t op_truncate(tr7_engine_t tsc)
{
   double r = tr7_to_double(TR7_CAR(tsc->args));
   r = r > 0 ? floor(r) : ceil(r);
   return s_return_double(tsc, r);
}

/* implement 'round' */
static eval_status_t op_round(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (TR7_IS_INT(x))
      return s_return_single(tsc, x);
   return s_return_double(tsc, rint(tr7_to_double(x)));
}
#endif

/*************************************************************************
* SECTION SCHEME_INEXACT
* ----------------------
*/
#if USE_SCHEME_INEXACT
/* implement 'finite?' */
static eval_status_t op_is_finite(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_finite(TR7_CAR(tsc->args)));
}

/* implement 'infinite?' */
static eval_status_t op_is_infinite(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_infinite(TR7_CAR(tsc->args)));
}

/* implement 'nan?' */
static eval_status_t op_is_nan(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_NaN(TR7_CAR(tsc->args)));
}

#if USE_MATH
/* implement 'exp' */
static eval_status_t op_exp(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, exp(tr7_to_double(x)));
}

/* implement 'log' */
static eval_status_t op_log(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, log(tr7_to_double(x)));
}

/* implement 'sin' */
static eval_status_t op_sin(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, sin(tr7_to_double(x)));
}

/* implement 'cos' */
static eval_status_t op_cos(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, cos(tr7_to_double(x)));
}

/* implement 'tan' */
static eval_status_t op_tan(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, tan(tr7_to_double(x)));
}

/* implement 'asin' */
static eval_status_t op_asin(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, asin(tr7_to_double(x)));
}

/* implement 'acos' */
static eval_status_t op_acos(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, acos(tr7_to_double(x)));
}

/* implement 'atan' */
static eval_status_t op_atan(tr7_engine_t tsc)
{
   double dd;
   tr7_t x = tsc->args;
   if (TR7_IS_NIL(TR7_CDR(x)))
      dd = atan(tr7_to_double(TR7_CAR(x)));
   else
      dd = atan2(tr7_to_double(TR7_CAR(x)), tr7_to_double(TR7_CADR(x)));
   return s_return_double(tsc, dd);
}

/* implement 'sqrt' */
static eval_status_t op_sqrt(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   return s_return_double(tsc, sqrt(tr7_to_double(x)));
}
#endif
#endif

/* implement 'boolean?' */
static eval_status_t op_is_boolean(tr7_engine_t tsc)
{
      return s_return_boolean(tsc, TR7_IS_BOOLEAN(TR7_CAR(tsc->args)));
}

/* implement 'not' */
static eval_status_t op_not(tr7_engine_t tsc)
{
      return s_return_boolean(tsc, TR7_IS_FALSE(TR7_CAR(tsc->args)));
}

/* implement 'boolean=?' */
static eval_status_t op_boolean_eq(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   tr7_t v = TR7_CAR(x);
   if (!TR7_IS_BOOLEAN(v))
      return s_return_false(tsc);
   for (x = TR7_CDR(x) ; TR7_IS_PAIR(x) ; x = TR7_CDR(x))
      if (v != TR7_CAR(x))
         return s_return_false(tsc);
   return s_return_true(tsc);
}

/* implement 'pair?' */
static eval_status_t op_is_pair(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_PAIR(TR7_CAR(tsc->args)));
}

/* implement 'cons' */
static eval_status_t op_cons(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_cons(tsc, TR7_CAR(tsc->args), TR7_CADR(tsc->args)));
}

/* implement 'car' */
static eval_status_t op_car(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_CAAR(tsc->args));
}

/* implement 'cdr' */
static eval_status_t op_cdr(tr7_engine_t tsc)
{
   return s_return_single(tsc, TR7_CDAR(tsc->args));
}

/* implement 'set-car!' */
static eval_status_t op_set_car(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (tr7_is_immutable(x))
      return error_immutable(tsc);
   TR7_CAR(x) = TR7_CADR(tsc->args);
   return s_return_single(tsc, x);
}

/* implement 'set-cdr!' */
static eval_status_t op_set_cdr(tr7_engine_t tsc)
{
   tr7_t x = TR7_CAR(tsc->args);
   if (tr7_is_immutable(x))
      return error_immutable(tsc);
   TR7_CDR(x) = TR7_CADR(tsc->args);
   return s_return_single(tsc, x);
}

static eval_status_t do_cxr(tr7_engine_t tsc, tr7_t (*cxr)(tr7_t))
{
   tr7_t v = cxr(TR7_CAR(tsc->args));
   if (TR7_IS_VOID(v))
      return error_invalid_argument(tsc);
   return s_return_single(tsc, v);
}

/* implement 'caar' */
static eval_status_t op_caar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caar_or_void);
}

/* implement 'cadr' */
static eval_status_t op_cadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cadr_or_void);
}

/* implement 'cdar' */
static eval_status_t op_cdar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdar_or_void);
}

/* implement 'cddr' */
static eval_status_t op_cddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cddr_or_void);
}

/* implement 'null?' */
static eval_status_t op_is_null(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_NIL(TR7_CAR(tsc->args)));
}

/* implement 'list?' */
static eval_status_t op_is_list(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_list_length(TR7_CAR(tsc->args)) >= 0);
}

/* implement 'make-list' */
static eval_status_t op_make_list(tr7_engine_t tsc)
{
   tr7_t v, x = tsc->args;
   tr7_int_t n = TR7_TO_INT(TR7_CAR(x));
   x = TR7_CDR(x);
   x = TR7_IS_PAIR(x) ? TR7_CAR(x) : TR7_NIL;
   for (v = TR7_NIL ; n > 0 ; n--)
      v = tr7_cons(tsc, x, v);
   return s_return_single(tsc, v);
}

/* implement 'list' */
static eval_status_t op_list(tr7_engine_t tsc)
{
   return s_return_single(tsc, tsc->args);
}

/* implement 'length' */
static eval_status_t op_length(tr7_engine_t tsc)
{
   tr7_int_t n = (tr7_int_t)tr7_list_length(TR7_CAR(tsc->args));
   if (n < 0)
      return error_invalid_argument(tsc);
   return s_return_integer(tsc, n);
}

/* implement 'append' */
static eval_status_t op_append(tr7_engine_t tsc)
{
   tr7_t v = TR7_NIL;
   tr7_t x = tsc->args;
   if (TR7EQ(x, v))
      return s_return_single(tsc, v);

   /* TR7_CDR() in the while condition is not a typo. If TR7_CAR() */
   /* is used (append '() 'a) will return the wrong result. */
   while (!TR7_IS_NIL(TR7_CDR(x))) {
      v = tr7_reverse(tsc, TR7_CAR(x), v);
      if (TR7_IS_VOID(v))
         return error_invalid_argument(tsc);
      x = TR7_CDR(x);
   }
   return s_return_single(tsc, tr7_reverse_in_place(v, TR7_CAR(x))); /* TODO: avoid reversing */
}

/* implement 'reverse' */
static eval_status_t op_reverse(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_reverse(tsc, TR7_CAR(tsc->args), TR7_NIL));

}

/* implement 'list-tail' */
static eval_status_t op_list_tail(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   tr7_t v = TR7_CAR(x);
   tr7_int_t n = TR7_TO_INT(TR7_CADR(x));
   for ( ; n > 0 ; n--, v = TR7_CDR(v))
      if (!TR7_IS_PAIR(v))
         return error_invalid_argument(tsc);
   return s_return_single(tsc, v);
}

/* implement 'list-ref' */
static eval_status_t op_list_ref(tr7_engine_t tsc)
{
   tr7_int_t n;
   tr7_t x = tsc->args;
   tr7_t v = TR7_CAR(x);
   for (n = TR7_TO_INT(TR7_CADR(x)) ; n > 0 ; n--, v = TR7_CDR(v))
      if (!TR7_IS_PAIR(v))
         return error_invalid_argument(tsc);
   if (!TR7_IS_PAIR(v))
      return error_invalid_argument(tsc);
   return s_return_single(tsc, TR7_CAR(v));
}

/* implement 'list-set!' */
static eval_status_t op_list_set(tr7_engine_t tsc)
{
   tr7_int_t n;
   tr7_t x = tsc->args;
   tr7_t v = TR7_CAR(x);
   x = TR7_CDR(x);
   for (n = TR7_TO_INT(TR7_CAR(x)) ; n > 0 ; n--, v = TR7_CDR(v))
      if (!TR7_IS_PAIR(v))
         return error_invalid_argument(tsc);
   if (!TR7_IS_PAIR(v))
      return error_invalid_argument(tsc);
   TR7_CAR(v) = TR7_CADR(x);
   return s_return_void(tsc);
}

/* implement 'list-copy' */
static eval_status_t op_list_copy(tr7_engine_t tsc)
{
   tr7_t v, x = TR7_CAR(tsc->args);
   if (!TR7_IS_PAIR(x))
      return s_return_single(tsc, x);
   if (!list_copy(tsc, x, &v, &x))
      return error_invalid_argument(tsc);
   return s_return_single(tsc, v);
}



























/* implement 'memq' */
static eval_status_t op_memq(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   return s_return_single(tsc, tr7_memq(TR7_CAR(x), TR7_CADR(x)));
}

/* implement 'memv' */
static eval_status_t op_memv(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   return s_return_single(tsc, tr7_memv(TR7_CAR(x), TR7_CADR(x)));
}

static int member_test(tr7_engine_t tsc)
{
   tr7_pair_t pairs[6];
   tr7_t list, slow;
   tr7_int_t idx;

   tr7_get_list_pairs(tsc->args, 6, pairs);
   list = pairs[0]->car;
   if (!TR7_IS_PAIR(list))
      return s_return_false(tsc);
   idx = TR7_TO_INT(pairs[1]->car);
   if (idx++ & 1) {
      slow = TR7_CDR(pairs[2]->car);
      if (idx > 2 && TR7EQ(slow, list))
         return s_return_false(tsc);
      pairs[2]->car = slow;
   }
   pairs[1]->car = TR7_FROM_INT(idx);
   pairs[5]->car = TR7_CAR(list);
   s_save(tsc, OP(MEMBER_THEN), tsc->args);
   return s_exec(tsc, pairs[3]->car, pairs[3]->cdr);
}

/* implement 'member' */
static eval_status_t op_member(tr7_engine_t tsc)
{
   tr7_t item, list, cmp, res, x = tsc->args;
   item = TR7_CAR(x);
   x = TR7_CDR(x);
   list = TR7_CAR(x);
   x = TR7_CDR(x);
   cmp = TR7_IS_NIL(x) ? TR7_FROM_OPER(OP(EQUAL)) : TR7_CAR(x);
   if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQUAL))))
      res = tr7_meme(item, list);
   else if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQV))))
      res = tr7_memv(item, list);
   else if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQ))))
      res = tr7_memq(item, list);
   else {
         tsc->args = TR7_LIST6(tsc, list, TR7_FROM_INT(0), list, cmp, item, TR7_NIL);
         return member_test(tsc);
   }
   return s_return_single(tsc, res);
}

/* helper to implement 'member' */
static eval_status_t op_member_then(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t list = TR7_CAR(args);
   if (!TR7_IS_FALSE(tsc->value))
      return s_return_single(tsc, list);
   TR7_CAR(args) = TR7_CDR(list);
   return member_test(tsc);
}

/* implement 'assq' */
static eval_status_t op_assq(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   return s_return_single(tsc, tr7_assq(TR7_CAR(x), TR7_CADR(x)));
}

/* implement 'assv' */
static eval_status_t op_assv(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   return s_return_single(tsc, tr7_assv(TR7_CAR(x), TR7_CADR(x)));
}

static int assoc_test(tr7_engine_t tsc)
{
   tr7_pair_t pairs[6];
   tr7_t list, head, slow;
   tr7_int_t idx;

   tr7_get_list_pairs(tsc->args, 6, pairs);
   list = pairs[0]->car;
   idx = TR7_TO_INT(pairs[1]->car);
   slow = pairs[2]->car;
   for ( ; TR7_IS_PAIR(list) ; list = TR7_CDR(list)) {
      if (idx++ & 1) {
         slow = TR7_CDR(slow);
         if (idx > 2 && TR7EQ(slow, list))
            return s_return_false(tsc);
      }
      head = TR7_CAR(list);
      if (TR7_IS_PAIR(head)) {
         pairs[0]->car = list;
         pairs[1]->car = TR7_FROM_INT(idx);
         pairs[2]->car = slow;
         pairs[5]->car = TR7_CAR(head);
         s_save(tsc, OP(ASSOC_THEN), tsc->args);
         return s_exec(tsc, pairs[3]->car, pairs[3]->cdr);
      }
   }
   return s_return_false(tsc);
}

/* implement 'assoc' */
static eval_status_t op_assoc(tr7_engine_t tsc)
{
   tr7_t item, list, cmp, res, x = tsc->args;
   item = TR7_CAR(x);
   x = TR7_CDR(x);
   list = TR7_CAR(x);
   x = TR7_CDR(x);
   cmp = TR7_IS_NIL(x) ? TR7_FROM_OPER(OP(EQUAL)) : TR7_CAR(x);
   if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQUAL))))
      res = tr7_asse(item, list);
   else if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQV))))
      res = tr7_assv(item, list);
   else if (TR7EQ(cmp, TR7_FROM_OPER(OP(EQ))))
      res = tr7_assq(item, list);
   else {
         tsc->args = TR7_LIST6(tsc, list, TR7_FROM_INT(0), list, cmp, item, TR7_NIL);
         return assoc_test(tsc);
   }
   return s_return_single(tsc, res);
}

/* helper to implement 'assoc' */
static eval_status_t op_assoc_then(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t list = TR7_CAR(args);
   if (!TR7_IS_FALSE(tsc->value))
      return s_return_single(tsc, TR7_CAR(list));
   TR7_CAR(args) = TR7_CDR(list);
   return assoc_test(tsc);
}

/*************************************************************************
* SECTION SCHEME_CXR
* ------------------
*/
#if USE_SCHEME_CXR

static eval_status_t op_caaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caaar_or_void);
}

static eval_status_t op_caadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caadr_or_void);
}

static eval_status_t op_cadar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cadar_or_void);
}

static eval_status_t op_caddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caddr_or_void);
}

static eval_status_t op_cdaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdaar_or_void);
}

static eval_status_t op_cdadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdadr_or_void);
}

static eval_status_t op_cddar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cddar_or_void);
}

static eval_status_t op_cdddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdddr_or_void);
}

static eval_status_t op_caaaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caaaar_or_void);
}

static eval_status_t op_caaadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caaadr_or_void);
}

static eval_status_t op_caadar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caadar_or_void);
}

static eval_status_t op_caaddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caaddr_or_void);
}

static eval_status_t op_cadaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cadaar_or_void);
}

static eval_status_t op_cadadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cadadr_or_void);
}

static eval_status_t op_caddar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_caddar_or_void);
}

static eval_status_t op_cadddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cadddr_or_void);
}

static eval_status_t op_cdaaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdaaar_or_void);
}

static eval_status_t op_cdaadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdaadr_or_void);
}

static eval_status_t op_cdadar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdadar_or_void);
}

static eval_status_t op_cdaddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdaddr_or_void);
}

static eval_status_t op_cddaar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cddaar_or_void);
}

static eval_status_t op_cddadr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cddadr_or_void);
}

static eval_status_t op_cdddar(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cdddar_or_void);
}

static eval_status_t op_cddddr(tr7_engine_t tsc)
{
   return do_cxr(tsc, tr7_cddddr_or_void);
}
#endif
/* ========== symbol ========== */

/* implement 'symbol?' */
static eval_status_t op_is_symbol(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_SYMBOL(TR7_CAR(tsc->args)));
}

/* implement 'symbol=?' */
static eval_status_t op_is_symbol_eq(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   tr7_t v = TR7_CAR(x);
   if (TR7_IS_SYMBOL(v))
      do {
         x = TR7_CDR(x);
         if (TR7_IS_NIL(x))
            return s_return_true(tsc);
      } while(v == TR7_CAR(x));
   return s_return_false(tsc);
}

/* implement 'string->symbol' */
static eval_status_t op_str2sym(tr7_engine_t tsc)
{
   tr7_t s = TR7_CAR(tsc->args);
   const char *v = (char *)TR7_CONTENT_STRING(s);
   size_t l = TR7_LENGTH_STRING(s);
   return s_return_single(tsc, tr7_get_symbol_length(tsc, v, l, 1));
}

/* implement 'symbol->string' */
static eval_status_t op_sym2str(tr7_engine_t tsc)
{
   tr7_t s = TR7_CAR(tsc->args);
   const char *v = (char *)TR7_CONTENT_SYMBOL(s);
   tr7_t x = tr7_make_string_copy(tsc, v);
   tr7_set_immutable(x);
   return s_return_single(tsc, x);
}

/* ========== char ========== */

/* implement 'char?' */
static eval_status_t op_is_char(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_CHAR(TR7_CAR(tsc->args)));
}

static eval_status_t do_char_cmp_fun(tr7_engine_t tsc, int cmpmsk, int (*cmpfun)(wint_t,wint_t))
{
   tr7_t x = tsc->args;
   int oc, c = (int)TR7_TO_CHAR(TR7_CAR(x));
   for(x = TR7_CDR(x) ; TR7_IS_PAIR(x) ; x = TR7_CDR(x)) {
      oc = (int)TR7_TO_CHAR(TR7_CAR(x));
      if (!(cmpfun(c, oc) & cmpmsk))
         return s_return_false(tsc);
      c = oc;
   }
   return s_return_true(tsc);
}

static eval_status_t do_char_cmp(tr7_engine_t tsc, int cmpmsk)
{
   return do_char_cmp_fun(tsc, cmpmsk, char_cmp);
}

/* implement 'char=?' */
static eval_status_t op_char_eq(tr7_engine_t tsc)
{
   return do_char_cmp(tsc, TR7_CMP_EQUAL);
}

/* implement 'char<?' */
static eval_status_t op_char_lt(tr7_engine_t tsc)
{
   return do_char_cmp(tsc, TR7_CMP_LESSER);
}

/* implement 'char>?' */
static eval_status_t op_char_gt(tr7_engine_t tsc)
{
   return do_char_cmp(tsc, TR7_CMP_GREATER);
}

/* implement 'char<=?' */
static eval_status_t op_char_le(tr7_engine_t tsc)
{
   return do_char_cmp(tsc, TR7_CMP_LESSER | TR7_CMP_EQUAL);
}

/* implement 'char>=?' */
static eval_status_t op_char_ge(tr7_engine_t tsc)
{
   return do_char_cmp(tsc, TR7_CMP_GREATER | TR7_CMP_EQUAL);
}

/* implement 'char->integer' */
static eval_status_t op_char2int(tr7_engine_t tsc)
{
   int c = (int)TR7_TO_CHAR(TR7_CAR(tsc->args));
   return s_return_integer(tsc, c);
}

/* implement 'integer->char' */
static eval_status_t op_int2char(tr7_engine_t tsc)
{
   int c = tr7_to_int(TR7_CAR(tsc->args));
   return s_return_single(tsc, TR7_FROM_CHAR(c));
}

/*************************************************************************
* SECTION OP(STRING)
* -----------------
*/
/* ========== string ========== */

/* implement 'string?' */
static eval_status_t op_is_string(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_string(TR7_CAR(tsc->args)));
}

/* implement 'make-string' */
static eval_status_t op_make_string(tr7_engine_t tsc)
{
   tr7_uint_t len = (tr7_uint_t)tr7_to_int(TR7_CAR(tsc->args));
   wint_t car = ' ';
   if (!TR7_IS_NIL(TR7_CDR(tsc->args)))
      car = TR7_TO_CHAR(TR7_CADR(tsc->args));
   return s_return_single_alloc(tsc, tr7_make_string_fill(tsc, car, len));
}

/* transform a list of chars to a string */
static eval_status_t do_list_to_string(tr7_engine_t tsc, tr7_t lst)
{
   uint8_t *str;
   tr7_t it, res;
   unsigned len, index;
   if (tr7_list_length(lst) < 0)
      return Error_0(tsc, "improper list");
   for (len = 0, it = lst ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      if (!TR7_IS_CHAR(TR7_CAR(it)))
         return Error_1(tsc, "expected char but got ", TR7_CAR(lst));
      len += char_length(TR7_TO_CHAR(TR7_CAR(it)));
   }
   res = tr7_make_string(tsc, len);
   if (!TR7_IS_NIL(res)) {
      str = TR7_CONTENT_STRING(res);
      for (index = 0, it = lst ; TR7_IS_PAIR(it) ; it = TR7_CDR(it))
         index += char_to_utf8(TR7_TO_CHAR(TR7_CAR(it)), &str[index]);
   }
   return s_return_single(tsc, res);
}

/* implement 'string' */
static eval_status_t op_string(tr7_engine_t tsc)
{
   return do_list_to_string(tsc, tsc->args);
}

/* implement 'list->string' */
static eval_status_t op_list_to_string(tr7_engine_t tsc)
{
   return do_list_to_string(tsc, TR7_CAR(tsc->args));
}

/* implement 'string-length' */
static eval_status_t op_string_length(tr7_engine_t tsc)
{
   return s_return_integer(tsc, tr7_string_length(TR7_CAR(tsc->args)));
}

/* implement 'string-ref' */
static eval_status_t op_string_ref(tr7_engine_t tsc)
{
   wint_t car = tr7_string_ref(TR7_CAR(tsc->args), (size_t)tr7_to_int(TR7_CADR(tsc->args)));
   if (car == (wint_t)WEOF)
      return error_out_of_bound(tsc);
   return s_return_single(tsc, TR7_FROM_CHAR(car));
}

/* implement 'string-set!' */
static eval_status_t op_string_set(tr7_engine_t tsc)
{
   tr7_t string = TR7_CAR(tsc->args);
   size_t index = (tr7_uint_t)tr7_to_int(TR7_CADR(tsc->args));
   wint_t car = TR7_TO_CHAR(TR7_CADDR(tsc->args));
   if (tr7_is_immutable(string))
      return error_immutable(tsc);
   if (!tr7_string_set(tsc, string, index, car))
      return error_out_of_bound(tsc);
   return s_return_void(tsc);
}

static eval_status_t do_string_cmp(tr7_engine_t tsc, int cmp, int (*cmpfun)(wint_t,wint_t))
{
   int c;
   wint_t car, car2;
   tr7_t x = tsc->args;
   const uint8_t *str2, *str = TR7_CONTENT_STRING(TR7_CAR(x));
   tr7_uint_t idx, idx2, len2, len = TR7_LENGTH_STRING(TR7_CAR(x));
   for(x = TR7_CDR(x) ; TR7_IS_PAIR(x) ; x = TR7_CDR(x)) {
      str2 = TR7_CONTENT_STRING(TR7_CAR(x));
      len2 = TR7_LENGTH_STRING(TR7_CAR(x));
      for (idx = idx2 = 0; ; ) {
         if (idx >= len)
            c = idx2 >= len2 ? TR7_CMP_EQUAL : TR7_CMP_LESSER;
         else if (idx2 >= len2)
            c = TR7_CMP_GREATER;
         else {
            idx += utf8_to_char(&str[idx], &car);
            idx2 += utf8_to_char(&str2[idx2], &car2);
            c = cmpfun(car, car2);
            if (c == TR7_CMP_EQUAL)
               continue;
         }
         break;
      }
      if (!(c & cmp))
         return s_return_false(tsc);
      str = str2;
      len = len2;
   }
   return s_return_true(tsc);
}

static eval_status_t do_string_compare(tr7_engine_t tsc, int cmp)
{
      return do_string_cmp(tsc, cmp, char_cmp);
}

/* implement 'string=?' */
static eval_status_t op_string_eq(tr7_engine_t tsc)
{
   return do_string_compare(tsc, TR7_CMP_EQUAL);
}

/* implement 'string<?' */
static eval_status_t op_string_lt(tr7_engine_t tsc)
{
   return do_string_compare(tsc, TR7_CMP_LESSER);
}

/* implement 'string>?' */
static eval_status_t op_string_gt(tr7_engine_t tsc)
{
   return do_string_compare(tsc, TR7_CMP_GREATER);
}

/* implement 'string<=?' */
static eval_status_t op_string_le(tr7_engine_t tsc)
{
   return do_string_compare(tsc, TR7_CMP_LESSER |TR7_CMP_EQUAL);
}

/* implement 'string>=?' */
static eval_status_t op_string_ge(tr7_engine_t tsc)
{
   return do_string_compare(tsc, TR7_CMP_GREATER |TR7_CMP_EQUAL);
}

/* implement 'string-append' */
static eval_status_t op_string_append(tr7_engine_t tsc)
{
   uint8_t *str;
   tr7_t x, r;
   tr7_uint_t len;
   /* compute needed length for new string */
   for (len = 0, x = tsc->args; TR7_IS_PAIR(x); x = TR7_CDR(x))
      len += TR7_LENGTH_STRING(TR7_CAR(x));
   /* allocate the result */
   r = tr7_make_string(tsc, len);
   if (TR7_IS_STRING(r)) {
      for (x = tsc->args, str = TR7_CONTENT_STRING(r) ; TR7_IS_PAIR(x); x = TR7_CDR(x)) {
         len = TR7_LENGTH_STRING(TR7_CAR(x));
         memcpy(str, TR7_CONTENT_STRING(TR7_CAR(x)), len);
         str += len;
      }
   }
   return s_return_single(tsc, r);
}

/*
* gets zero, one or two indexes from args
* returns the count of indexes gotten
*/
static int get_start_end(tr7_t args, tr7_int_t indexes[2])
{
   if (!TR7_IS_PAIR(args))
      return 0;

   indexes[0] = tr7_to_int(TR7_CAR(args));
   if (indexes[0] < 0)
      return -1;
   args = TR7_CDR(args);
   if (!TR7_IS_PAIR(args))
      return 1;

   indexes[1] = tr7_to_int(TR7_CAR(args));
   if (indexes[1] < indexes[0])
      return -1;
   return 2;
}

struct substring_desc {
   uint8_t *string;
   tr7_uint_t length;
   tr7_int_t indexes[2];
   tr7_int_t offsets[2];
};

static int make_substring_desc(tr7_t args, struct substring_desc *subd, tr7_t string)
{
   int n;
   ssize_t ssz;
   tr7_uint_t len;

   /* get string */
   subd->string = TR7_CONTENT_STRING(string);
   subd->length = len = TR7_LENGTH_STRING(string);

   /* get bounds */
   n = get_start_end(args, subd->indexes);
   if (n < 0)
      return 0;
   if (n == 0) {
      subd->indexes[0] = 0;
      subd->offsets[0] = 0;
   }
   else {
      ssz = utf8str_offset((uint8_t*)subd->string, len, (tr7_uint_t)subd->indexes[0]);
      if (ssz < 0)
         return 0;
      subd->offsets[0] = ssz;
   }
   if (n <= 1) {
      subd->indexes[1] = subd->indexes[0] + utf8str_nchars(&subd->string[subd->indexes[0]], len - subd->indexes[0]);
      subd->offsets[1] = len;
   }
   else {
      ssz = utf8str_offset((uint8_t*)&subd->string[subd->offsets[0]], len - subd->offsets[0], (tr7_uint_t)(subd->indexes[1] - subd->indexes[0]));
      if (ssz < 0)
         return 0;
      subd->offsets[1] = subd->offsets[0] + ssz;
   }
   return 1;
}

static int get_substring_desc(tr7_t args, struct substring_desc *subd)
{
   return make_substring_desc(TR7_CDR(args), subd, TR7_CAR(args));
}

/* implement 'string->list' */
static eval_status_t op_string_to_list(tr7_engine_t tsc)
{
   wint_t car;
   tr7_t *l, r, x;
   struct substring_desc subd;

   if (!get_substring_desc(tsc->args, &subd))
      goto bound_error;

   /* make list */
   r = TR7_NIL;
   l = &r;
   while(subd.offsets[0] < subd.offsets[1]) {
      subd.offsets[0] += utf8_to_char((uint8_t*)&subd.string[subd.offsets[0]], &car);
      *l = x = tr7_cons(tsc, TR7_FROM_CHAR(car), TR7_NIL);
      l = &TR7_CDR(x);
   }
   return s_return_single(tsc, r);

bound_error:
   return error_out_of_bound(tsc);
}

/* implement 'substring' and 'string-copy' */
static eval_status_t op_string_copy(tr7_engine_t tsc)
{
   tr7_t res;
   struct substring_desc subd;
   if (!get_substring_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);
   res = tr7_make_string_copy_length(tsc, (char*)&subd.string[subd.offsets[0]], subd.offsets[1] - subd.offsets[0]);
   return s_return_single_alloc(tsc, res);
}

/* implement 'string-copy!' */
static eval_status_t op_string_copy_to(tr7_engine_t tsc)
{
   tr7_t args, to;
   uint8_t *str, *cpy;
   ssize_t ssz;
   size_t at, len, start, stop, slen, dlen;
   struct substring_desc subd;

   /* get destination data */
   args = tsc->args;
   to = TR7_CAR(args);
   str = TR7_CONTENT_STRING(to);
   len = TR7_LENGTH_STRING(to);
   args = TR7_CDR(args);
   at = tr7_to_int(TR7_CAR(args));
   ssz = utf8str_offset(str, len, at);
   if (ssz < 0)
      goto bound_error;

   /* get source data */
   if (!get_substring_desc(TR7_CDR(args), &subd))
      goto bound_error;

   /* replaced destination length */
   start = (size_t)ssz;
   ssz = utf8str_offset(&str[start], len - start, subd.indexes[1] - subd.indexes[0]);
   if (ssz < 0)
      goto bound_error;
   dlen = (size_t)ssz;
   stop = start + dlen;

   /* copy */
   slen = subd.offsets[1] - subd.offsets[0];
   if (dlen >= slen) {
      /* replaced length greater than replacement's one */
      memcpy(&str[start], &subd.string[subd.offsets[0]], slen);
      if (dlen > slen) {
         memmove(&str[start + slen], &str[start + dlen], len - stop);
         TR7_SET_LENGTH_STRING(to, len + slen - dlen);
      }
   }
   else {
      cpy = memalloc(tsc, len + slen - dlen + 1);
      if (!cpy)
         return error_out_of_memory(tsc);
      TR7_SET_LENGTH_STRING(to, len + slen - dlen);
      cpy[len + slen - dlen] = 0;
      TR7_CONTENT_STRING(to) = cpy;
      memcpy(cpy, str, start);
      memcpy(&cpy[start], &subd.string[subd.offsets[0]], slen);
      memcpy(&cpy[start + slen], &str[start + dlen], len - stop);
      memfree(tsc, str);
   }
   return s_return_single(tsc, to);

bound_error:
   return error_out_of_bound(tsc);
}

/* implement 'string-fill!' */
static eval_status_t op_string_fill(tr7_engine_t tsc)
{
   uint8_t buf[UTF8BUFFSIZE];
   wint_t car;
   tr7_t args, to;
   uint8_t *cpy;
   size_t szc, nlen, olen, nc;
   struct substring_desc subd;

   /* get parameters */
   args = tsc->args;
   to = TR7_CAR(args);
   args = TR7_CDR(args);
   car = TR7_TO_CHAR(TR7_CAR(args));
   if (!make_substring_desc(TR7_CDR(args), &subd, to))
      return error_out_of_bound(tsc);

   /* lengths */
   szc = char_to_utf8(car, buf);
   nc = subd.indexes[1] - subd.indexes[0];
   nlen = szc * nc;
   olen = subd.offsets[1] - subd.offsets[0];

   /* set */
   if (olen >= nlen) {
      /* replaced length greater than replacement's one */
      while(nc) {
         memcpy(&subd.string[subd.offsets[0]], buf, szc);
         subd.offsets[0] += szc;
         nc--;
      }
      if (olen > nlen) {
         memmove(&subd.string[subd.offsets[0]], &subd.string[subd.offsets[1]], subd.length - subd.offsets[1]);
         TR7_SET_LENGTH_STRING(to, subd.length + nlen - olen);
      }
   }
   else {
      cpy = memalloc(tsc, subd.length + nlen - olen + 1);
      if (!cpy)
         return error_out_of_memory(tsc);
      TR7_SET_LENGTH_STRING(to, subd.length + nlen - olen);
      cpy[subd.length + nlen - olen] = 0;
      TR7_CONTENT_STRING(to) = cpy;
      memcpy(cpy, subd.string, subd.offsets[0]);
      while(nc) {
         memcpy(&cpy[subd.offsets[0]], buf, szc);
         subd.offsets[0] += szc;
         nc--;
      }
      memcpy(&cpy[subd.offsets[0]], &subd.string[subd.offsets[1]], subd.length - subd.offsets[1]);
      memfree(tsc, subd.string);
   }
   return s_return_single(tsc, to);
}

/*************************************************************************
* SECTION SCHEME_CHAR
* ----------------------
*/
#if USE_SCHEME_CHAR
static eval_status_t do_char_cmp_ci(tr7_engine_t tsc, int cmpmsk)
{
   return do_char_cmp_fun(tsc, cmpmsk, char_cmp_ci);
}

/* implement 'char-ci=?' */
static eval_status_t op_char_eq_ci(tr7_engine_t tsc)
{
   return do_char_cmp_ci(tsc, TR7_CMP_EQUAL);
}

/* implement 'char-ci<?' */
static eval_status_t op_char_lt_ci(tr7_engine_t tsc)
{
   return do_char_cmp_ci(tsc, TR7_CMP_LESSER);
}

/* implement 'char-ci>?' */
static eval_status_t op_char_gt_ci(tr7_engine_t tsc)
{
   return do_char_cmp_ci(tsc, TR7_CMP_GREATER);
}

/* implement 'char-ci<=?' */
static eval_status_t op_char_le_ci(tr7_engine_t tsc)
{
   return do_char_cmp_ci(tsc, TR7_CMP_LESSER | TR7_CMP_EQUAL);
}

/* implement 'char-ci>=?' */
static eval_status_t op_char_ge_ci(tr7_engine_t tsc)
{
   return do_char_cmp_ci(tsc, TR7_CMP_GREATER | TR7_CMP_EQUAL);
}

/* implement 'char-alphabetic?' */
static eval_status_t op_char_is_alpha(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, Cisalpha((int)TR7_TO_CHAR(TR7_CAR(tsc->args))));
}

/* implement 'char-numeric?' */
static eval_status_t op_char_is_num(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, Cisdigit((int)TR7_TO_CHAR(TR7_CAR(tsc->args))));
}

/* implement 'char-whitespace?' */
static eval_status_t op_char_is_space(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, Cisspace((int)TR7_TO_CHAR(TR7_CAR(tsc->args))));
}

/* implement 'char-upper-case?' */
static eval_status_t op_char_is_upper(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, Cisupper((int)TR7_TO_CHAR(TR7_CAR(tsc->args))));
}

/* implement 'char-lower-case?' */
static eval_status_t op_char_is_lower(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, Cislower((int)TR7_TO_CHAR(TR7_CAR(tsc->args))));
}

/* implement 'char-upcase' */
static eval_status_t op_char_upcase(tr7_engine_t tsc)
{
   int c = (int)TR7_TO_CHAR(TR7_CAR(tsc->args));
   c = towupper(c);
   return s_return_single(tsc, TR7_FROM_CHAR(c));
}

/* implement 'char-downcase' and 'char-foldcase' */
static eval_status_t op_char_downcase(tr7_engine_t tsc)
{
   int c = (int)TR7_TO_CHAR(TR7_CAR(tsc->args));
   c = towlower(c);
   return s_return_single(tsc, TR7_FROM_CHAR(c));
}

/* implement 'digit-value' */
static eval_status_t op_char_digit_value(tr7_engine_t tsc)
{
   int c = (int)TR7_TO_CHAR(TR7_CAR(tsc->args));
   if (!Cisdigit(c))
      return s_return_false(tsc);
   return s_return_integer(tsc, (int)(c - '0'));
}

static eval_status_t do_string_compare_ci(tr7_engine_t tsc, int cmp)
{
      return do_string_cmp(tsc, cmp, char_cmp_ci);
}

/* implement 'string-ci=?' */
static eval_status_t op_string_eq_ci(tr7_engine_t tsc)
{
   return do_string_compare_ci(tsc, TR7_CMP_EQUAL);
}

/* implement 'string-ci<?' */
static eval_status_t op_string_lt_ci(tr7_engine_t tsc)
{
   return do_string_compare_ci(tsc, TR7_CMP_LESSER);
}

/* implement 'string-ci>?' */
static eval_status_t op_string_gt_ci(tr7_engine_t tsc)
{
   return do_string_compare_ci(tsc, TR7_CMP_GREATER);
}

/* implement 'string-ci<=?' */
static eval_status_t op_string_le_ci(tr7_engine_t tsc)
{
   return do_string_compare_ci(tsc, TR7_CMP_LESSER |TR7_CMP_EQUAL);
}

/* implement 'string-ci>=?' */
static eval_status_t op_string_ge_ci(tr7_engine_t tsc)
{
   return do_string_compare_ci(tsc, TR7_CMP_GREATER |TR7_CMP_EQUAL);
}

static eval_status_t do_change_case(tr7_engine_t tsc, wint_t (*cvtfun)(wint_t))
{
   uint8_t *rstr;
   wint_t car;
   tr7_t r, x = TR7_CAR(tsc->args);
   const uint8_t *str = TR7_CONTENT_STRING(x);
   size_t index, len2, len = TR7_LENGTH_STRING(x);
   for (len2 = 0, index = 0 ; index < len ;) {
      index += utf8_to_char(&str[index], &car);
      len2 += char_length(cvtfun(car));
   }
   r = tr7_make_string(tsc, len2);
   if (TR7_IS_STRING(r)) {
      rstr = TR7_CONTENT_STRING(r);
      for (index = 0 ; index < len ; ) {
         index += utf8_to_char(&str[index], &car);
         rstr += char_to_utf8(cvtfun(car), rstr);
      }
   }
   return s_return_single(tsc, r);
}

/* implement 'string-upcase' */
static eval_status_t op_string_upcase(tr7_engine_t tsc)
{
   return do_change_case(tsc, towupper);
}

/* implement 'string-downcase' and 'string-foldcase' */
static eval_status_t op_string_downcase(tr7_engine_t tsc)
{
   return do_change_case(tsc, towlower);
}
#endif
/*************************************************************************
* SECTION OP(VECTOR)
* -----------------
*
* implement 'vector?'
*/
static eval_status_t op_is_vector(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_VECTOR(TR7_CAR(tsc->args)));
}
/*  */
static tr7_t list_to_vector(tr7_engine_t tsc, tr7_t list)
{
   tr7_t vec = TR7_NIL, *items;
   tr7_uint_t len, idx;
   int i = tr7_list_length(list);
   if (i >= 0) {
      len = (tr7_uint_t)i;
      vec = tr7_make_vector(tsc, len);
      if (!TR7_IS_NIL(vec)) {
         items = TR7_ITEMS_VECTOR(vec);
         for (idx = 0; idx < len ; list = TR7_CDR(list))
            items[idx++] = TR7_CAR(list);
      }
   }
   return vec;
}
/*  */
static eval_status_t do_list_to_vector(tr7_engine_t tsc, tr7_t list)
{
   tr7_t vec = list_to_vector(tsc, list);
   if (!TR7_IS_NIL(vec))
      return s_return_single(tsc, vec);
   if (tr7_list_length(list) < 0)
      return Error_1(tsc, "vector: not a proper list:", list);
   return error_out_of_memory(tsc);
}
/*
* implement 'vector'
*/
static eval_status_t op_vector(tr7_engine_t tsc)
{
   return do_list_to_vector(tsc, tsc->args);
}
/*
* implement 'list->vector'
*/
static eval_status_t op_list_to_vector(tr7_engine_t tsc)
{
   return do_list_to_vector(tsc, TR7_CAR(tsc->args));
}
/*
* implement 'make-vector'
*/
static eval_status_t op_make_vector(tr7_engine_t tsc)
{
   tr7_t item, vec, args = tsc->args;
   tr7_uint_t len = (tr7_uint_t)tr7_to_int(TR7_CAR(args));
   args = TR7_CDR(args);
   item = TR7_IS_PAIR(args) ? TR7_CAR(args) : TR7_NIL;
   vec = tr7_make_vector_fill(tsc, item, len);
   return s_return_single_alloc(tsc, vec);
}
/*
* implement 'vector-length'
*/
static eval_status_t op_vector_length(tr7_engine_t tsc)
{
   return s_return_integer(tsc, TR7_LENGTH_VECTOR(TR7_CAR(tsc->args)));
}
/*
* implement 'vector-ref'
*/
static eval_status_t op_vector_ref(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t vec = TR7_CAR(args);
   tr7_uint_t idx = (tr7_uint_t)tr7_to_int(TR7_CADR(args));
   if (idx >= TR7_LENGTH_VECTOR(vec))
      return error_out_of_bound(tsc);
   return s_return_single(tsc, TR7_ITEM_VECTOR(vec, idx));
}

/* implement 'vector-set!' */
static eval_status_t op_vector_set(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t vec = TR7_CAR(args);
   args = TR7_CDR(args);
   tr7_uint_t idx = (tr7_uint_t)tr7_to_int(TR7_CAR(args));
   if (tr7_is_immutable(vec))
      return error_immutable(tsc);
   if (idx >= TR7_LENGTH_VECTOR(vec))
      return error_out_of_bound(tsc);
   TR7_ITEM_VECTOR(vec, idx) = TR7_CADR(args);
   return s_return_single(tsc, vec);
}

struct subvector_desc {
   tr7_t *items;
   tr7_uint_t length;
   tr7_int_t indexes[2];
};

static int make_subvector_desc(tr7_t args, struct subvector_desc *subd, tr7_t vector)
{
   int n;
   tr7_vector_t vec = TR7_TO_VECTOR(vector);

   subd->items = vec->items;
   subd->length = TR7_VECTOR_LENGTH(vec);

   /* get bounds */
   n = get_start_end(args, subd->indexes);
   if (n < 0)
      return 0;
   if (n == 0)
      subd->indexes[0] = 0;
   else if (subd->indexes[0] < 0 || (tr7_uint_t)subd->indexes[0] > subd->length)
      return 0;
   if (n <= 1)
      subd->indexes[1] = (tr7_int_t)subd->length;
   else if (subd->indexes[1] < subd->indexes[0] || (tr7_uint_t)subd->indexes[1] > subd->length)
      return 0;
   return 1;
}

static int get_subvector_desc(tr7_t args, struct subvector_desc *subd)
{
   return make_subvector_desc(TR7_CDR(args), subd, TR7_CAR(args));
}


/* implement 'vector->list' */
static eval_status_t op_vector_to_list(tr7_engine_t tsc)
{
   tr7_t l = TR7_NIL;
   struct subvector_desc subd;

   if (!get_subvector_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   while(subd.indexes[0] < subd.indexes[1])
      l = tr7_cons(tsc, subd.items[--subd.indexes[1]], l);
   return s_return_single(tsc, l);
}

/* implement 'vector->string' */
static eval_status_t op_vector_to_string(tr7_engine_t tsc)
{
   struct subvector_desc subd;
   tr7_t res, car;
   tr7_int_t idx, len;
   uint8_t *str;

   if (!get_subvector_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   for (len = 0, idx = subd.indexes[0] ; idx < subd.indexes[1] ; idx++) {
      car = subd.items[idx];
      if (!TR7_IS_CHAR(car))
         return Error_1(tsc, "expected char but got ", car);
      len += char_length(TR7_TO_CHAR(car));
   }

   res = tr7_make_string(tsc, len);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);

   str = TR7_CONTENT_STRING(res);
   for (len = 0, idx = subd.indexes[0] ; idx < subd.indexes[1] ; idx++) {
      car = subd.items[idx];
      len += char_to_utf8(TR7_TO_CHAR(car), &str[len]);
   }
   return s_return_single(tsc, res);
}

/* implement 'vector-copy' */
static eval_status_t op_vector_copy(tr7_engine_t tsc)
{
   struct subvector_desc subd;
   tr7_t res;

   if (!get_subvector_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   res = tr7_make_vector_copy(tsc, &subd.items[subd.indexes[0]], (subd.indexes[1] - subd.indexes[0]));
   return s_return_single_alloc(tsc, res);
}

/* implement 'vector-copy!' */
static eval_status_t op_vector_copy_to(tr7_engine_t tsc)
{
   struct subvector_desc subd;
   tr7_vector_t vecto;
   tr7_t args, to;
   tr7_int_t at;

   /* get destination data */
   args = tsc->args;
   to = TR7_CAR(args);
   vecto = TR7_TO_VECTOR(to);
   args = TR7_CDR(args);
   at = tr7_to_int(TR7_CAR(args));

   /* get source data */
   if (!get_subvector_desc(TR7_CDR(args), &subd)
     || at < 0
     || at + subd.indexes[1] - subd.indexes[0] > (tr7_int_t)TR7_VECTOR_LENGTH(vecto))
      return error_out_of_bound(tsc);

   memmove(&vecto->items[at], &subd.items[subd.indexes[0]], (subd.indexes[1] - subd.indexes[0]) * sizeof *vecto->items);

   return s_return_single(tsc, to);
}

/* implement 'vector-fill!' */
static eval_status_t op_vector_fill(tr7_engine_t tsc)
{
   struct subvector_desc subd;
   tr7_t args, vec, fill;

   args = tsc->args;
   vec = TR7_CAR(args);
   args = TR7_CDR(args);
   fill = TR7_CAR(args);
   if (!make_subvector_desc(TR7_CDR(args), &subd, vec))
      return error_out_of_bound(tsc);

   while (subd.indexes[0] < subd.indexes[1])
      subd.items[subd.indexes[0]++] = fill;

   return s_return_single(tsc, vec);
}

/* implement 'vector-append' */
static eval_status_t op_vector_append(tr7_engine_t tsc)
{
   tr7_t *to, *from;
   tr7_t args, res;
   tr7_int_t len;

   /* compute needed length for new vector */
   len = 0;
   for (len = 0, args = tsc->args; TR7_IS_PAIR(args); args = TR7_CDR(args))
      len += TR7_LENGTH_VECTOR(TR7_CAR(args));

   /* allocate */
   res = tr7_make_vector(tsc, len);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);

   /* store the contents of the argument vectors into the new vector */
   to = TR7_ITEMS_VECTOR(res);
   for (args = tsc->args; TR7_IS_PAIR(args); args = TR7_CDR(args)) {
      from = TR7_ITEMS_VECTOR(TR7_CAR(args));
      len = TR7_LENGTH_VECTOR(TR7_CAR(args));
      memcpy(to, from, len * sizeof *to);
      to += len;
   }
   return s_return_single(tsc, res);
}

/* implement 'string->vector' */
static eval_status_t op_string_to_vector(tr7_engine_t tsc)
{
   wint_t car;
   struct substring_desc subd;
   tr7_t res, *items;

   if (!get_substring_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   /* allocate */
   res = tr7_make_vector(tsc, subd.indexes[1] - subd.indexes[0]);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);

   items = TR7_ITEMS_VECTOR(res);
   while(subd.offsets[0] < subd.offsets[1]) {
      subd.offsets[0] += utf8_to_char((uint8_t*)&subd.string[subd.offsets[0]], &car);
      *items++ = TR7_FROM_CHAR(car);
   }

   return s_return_single(tsc, res);
}

/*************************************************************************
* SECTION OP(BYTEVECTOR)
* ---------------------
*/
/* implement 'bytevector?' */
static eval_status_t op_is_bytevector(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_BYTEVECTOR(TR7_CAR(tsc->args)));
}

/* implement 'bytevector-length' */
static eval_status_t op_bytevector_length(tr7_engine_t tsc)
{
   tr7_buffer_t bv = TR7_TO_BYTEVECTOR(TR7_CAR(tsc->args));
   tr7_uint_t length = TR7_BUFFER_LENGTH(bv);
   return s_return_single(tsc, TR7_FROM_INT(length));
}
/*
* implement 'make-bytevector'
*/
static eval_status_t op_make_bytevector(tr7_engine_t tsc)
{
   int i;
   tr7_t vec, args = tsc->args;
   tr7_uint_t len = (tr7_uint_t)tr7_to_int(TR7_CAR(args));
   args = TR7_CDR(args);
   i = TR7_IS_PAIR(args) ? (int)tr7_to_int(TR7_CAR(args)) : 0;
   vec = tr7_make_bytevector_fill(tsc, (char) i, len);
   return s_return_single_alloc(tsc, vec);
}
/*
*/
static tr7_t list_to_bytevector(tr7_engine_t tsc, tr7_t list)
{
   tr7_int_t ival;
   tr7_buffer_t bv;
   tr7_t res = TR7_NIL, item;
   int i, listlen = tr7_list_length(list);
   if (listlen >= 0) {
      res = tr7_make_bytevector(tsc, listlen);
      if (!TR7_IS_NIL(res)) {
         bv = TR7_TO_BYTEVECTOR(res);
         for (i = 0; TR7_IS_PAIR(list); list = TR7_CDR(list)) {
            item = TR7_CAR(list);
            ival = TR7_IS_INT(item) ? TR7_TO_INT(item) : -1;
            if (ival < 0 || ival > 255) {
               res = TR7_NIL;
               break;
            }
            bv->content[i++] = (char)ival;
         }
      }
   }
   return res;
}
/*
* implement 'bytevector'
*/
static eval_status_t op_bytevector(tr7_engine_t tsc)
{
   tr7_t bv = list_to_bytevector(tsc, tsc->args);
   if (!TR7_IS_NIL(bv))
      return s_return_single(tsc, bv);
   if (tr7_list_length(tsc->args) < 0)
      return Error_1(tsc, "not a proper list:", tsc->args);
   return Error_1(tsc, "wrong byte value", tsc->args); /* TODO improve error reporting */
}
/*
* implement 'bytevector-u8-ref'
*/
static eval_status_t op_bytevector_u8_ref(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_buffer_t bv = TR7_TO_BYTEVECTOR(TR7_CAR(args));
   tr7_uint_t idx = (tr7_uint_t)tr7_to_int(TR7_CADR(args));
   if (idx >= TR7_BYTEVECTOR_LENGTH(bv))
      return error_out_of_bound(tsc);
   return s_return_single(tsc, TR7_FROM_INT(bv->content[idx]));
}

/* implement 'bytevector-u8-set!' */
static eval_status_t op_bytevector_u8_set(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t vec = TR7_CAR(args);
   tr7_buffer_t bv = TR7_TO_BYTEVECTOR(vec);
   args = TR7_CDR(args);
   tr7_uint_t idx = (tr7_uint_t)tr7_to_int(TR7_CAR(args));
   if (tr7_is_immutable(vec))
      return error_immutable(tsc);
   if (idx >= TR7_BYTEVECTOR_LENGTH(bv))
      return error_out_of_bound(tsc);
   bv->content[idx] = (char)tr7_to_int(TR7_CADR(args));
   return s_return_single(tsc, vec);
}

struct subbytevector_desc {
   uint8_t *content;
   tr7_uint_t length;
   tr7_int_t indexes[2];
};

static int make_subbytevector_desc(tr7_t args, struct subbytevector_desc *subd, tr7_t bytevector)
{
   int n;
   tr7_buffer_t bv = TR7_TO_BYTEVECTOR(bytevector);

   subd->content = bv->content;
   subd->length = TR7_BYTEVECTOR_LENGTH(bv);

   /* get bounds */
   n = get_start_end(args, subd->indexes);
   if (n < 0)
      return 0;
   if (n == 0)
      subd->indexes[0] = 0;
   else if (subd->indexes[0] < 0 || (tr7_uint_t)subd->indexes[0] > subd->length)
      return 0;
   if (n <= 1)
      subd->indexes[1] = (tr7_int_t)subd->length;
   else if (subd->indexes[1] < subd->indexes[0] || (tr7_uint_t)subd->indexes[1] > subd->length)
      return 0;
   return 1;
}

static int get_subbytevector_desc(tr7_t args, struct subbytevector_desc *subd)
{
   return make_subbytevector_desc(TR7_CDR(args), subd, TR7_CAR(args));
}

/* implement 'bytevector-copy' */
static eval_status_t op_bytevector_copy(tr7_engine_t tsc)
{
   struct subbytevector_desc subd;
   tr7_t res;

   if (!get_subbytevector_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   res = tr7_make_bytevector_copy(tsc, (uint8_t*)&subd.content[subd.indexes[0]], subd.indexes[1] - subd.indexes[0]);
   return s_return_single_alloc(tsc, res);
}

/* implement 'bytevector-copy!' */
static eval_status_t op_bytevector_copy_to(tr7_engine_t tsc)
{
   struct subbytevector_desc subd;
   tr7_buffer_t vecto;
   tr7_t args, to;
   tr7_int_t at;

   /* get destination data */
   args = tsc->args;
   to = TR7_CAR(args);
   vecto = TR7_TO_BYTEVECTOR(to);
   args = TR7_CDR(args);
   at = tr7_to_int(TR7_CAR(args));

   /* get source data */
   if (!get_subbytevector_desc(TR7_CDR(args), &subd)
     || at < 0
     || at + subd.indexes[1] - subd.indexes[0] > (tr7_int_t)TR7_BYTEVECTOR_LENGTH(vecto))
      return error_out_of_bound(tsc);

   memmove(&vecto->content[at], &subd.content[subd.indexes[0]], (subd.indexes[1] - subd.indexes[0]) * sizeof(char));

   return s_return_single(tsc, to);
}

/* implement 'bytevector-fill!' */
static eval_status_t op_bytevector_fill(tr7_engine_t tsc)
{
   struct subbytevector_desc subd;
   tr7_t args, vec;
   char fill;

   args = tsc->args;
   vec = TR7_CAR(args);
   args = TR7_CDR(args);
   fill = (char)tr7_to_int(TR7_CAR(args));
   if (!make_subbytevector_desc(TR7_CDR(args), &subd, vec))
      return error_out_of_bound(tsc);

   memset(&subd.content[subd.indexes[0]], fill, subd.indexes[1] - subd.indexes[0]);

   return s_return_single(tsc, vec);
}

/* implement 'bytevector-append' */
static eval_status_t op_bytevector_append(tr7_engine_t tsc)
{
   tr7_buffer_t vec, from;
   tr7_t args, res;
   tr7_int_t pos, len;

   /* compute needed length for new bytevector */
   len = 0;
   for (len = 0, args = tsc->args; TR7_IS_PAIR(args); args = TR7_CDR(args))
      len += TR7_LENGTH_BYTEVECTOR(TR7_CAR(args));

   /* allocate */
   res = tr7_make_bytevector(tsc, len);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);
   vec = TR7_TO_BYTEVECTOR(res);

   /* store the contents of the argument bytevectors into the new bytevector */
   for (pos = 0, args = tsc->args; TR7_IS_PAIR(args); args = TR7_CDR(args)) {
      from = TR7_TO_BYTEVECTOR(TR7_CAR(args));
      len = TR7_BYTEVECTOR_LENGTH(from);
      memcpy(&vec->content[pos], from->content, len * sizeof(char));
      pos += len;
   }
   return s_return_single(tsc, res);
}


/* implement 'utf8->string' */
static eval_status_t op_utf8_to_string(tr7_engine_t tsc)
{
   struct subbytevector_desc subd;
   tr7_t res;

   if (!get_subbytevector_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   if (!utf8str_is_valid((uint8_t*)&subd.content[subd.indexes[0]], subd.indexes[1] - subd.indexes[0]))
      return error_invalid_argument(tsc);

   res = tr7_make_string_copy_length(tsc, (char*)&subd.content[subd.indexes[0]], subd.indexes[1] - subd.indexes[0]);
   return s_return_single_alloc(tsc, res);
}

/* implement 'string->utf8' */
static eval_status_t op_string_to_utf8(tr7_engine_t tsc)
{
   tr7_t res;
   struct substring_desc subd;

   if (!get_substring_desc(tsc->args, &subd))
      return error_out_of_bound(tsc);

   res = tr7_make_bytevector_copy(tsc, (uint8_t*)&subd.string[subd.offsets[0]], subd.offsets[1] - subd.offsets[0]);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);

   return s_return_single(tsc, res);
}

/* implement 'procedure?' */
static eval_status_t op_procedure_p(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_proc(TR7_CAR(tsc->args)));
}

/* implement 'apply' */
static eval_status_t op_apply(tr7_engine_t tsc)
{
   return s_exec(tsc, TR7_CAR(tsc->args), list_star(tsc, TR7_CDR(tsc->args)));
}

/* implement 'call-with-current-continuation' */
static eval_status_t op_callcc(tr7_engine_t tsc)
{
   return s_exec_1(tsc, TR7_CAR(tsc->args), mk_continuation(tsc, tsc->dump));
}

/* implement 'values' */
static eval_status_t op_values(tr7_engine_t tsc)
{
   return s_return_values(tsc, tsc->args);
}

/* implement 'call-with-values' (main) */
static eval_status_t op_callvals(tr7_engine_t tsc)
{
   s_save(tsc, OP(CALLVALS_THEN), TR7_CADR(tsc->args));
   return s_exec_0(tsc, TR7_CAR(tsc->args));
}

/* implement 'call-with-values' (then) */
static eval_status_t op_callvals_then(tr7_engine_t tsc)
{
   return s_exec(tsc, tsc->args, get_values(tsc));
}

/* implementation of 'map' */
static int map_aux(tr7_engine_t tsc, tr7_t it)
{
   /* 2 loops to ensure GC resilience */
   tr7_t savit = it;
   tr7_t list = TR7_NIL;
   for ( ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      tr7_t x = TR7_CAR(it);
      if (!TR7_IS_PAIR(x))
         return s_return_single(tsc, tr7_reverse_in_place(TR7_CAR(tsc->args), TR7_NIL));
      list = tr7_cons(tsc, TR7_CAR(x), list);
   }
   for (it = savit ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      tr7_t x = TR7_CAR(it);
      TR7_CAR(it) = TR7_CDR(x);
   }
   s_save(tsc, OP(MAP_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_map(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   do {
      ml = tr7_cons(tsc, TR7_CAR(args), ml);
      args = TR7_CDR(args);
   } while(TR7_IS_PAIR(args));
   tsc->args = tr7_cons(tsc, TR7_NIL, ml);
   return map_aux(tsc, ml);
}

static eval_status_t op_map_then(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   TR7_CAR(args) = tr7_cons(tsc, tsc->value, TR7_CAR(args));
   return map_aux(tsc, TR7_CDR(args));
}

/* implementation of 'string-map' */
static int strmap_aux(tr7_engine_t tsc, tr7_int_t idx, tr7_t it)
{
   tr7_t str, list = TR7_NIL;

   if (idx >= TR7_TO_INT(TR7_CAR(it)))
      return s_return_single(tsc, TR7_CAR(tsc->args));

   for (it = TR7_CDR(it) ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      str = TR7_CAR(it);
      list = tr7_cons(tsc, TR7_FROM_CHAR(TR7_CONTENT_STRING(str)[idx]), list);
   }
   s_save(tsc, OP(STRMAP_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_strmap(tr7_engine_t tsc)
{
   tr7_int_t minlen, len;
   tr7_t x, str, ml, args = tsc->args;
   ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   minlen = -1;
   do {
      x = TR7_CAR(args);
      args = TR7_CDR(args);
      ml = tr7_cons(tsc, x, ml);
      len = TR7_LENGTH_STRING(x);
      if (minlen < 0 || len < minlen)
         minlen = len;
   } while(TR7_IS_PAIR(args));
   ml = tr7_cons(tsc, TR7_FROM_INT(minlen), ml);
   str = tr7_make_string(tsc, minlen);
   tsc->args = TR7_CONS3(tsc, str, TR7_FROM_INT(0), ml);
   return strmap_aux(tsc, 0, ml);
}

static eval_status_t op_strmap_then(tr7_engine_t tsc)
{
   tr7_int_t idx;
   tr7_t args = tsc->args;
   tr7_t str = TR7_CAR(args);
   args = TR7_CDR(args);
   idx = TR7_TO_INT(TR7_CAR(args));
   if (!TR7_IS_CHAR(tsc->value))
      return Error_0(tsc, "character expected as result of string-map proc");
   TR7_CONTENT_STRING(str)[idx++] = (char)TR7_TO_CHAR(tsc->value);
   TR7_CAR(args) = TR7_FROM_INT(idx);
   return strmap_aux(tsc, idx, TR7_CDR(args));
}

/* implementation of 'vector-map' */
static int vecmap_aux(tr7_engine_t tsc, tr7_int_t idx, tr7_t it)
{
   tr7_t vec, list = TR7_NIL;

   if (idx >= TR7_TO_INT(TR7_CAR(it)))
      return s_return_single(tsc, TR7_CAR(tsc->args));

   for (it = TR7_CDR(it) ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      vec = TR7_CAR(it);
      list = tr7_cons(tsc, TR7_ITEM_VECTOR(vec, idx), list);
   }
   s_save(tsc, OP(VECMAP_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_vecmap(tr7_engine_t tsc)
{
   tr7_int_t minlen, len;
   tr7_t x, vec, args = tsc->args;
   tr7_t ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   minlen = -1;
   do {
      x = TR7_CAR(args);
      args = TR7_CDR(args);
      ml = tr7_cons(tsc, x, ml);
      len = TR7_LENGTH_VECTOR(x);
      if (minlen < 0 || len < minlen)
         minlen = len;
   } while(TR7_IS_PAIR(args));
   ml = tr7_cons(tsc, TR7_FROM_INT(minlen), ml);
   vec = tr7_make_vector_fill(tsc, TR7_VOID, minlen);
   tsc->args = TR7_CONS3(tsc, vec, TR7_FROM_INT(0), ml);
   return vecmap_aux(tsc, 0, ml);
}

static eval_status_t op_vecmap_then(tr7_engine_t tsc)
{
   tr7_int_t idx;
   tr7_t args = tsc->args;
   tr7_t vec = TR7_CAR(args);
   args = TR7_CDR(args);
   idx = TR7_TO_INT(TR7_CAR(args));
   TR7_ITEM_VECTOR(vec, idx++) = tsc->value;
   TR7_CAR(args) = TR7_FROM_INT(idx);
   return vecmap_aux(tsc, idx, TR7_CDR(args));
}

/* implementation of 'for-each' */
static eval_status_t op_foreach_then(tr7_engine_t tsc)
{
   tr7_t it = tsc->args;
   tr7_t list = TR7_NIL;
   for ( ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      tr7_t x = TR7_CAR(it);
      if (!TR7_IS_PAIR(x))
         return s_return(tsc);
      TR7_CAR(it) = TR7_CDR(x);
      list = tr7_cons(tsc, TR7_CAR(x), list);
   }
   s_save(tsc, OP(FOREACH_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_foreach(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   do {
      ml = tr7_cons(tsc, TR7_CAR(args), ml);
      args = TR7_CDR(args);
   } while(TR7_IS_PAIR(args));
   tsc->args = ml;
   return op_foreach_then(tsc);
}

/* implementation of 'string-foreach' */
static int strforeach_aux(tr7_engine_t tsc, tr7_int_t idx, tr7_t it)
{
   tr7_t str, list = TR7_NIL;

   for ( ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      str = TR7_CAR(it);
      if (idx >= (tr7_int_t)TR7_LENGTH_STRING(str))
         return s_return(tsc);
      list = tr7_cons(tsc, TR7_FROM_CHAR(TR7_CONTENT_STRING(str)[idx]), list);
   }
   s_save(tsc, OP(STRFOREACH_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_strforeach(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   do {
      ml = tr7_cons(tsc, TR7_CAR(args), ml);
      args = TR7_CDR(args);
   } while(TR7_IS_PAIR(args));
   tsc->args = tr7_cons(tsc, TR7_FROM_INT(0), ml);
   return strforeach_aux(tsc, 0, ml);
}

static eval_status_t op_strforeach_then(tr7_engine_t tsc)
{
   tr7_int_t idx;
   tr7_t args = tsc->args;
   idx = TR7_TO_INT(TR7_CAR(args)) + 1;
   TR7_CAR(args) = TR7_FROM_INT(idx);
   return strforeach_aux(tsc, idx, TR7_CDR(args));
}

/* implementation of 'vector-foreach' */
static int vecforeach_aux(tr7_engine_t tsc, tr7_int_t idx, tr7_t it)
{
   tr7_t vec, list = TR7_NIL;

   for ( ; TR7_IS_PAIR(it) ; it = TR7_CDR(it)) {
      vec = TR7_CAR(it);
      if (idx >= (tr7_int_t)TR7_LENGTH_VECTOR(vec))
         return s_return(tsc);
      list = tr7_cons(tsc, TR7_ITEM_VECTOR(vec, idx), list);
   }
   s_save(tsc, OP(VECFOREACH_THEN), tsc->args);
   return s_exec(tsc, it, list);
}

static eval_status_t op_vecforeach(tr7_engine_t tsc)
{
   tr7_t args = tsc->args;
   tr7_t ml = TR7_CAR(args);
   args = TR7_CDR(args);
   if (TR7_IS_NIL(args))
      return s_exec_0(tsc, TR7_CAR(args));
   do {
      ml = tr7_cons(tsc, TR7_CAR(args), ml);
      args = TR7_CDR(args);
   } while(TR7_IS_PAIR(args));
   tsc->args = tr7_cons(tsc, TR7_FROM_INT(0), ml);
   return vecforeach_aux(tsc, 0, ml);
}

static eval_status_t op_vecforeach_then(tr7_engine_t tsc)
{
   tr7_int_t idx;
   tr7_t args = tsc->args;
   idx = TR7_TO_INT(TR7_CAR(args)) + 1;
   TR7_CAR(args) = TR7_FROM_INT(idx);
   return vecforeach_aux(tsc, idx, TR7_CDR(args));
}

/* helper for calling with a port */
static eval_status_t do_call_with_port(tr7_engine_t tsc, tr7_t port, tr7_t proc)
{
   tr7_t args = TR7_LIST1(tsc, port);
   s_save(tsc, OP(CLOSE_PORT), args);
   return s_exec(tsc, proc, args);
}

/* helper for opening file */
static tr7_t do_open_file_arg(tr7_engine_t tsc, int prop)
{
   return port_from_filename(tsc, (char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args)), prop);
}

/* implementation of 'call-with-port' */
static eval_status_t op_call_with_port(tr7_engine_t tsc)
{
   return do_call_with_port(tsc, TR7_CAR(tsc->args), TR7_CADR(tsc->args));
}

/* helper for calling with a port */
static eval_status_t do_with_port(tr7_engine_t tsc, tr7_t port, int idx, tr7_t thunk)
{
   if (TR7_IS_FALSE(port))
      return file_error_1(tsc, "can't open", TR7_CAR(tsc->args));
   s_save_arg1(tsc, OP(CLOSE_PORT), port);
   push_stdport(tsc, port, idx);
   return s_exec_0(tsc, thunk);
}

/* implementation of 'input-port?' */
static eval_status_t op_is_input_port(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_input_port(TR7_CAR(tsc->args)));
}

/* implementation of 'output-port?' */
static eval_status_t op_is_output_port(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_output_port(TR7_CAR(tsc->args)));
}

/* implementation of 'textual-port?' */
static eval_status_t op_is_textual_port(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_textual_port(TR7_CAR(tsc->args)));
}

/* implementation of 'binary-port?' */
static eval_status_t op_is_binary_port(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_is_binary_port(TR7_CAR(tsc->args)));
}

/* implementation of 'port?' */
static eval_status_t op_is_port(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_PORT(TR7_CAR(tsc->args)));
}

/* implementation of 'close-port' */
static eval_status_t op_close_port(tr7_engine_t tsc)
{
   port_close(tsc, TR7_CAR(tsc->args), port_input|port_output);
   return s_return(tsc);
}

/* implementation of 'close-input-port' */
static eval_status_t op_close_input_port(tr7_engine_t tsc)
{
   port_close(tsc, TR7_CAR(tsc->args), port_input);
   return s_return(tsc);
}

/* implementation of 'close-output-port' */
static eval_status_t op_close_output_port(tr7_engine_t tsc)
{
   port_close(tsc, TR7_CAR(tsc->args), port_output);
   return s_return(tsc);
}

/* implementation of 'open-input-string' */
static eval_status_t op_open_input_string(tr7_engine_t tsc)
{
   tr7_t string = TR7_CAR(tsc->args);
   tr7_t p = port_from_string(tsc, string, TR7_CONTENT_STRING(string), NULL);
   return s_return_single(tsc, p);
}

/* implementation of 'open-output-string' */
static eval_status_t op_open_output_string(tr7_engine_t tsc)
{
   return s_return_single(tsc, port_from_scratch(tsc, port_string));
}

/* implementation of 'get-output-string' */
static eval_status_t op_get_output_string(tr7_engine_t tsc)
{
   return s_return_single(tsc, port_get_string(tsc, TR7_CAR(tsc->args)));
}

/* implementation of 'open-input-bytevector' */
static eval_status_t op_open_input_bytevector(tr7_engine_t tsc)
{
   tr7_t bytevector = TR7_CAR(tsc->args);
   tr7_t p = port_from_bytevector(tsc, bytevector);
   return s_return_single(tsc, p);
}

/* implementation of 'open-output-bytevector' */
static eval_status_t op_open_output_bytevector(tr7_engine_t tsc)
{
   return s_return_single(tsc, port_from_scratch(tsc, port_bytevector | port_binary));
}

/* implementation of 'get-output-bytevector' */
static eval_status_t op_get_output_bytevector(tr7_engine_t tsc)
{
   return s_return_single(tsc, port_get_bytevector(tsc, TR7_CAR(tsc->args)));
}

/* helper for opening file */
static eval_status_t do_open_file(tr7_engine_t tsc, int prop)
{
   tr7_t p = do_open_file_arg(tsc, prop);
   if (TR7_IS_FALSE(p))
      return file_error_1(tsc, "can't open", TR7_CAR(tsc->args));
   return s_return_single(tsc, p);
}

static port_t *get_optional_port(tr7_engine_t tsc, tr7_t args, int index)
{
   tr7_t port = TR7_IS_PAIR(args) ? TR7_CAR(args) : get_stdport(tsc, index);
   return TR7__PORT__PORT(port);
}

static port_t *get_optional_inport(tr7_engine_t tsc, tr7_t args)
{
   return get_optional_port(tsc, args, IDX_STDIN);
}

static port_t *get_optional_outport(tr7_engine_t tsc, tr7_t args)
{
   return get_optional_port(tsc, args, IDX_STDOUT);
}

/* implementation of 'read-char' */
static eval_status_t op_read_char(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   wint_t car = port_read_char(tsc, pt);
   if (car == WEOF)
      return s_return_EOF(tsc);
   return s_return_single(tsc, TR7_FROM_CHAR(car));
}

/* implementation of 'peek-char' */
static eval_status_t op_peek_char(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   wint_t car = port_read_char(tsc, pt);
   if (car == WEOF)
      return s_return_EOF(tsc);
   port_unread_char(tsc, pt, car);
   return s_return_single(tsc, TR7_FROM_CHAR(car));
}

/* implementation of 'read-line' */
static eval_status_t op_read_line(tr7_engine_t tsc)
{
   tr7_t res;
   wint_t car;
   int cr = 0;
   port_t *pt = get_optional_inport(tsc, tsc->args);

   strbuff_start(tsc);
   for(;;) {
      car = port_read_char(tsc, pt);
      if (car == WEOF && strbuff_length(tsc) == 0)
         return s_return_EOF(tsc);
      if (car == WEOF || car == '\n') {
         if (!strbuff_stop(tsc))
            break;
         res = strbuff_string(tsc);
         return s_return_single_alloc(tsc, res);
      }
      if (cr && !strbuff_add(tsc, '\r'))
         break;
      if (car == '\r')
         cr = 1;
      else if (!strbuff_add(tsc, car))
         break;
   }
   return file_error_1(tsc, "line too long", TR7_NIL);
}

/* implementation of 'eof-object?' */
static eval_status_t op_is_eof_object(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_EOF(TR7_CAR(tsc->args)));
}

/* implementation of 'eof-object' */
static eval_status_t op_eof_object(tr7_engine_t tsc)
{
   return s_return_EOF(tsc);
}

/* implementation of 'char-ready?' */
static eval_status_t op_is_char_ready(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   int ready = port_has_char(tsc, pt);
   return s_return_boolean(tsc, ready);
}

/* implementation of 'read-string' */
static eval_status_t op_read_string(tr7_engine_t tsc)
{
   tr7_t res;
   wint_t car;
   tr7_int_t idx, len = tr7_to_int(TR7_CAR(tsc->args));
   port_t *pt = get_optional_inport(tsc, TR7_CDR(tsc->args));
   strbuff_start(tsc);
   for (idx = 0 ; idx < len ; idx++) {
      car = port_read_char(tsc, pt);
      if (car == WEOF)
         break;
      if (!strbuff_add(tsc, car))
         return error_out_of_memory(tsc);
   }
   res = strbuff_string(tsc);
   return s_return_single_alloc(tsc, res);
}

/* implementation of 'read-u8' */
static eval_status_t op_read_u8(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   int byte = port_read_byte(tsc, pt);
   if (byte == EOF)
      return s_return_EOF(tsc);
   return s_return_single(tsc, TR7_FROM_INT(byte));
}

/* implementation of 'peek-u8' */
static eval_status_t op_peek_u8(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   int byte = port_read_byte(tsc, pt);
   if (byte == EOF)
      return s_return_EOF(tsc);
   port_unread_byte(tsc, pt, byte);
   return s_return_single(tsc, TR7_FROM_INT(byte));
}

/* implementation of 'u8-ready?' */
static eval_status_t op_is_u8_ready(tr7_engine_t tsc)
{
   port_t *pt = get_optional_inport(tsc, tsc->args);
   int ready = port_has_byte(tsc, pt);
   return s_return_boolean(tsc, ready);
}

/* implementation of 'read-bytevector' */
static eval_status_t op_read_bytevector(tr7_engine_t tsc)
{
   tr7_t res;
   uint8_t *content;
   tr7_int_t nread, len = tr7_to_int(TR7_CAR(tsc->args));
   port_t *pt = get_optional_inport(tsc, TR7_CDR(tsc->args));
   res = tr7_make_bytevector(tsc, len);
   if (TR7_IS_NIL(res))
      return error_out_of_memory(tsc);
   content = TR7_CONTENT_BYTEVECTOR(res);
   nread = port_read_bytes(tsc, pt, content, len);
   if (nread == EOF)
      return s_return_EOF(tsc);
   if (nread < len)
      TR7_SET_LENGTH_BYTEVECTOR(res, nread);
   return s_return_single(tsc, res);
}

/* implementation of 'read-bytevector!' */
static eval_status_t op_read_bytevector_to(tr7_engine_t tsc)
{
   port_t *pt;
   struct subbytevector_desc subd;
   tr7_t args, bv;
   tr7_int_t nread;

   args = tsc->args;
   bv = TR7_CAR(args);
   args = TR7_CDR(args);
   pt = get_optional_inport(tsc, args);
   if (!TR7_IS_NIL(args))
      args = TR7_CDR(args);
   if (!make_subbytevector_desc(args, &subd, bv))
      return error_out_of_bound(tsc);

   nread = port_read_bytes(tsc, pt, &subd.content[subd.indexes[0]], subd.indexes[1] - subd.indexes[0]);
   if (nread == EOF)
      return s_return_EOF(tsc);
   return s_return_single(tsc, TR7_FROM_INT(nread));
}

/*************************************************************************
* SECTION SCHEME_WRITE
* --------------------
*/
#if USE_SCHEME_WRITE
static eval_status_t do_write(tr7_engine_t tsc, int pflags)
{
   tr7_t obj = TR7_CAR(tsc->args);
   port_t *pt = get_optional_outport(tsc, TR7_CDR(tsc->args));
   do_print(tsc, pt, pflags, obj);
   return s_return_void(tsc);
}

/* implementation of 'write' */
static eval_status_t op_write(tr7_engine_t tsc)
{
   return do_write(tsc, PRTFLG_LOOPS | PRTFLG_ESCAPE);
}

/* implementation of 'write-simple' */
static eval_status_t op_write_simple(tr7_engine_t tsc)
{
   return do_write(tsc, PRTFLG_ESCAPE);
}

/* implementation of 'write-shared' */
static eval_status_t op_write_shared(tr7_engine_t tsc)
{
   return do_write(tsc, PRTFLG_SHAREDS | PRTFLG_ESCAPE);
}

/* implementation of 'display' */
static eval_status_t op_display(tr7_engine_t tsc)
{
   return do_write(tsc, PRTFLG_LOOPS);
}
#endif

/* implementation of 'write-char' */
static eval_status_t op_write_char(tr7_engine_t tsc)
{
   wint_t car = TR7_TO_CHAR(TR7_CAR(tsc->args));
   port_t *pt = get_optional_outport(tsc, TR7_CDR(tsc->args));
   port_write_char(tsc, pt, car);
   return s_return_void(tsc);
}

/* implementation of 'newline' */
static eval_status_t op_write_newline(tr7_engine_t tsc)
{
   port_t *pt = get_optional_outport(tsc, tsc->args);
   port_write_char(tsc, pt, '\n');
   return s_return_void(tsc);
}

/* implementation of 'write-string' */
static eval_status_t op_write_string(tr7_engine_t tsc)
{
   struct substring_desc subd;
   tr7_t args = tsc->args;
   tr7_t str = TR7_CAR(args);
   port_t *pt;
   args = TR7_CDR(args);
   pt = get_optional_outport(tsc, args);
   if (!TR7_IS_NIL(args))
      args = TR7_CDR(args);
   if (!make_substring_desc(args, &subd, str))
      return error_out_of_bound(tsc);
   port_write_utf8_length(tsc, pt, (char*)&subd.string[subd.offsets[0]], subd.offsets[1] - subd.offsets[0]);
   return s_return_void(tsc);
}

/* implementation of 'write-u8' */
static eval_status_t op_write_u8(tr7_engine_t tsc)
{
   uint8_t byte = (uint8_t)TR7_TO_INT(TR7_CAR(tsc->args));
   port_t *pt = get_optional_outport(tsc, TR7_CDR(tsc->args));
   port_write_bytes(tsc, pt, &byte, 1);
   return s_return_void(tsc);
}

/* implementation of 'write-bytevector' */
static eval_status_t op_write_bytevector(tr7_engine_t tsc)
{
   struct subbytevector_desc subd;
   tr7_t args = tsc->args;
   tr7_t str = TR7_CAR(args);
   port_t *pt;
   args = TR7_CDR(args);
   pt = get_optional_outport(tsc, args);
   if (!TR7_IS_NIL(args))
      args = TR7_CDR(args);
   if (!make_subbytevector_desc(args, &subd, str))
      return error_out_of_bound(tsc);
   port_write_bytes(tsc, pt, &subd.content[subd.indexes[0]], subd.indexes[1] - subd.indexes[0]);
   return s_return_void(tsc);
}

/* implementation of 'flush-output-port' */
static eval_status_t op_flush_output_port(tr7_engine_t tsc)
{
   port_t *pt = get_optional_outport(tsc, tsc->args);
   port_flush(tsc, pt);
   return s_return_void(tsc);
}
/*
**************************************************************************
* SECTION SCHEME_PROCESS_CONTEXT
* ------------------------------
*
*
*/
#if USE_SCHEME_PROCESS_CONTEXT
extern char **environ;
static char **memo_argv = NULL;

void tr7_set_argv(char **argv)
{
   memo_argv = argv;
}

tr7_t tr7_command_line(tr7_engine_t tsc)
{
   tr7_t result, *prev, item;
   char **argv;

   result = TR7_NIL;
   argv = memo_argv;
   if (argv) {
      prev = &result;
      while (*argv) {
         item = tr7_make_string_static(tsc, *argv++);
         *prev = tr7_cons(tsc, item, TR7_NIL);
         prev = &TR7_CDR(*prev);
      }
   }
   return result;
}

/* implementation of 'command-line' */
static eval_status_t op_command_line(tr7_engine_t tsc)
{
   return s_return_single(tsc, tr7_command_line(tsc));
}

/* realization of exits */
static eval_status_t do_exit(tr7_engine_t tsc, int urge)
{
      int rc = EXIT_SUCCESS;
      if (TR7_IS_PAIR(tsc->args)) {
         tr7_t x = TR7_CAR(tsc->args);
         if (!TR7_IS_TRUE(x)) {
            if (TR7_IS_INT(x))
               rc = (int)TR7_TO_INT(x);
            else
               rc = EXIT_FAILURE;
         }
      }
      if (urge)
         _exit(rc);
      else {
         /* TODO unwind afters */
         exit(rc);
      }
      return Error_0(tsc, "exit failed?");
}

/* implementation of 'exit' */
static eval_status_t op_exit(tr7_engine_t tsc)
{
   return do_exit(tsc, 0);
}

/* implementation of 'emergency-exit' */
static eval_status_t op_emergency_exit(tr7_engine_t tsc)
{
   return do_exit(tsc, 1);
}

/* implementation of 'get-environment-variable' */
static eval_status_t op_get_env_var(tr7_engine_t tsc)
{
   /* TODO: avoid copy and make immutable */
   char *str = getenv((const char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args)));
   tr7_t val = str ? tr7_make_string_copy(tsc, str) : TR7_FALSE;
   return s_return_single(tsc, val);
}

/* implementation of 'get-environment-variables' */
static eval_status_t op_get_env_vars(tr7_engine_t tsc)
{
   /* TODO: avoid copy and make immutable */
   tr7_t tn, tv, res = TR7_NIL;
   char **it, *name, *val;
   for (it = environ; it && (name = *it) ; it++) {
      val = strchr(*it, '=');
      if (val == NULL) {
         tn = tr7_make_string_copy(tsc, *it);
         tv = tr7_make_string_copy(tsc, "");
      } else {
         tn = tr7_make_string_copy_length(tsc, *it, (unsigned)(val - *it));
         tv = tr7_make_string_copy(tsc, &val[1]);
      }
      res = tr7_cons(tsc, tr7_cons(tsc, tn, tv), res);
   }
   return s_return_single(tsc, res);
}
#else
void tr7_set_argv(char **argv) {}
tr7_t tr7_command_line(tr7_engine_t tsc) { return TR7_NIL; }
#endif
/*
**************************************************************************
* SECTION SCHEME_TIME
* -------------------
*
*
*/
#if USE_SCHEME_TIME

/* implement 'current-second' */
static eval_status_t op_current_second(tr7_engine_t tsc)
{
   return s_return_double(tsc, (double)time(NULL));
}

/* implement 'current-jiffy' */
static eval_status_t op_current_jiffy(tr7_engine_t tsc)
{
   static time_t base = 0;
   time_t t = time(NULL);
   if (base == 0)
      base = t;
   return s_return_integer(tsc, (int)(t - base));
}

/* implement 'jiffies-per-second' */
static eval_status_t op_jiffies_per_second(tr7_engine_t tsc)
{
   return s_return_integer(tsc, 1);
}
#endif

/*************************************************************************
* SECTION SCHEME_LOAD
* -------------------
*/
#if USE_SCHEME_LOAD
/* implement 'load' */
static eval_status_t op_load(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   tr7_t y = TR7_CAR(x);
   const char *base = (const char*)TR7_CONTENT_STRING(y);
   if (!load_enter_search_load(tsc, NULL, base, 0))
      return Error_1(tsc, "unable to open", y);
   x = TR7_CDR(x);
   tsc->envir = TR7_IS_PAIR(x) ? TR7_CAR(x) : tsc->global_env;
   return s_goto(tsc, OP(REPL_READ));
}
#endif

/*************************************************************************
* SECTION SCHEME_FILE
* -------------------
*/
#if USE_SCHEME_FILE
/* implement 'file-exists?' */
static eval_status_t op_file_exists(tr7_engine_t tsc)
{
   const char *filename = (const char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args));
   return s_return_boolean(tsc, access(filename, F_OK) == 0);
}

/* implement 'delete-file' */
static eval_status_t op_delete_file(tr7_engine_t tsc)
{
   const char *filename = (const char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args));
   if (unlink(filename) < 0) /* TODO: file-error */
      return Error_1(tsc, "can't delete", TR7_CAR(tsc->args));
   return s_return_true(tsc);
}

/* implementation of 'open-input-file' */
static eval_status_t op_open_input_file(tr7_engine_t tsc)
{
   return do_open_file(tsc, port_input);
}

/* implementation of 'open-binary-input-file' */
static eval_status_t op_open_binary_input_file(tr7_engine_t tsc)
{
   return do_open_file(tsc, port_input | port_binary);
}

/* implementation of 'open-output-file' */
static eval_status_t op_open_output_file(tr7_engine_t tsc)
{
   return do_open_file(tsc, port_output);
}

/* implementation of 'open-binary-output-file' */
static eval_status_t op_open_binary_output_file(tr7_engine_t tsc)
{
   return do_open_file(tsc, port_output | port_binary);
}

/* implementation of 'call-with-input-file' */
static eval_status_t op_call_with_input_file(tr7_engine_t tsc)
{
   return do_call_with_port(tsc, do_open_file_arg(tsc, port_input), TR7_CADR(tsc->args));
}

/* implementation of 'call-with-output-file' */
static eval_status_t op_call_with_output_file(tr7_engine_t tsc)
{
   return do_call_with_port(tsc, do_open_file_arg(tsc, port_output), TR7_CADR(tsc->args));
}

/* implementation of 'with-input-from-file' */
static eval_status_t op_with_input_file(tr7_engine_t tsc)
{
   return do_with_port(tsc, do_open_file_arg(tsc, port_input), IDX_STDIN, TR7_CADR(tsc->args));
}

/* implementation of 'with-output-to-file' */
static eval_status_t op_with_output_file(tr7_engine_t tsc)
{
   return do_with_port(tsc, do_open_file_arg(tsc, port_output), IDX_STDOUT, TR7_CADR(tsc->args));
}
#endif

/*================= load  library ====================*/

/* helper to implement 'include' and 'include-ci' */
static eval_status_t do_include(tr7_engine_t tsc, unsigned ci_loadflag, opidx_t op)
{
   tr7_t x = tsc->args;
   tr7_t y = TR7_CAR(x);
   const char *base = (const char*)TR7_CONTENT_STRING(y);
   x = TR7_CDR(x);
   if (!TR7_IS_NIL(x))
      s_save(tsc, op, x);
   if (tsc->loadflags & LOADF_SHOW_LOAD) {
      log_str(tsc, "Loading ");
      log_str(tsc, base);
      log_str(tsc, "\n");
   }
   if (!load_enter_search_include(tsc, base, ci_loadflag))
      return Error_1(tsc, "unable to open", y);
   return s_goto(tsc, OP(REPL_READ));
}

/* implement 'include' */
static eval_status_t op_include(tr7_engine_t tsc)
{
   return do_include(tsc, 0, OP(INCLUDE));
}

/* implement 'include-ci' */
static eval_status_t op_include_ci(tr7_engine_t tsc)
{
   return do_include(tsc, LOADF_FOLDCASE, OP(INCLUDE_CI));
}

/* push a boolean status of importing the library */
static eval_status_t op_import_check(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, tr7_asse_pair(tsc->args, tsc->libraries) != NULL);
}

/* push the environment of the library of name libname */
static eval_status_t op_import_get(tr7_engine_t tsc)
{
   tr7_t libname = tsc->args;
   tr7_pair_t pair = tr7_asse_pair(libname, tsc->libraries);
   if (NULL == pair)
      return Error_1(tsc, "import failed", libname);
   return s_return_single(tsc, pair->cdr);
}

/* push the environment of the library of name libname */
static int import_get_or_check(tr7_engine_t tsc, tr7_t libname, opidx_t op)
{
   char basename[BASENAME_SIZE + 1];
   const libdef_t *slib;
   tr7_pair_t pair;
   int sts;

   /* validate the name */
   if (!is_valid_libname(libname))
      return Error_1(tsc, "invalid import name", libname); /* invalid libname */

   /* check if already here */
   pair = tr7_asse_pair(libname, tsc->libraries);
   if (NULL != pair)
      return s_return_single(tsc, pair->cdr);

   /* convert libname to basename */
   sts = get_libname(libname, basename, (int)sizeof basename);
   if (sts > BASENAME_SIZE)
      return Error_1(tsc, "invalid import name", libname); /* invalid libname */

   /* search a standard library */
   basename[sts] = 0;
   slib = search_builtin_lib(basename);
   if (slib) {
      if (op == OP(IMPORT_CHECK))
         return s_return_true(tsc);
      tsc->envir = mk_environment(tsc, tsc->global_env, 1);
      add_builtin_lib(tsc, slib);
      tsc->libraries = tr7_cons(tsc, tr7_cons(tsc, libname, tsc->envir), tsc->libraries);
      return s_return_single(tsc, tsc->envir);
   }

#if USE_SCHEME_R5RS
   /* special (scheme r5rs) case */
   if (strcmp(basename, "scheme/r5rs")
    || !load_enter_string(tsc, (uint8_t*)deflib_r5rs, (uint8_t*)&deflib_r5rs[sizeof deflib_r5rs - 1], 0))
#endif
   /* load the file */
   if (!load_enter_search_import(tsc, basename, sts))
      return Error_1(tsc, "unable to import", libname);
   s_save(tsc, op, libname);
   tsc->envir = mk_environment(tsc, tsc->global_env, 1);
   return s_goto(tsc, OP(REPL_READ));
}

/* check import lists */
static eval_status_t check_import_args(tr7_engine_t tsc, tr7_t envir, tr7_t list, int rename)
{
   tr7_t iter, sym, value;

   for (iter = list ; TR7_IS_PAIR(iter) ; iter = TR7_CDR(iter)) {
      sym = TR7_CAR(iter);
      if (rename) {
         if (!TR7_IS_PAIR(sym) || !TR7_IS_PAIR(TR7_CDR(sym))
          || !TR7_IS_NIL(TR7_CDDR(sym)) || !TR7_IS_SYMBOL(TR7_CADR(sym)))
            return Error_1(tsc, "bad remname spec", sym);
         sym = TR7_CAR(sym);
      }
      if (!TR7_IS_SYMBOL(sym))
         return Error_1(tsc, "not a symbol", sym);
      if (!environment_get(envir, sym, &value))
         return Error_1(tsc, "symbol not found", sym);
   }
   if (!TR7_IS_NIL(iter))
         return Error_1(tsc, "improper import", list);
   return Cycle_Continue;
}

/* apply import-only */
static eval_status_t op_import_only(tr7_engine_t tsc)
{
   tr7_t sym, value;
   tr7_t env_out = mk_environment(tsc, TR7_NIL, 1);
   tr7_t env_in = tsc->value;
   tr7_t list = tsc->args;
   int sts = check_import_args(tsc, env_in, list, 0);
   if (sts != Cycle_Continue)
      return sts;
   while(TR7_IS_PAIR(list)) {
      sym = TR7_CAR(list);
      list = TR7_CDR(list);
      if (environment_get(env_in, sym, &value))
         environment_set(tsc, sym, value, env_out, 1);
   }
   return s_return_single(tsc, env_out);
}

/* apply import-except */
struct import_except_s {
   tr7_engine_t tsc;
   tr7_t envir;
   tr7_t list;
};
static int import_except_cb(void *closure, tr7_t sym, tr7_t val)
{
   struct import_except_s *s = (struct import_except_s*)closure;
   if (tr7_memq_pair(sym, s->list) == NULL)
      environment_set(s->tsc, sym, val, s->envir, 1);
   return 0;
}
static eval_status_t op_import_except(tr7_engine_t tsc)
{
   struct import_except_s s;
   int sts = check_import_args(tsc, tsc->value, tsc->args, 0);
   if (sts != Cycle_Continue)
      return sts;

   s.tsc = tsc;
   s.envir = mk_environment(tsc, TR7_NIL, 1);
   s.list = tsc->args;

   environment_enumerate(tsc->value, import_except_cb, &s);
   return s_return_single(tsc, s.envir);
}

/* apply import-prefix */
struct import_prefix_s {
   tr7_engine_t tsc;
   tr7_t envir;
   eval_status_t status;
   unsigned offset;
   char buffer[256];
};
static int import_prefix_cb(void *closure, tr7_t sym, tr7_t val)
{
   tr7_t nsym;
   struct import_prefix_s *s = (struct import_prefix_s*)closure;
   const char *post = tr7_symbol_string(sym);
   if (strlen(post) + s->offset >= sizeof s->buffer) {
      s->status = Error_1(s->tsc, "prefixed too long", sym);
      return 1;
   }
   strcpy(&s->buffer[s->offset], post);
   nsym = tr7_get_symbol(s->tsc, s->buffer, 1); /* TODO size known */
   environment_set(s->tsc, nsym, val, s->envir, 1);
   return 0;
}
static eval_status_t op_import_prefix(tr7_engine_t tsc)
{
   struct import_prefix_s s;
   const char *prestr;
   tr7_t pre;
   int sts;

   pre = tsc->args;
   if (!TR7_IS_PAIR(pre) || !TR7_IS_NIL(TR7_CDR(pre))
    || !TR7_IS_SYMBOL(TR7_CAR(pre)))
      return Error_1(tsc, "invalid prefix", pre);

   pre = TR7_CAR(pre);
   prestr = tr7_symbol_string(pre);

   s.tsc = tsc;
   s.envir = mk_environment(tsc, TR7_NIL, 1);
   s.status = Cycle_Continue;
   s.offset = (unsigned)strlen(prestr);
   if (s.offset >= sizeof s.buffer)
      return Error_1(tsc, "prefix too long", pre);
   strcpy(s.buffer, prestr);
   sts = environment_enumerate(tsc->value, import_prefix_cb, &s);
   return sts ? s.status : s_return_single(tsc, s.envir);
}

/* apply import-rename */
struct import_rename_s {
   tr7_engine_t tsc;
   tr7_t envir;
   tr7_t list;
};
static int import_rename_cb(void *closure, tr7_t sym, tr7_t val)
{
   struct import_rename_s *s = (struct import_rename_s*)closure;
   tr7_pair_t ren = tr7_assq_pair(sym, s->list);
   tr7_t name = ren == NULL ? sym : TR7_CAR(ren->cdr);
   environment_set(s->tsc, name, val, s->envir, 1);
   return 0;
}
static eval_status_t op_import_rename(tr7_engine_t tsc)
{
   struct import_rename_s s;
   int sts = check_import_args(tsc, tsc->value, tsc->args, 1);
   if (sts != Cycle_Continue)
      return sts;

   s.tsc = tsc;
   s.envir = mk_environment(tsc, TR7_NIL, 1);
   s.list = tsc->args;

   environment_enumerate(tsc->value, import_rename_cb, &s);
   return s_return_single(tsc, s.envir);
}

/* apply import */
struct import_add_s {
   tr7_engine_t tsc;
   tr7_t envir;
};
static int import_add_cb(void *closure, tr7_t sym, tr7_t val)
{
   struct import_rename_s *s = (struct import_rename_s*)closure;
   environment_set(s->tsc, sym, val, s->envir, 1);
   return 0;
}
static eval_status_t op_import_add(tr7_engine_t tsc)
{
   struct import_add_s s;

   s.tsc = tsc;
   s.envir = tsc->args;

   environment_enumerate(tsc->value, import_add_cb, &s);
   return s_return(tsc);
}

/* implement import of an import set */
static int import_importset(tr7_engine_t tsc, tr7_t set)
{
   tr7_t symbol, args, subset;
   opidx_t op;

   if (TR7_IS_PAIR(set)) {
      symbol = TR7_CAR(set);
      if (TR7EQ(symbol, SYMBOL(ONLY)))
         op = OP(IMPORT_ONLY);
      else if (TR7EQ(symbol, SYMBOL(EXCEPT)))
         op = OP(IMPORT_EXCEPT);
      else if (TR7EQ(symbol, SYMBOL(PREFIX)))
         op = OP(IMPORT_PREFIX);
      else if (TR7EQ(symbol, SYMBOL(RENAME)))
         op = OP(IMPORT_RENAME);
      else
         return import_get_or_check(tsc, set, OP(IMPORT_GET));
      args = TR7_CDR(set);
      if (TR7_IS_PAIR(args)) {
         subset = TR7_CAR(args);
         args = TR7_CDR(args);
         s_save(tsc, op, args);
         return import_importset(tsc, subset);
      }
   }
   return Error_1(tsc, "invalid import set", set);
}

/* implement 'import' */
static eval_status_t op_import(tr7_engine_t tsc)
{
   tr7_t x, set;

   x = tsc->args;
   if (!TR7_IS_PAIR(x))
      return Error_0(tsc, TR7_IS_NIL(x) ? "empty import set" : "bad import set");

   set = TR7_CAR(x);
   x = TR7_CDR(x);
   if (!TR7_IS_NIL(x))
      s_save(tsc, OP(IMPORT), x);

   s_save(tsc, OP(IMPORT_ADD), tsc->envir);
   return import_importset(tsc, set);
}

/* implement 'features' */
static eval_status_t op_features(tr7_engine_t tsc)
{
   return s_return_single(tsc, tsc->features);
}

static int invalid_cond_expand_cond(tr7_engine_t tsc, tr7_t cond)
{
   return Error_1(tsc, "invalid cond-expand condition", cond);
}

static int eval_cond_expand_args(tr7_engine_t tsc, tr7_t args, opidx_t op)
{
   tr7_t y = TR7_CDR(args);
   if (TR7_IS_PAIR(y))
      s_save(tsc, op, y);
   return eval_cond_expand_cond(tsc, TR7_CAR(args));
}

/* implement evaluation of 'cond-expand' condition */
static int eval_cond_expand_cond(tr7_engine_t tsc, tr7_t cond)
{
   tr7_t kw, args;

   /* if symbol, check in features list */
   if (TR7_IS_SYMBOL(cond))
      return s_return_boolean(tsc, tr7_memq_pair(cond, tsc->features) != NULL);

   /* otherwise, should be a list */
   if (!TR7_IS_PAIR(cond))
      return invalid_cond_expand_cond(tsc, cond);

   /* get keyword and arg list */
   kw = TR7_CAR(cond);
   args = TR7_CDR(cond);

   /* evaluation of (not feature-requirement) */
   if (kw == SYMBOL(NOT)) {
      if (!TR7_IS_PAIR(args))
         return invalid_cond_expand_cond(tsc, cond);
      s_save_nil(tsc, OP(COND_EXPAND_NOT));
      return eval_cond_expand_cond(tsc, TR7_CAR(args));
   }

   /* evaluation of (and feature-requirement...) */
   if (kw == SYMBOL(AND)) {
      tsc->value = TR7_TRUE;
      if (!TR7_IS_PAIR(args))
         return s_return(tsc);
      return eval_cond_expand_args(tsc, args, OP(COND_EXPAND_AND));
   }

   /* evaluation of (or feature-requirement...) */
   if (kw == SYMBOL(OR)) {
      tsc->value = TR7_FALSE;
      if (!TR7_IS_PAIR(args))
         return s_return(tsc);
      return eval_cond_expand_args(tsc, args, OP(COND_EXPAND_OR));
   }

   /* evaluation of (library ln) */
   if (kw == SYMBOL(LIBRARY)) {
      if (TR7_IS_PAIR(args))
         return import_get_or_check(tsc, TR7_CAR(args), OP(IMPORT_CHECK));
   }

   return invalid_cond_expand_cond(tsc, cond);
}

static eval_status_t op_cond_expand_not(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, !!TR7_IS_FALSE(tsc->value));
}

static eval_status_t op_cond_expand_and(tr7_engine_t tsc)
{
   if (TR7_IS_FALSE(tsc->value))
      return s_return(tsc);
   return eval_cond_expand_args(tsc, tsc->args, OP(COND_EXPAND_AND));
}

static eval_status_t op_cond_expand_or(tr7_engine_t tsc)
{
   if (!TR7_IS_FALSE(tsc->value))
      return s_return(tsc);
   return eval_cond_expand_args(tsc, tsc->args, OP(COND_EXPAND_OR));
}

/* implement evaluation of 'cond-expand' condition */
static int eval_cond_expand_cond_or_else(tr7_engine_t tsc, tr7_t cond)
{
   /* is else ? */
   if (TR7EQ(cond, SYMBOL(ELSE)))
      return s_return_true(tsc);
   return eval_cond_expand_cond(tsc, cond);
}

/* implement 'cond-expand' */
static int cond_expand_do(tr7_engine_t tsc, tr7_t next, opidx_t op)
{
   tr7_t y;

   if (!TR7_IS_PAIR(next))
      return s_return(tsc);

   y = TR7_CAR(next);
   if (!TR7_IS_PAIR(y))
      return Error_1(tsc, "invalid cond-expand clause", y);

   s_save(tsc, op, next);
   return eval_cond_expand_cond_or_else(tsc, TR7_CAR(y));
}

/* implement 'cond-expand' */
static eval_status_t op_cond_expand(tr7_engine_t tsc)
{
   return cond_expand_do(tsc, tsc->args, OP(COND_EXPAND_THEN));
}

/* implement 'cond-expand' after evaluation of cond */
static eval_status_t op_cond_expand_then(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   if (TR7_IS_FALSE(tsc->value))
      return cond_expand_do(tsc, TR7_CDR(x), OP(COND_EXPAND_THEN));
   return s_begin(tsc, TR7_CDAR(x));
}

/* helper for define library exports */
static eval_status_t op_define_library_export(tr7_engine_t tsc)
{
   tr7_t y, libexp, libdsc, argdcl, exports[3], value, it;
   int sts;

   libexp = tsc->args;
   libdsc = TR7_CAR(libexp);
   for (it = TR7_CDR(libexp) ; !TR7_IS_NIL(it) ; it = TR7_CDR(it)) {
      argdcl = TR7_CAR(it);
      for( ; TR7_IS_PAIR(argdcl) ; argdcl = TR7_CDR(argdcl)) {
         y = TR7_CAR(argdcl);
         if (TR7_IS_SYMBOL(y))
            exports[1] = exports[2] = y;
         else {
            sts = tr7_get_list_cars(y, 3, exports, NULL);
            if (sts != 3 || !TR7EQ(exports[0], SYMBOL(RENAME))
               || !TR7_IS_SYMBOL(exports[1]) ||   !TR7_IS_SYMBOL(exports[2]))
               return Error_1(tsc, "invalid export", y);
         }
         sts = environment_get(tsc->envir, exports[1], &value);
         if (!sts)
            return Error_1(tsc, "export not found", exports[1]);
         environment_set(tsc, exports[2], value, TR7_CDR(libdsc), 1);
      }
      if (!TR7_IS_NIL(argdcl))
         return Error_0(tsc, "invalid export");
   }
   return s_return_void(tsc);
}

/* implement 'cond-expand' after evaluation of cond */
static eval_status_t op_define_library_cond(tr7_engine_t tsc)
{
   tr7_t x = tsc->args;
   if (TR7_IS_FALSE(tsc->value))
      return cond_expand_do(tsc, TR7_CDR(x), OP(DEFLIB_COND));
   tsc->args = TR7_CDAR(x);
   return s_goto(tsc, OP(DEFLIB_INNER));
}

static void s_save_deflib(tr7_engine_t tsc, opidx_t op, tr7_t car, tr7_t cdr)
{
   if (!TR7_IS_NIL(cdr))
      s_save_cons(tsc, op, car, cdr);
}

/* helper for 'include-library-declarations' */
static eval_status_t do_define_library_include(tr7_engine_t tsc, tr7_t libexp, tr7_t args)
{
   int sts;
   tr7_t name;
   if (!TR7_IS_PAIR(args))
      return s_return(tsc);
   name = TR7_CAR(args);
   if (!TR7_IS_STRING(name))
      return Error_1(tsc, "string expected", name);
   if (!load_enter_search_include(tsc, (const char*)TR7_CONTENT_STRING(name), 0))
      return Error_1(tsc, "can't include", name);
   sts = do_read_all_with_datum(tsc, TR7__PORT__PORT(tsc->loadport));
   load_leave(tsc);
   if (sts < 0)
      return Error_1(tsc, "read error", name);
   s_save_deflib(tsc, OP(IMPORT_LIBDECL), libexp, TR7_CDR(args));
   return do_define_library_inner(tsc, libexp, tsc->value);
}

/* helper for 'include-library-declarations' */
static eval_status_t op_define_library_include(tr7_engine_t tsc)
{
   return do_define_library_include(tsc, TR7_CAR(tsc->args), TR7_CDR(tsc->args));
}

/* helper for define library */
static eval_status_t do_define_library_inner(tr7_engine_t tsc, tr7_t libexp, tr7_t cont)
{
   tr7_t decl, symbol, argdcl;

   while (TR7_IS_PAIR(cont)) {
      decl = TR7_CAR(cont);
      if (!TR7_IS_PAIR(decl))
         return Error_0(tsc, "bad library declaration");
      symbol = TR7_CAR(decl);
      cont = TR7_CDR(cont);
      argdcl = TR7_CDR(decl);
      /* export */
      if (TR7EQ(symbol, SYMBOL(EXPORT)))
         TR7_CDR(libexp) = tr7_cons(tsc, argdcl, TR7_CDR(libexp));
      /* cond-expand */
      else if (TR7EQ(symbol, SYMBOL(COND_EXPAND))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         return cond_expand_do(tsc, argdcl, OP(DEFLIB_COND));
      }
      /* import */
      else if (TR7EQ(symbol, SYMBOL(IMPORT))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         tsc->args = argdcl;
         return s_goto(tsc, OP(IMPORT));
      }
      /* begin */
      else if (TR7EQ(symbol, SYMBOL(BEGIN))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         return s_begin(tsc, argdcl);
      }
      /* include */
      else if (TR7EQ(symbol, SYMBOL(INCLUDE))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         tsc->args = argdcl;
         return op_include(tsc);
      }
      /* include-ci */
      else if (TR7EQ(symbol, SYMBOL(INCLUDE_CI))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         tsc->args = argdcl;
         return op_include_ci(tsc);
      }
      /* include-library-declaration */
      else if (TR7EQ(symbol, SYMBOL(INCLUDE_LIB_DECL))) {
         s_save_deflib(tsc, OP(DEFLIB_INNER), libexp, cont);
         return do_define_library_include(tsc, libexp, argdcl);
      }
      /* invalid then */
      else
         return Error_0(tsc, "unexpected library declaration");
   }
   if (!TR7_IS_NIL(cont))
      return Error_0(tsc, "invalid library declaration");
   return s_return_void(tsc);
}

/* helper for define library */
static eval_status_t op_define_library_inner(tr7_engine_t tsc)
{
   return do_define_library_inner(tsc, TR7_CAR(tsc->args), TR7_CDR(tsc->args));
}

/* implement 'define-library' */
static eval_status_t op_define_library(tr7_engine_t tsc)
{
   tr7_t name, libdsc, libexp, x = tsc->args;
   if (!TR7_IS_PAIR(x))
      return Error_0(tsc, "missing library name");
   name = TR7_CAR(x);
   if (!is_valid_libname(name))
      return Error_0(tsc, "bad library name");
   if (NULL != tr7_asse_pair(name, tsc->libraries))
      return Error_0(tsc, "already defined library");
   tsc->envir = mk_environment(tsc, tsc->global_env, 1);
   libdsc = tr7_cons(tsc, name, mk_environment(tsc, TR7_NIL, 1));
   tsc->libraries = tr7_cons(tsc, libdsc, tsc->libraries);
   libexp = tr7_cons(tsc, libdsc, TR7_NIL);
   s_save(tsc, OP(DEFLIB_EXPORT), libexp);
   return do_define_library_inner(tsc, libexp, TR7_CDR(x));
}
/*************************************************************************
* SECTION SCHEME_EVAL
* -------------------
*/

#if USE_SCHEME_EVAL
/* implement 'eval' */
static eval_status_t op_eval(tr7_engine_t tsc)
{
   if (!TR7_IS_NIL(TR7_CDR(tsc->args)))
      tsc->envir = TR7_CADR(tsc->args);
   return s_eval(tsc, TR7_CAR(tsc->args));
}

/* implement 'environment' */
static eval_status_t op_environment(tr7_engine_t tsc)
{
   s_save(tsc, OP(CURR_ENV), TR7_NIL);
   tsc->envir = mk_environment(tsc, TR7_NIL, 1);
   return s_goto(tsc, OP(IMPORT));
}
#endif

/*************************************************************************
* SECTION SCHEME_REPL
* -------------------
*/
#if USE_SCHEME_REPL
/* implement 'interaction-environment' */
static eval_status_t op_interaction_environment(tr7_engine_t tsc)
{
   return s_return_single(tsc, tsc->global_env);
}
#endif

/* operation 'environment?' */
static eval_status_t op_is_environment(tr7_engine_t tsc)
{
   return s_return_boolean(tsc, TR7_IS_ENVIRONMENT(TR7_CAR(tsc->args)));
}

/* operation 'defined?' */
static eval_status_t op_is_defined(tr7_engine_t tsc)
{
   tr7_t x, env = TR7_IS_NIL(TR7_CDR(tsc->args)) ? tsc->envir : TR7_CADR(tsc->args);
   return s_return_boolean(tsc, environment_get(env, TR7_CAR(tsc->args), &x));
}

/* operation 'new-segment' */
static eval_status_t op_new_segment(tr7_engine_t tsc)
{
   int nr = TR7_IS_NIL(tsc->args) ? 1 : (int)tr7_to_int(TR7_CAR(tsc->args));
   memseg_multi_alloc(tsc, nr, ITEM_SEGSIZE);
   return s_return_true(tsc);
}

/* operation 'oblist' */
static eval_status_t op_oblist(tr7_engine_t tsc)
{
   return s_return_single(tsc, oblist_all_symbols(tsc));
}

/* operation 'current-environment' */
static eval_status_t op_current_environment(tr7_engine_t tsc)
{
   return s_return_single(tsc, tsc->envir);
}

#if USE_DL
/* operation 'load-extension' */
static eval_status_t op_load_extension(tr7_engine_t tsc)
{
   if (!dl_load_ext(tsc, (const char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args))))
      return Error_1(tsc, "unable to load extension", TR7_CAR(tsc->args));
   return s_return_void(tsc);
}
#endif

/*************************************************************************
* SECTION READ
* ------------
*/
enum read_errors {
   read_no_error,
   read_error_oom,
   read_error_unexpected_end,
   read_error_dot_at_begin,
   read_error_dot_at_middle,
   read_error_dot_at_end,
   read_error_unclosed_parenthesis,
   read_error_unopened_parenthesis,
   read_error_unbound_unquote,
   read_error_unbound_datum,
   read_error_duplicated_datum,
   read_error_illegal_token
};




static eval_status_t do_read_set(tr7_engine_t tsc, tr7_t item, int stsok)
{
   tsc->value = item;
   return TR7_IS_NIL(item) ? read_error_oom : stsok;
}

static eval_status_t do_read_set_list2(tr7_engine_t tsc, tr7_t item, int stsok)
{
   return do_read_set(tsc, TR7_LIST2(tsc, item, tsc->value), stsok);
}

static eval_status_t do_read_set_list3(tr7_engine_t tsc, tr7_t item, tr7_t item2, int stsok)
{
   return do_read_set(tsc, TR7_LIST3(tsc, item, item2, tsc->value), stsok);
}

static eval_status_t do_read_mix_status(int prvsts, int newsts)
{
   return prvsts < 0 ? prvsts : newsts < 0 ? newsts : prvsts > newsts ? prvsts : newsts;
}

static eval_status_t do_read(tr7_engine_t tsc, port_t *pt, int funq)
{
   int tok = read_token(tsc, pt);
   return do_read_with_token(tsc, pt, funq, tok);
}

static eval_status_t do_read_list(tr7_engine_t tsc, port_t *pt, int funq)
{
   tr7_t pair, head = TR7_NIL;
   tr7_t *prev = &head;
   int sts = 0, insts, tok;

   tok = read_token(tsc, pt);
   if (tok != TOK_RPAREN) {
      if (tok == TOK_VALUE) {
         if (TR7EQ(tsc->value, SYMBOL(QUASIQUOTE)))
            funq++;
         else if (TR7EQ(tsc->value, SYMBOL(UNQUOTE))
               || TR7EQ(tsc->value, SYMBOL(UNQUOTE_SPLICING)))
            sts = 1;
      }
      while(tok != TOK_RPAREN && tok != TOK_EOF && tok != TOK_DOT) {
         insts = do_read_with_token(tsc, pt, funq, tok);
         if (insts >= 0 && sts >= 0) {
            pair = tr7_cons(tsc, tsc->value, TR7_NIL);
            if (!TR7_IS_PAIR(pair))
               sts = -read_error_oom;
            else {
               *prev = pair;
               prev = &TR7_CDR(pair);
            }
         }
         sts = do_read_mix_status(sts, insts);
         tok = read_token(tsc, pt);
      }
      if (tok == TOK_DOT) {
         if (TR7_IS_NIL(head))
            sts = -read_error_dot_at_begin;
         tok = read_token(tsc, pt);
         if (tok == TOK_RPAREN)
            sts = do_read_mix_status(sts, -read_error_dot_at_end);
         else if (tok != TOK_EOF) {
            insts = do_read_with_token(tsc, pt, funq, tok);
            *prev = tsc->value;
            sts = do_read_mix_status(sts, insts);
            tok = read_token(tsc, pt);
            while (tok != TOK_RPAREN && tok != TOK_EOF) {
               sts = do_read_mix_status(sts, -read_error_dot_at_middle);
               do_read_with_token(tsc, pt, funq, tok);
               tok = read_token(tsc, pt);
            }
         }
      }
      if (tok != TOK_RPAREN)
         sts = do_read_mix_status(sts, -read_error_unclosed_parenthesis);
   }
   tsc->value = sts < 0 ? TR7_NIL : head;
   return sts;
}

static eval_status_t do_read_with_token(tr7_engine_t tsc, port_t *pt, int funq, int tok)
{
   int sts;

   switch (tok) {
   case TOK_VEC_U8: /* #u8( */
      sts = do_read_list(tsc, pt, funq);
      if (sts == 0)
         sts = do_read_set(tsc, list_to_bytevector(tsc, tsc->value), 0);
      else if (sts > 0) {
         sts = do_read_set_list2(tsc, SYMBOL(QUASIQUOTE), sts);
         sts = do_read_set_list3(tsc, SYMBOL(APPLY), SYMBOL(BYTEVECTOR), sts);
         sts = do_read_set_list2(tsc, SYMBOL(UNQUOTE), sts);
      }
      break;

   case TOK_VEC: /* #( */
      sts = do_read_list(tsc, pt, funq);
      if (sts == 0)
         sts = do_read_set(tsc, list_to_vector(tsc, tsc->value), 0);
      else if (sts > 0) {
         sts = do_read_set_list2(tsc, SYMBOL(QUASIQUOTE), sts);
         sts = do_read_set_list3(tsc, SYMBOL(APPLY), SYMBOL(VECTOR), sts);
         sts = do_read_set_list2(tsc, SYMBOL(UNQUOTE), sts);
      }
      break;

   case TOK_RPAREN: /* ) */
      sts = -read_error_unopened_parenthesis;
      break;

   case TOK_LPAREN: /* ( */
      sts = do_read_list(tsc, pt, funq);
      break;

   case TOK_QUOTE: /* ' */
      sts = do_read(tsc, pt, funq);
      if (sts >= 0)
         sts = do_read_set_list2(tsc, SYMBOL(QUOTE), sts);
      break;

   case TOK_BQUOTE: /* ` */
      tok = read_token(tsc, pt);
      switch(tok) {
      case TOK_VEC_U8: /* `#u8( */
         sts = do_read_list(tsc, pt, funq + 1);
         if (sts == 0)
            sts = do_read_set(tsc, list_to_bytevector(tsc, tsc->value), 0);
         else if (sts > 0) {
            sts = do_read_set_list2(tsc, SYMBOL(QUASIQUOTE), sts - 1);
            sts = do_read_set_list3(tsc, SYMBOL(APPLY), SYMBOL(BYTEVECTOR), sts);
         }
         break;
      case TOK_VEC: /* `#( */
         sts = do_read_list(tsc, pt, funq + 1);
         if (sts == 0)
            sts = do_read_set(tsc, list_to_vector(tsc, tsc->value), 0);
         else if (sts > 0) {
            sts = do_read_set_list2(tsc, SYMBOL(QUASIQUOTE), sts - 1);
            sts = do_read_set_list3(tsc, SYMBOL(APPLY), SYMBOL(VECTOR), sts);
         }
         break;
      default: /* ` ? */
         sts = do_read_with_token(tsc, pt, funq + 1, tok);
         if (sts == 0)
            sts = do_read_set_list2(tsc, SYMBOL(QUOTE), 0);
         else if (sts > 0)
            sts = do_read_set_list2(tsc, SYMBOL(QUASIQUOTE), sts - 1);
         break;
      }
      break;

   case TOK_COMMA: /* , */
      sts = do_read(tsc, pt, funq - !!funq);
      if (!funq)
         sts = -read_error_unbound_unquote;
      else if (sts >= 0)
         sts = do_read_set_list2(tsc, SYMBOL(UNQUOTE), sts + 1);
      break;

   case TOK_ATMARK: /* @, */
      sts = do_read(tsc, pt, funq - !!funq);
      if (!funq)
         sts = -read_error_unbound_unquote;
      else if (sts >= 0)
         sts = do_read_set_list2(tsc, SYMBOL(UNQUOTE_SPLICING), sts + 1);
      break;

   case TOK_VALUE: /* ? */
      sts = 0;
      break;

   case TOK_COMDAT: /* #; */
      sts = do_read(tsc, pt, funq);
      sts = do_read_mix_status(sts, do_read(tsc, pt, funq));
      break;

   case TOK_SHARP: /* #... */
      sts = do_read_set_list2(tsc, TR7_FROM_SYNTAX(OP(SHARP)), 0);
      break;

   case TOK_DATSET: /* #N= */
      if (tr7_assq_pair(tsc->value, tsc->datums))
         sts = do_read_mix_status(do_read(tsc, pt, funq), -read_error_duplicated_datum);
      else {
         tr7_t pair = tr7_cons(tsc, tsc->value, TR7_VOID);
         tsc->datums = tr7_cons(tsc, pair, tsc->datums);
         sts = do_read(tsc, pt, funq);
         if (sts >= 0)
            TR7_CDR(pair) = tsc->value;
      }
      break;

   case TOK_DATREF: /* #N# */
      tsc->value = tr7_assq(tsc->value, tsc->datums);
      sts = TR7_IS_NIL(tsc->value) ? -read_error_unbound_datum : 0;
      break;

   case TOK_EOF:
      sts = -read_error_unexpected_end;
      break;

   default:
   case TOK_ERROR:
      sts = -read_error_illegal_token;
      break;
   }
   return sts;
}

static tr7_t solve_datums(tr7_t item, tr7_t datums)
{
   tr7_vector_t vec;
   tr7_uint_t idx, cnt;

   if (TR7_IS_PAIR(item)) {
      if (tr7_memq_pair(item, datums))
         item = TR7_CDR(item);
      else {
         TR7_CAR(item) = solve_datums(TR7_CAR(item), datums);
         TR7_CDR(item) = solve_datums(TR7_CDR(item), datums);
      }
   }
   else if (TR7_IS_VECTOR(item)) {
      vec = TR7_TO_VECTOR(item);
      cnt = TR7_HEAD_UVALUE(vec->head);
      for(idx = 0 ; idx < cnt ; idx++)
         vec->items[idx] = solve_datums(vec->items[idx], datums);
   }
   return item;
}

static eval_status_t do_read_with_datum(tr7_engine_t tsc, port_t *pt)
{
   int sts, tok;
   tsc->datums = TR7_NIL;
   tok = read_token(tsc, pt);
   if (tok == TOK_EOF) {
      tsc->value = TR7_EOF;
      sts = 0;
   }
   else {
      sts = do_read_with_token(tsc, pt, 0, tok);
      if (sts < 0)
         tsc->value = TR7_NIL;
      else if (!TR7_IS_NIL(tsc->datums)) {
         tsc->value = solve_datums(tsc->value, tsc->datums);
         sts = !TR7_IS_NIL(tsc->value);
      }
      tsc->datums = TR7_NIL;
   }
   return sts;
}

static eval_status_t do_read_all_with_datum(tr7_engine_t tsc, port_t *pt)
{
   tr7_t head = TR7_NIL, *tail = &head;
   for(;;) {
      int sts = do_read_with_datum(tsc, pt);
      if (sts < 0) {
         tsc->value = TR7_NIL;
         return sts;
      }
      if (TR7_IS_EOF(tsc->value)) {
         tsc->value = head;
         return 0;
      }
      *tail = tr7_cons(tsc, tsc->value, TR7_NIL);
      if (!TR7_IS_PAIR(*tail)) {
         tsc->value = TR7_NIL;
         return -read_error_oom;
      }
      tail = &TR7_CDR(*tail);
   }
}

/*************************************************************************
* SECTION 
* ------------
*/
static eval_status_t op_sharp(tr7_engine_t tsc)
{
#if WITH_SHARP_HOOK
   tr7_t x;
   if (environment_get(tsc->envir, SYMBOL(SHARP_HOOK), &x))
      return s_exec(tsc, x, tsc->args);
#endif
#if AUTO_SHARP_TO_SYMBOL
   return s_return_single(tsc, tr7_get_symbol_length(tsc,
            (const char*)TR7_CONTENT_STRING(TR7_CAR(tsc->args)),
            TR7_LENGTH_STRING(TR7_CAR(tsc->args)), 1));
#else
   return Error_1(tsc, "undefined sharp expression", TR7_CAR(tsc->args));
#endif
}

#if USE_SCHEME_READ
static eval_status_t op_read(tr7_engine_t tsc)
{
   if (tsc->loadflags >= LOADF_NESTING) {
      int n = tsc->loadflags / LOADF_NESTING;
      tsc->loadflags &= LOADF_NESTING - 1;
      tsc->retcode = -1;
      return Error_1(tsc, "unmatched parentheses:", tr7_from_int(tsc, n));
   }

   if (TR7_IS_PAIR(tsc->args))
      push_stdport(tsc, TR7_CAR(tsc->args), IDX_STDIN);
   return s_goto(tsc, OP(READ_INTERNAL));
}
#endif

/* implement internal read */
static eval_status_t op_read_internal(tr7_engine_t tsc)
{
   int sts;
   port_t *pt;

   pt = TR7__PORT__PORT(get_stdport(tsc, IDX_STDIN));
   sts = do_read_with_datum(tsc, pt);
   if (sts < 0)
      return read_error_1(tsc, "read error", TR7_FROM_INT(-sts));
   return s_return_single(tsc, tsc->value);
}

/* implement repl, part read */
static eval_status_t op_repl_read(tr7_engine_t tsc)
{

   /* If interactive, be nice to user. */
   if (tsc->loadflags & LOADF_SHOW_PROMPT) {
      tsc->envir = tsc->global_env;
      tsc->dump = TR7_NIL;
#if OPTIMIZE_NO_CALLCC
      tsc->savestack = TR7_NIL;
#endif
      if (tsc->strings[Tr7_StrID_Prompt])
         tr7_display_string(tsc, tsc->strings[Tr7_StrID_Prompt]);
   }

   s_guard_kind(tsc, TR7_FROM_OPER(OP(REPL_GUARD)), TR7_GUARD_KIND_ROOT, tsc->envir);

   /* Set up another iteration of REPL */
   tsc->loadflags &= LOADF_NESTING - 1;
   s_save_nil(tsc, OP(REPL_EVAL));
   push_stdport(tsc, tsc->loadport, IDX_STDIN);
   return s_goto(tsc, OP(READ_INTERNAL));
}

/* implement repl, part eval */
static eval_status_t op_repl_eval(tr7_engine_t tsc)
{
   tr7_t x;

   x = tsc->value;
   /* If we reached the end of file, this loop is done. */
   if (TR7_IS_EOF(x)) {
      load_leave(tsc);
      return s_return(tsc);
   }
   if (tsc->loadflags & LOADF_SHOW_EVAL) {
      tr7_write(tsc, x);
      tr7_display_string(tsc, "\n");
   }
   s_save_nil(tsc, OP(REPL_READ));
   s_save_nil(tsc, OP(REPL_PRINT));
   return s_eval(tsc, x);
}

/* implement repl, part print */
static eval_status_t op_repl_print(tr7_engine_t tsc)
{

   /* OP(REPL_PRINT) is always pushed, because when changing from
      non-interactive to interactive mode, it needs to be
      already on the stack */
   if (!TR7_IS_VOID(tsc->value)) {
      if (tsc->tracing)
         log_str(tsc, "\nGives: ");
      if (tsc->tracing || (tsc->loadflags & LOADF_SHOW_RESULT)) {
         tr7_write(tsc, tsc->value);
         tr7_display_string(tsc, "\n");
      }
   }
   return s_return(tsc);
}

/* implement repl, part guard */
static eval_status_t op_repl_guard(tr7_engine_t tsc)
{
   tr7_t x;

   /* no guard found, print the error */
   /* TODO TODO TODO TODO */
   /* TODO TODO TODO TODO */
   x = tsc->args;
   x = TR7_IS_PAIR(x) ? TR7_CAR(x) : x;
   tr7_record_t err = tr7_as_record_cond(x, tsc->error_desc);
   if (err != NULL) {
      log_str(tsc, "Error: ");
      log_item_string(tsc, err->items[1]);
      if (!TR7_IS_NIL(err->items[2])) {
         log_str(tsc, ": ");
         log_item(tsc, err->items[2]);
      }
   }
   else {
      log_str(tsc, "Exception: ");
      log_item(tsc, x);
   }
   log_str(tsc, "\n");
   /* TODO TODO TODO TODO */
   /* TODO hum? env? params? Exit? */
   tsc->retcode = -1;
   return s_goto(tsc, OP(REPL_READ)); /* EXIT? */
}

/* operation 'tr7-tracing' */
static eval_status_t op_tracing(tr7_engine_t tsc)
{
#if USE_TRACING
   tr7_t x = tr7_from_int(tsc, tsc->tracing);
   tsc->tracing = 0 != tr7_to_int(TR7_CAR(tsc->args));
   return s_return_single(tsc, x);
#else
   return Error_unavailable_feature(tsc, "USE_TRACING");
#endif
}

/* operation 'tr7-gc' */
static eval_status_t op_gc(tr7_engine_t tsc)
{
   int was = tsc->gc_verbose;
   if (TR7_IS_PAIR(tsc->args))
      tsc->gc_verbose = !TR7_IS_FALSE(TR7_CAR(tsc->args));
   collect_garbage(tsc);
   tsc->gc_verbose = was;
   return s_return_void(tsc);
}

/* operation 'tr7-gc-verbose' */
static eval_status_t op_gc_verbose(tr7_engine_t tsc)
{
   int was = tsc->gc_verbose;
   if (TR7_IS_PAIR(tsc->args))
      tsc->gc_verbose = !TR7_IS_FALSE(TR7_CAR(tsc->args));
   return s_return_boolean(tsc, was);
}

/* implement show */
static eval_status_t do_show(tr7_engine_t tsc, tr7_uint_t flag)
{
   int was = tsc->loadflags & flag;
   if (TR7_IS_PAIR(tsc->args)) {
      if (TR7_IS_FALSE(TR7_CAR(tsc->args)))
         tsc->loadflags &= ~flag;
      else
         tsc->loadflags |= flag;
   }
   return s_return_boolean(tsc, was);
}

/* operation 'tr7-show-prompt */
static eval_status_t op_show_prompt(tr7_engine_t tsc)
{
   return do_show(tsc, LOADF_SHOW_PROMPT);
}

/* operation 'tr7-show-eval */
static eval_status_t op_show_eval(tr7_engine_t tsc)
{
   return do_show(tsc, LOADF_SHOW_EVAL);
}

/* operation 'tr7-show-result */
static eval_status_t op_show_result(tr7_engine_t tsc)
{
   return do_show(tsc, LOADF_SHOW_RESULT);
}

/* apply tr7-environment->list */
struct env2list_s {
   tr7_engine_t tsc;
   tr7_t list;
};
static int env2list_cb(void *closure, tr7_t sym, tr7_t val)
{
   struct env2list_s *s = (struct env2list_s*)closure;
   s->list = tr7_cons(s->tsc, sym, s->list);
   return 0;
}
static eval_status_t op_environment_list(tr7_engine_t tsc)
{
   struct env2list_s s;
   s.tsc = tsc;
   s.list = TR7_NIL;
   environment_enumerate(TR7_IS_NIL(tsc->args) ? tsc->envir : TR7_CAR(tsc->args), env2list_cb, &s);
   return s_return_single(tsc, s.list);
}

/*****************************************************************************/
static int check_args(tr7_engine_t tsc, tr7_t arglist, int min_args, int max_args, const char *tststr, tr7_t argv[TR7_FF_NARGS_MAXIMUM])
{
   tr7_t arg, argsit = arglist;
   const char *diag;
   char msg[STRBUFFSIZE];
   char nxttst, curtst;
   int i, n;

   /* init testing type of arguments */
   if (!tststr)
      nxttst = curtst = 0;
   else {
      curtst = *tststr++;
      nxttst = curtst ? *tststr++ : 0;
   }
   /* count of scanned arguments */
   n = max_args >= 0 ? max_args : -1 - max_args;
   for(i = 0 ; i < n ; i++, argsit = TR7_CDR(argsit)) {
      if (!TR7_IS_PAIR(argsit)) {
         if (i >= min_args)
            return i;
         snprintf(msg, STRBUFFSIZE, "needs%s %d argument(s)",
                  min_args == max_args ? "" : " at least", min_args);
         goto error;
      }
      arg = argv[i] = TR7_CAR(argsit);
      if (curtst) {
         /* check the type of the argument */
         diag = NULL;
         switch (curtst) {
         case TR7ARG_STRING_NUM: if (!(TR7_IS_STRING(arg))) diag = "string"; break;
         case TR7ARG_SYMBOL_NUM: if (!(TR7_IS_SYMBOL(arg))) diag = "symbol"; break;
         case TR7ARG_PORT_NUM: if (!(TR7_IS_PORT(arg))) diag = "port"; break;
         case TR7ARG_INPORT_NUM: if (!(tr7_is_input_port(arg))) diag = "input port"; break;
         case TR7ARG_OUTPORT_NUM: if (!(tr7_is_output_port(arg))) diag = "output port"; break;
         case TR7ARG_ENVIRONMENT_NUM: if (!(TR7_IS_ENVIRONMENT(arg))) diag = "environment"; break;
         case TR7ARG_PAIR_NUM: if (!(TR7_IS_PAIR(arg))) diag = "pair"; break;
         case TR7ARG_LIST_NUM: if (!(TR7_IS_PAIR(arg) || TR7_IS_NIL(arg))) diag = "pair or '()"; break;
         case TR7ARG_CHAR_NUM: if (!(TR7_IS_CHAR(arg))) diag = "character"; break;
         case TR7ARG_VECTOR_NUM: if (!(TR7_IS_VECTOR(arg))) diag = "vector"; break;
         case TR7ARG_NUMBER_NUM: if (!(tr7_is_number(arg))) diag = "number"; break;
         case TR7ARG_INTEGER_NUM: if (!(tr7_is_integer(arg))) diag = "integer"; break;
         case TR7ARG_NATURAL_NUM: if (!(tr7_is_integer(arg) && tr7_to_int(arg) >= 0)) diag = "non-negative integer"; break;
         case TR7ARG_BYTEVEC_NUM: if (!(TR7_IS_BYTEVECTOR(arg))) diag = "bytevector"; break;
         case TR7ARG_PROC_NUM: if (!(tr7_is_proc(arg))) diag = "proc"; break;
         case TR7ARG_ERROBJ_NUM: if (!tr7_is_error(tsc, arg)) diag = "error-object"; break;
         case TR7ARG_BYTE_NUM: if (!(tr7_is_integer(arg) && tr7_to_int(arg) >= 0 && tr7_to_int(arg) <= 255)) diag = "byte"; break;
         case TR7ARG_RECORD_NUM: if (!tr7_is_record(arg)) diag = "record"; break;
         case TR7ARG_RECORD_DESC_NUM: if (!tr7_is_record_desc(arg)) diag = "record type descriptor"; break;
         default: break;
         }
         if (diag) {
            snprintf(msg, STRBUFFSIZE, "argument %d must be %s", i + 1, diag);
            goto error;
         }
         if (nxttst) {
            curtst = nxttst;
            nxttst = *tststr++;
         }
      }
   }
   if (max_args < 0)
      argv[i] = argsit;
   else if (!TR7_IS_NIL(argsit)) {
      snprintf(msg, STRBUFFSIZE, "needs%s %d argument(s)",
                  min_args == max_args ? "" : " at least", min_args);
      goto error;
   }
   return i;
error:
   tsc->value = mk_error_1(tsc, msg, arglist, tsc->error_desc);
   return -1;
}

static int check_args_nv(tr7_engine_t tsc, tr7_t arglist, int min_args, int max_args, const char *tststr)
{
   tr7_t argv[TR7_FF_NARGS_MAXIMUM];
   return check_args(tsc, arglist, min_args, max_args, tststr, argv);
}

/*****************************************************************************/

/* kernel of this interpreter */
static void Eval_Cycle(tr7_engine_t tsc, opidx_t op)
{
   int r;

   tsc->curop = op;
   for (;;) {
      const opdesc_t *pcd = opdesc_of_index(tsc->curop);
#if USE_TRACING
      if (tsc->tracing) {
         char msg[STRBUFFSIZE];
         snprintf(msg, STRBUFFSIZE, "\n[OP %u%s%s]", (unsigned)tsc->curop, pcd->hassymbol ? " " : "", pcd->hassymbol ? get_opname(tsc->curop, "ILLEGAL!") : "");
         log_str(tsc, msg);
      }
#endif
      /* check arguments */
      if (pcd->check
      && 0 > check_args_nv(tsc, tsc->args, pcd->min_arity, pcd->max_arity, pcd->arg_tests_encoding))
         r = do_raise(tsc, tsc->value, 0);
      else
         r = pcd->func(tsc);
      if (tsc->no_memory) {
         fprintf(stderr, "No memory!\n");
         break;
      }
      ok_to_freely_gc(tsc);
      if (r == Cycle_Exit)
         break;
#if USE_TRACING
      if (r == Cycle_Return && tsc->tracing) {
         log_str(tsc, "\nReturns: ");
         log_item(tsc, tsc->value);
      }
#endif
   }
}


/*
**************************************************************************
* SECTION FEATURING
* -----------------
*
* The feature list
*/
static const char *feature_list[] = {
   "r7rs",
   "tr7",
#if USE_MATH
   "tr7-use-math",
#endif
#if USE_RATIOS
   "ratios",
#endif
};
/*
* init the list of features
*/
static void init_features(tr7_engine_t tsc)
{
   unsigned n = sizeof feature_list / sizeof *feature_list;
   tsc->features = TR7_NIL;
   while (n) {
      tr7_t sf = tr7_get_symbol(tsc, feature_list[--n], 0);
      tsc->features = tr7_cons(tsc, sf, tsc->features);
   }
}

/* ========== Initialization of internals ========== */

/* initialization of TR7 */

/* The interaction-environment has about 300 variables in it. */
#define SYMBOL_DICTIONARY_SIZE 461

void tr7_config_init_default(tr7_config_t *config)
{
   config->main_dictionary_size = SYMBOL_DICTIONARY_SIZE;
   config->malloc = malloc;
   config->free = free;
}

static int scheme_init(tr7_engine_t tsc, tr7_config_t *config)
{
   /* init sink */
   tsc->malloc = config->malloc;
   tsc->free = config->free;
   tsc->no_memory = 0;
#if STRESS_GC_RESILIENCE
   tsc->gc_resilience = 0;
#endif
   tsc->features = TR7_NIL;
   tsc->stdports[IDX_STDIN] = TR7_NIL;
   tsc->stdports[IDX_STDOUT] = TR7_NIL;
   tsc->stdports[IDX_STDERR] = TR7_NIL;
   tsc->loadport = TR7_NIL;
   tsc->loadenv = TR7_NIL;
   tsc->loadflags = 0;
   tsc->global_env = TR7_NIL;
   tsc->libraries = TR7_NIL;
   tsc->params = TR7_NIL;
   tsc->args = TR7_NIL;
   tsc->envir = TR7_NIL;
   tsc->oper = TR7_NIL;
   tsc->dump = TR7_NIL;
#if OPTIMIZE_NO_CALLCC
   tsc->savestack = TR7_NIL;
#endif
   tsc->values = TR7_NIL;
   tsc->value = TR7_NIL;
   tsc->oblist = TR7_NIL;
   tsc->c_nest = TR7_NIL;
   tsc->held = TR7_NIL;
   tsc->datums = TR7_NIL;
   tsc->strings[Tr7_StrID_Path] = NULL;
   tsc->strings[Tr7_StrID_Library_Path] = NULL;
   tsc->strings[Tr7_StrID_Include_Path] = NULL;
   tsc->strings[Tr7_StrID_Extension_Path] = NULL;
   tsc->strings[Tr7_StrID_Prompt] = NULL;

   tsc->free_cells = 0;
   tsc->nmemseg = 0;
   tsc->nsuccfrees = 0;
   tsc->firstfree = NULL;
   tsc->freeshead = NULL;
   tsc->freestail = NULL;

   tsc->strbuff.length = 0;
   tsc->strbuff.size = (unsigned)sizeof tsc->strbuff.buffer;
   tsc->strbuff.head = tsc->strbuff.buffer;

   if (memseg_multi_alloc(tsc, NSEGMENT_INITIAL, ITEM_SEGSIZE) != NSEGMENT_INITIAL) {
      tsc->no_memory = 1;
      return 0;
   }

   tsc->no_recent = 1;           /* dont record recents during init */
   tsc->recent_count = 0;
   tsc->gc_verbose = 0;
   tsc->tracing = 0;

   tsc->oblist = oblist_initial_value(tsc, config->main_dictionary_size);
   /* init global_env */
   tsc->global_env = mk_environment(tsc, TR7_NIL, SYMBOL_DICTIONARY_SIZE);
   tsc->envir = tsc->global_env;
   oblist_add_predefined_symbols(tsc);
   init_features(tsc);
   init_stdports(tsc);
   init_opers(tsc);
   tsc->error_desc = mk_record_desc(tsc, SYMBOL(ERROR), TR7_FALSE, 2);
   tsc->read_error_desc = mk_record_desc(tsc, SYMBOL(ERROR), tsc->error_desc, 0);
   tsc->file_error_desc = mk_record_desc(tsc, SYMBOL(ERROR), tsc->error_desc, 0);

   /* init else */
   new_slot_in_env(tsc, SYMBOL(ELSE), TR7_TRUE);    /* should really else be #t ? yes evals to true */

   tsc->no_recent = 0;           /* init done, record recents */
#if STRESS_GC_RESILIENCE
   tsc->gc_resilience = 1;
#endif
   return !tsc->no_memory;
}

static void scheme_deinit(tr7_engine_t tsc)
{
   unsigned i;

   strbuff_start(tsc);
   tsc->oblist = TR7_NIL;
   tsc->global_env = TR7_NIL;
   tsc->libraries = TR7_NIL;
   tsc->dump = TR7_NIL;
#if OPTIMIZE_NO_CALLCC
   tsc->savestack = TR7_NIL;
#endif
   tsc->envir = TR7_NIL;
   tsc->oper = TR7_NIL;
   tsc->args = TR7_NIL;
   tsc->value = TR7_NIL;
   tsc->values = TR7_NIL;
   tsc->loadport = TR7_NIL;
   tsc->stdports[IDX_STDIN] = TR7_NIL;
   tsc->stdports[IDX_STDOUT] = TR7_NIL;
   tsc->stdports[IDX_STDERR] = TR7_NIL;
   tsc->loadenv = TR7_NIL;
   tsc->datums = TR7_NIL;
   tsc->gc_verbose = 0;
   collect_garbage(tsc);

   for (i = 0; i < tsc->nmemseg; i++)
      tsc->free(tsc->memsegs[i]);
}

tr7_engine_t tr7_engine_create(tr7_config_t *config)
{
   tr7_engine_t tsc;
   tr7_config_t cfg;

   if (!config)
      tr7_config_init_default(&cfg);
   else {
      cfg = *config;
      if (!cfg.malloc || !cfg.free || !cfg.main_dictionary_size)
         return NULL;
   }
   tsc = (tr7_engine_t)cfg.malloc(sizeof(*tsc));
   if (scheme_init(tsc, &cfg))
      return tsc;
   cfg.free(tsc);
   return NULL;
}

void tr7_engine_destroy(tr7_engine_t tsc)
{
   scheme_deinit(tsc);
   tsc->free(tsc);
}

void tr7_set_standard_ports(tr7_engine_t tsc)
{
   tr7_set_ports(tsc, stdin, stdout, stderr);
}

void tr7_set_ports(tr7_engine_t tsc, FILE * fin, FILE * fout, FILE * ferr)
{
   tr7_set_input_port_file(tsc, fin);
   tr7_set_output_port_file(tsc, fout);
   tr7_set_error_port_file(tsc, ferr);
}

void tr7_set_input_port_file(tr7_engine_t tsc, FILE * fin)
{
   set_stdport(tsc, port_from_file(tsc, fin, port_input), IDX_STDIN);
}

void tr7_set_input_port_string(tr7_engine_t tsc, char *start, char *end)
{
   set_stdport(tsc, port_from_string(tsc, TR7_NIL, (uint8_t*)start, (uint8_t*)end), IDX_STDIN);
}

void tr7_set_output_port_file(tr7_engine_t tsc, FILE * fout)
{
   set_stdport(tsc, port_from_file(tsc, fout, port_output), IDX_STDOUT);
}

void tr7_set_error_port_file(tr7_engine_t tsc, FILE * ferr)
{
   set_stdport(tsc, port_from_file(tsc, ferr, port_output), IDX_STDERR);
}

int tr7_load_file(tr7_engine_t tsc, FILE * fin, const char *filename)
{
   if (!load_enter_search_load(tsc, fin, filename,
                           fin == stdin ? LOADF_INTERACTIVE : 0))
      return 0; /* not found */

   tsc->dump = TR7_NIL;
   tsc->envir = tsc->global_env;
   tsc->retcode = 0;
   Eval_Cycle(tsc, OP(REPL_READ));
   if (tsc->retcode == 0)
      tsc->retcode = tsc->loadflags >= LOADF_NESTING;
   return 1;
}

void tr7_load_string(tr7_engine_t tsc, const char *cmd)
{
   load_enter_string(tsc, (uint8_t*)cmd, NULL, 0);

   tsc->dump = TR7_NIL;
   tsc->envir = tsc->global_env;
   tsc->retcode = 0;
   Eval_Cycle(tsc, OP(REPL_READ));
   if (tsc->retcode == 0)
      tsc->retcode = tsc->loadflags >= LOADF_NESTING;
}

void tr7_define(tr7_engine_t tsc, tr7_t envir, tr7_t symbol, tr7_t value)
{
   if (TR7_IS_VOID(envir))
      envir = tsc->envir;
   if (TR7_IS_NIL(envir))
      envir = tsc->global_env;
   environment_set(tsc, symbol, value, envir, 1);
}

void tr7_register_C_func(tr7_engine_t tsc, const char *name, int min_args, int max_args, const char *typargs, tr7_C_func_t func, void *closure)
{
   tr7_define(tsc, tsc->global_env, tr7_get_symbol(tsc, name, 1), tr7_make_C_func(tsc, min_args, max_args, typargs, func, closure));
}

void tr7_register_C_func_list(tr7_engine_t tsc, const tr7_C_func_list_t *list, unsigned count)
{
   const tr7_C_func_list_t *end = &list[count];
   for (; list != end ; list++)
      tr7_register_C_func(tsc, list->name, list->min_args, list->max_args, list->typargs, list->func, list->closure);
}

void tr7_hold(tr7_engine_t tsc, tr7_t value)
{
   tsc->held = tr7_cons(tsc, value, tsc->held);
}

void tr7_unhold(tr7_engine_t tsc, tr7_t value)
{
   tr7_t *p = &tsc->held, c = tsc->held;
   while (!TR7_IS_NIL(c))
      if (!TR7EQ(value, TR7_CAR(c)))
         c = *(p = &TR7_CDR(c));
      else {
         *p = TR7_CDR(c);
         break;
      }
}

void tr7_set_string(tr7_engine_t tsc, tr7_strid_t strid, const char *value)
{
   int idx = (int)strid;
   if (idx >= 0 && idx < (int)(sizeof tsc->strings / sizeof *tsc->strings))
      tsc->strings[idx] = value;
}

void save_from_C_call(tr7_engine_t tsc)
{
   tr7_t recents = wrap_recent_allocs(tsc);
   tr7_t v = tr7_make_vector(tsc, 5);
   tr7_vector_t vec = TR7_TO_VECTOR(v);
   vec->items[1] = tsc->dump;
   vec->items[2] = tsc->envir;
   vec->items[3] = tsc->params;
   vec->items[4] = recents;
   vec->items[0] = tsc->c_nest;
   tsc->c_nest = v;
   tsc->recent_count = 0;
   /* Truncate the dump stack so TS will return here when done, not
      directly resume pre-C-call. */
   tsc->dump = TR7_NIL;
}

void restore_from_C_call(tr7_engine_t tsc)
{
   tr7_vector_t vec = TR7_TO_VECTOR(tsc->c_nest);
   tsc->dump = vec->items[1];
   tsc->envir = vec->items[2];
   tsc->params = vec->items[3];
   tr7_t recents = vec->items[4];
   tsc->recent_count = 0;
   if (!TR7_IS_FALSE(recents))
      push_recent_alloc(tsc, recents);
   tsc->c_nest = vec->items[0];
}

/* "func" and "args" are assumed to be already eval'ed. */
tr7_t tr7_call(tr7_engine_t tsc, tr7_t func, tr7_t args)
{
   tr7_uint_t old_loadflags = tsc->loadflags;
   tsc->loadflags = 0;
   save_from_C_call(tsc);
   tsc->envir = tsc->global_env;
   tsc->retcode = 0;
   tsc->args = args;
   tsc->oper = func;
   Eval_Cycle(tsc, OP(EXEC));
   tsc->loadflags = old_loadflags;
   restore_from_C_call(tsc);
   return tsc->value;
}

tr7_t tr7_eval(tr7_engine_t tsc, tr7_t obj)
{
   tr7_uint_t old_loadflags = tsc->loadflags;
   tsc->loadflags = 0;
   save_from_C_call(tsc);
   tsc->args = obj;
   tsc->retcode = 0;
   Eval_Cycle(tsc, OP(IEVAL));
   tsc->loadflags = old_loadflags;
   restore_from_C_call(tsc);
   return tsc->value;
}

tr7_t tr7_apply0(tr7_engine_t tsc, const char *procname)
{
   return tr7_eval(tsc, tr7_cons(tsc, tr7_get_symbol(tsc, procname, 1), TR7_NIL));
}

#ifdef TR7_EXTRA_CODE
#include TR7_EXTRA_CODE
#endif

#else /* ___OPER_ */
/*
**************************************************************************
*
* Definition of symbols
*/
   _SYMBOL_("call/cc", CALL_CC)
   _SYMBOL_("unquote", UNQUOTE)
   _SYMBOL_("unquote-splicing", UNQUOTE_SPLICING)
   _SYMBOL_("λ", LAMBDA_CHAR)
   _SYMBOL_("else", ELSE)
   _SYMBOL_("=>", FEED_TO)
   _SYMBOL_("...", ELLIPSIS)
   _SYMBOL_("_", UNDERSCORE)
   _SYMBOL_("current-input-port", CURR_INPORT)
   _SYMBOL_("current-output-port", CURR_OUTPORT)
   _SYMBOL_("current-error-port", CURR_ERRPORT)
   _SYMBOL_("scheme", SCHEME)
   _SYMBOL_("only", ONLY)
   _SYMBOL_("except", EXCEPT)
   _SYMBOL_("prefix", PREFIX)
   _SYMBOL_("rename", RENAME)
   _SYMBOL_("library", LIBRARY)
   _SYMBOL_("export", EXPORT)
   _SYMBOL_("include-library-declarations", INCLUDE_LIB_DECL)
   _SYMBOL_("*compile-hook*", COMPILE_HOOK)
#if WITH_SHARP_HOOK
   _SYMBOL_("*sharp-hook*", SHARP_HOOK)
#endif
#if USE_SRFI_136
   _SYMBOL_("mutable", MUTABLE)
   _SYMBOL_("immutable", IMMUTABLE)
#endif

/*
**************************************************************************
* SECTION OPERATIONS
* ------------------
*/

/*================= (scheme base) ====================*/
_BEGIN_LIBRARY_(base, "scheme/base")

   /*------- primitive (4.1) ----------*/
   ___OPER_(op_ieval, NULL, 0, 0, NULL, IEVAL)
   ___OPER_(op_e0args, NULL, 0, 0, NULL, E0ARGS)
#if OPTIMIZE_NO_CALLCC
   ___OPER_(op_e1args_nocc, NULL, 0, 0, NULL, E1ARGS_NOCC)
   ___OPER_(op_e2args_nocc, NULL, 0, 0, NULL, E2ARGS_NOCC)
#endif
   ___OPER_(op_e1args, NULL, 0, 0, NULL, E1ARGS)
   ___OPER_(op_e2args, NULL, 0, 0, NULL, E2ARGS)
   ___OPER_(op_exec, NULL, 0, 0, NULL, EXEC)
   ___OPER_(op_thunk, NULL, 0, 0, NULL, THUNK)
   ___OPER_(op_xapply, NULL, 0, 0, NULL, XAPPLY)
   ___OPER_(op_xeval, NULL, 0, 0, NULL, XEVAL)
   ___OPER_(op_xreturn, NULL, 0, 0, NULL, XRETURN)
   _SYNTAX_(op_quote, "quote", 0, 0, NULL, QUOTE)
   _SYNTAX_(op_lambda, "lambda", 0, 0, NULL, LAMBDA)
   ___OPER_(op_lambda_next, NULL, 0, 0, NULL, LAMBDA_NEXT)
   _SYNTAX_(op_if, "if", 0, 0, NULL, IF)
   ___OPER_(op_if_next, NULL, 0, 0, NULL, IF_NEXT)
   _SYNTAX_(op_set, "set!", 0, 0, NULL, SET)
   ___OPER_(op_set_next, NULL, 0, 0, NULL, SET_NEXT)
   _PROC___(op_include, "include", 1, INF_ARG, TR7ARG_STRING, INCLUDE)
   _PROC___(op_include_ci, "include-ci", 1, INF_ARG, TR7ARG_STRING, INCLUDE_CI)

   /*------- derived (4.2) ----------*/

   _SYNTAX_(op_cond, "cond", 0, 0, NULL, COND)
   ___OPER_(op_cond_next, NULL, 0, 0, NULL, COND_NEXT)
   _SYNTAX_(op_case, "case", 0, 0, NULL, CASE)
   ___OPER_(op_case_next, NULL, 0, 0, NULL, CASE_NEXT)

   _SYNTAX_(op_and, "and", 0, 0, NULL, AND)
   ___OPER_(op_and_next, NULL, 0, 0, NULL, AND_NEXT)
   _SYNTAX_(op_or, "or", 0, 0, NULL, OR)
   ___OPER_(op_or_next, NULL, 0, 0, NULL, OR_NEXT)

   _SYNTAX_(op_when, "when", 0, 0, NULL, WHEN)
   ___OPER_(op_when_next, NULL, 0, 0, NULL, WHEN_NEXT)
   _SYNTAX_(op_unless, "unless", 0, 0, NULL, UNLESS)
   ___OPER_(op_unless_next, NULL, 0, 0, NULL, UNLESS_NEXT)

   _SYNTAX_(op_cond_expand, "cond-expand", 0, 0, NULL, COND_EXPAND)
   ___OPER_(op_cond_expand_then, NULL, 0, 0, NULL, COND_EXPAND_THEN)
   ___OPER_(op_cond_expand_and, NULL, 0, 0, NULL, COND_EXPAND_AND)
   ___OPER_(op_cond_expand_or, NULL, 0, 0, NULL, COND_EXPAND_OR)
   ___OPER_(op_cond_expand_not, NULL, 0, 0, NULL, COND_EXPAND_NOT)

   _SYNTAX_(op_let, "let", 0, 0, NULL, LET0)
   ___OPER_(op_let1, NULL, 0, 0, NULL, LET1)
   ___OPER_(op_let2, NULL, 0, 0, NULL, LET2)
   _SYNTAX_(op_letrec, "letrec", 0, 0, NULL, LET0REC)
   _SYNTAX_(op_let_values, "let-values", 0, 0, NULL, LET0VAL)
   ___OPER_(op_let1val, NULL, 0, 0, NULL, LET1VAL)
   ___OPER_(op_let2val, NULL, 0, 0, NULL, LET2VAL)
   ___OPER_(op_let3, NULL, 0, 0, NULL, LET3)

   _SYNTAX_(op_letstar, "let*", 0, 0, NULL, LET0STAR)
   ___OPER_(op_let1star, NULL, 0, 0, NULL, LET1STAR)
   _SYNTAX_(op_letrecstar, "letrec*", 0, 0, NULL, LET0STARREC)
   _SYNTAX_(op_letstarval, "let*-values", 0, 0, NULL, LET0STARVAL)
   ___OPER_(op_let1starval, NULL, 0, 0, NULL, LET1STARVAL)

   _SYNTAX_(op_begin, "begin", 0, 0, NULL, BEGIN)
   _SYNTAX_(op_do, "do", 0, 0, NULL, DO)

   _PROC___(op_mkparam, "make-parameter", 1, 2, TR7ARG_ANY TR7ARG_PROC, MKPARAM)
   ___OPER_(op_mkparamcvt, NULL, 0, 0, NULL, MKPARAMCVT)
   ___OPER_(op_paramcvt, NULL, 0, 0, NULL, PARAMCVT)
   _SYNTAX_(op_parameterize, "parameterize", 1, INF_ARG, TR7ARG_LIST TR7ARG_ANY, PARAM0)
   ___OPER_(op_param1, NULL, 0, 0, NULL, PARAM1)
   ___OPER_(op_param2, NULL, 0, 0, NULL, PARAM2)

   _SYNTAX_(op_guard, "guard", 0, 0, NULL, GUARD)
   ___OPER_(op_guard_cond, NULL, 0, 0, NULL, GUARD_COND)

   _SYNTAX_(op_quasiquote, "quasiquote", 0, 0, NULL, QUASIQUOTE)
   ___OPER_(op_qq_0, NULL, 0, 0, NULL, QQ_0)
   ___OPER_(op_qq_1, NULL, 0, 0, NULL, QQ_1)
   ___OPER_(op_qq_2, NULL, 0, 0, NULL, QQ_2)
   ___OPER_(op_qq_n, NULL, 0, 0, NULL, QQ_N)
   ___OPER_(op_qq_append, NULL, 0, 0, NULL, QQ_APPEND)
   ___OPER_(op_qq_append_next, NULL, 0, 0, NULL, QQ_APPEND_NEXT)
   ___OPER_(op_qq_cons, NULL, 0, 0, NULL, QQ_CONS)
   ___OPER_(op_qq_consif, NULL, 0, 0, NULL, QQ_CONSIF)

   /*------- macro (4.3) ----------*/
   _SYNTAX_(op_let_syntax, "let-syntax", 0, 0, NULL, LET0SYN)
   _SYNTAX_(op_letrec_syntax, "letrec-syntax", 0, 0, NULL, LET0SYNREC)

   _SYNTAX_(op_macro, "macro", 0, 0, NULL, MACRO)
   ___OPER_(op_macro_next, NULL, 0, 0, NULL, MACRO_NEXT)

   _SYNTAX_(op_import, "import", 0, 0, NULL, IMPORT)
   ___OPER_(op_import_only, NULL, 0, 0, NULL, IMPORT_ONLY)
   ___OPER_(op_import_except, NULL, 0, 0, NULL, IMPORT_EXCEPT)
   ___OPER_(op_import_prefix, NULL, 0, 0, NULL, IMPORT_PREFIX)
   ___OPER_(op_import_rename, NULL, 0, 0, NULL, IMPORT_RENAME)
   ___OPER_(op_import_add, NULL, 0, 0, NULL, IMPORT_ADD)
   ___OPER_(op_import_check, NULL, 0, 0, NULL, IMPORT_CHECK)
   ___OPER_(op_import_get, NULL, 0, 0, NULL, IMPORT_GET)

   _SYNTAX_(op_define, "define", 0, 0, NULL, DEFINE)
   ___OPER_(op_define_next, NULL, 0, 0, NULL, DEFINE_NEXT)

   _SYNTAX_(op_define_values, "define-values", 0, 0, NULL, DEFVAL)
   ___OPER_(op_define_values_next, NULL, 0, 0, NULL, DEFVAL_NEXT)

   _SYNTAX_(op_define_syntax, "define-syntax", 0, 0, NULL, DEFSYN)

   _SYNTAX_(op_define_library, "define-library", 0, 0, NULL, DEFLIB)
   ___OPER_(op_define_library_inner, NULL, 0, 0, NULL, DEFLIB_INNER)
   ___OPER_(op_define_library_export, NULL, 0, 0, NULL, DEFLIB_EXPORT)
   ___OPER_(op_define_library_cond, NULL, 0, 0, NULL, DEFLIB_COND)
   ___OPER_(op_define_library_include, NULL, 0, 0, NULL, IMPORT_LIBDECL)

   /*------- record ----------*/
   _SYNTAX_(op_define_record_type, "define-record-type", 0, 0, NULL, TYPDEF)
   ___OPER_(op_is_type, NULL, 0, 0, NULL, TYPP)
   ___OPER_(op_type_constructor, NULL, 0, 0, NULL, TYPCON)
   ___OPER_(op_type_accessor, NULL, 0, 0, NULL, TYPACC)
   ___OPER_(op_type_modifier, NULL, 0, 0, NULL, TYPMOD)

   /*------- equivalence ----------*/
   _PROC___(op_eq, "eq?", 2, 2, TR7ARG_ANY, EQ)
   _PROC___(op_eqv, "eqv?", 2, 2, TR7ARG_ANY, EQV)
   _PROC___(op_equal, "equal?", 2, 2, TR7ARG_ANY, EQUAL)

   /*------- number ----------*/
   _PROC___(op_is_number, "number?", 1, 1, TR7ARG_ANY, NUMBERP)
   _PROC___(op_is_complex, "complex?", 1, 1, TR7ARG_ANY, COMPLEXP)
   _PROC___(op_is_real, "real?", 1, 1, TR7ARG_ANY, REALP)
   _PROC___(op_is_rational, "rational?", 1, 1, TR7ARG_ANY, RATIONALP)
   _PROC___(op_is_integer, "integer?", 1, 1, TR7ARG_ANY, INTEGERP)

   _PROC___(op_is_exact, "exact?", 1, 1, TR7ARG_NUMBER, EXACTP)
   _PROC___(op_is_inexact, "inexact?", 1, 1, TR7ARG_NUMBER, INEXACTP)
   _PROC___(op_is_exact_int, "exact-integer?", 1, 1, TR7ARG_NUMBER, EXACTINTP)

   _PROC___(op_is_zero, "zero?", 1, 1, TR7ARG_NUMBER, ZEROP)
   _PROC___(op_is_positive, "positive?", 1, 1, TR7ARG_NUMBER, POSITIVEP)
   _PROC___(op_is_negative, "negative?", 1, 1, TR7ARG_NUMBER, NEGATIVEP)
   _PROC___(op_is_odd, "odd?", 1, 1, TR7ARG_INTEGER, ODDP)
   _PROC___(op_is_even, "even?", 1, 1, TR7ARG_INTEGER, EVENP)

   _PROC___(op_number_eq, "=", 2, INF_ARG, TR7ARG_NUMBER, NUMEQ)
   _PROC___(op_number_lt, "<", 2, INF_ARG, TR7ARG_NUMBER, LESS)
   _PROC___(op_number_gt, ">", 2, INF_ARG, TR7ARG_NUMBER, GRE)
   _PROC___(op_number_le, "<=", 2, INF_ARG, TR7ARG_NUMBER, LEQ)
   _PROC___(op_number_ge, ">=", 2, INF_ARG, TR7ARG_NUMBER, GEQ)

   _PROC___(op_max, "max", 1, INF_ARG, TR7ARG_NUMBER, MAX)
   _PROC___(op_min, "min", 1, INF_ARG, TR7ARG_NUMBER, MIN)

   _PROC___(op_add, "+", 0, INF_ARG, TR7ARG_NUMBER, ADD)
   _PROC___(op_mul, "*", 0, INF_ARG, TR7ARG_NUMBER, MUL)
   _PROC___(op_sub, "-", 1, INF_ARG, TR7ARG_NUMBER, SUB)
   _PROC___(op_div, "/", 1, INF_ARG, TR7ARG_NUMBER, DIV)

   _PROC___(op_abs, "abs", 1, 1, TR7ARG_NUMBER, ABS)

   _PROC___(op_floor_div, "floor/", 2, 2, TR7ARG_INTEGER, FLOOR_DIV)
   _PROC___(op_floor_quotient, "floor-quotient", 2, 2, TR7ARG_INTEGER, FLOOR_QUO)
   _PROC___(op_floor_rem, "floor-remainder", 2, 2, TR7ARG_INTEGER, FLOOR_REM)
   _PROC___(op_floor_rem, "modulo", 2, 2, TR7ARG_INTEGER, MOD) /* TODO ALIAS? */
   _PROC___(op_truncate_div, "truncate/", 2, 2, TR7ARG_INTEGER, TRUNC_DIV)
   _PROC___(op_truncate_quotient, "truncate-quotient", 2, 2, TR7ARG_INTEGER, TRUNC_QUO)
   _PROC___(op_truncate_quotient, "quotient", 2, 2, TR7ARG_INTEGER, INTDIV) /* TODO ALIAS? */
   _PROC___(op_truncate_rem, "truncate-remainder", 2, 2, TR7ARG_INTEGER, TRUNC_REM)
   _PROC___(op_truncate_rem, "remainder", 2, 2, TR7ARG_INTEGER, REM) /* TODO ALIAS? */

   _PROC___(op_gcd, "gcd", 0, INF_ARG, TR7ARG_INTEGER, GCD)
   _PROC___(op_lcm, "lcm", 0, INF_ARG, TR7ARG_INTEGER, LCM)

   _PROC___(op_square, "square", 1, 1, TR7ARG_NUMBER, SQUARE)

   _PROC___(op_int_sqrt, "exact-integer-sqrt", 1, 1, TR7ARG_NUMBER, EXACT_SQRT)

#if USE_RATIOS
/*   _PROC___(op_numerator, "numerator", 1, 1, TR7ARG_NUMBER, NUMERATOR) */
/*   _PROC___(op_denominator, "denominator", 1, 1, TR7ARG_NUMBER, DENOMINATOR) */
/*   _PROC___(op_rationalize, "rationalize", 2, 2, TR7ARG_NUMBER, RATIONALIZE) */
#endif

   _PROC___(op_num2str, "number->string", 1, 2, TR7ARG_NUMBER TR7ARG_INTEGER, NUM2STR)
   _PROC___(op_str2num, "string->number", 1, 2, TR7ARG_STRING TR7ARG_INTEGER, STR2NUM)

#if USE_MATH
   _PROC___(op_floor, "floor", 1, 1, TR7ARG_NUMBER, FLOOR)
   _PROC___(op_truncate, "truncate", 1, 1, TR7ARG_NUMBER, TRUNCATE)
   _PROC___(op_ceiling, "ceiling", 1, 1, TR7ARG_NUMBER, CEILING)
   _PROC___(op_round, "round", 1, 1, TR7ARG_NUMBER, ROUND)

   _PROC___(op_expt, "expt", 2, 2, TR7ARG_NUMBER, EXPT)

   _PROC___(op_exact, "exact", 1, 1, TR7ARG_NUMBER, INEX2EX)
   _PROC___(op_inexact, "inexact", 1, 1, TR7ARG_NUMBER, EX2INEX)
#endif

   /*------- boolean ----------*/
   _PROC___(op_is_boolean, "boolean?", 1, 1, NULL, BOOLP)
   _PROC___(op_not, "not", 1, 1, NULL, NOT)
   _PROC___(op_boolean_eq, "boolean=?", 1, INF_ARG, NULL, BOOLEQP)

   /*------- pair and list ----------*/
   _PROC___(op_is_pair, "pair?", 1, 1, TR7ARG_ANY, PAIRP)
   _PROC___(op_cons, "cons", 2, 2, NULL, CONS)
   _PROC___(op_car, "car", 1, 1, TR7ARG_PAIR, CAR)
   _PROC___(op_cdr, "cdr", 1, 1, TR7ARG_PAIR, CDR)
   _PROC___(op_set_car, "set-car!", 2, 2, TR7ARG_PAIR TR7ARG_ANY, SETCAR)
   _PROC___(op_set_cdr, "set-cdr!", 2, 2, TR7ARG_PAIR TR7ARG_ANY, SETCDR)
   _PROC___(op_caar, "caar", 1, 1, TR7ARG_PAIR, CAAR)
   _PROC___(op_cadr, "cadr", 1, 1, TR7ARG_PAIR, CADR)
   _PROC___(op_cdar, "cdar", 1, 1, TR7ARG_PAIR, CDAR)
   _PROC___(op_cddr, "cddr", 1, 1, TR7ARG_PAIR, CDDR)
   _PROC___(op_is_null, "null?", 1, 1, NULL, NULLP)
   _PROC___(op_is_list, "list?", 1, 1, TR7ARG_ANY, LISTP)
   _PROC___(op_make_list, "make-list", 1, 2, TR7ARG_NATURAL TR7ARG_ANY, MKLIST)
   _PROC___(op_list, "list", 0, INF_ARG, TR7ARG_ANY, LIST)
   _PROC___(op_length, "length", 1, 1, TR7ARG_LIST, LIST_LENGTH)
   _PROC___(op_append, "append", 0, INF_ARG, NULL, APPEND)
   _PROC___(op_reverse, "reverse", 1, 1, TR7ARG_LIST, REVERSE)
   _PROC___(op_list_tail, "list-tail", 2, 2, TR7ARG_LIST TR7ARG_NATURAL, LISTTAIL)
   _PROC___(op_list_ref, "list-ref", 2, 2, TR7ARG_LIST TR7ARG_NATURAL, LISTREF)
   _PROC___(op_list_set, "list-set!", 3, 3, TR7ARG_LIST TR7ARG_NATURAL TR7ARG_ANY, LISTSET)
   _PROC___(op_memq, "memq", 2, 2, TR7ARG_ANY TR7ARG_LIST, MEMQ)
   _PROC___(op_memv, "memv", 2, 2, TR7ARG_ANY TR7ARG_LIST, MEMV)
   _PROC___(op_member, "member", 2, 3, TR7ARG_ANY TR7ARG_LIST TR7ARG_PROC, MEMBER)
   ___OPER_(op_member_then, NULL, 0, 0, NULL, MEMBER_THEN)
   _PROC___(op_assq, "assq", 2, 2, TR7ARG_ANY TR7ARG_LIST, ASSQ)
   _PROC___(op_assv, "assv", 2, 2, TR7ARG_ANY TR7ARG_LIST, ASSV)
   _PROC___(op_assoc, "assoc", 2, 3, TR7ARG_ANY TR7ARG_LIST TR7ARG_PROC, ASSOC)
   ___OPER_(op_assoc_then, NULL, 0, 0, NULL, ASSOC_THEN)
   _PROC___(op_list_copy, "list-copy", 1, 1, TR7ARG_ANY, LISTCOPY)

   /*------- symbol ----------*/
   _PROC___(op_is_symbol, "symbol?", 1, 1, TR7ARG_ANY, SYMBOLP)
   _PROC___(op_is_symbol_eq, "symbol=?", 1, INF_ARG, TR7ARG_ANY, SYMBOLEQP)
   _PROC___(op_sym2str, "symbol->string", 1, 1, TR7ARG_SYMBOL, SYM2STR)
   _PROC___(op_str2sym, "string->symbol", 1, 1, TR7ARG_STRING, STR2SYM)

   /*------- char ----------*/
   _PROC___(op_is_char, "char?", 1, 1, TR7ARG_ANY, CHARP)
   _PROC___(op_char_eq, "char=?", 2, INF_ARG, TR7ARG_CHAR, CHAREQP)
   _PROC___(op_char_lt, "char<?", 2, INF_ARG, TR7ARG_CHAR, CHARLTP)
   _PROC___(op_char_gt, "char>?", 2, INF_ARG, TR7ARG_CHAR, CHARGTP)
   _PROC___(op_char_le, "char<=?", 2, INF_ARG, TR7ARG_CHAR, CHARLEP)
   _PROC___(op_char_ge, "char>=?", 2, INF_ARG, TR7ARG_CHAR, CHARGEP)
   _PROC___(op_char2int, "char->integer", 1, 1, TR7ARG_CHAR, CHAR2INT)
   _PROC___(op_int2char, "integer->char", 1, 1, TR7ARG_NATURAL, INT2CHAR)

   /*------- string ----------*/
   _PROC___(op_is_string, "string?", 1, 1, TR7ARG_ANY, STRINGP)
   _PROC___(op_make_string, "make-string", 1, 2, TR7ARG_NATURAL TR7ARG_CHAR, MKSTRING)
   _PROC___(op_string, "string", 0, INF_ARG, TR7ARG_CHAR, STRING)
   _PROC___(op_string_length, "string-length", 1, 1, TR7ARG_STRING, STRLEN)
   _PROC___(op_string_ref, "string-ref", 2, 2, TR7ARG_STRING TR7ARG_NATURAL, STRREF)
   _PROC___(op_string_set, "string-set!", 3, 3, TR7ARG_STRING TR7ARG_NATURAL TR7ARG_CHAR, STRSET)
   _PROC___(op_string_eq, "string=?", 2, INF_ARG, TR7ARG_STRING, STRING_EQP)
   _PROC___(op_string_lt, "string<?", 2, INF_ARG, TR7ARG_STRING, STRING_LTP)
   _PROC___(op_string_gt, "string>?", 2, INF_ARG, TR7ARG_STRING, STRING_GTP)
   _PROC___(op_string_le, "string<=?", 2, INF_ARG, TR7ARG_STRING, STRING_LEP)
   _PROC___(op_string_ge, "string>=?", 2, INF_ARG, TR7ARG_STRING, STRING_GEP)
   _PROC___(op_string_append, "string-append", 0, INF_ARG, TR7ARG_STRING, STRAPPEND)
   _PROC___(op_string_to_list, "string->list", 1, 3, TR7ARG_STRING TR7ARG_NATURAL TR7ARG_NATURAL, STR2LST)
   _PROC___(op_list_to_string, "list->string", 1, 1, TR7ARG_LIST, LST2STR)
   _PROC___(op_string_copy, "substring", 1, 3, TR7ARG_STRING TR7ARG_NATURAL, SUBSTR)
   _PROC___(op_string_copy, "string-copy", 1, 3, TR7ARG_STRING TR7ARG_NATURAL, STRCPY)
   _PROC___(op_string_copy_to, "string-copy!", 3, 5, TR7ARG_STRING TR7ARG_NATURAL TR7ARG_STRING TR7ARG_NATURAL, STRCPYSET)
   _PROC___(op_string_fill, "string-fill!", 2, 4, TR7ARG_STRING TR7ARG_CHAR TR7ARG_NATURAL, STRFILL)

   /*------- vector ----------*/
   _PROC___(op_is_vector, "vector?", 1, 1, TR7ARG_ANY, VECTORP)
   _PROC___(op_make_vector, "make-vector", 1, 2, TR7ARG_NATURAL TR7ARG_ANY, MKVECTOR)
   _PROC___(op_vector, "vector", 0, INF_ARG, NULL, VECTOR)
   _PROC___(op_vector_length, "vector-length", 1, 1, TR7ARG_VECTOR, VECLEN)
   _PROC___(op_vector_ref, "vector-ref", 2, 2, TR7ARG_VECTOR TR7ARG_NATURAL, VECREF)
   _PROC___(op_vector_set, "vector-set!", 3, 3, TR7ARG_VECTOR TR7ARG_NATURAL TR7ARG_ANY, VECSET)
   _PROC___(op_vector_to_list, "vector->list", 1, 3, TR7ARG_VECTOR TR7ARG_NATURAL, VEC2LST)
   _PROC___(op_list_to_vector, "list->vector", 1, 1, TR7ARG_LIST, LST2VEC)
   _PROC___(op_vector_to_string, "vector->string", 1, 3, TR7ARG_VECTOR TR7ARG_NATURAL, VEC2STR)
   _PROC___(op_string_to_vector, "string->vector", 1, 3, TR7ARG_STRING TR7ARG_NATURAL, STR2VEC)
   _PROC___(op_vector_copy, "vector-copy", 1, 3, TR7ARG_VECTOR TR7ARG_NATURAL, VECCPY)
   _PROC___(op_vector_copy_to, "vector-copy!", 3, 5, TR7ARG_VECTOR TR7ARG_NATURAL TR7ARG_VECTOR TR7ARG_NATURAL, VECCPYSET)
   _PROC___(op_vector_append, "vector-append", 1, INF_ARG, TR7ARG_VECTOR, VECAPPEND)
   _PROC___(op_vector_fill, "vector-fill!", 2, 4, TR7ARG_VECTOR TR7ARG_ANY TR7ARG_NATURAL, VECFILL)

   /*------- bytevector ----------*/
   _PROC___(op_is_bytevector, "bytevector?", 1, 1, TR7ARG_ANY, BYTEVECP)
   _PROC___(op_bytevector, "bytevector", 0, INF_ARG, NULL, BYTEVECTOR)
   _PROC___(op_make_bytevector, "make-bytevector", 1, 2, TR7ARG_NATURAL TR7ARG_BYTE, MKBYTEVEC)
   _PROC___(op_bytevector_length, "bytevector-length", 1, 1, TR7ARG_BYTEVEC, BYTEVECLEN)
   _PROC___(op_bytevector_u8_ref, "bytevector-u8-ref", 2, 2, TR7ARG_BYTEVEC TR7ARG_NATURAL, BYTEVECREF)
   _PROC___(op_bytevector_u8_set, "bytevector-u8-set!", 3, 3, TR7ARG_BYTEVEC TR7ARG_NATURAL TR7ARG_BYTE, BYTEVECSET)
   _PROC___(op_bytevector_copy, "bytevector-copy", 1, 3, TR7ARG_BYTEVEC TR7ARG_NATURAL, BYTEVECCOPY)
   _PROC___(op_bytevector_copy_to, "bytevector-copy!", 3, 5, TR7ARG_BYTEVEC TR7ARG_NATURAL TR7ARG_BYTEVEC TR7ARG_NATURAL, BYTEVECCOPYSET)
   _PROC___(op_bytevector_append, "bytevector-append", 1, INF_ARG, TR7ARG_BYTEVEC, BYTEVECAPPEND)
   _PROC___(op_bytevector_fill, "bytevector-fill!", 2, 4, TR7ARG_BYTEVEC TR7ARG_BYTE TR7ARG_NATURAL, BYTEVECFILL)
   _PROC___(op_utf8_to_string, "utf8->string", 1, 3, TR7ARG_BYTEVEC TR7ARG_NATURAL, BYTEVECSTRING)
   _PROC___(op_string_to_utf8, "string->utf8", 1, 3, TR7ARG_STRING TR7ARG_NATURAL, STRINGBYTEVEC)

   /*------- control ----------*/
   _PROC___(op_procedure_p, "procedure?", 1, 1, TR7ARG_ANY, PROCP)
   _PROC___(op_apply, "apply", 1, INF_ARG, NULL, APPLY)
   _PROC___(op_map, "map", 1, INF_ARG, TR7ARG_PROC TR7ARG_LIST, MAP)
   ___OPER_(op_map_then, NULL, 0, 0, NULL, MAP_THEN)
   _PROC___(op_strmap, "string-map", 1, INF_ARG, TR7ARG_PROC TR7ARG_STRING, STRMAP)
   ___OPER_(op_strmap_then, NULL, 0, 0, NULL, STRMAP_THEN)
   _PROC___(op_vecmap, "vector-map", 1, INF_ARG, TR7ARG_PROC TR7ARG_VECTOR, VECMAP)
   ___OPER_(op_vecmap_then, NULL, 0, 0, NULL, VECMAP_THEN)
   _PROC___(op_foreach, "for-each", 2, INF_ARG, TR7ARG_PROC TR7ARG_LIST, FOREACH)
   ___OPER_(op_foreach_then, NULL, 0, 0, NULL, FOREACH_THEN)
   _PROC___(op_strforeach, "string-for-each", 2, INF_ARG, TR7ARG_PROC TR7ARG_STRING, STRFOREACH)
   ___OPER_(op_strforeach_then, NULL, 0, 0, NULL, STRFOREACH_THEN)
   _PROC___(op_vecforeach, "vector-for-each", 2, INF_ARG, TR7ARG_PROC TR7ARG_VECTOR, VECFOREACH)
   ___OPER_(op_vecforeach_then, NULL, 0, 0, NULL, VECFOREACH_THEN)
   _PROC___(op_callcc, "call-with-current-continuation", 1, 1, NULL, CALLCC)
   _PROC___(op_values, "values", 0, INF_ARG, TR7ARG_ANY, VALUES)
   _PROC___(op_callvals, "call-with-values", 2, 2, TR7ARG_ANY, CALLVALS)
   ___OPER_(op_callvals_then, NULL, 0, 0, NULL, CALLVALS_THEN)
   _PROC___(op_dynamic_wind, "dynamic-wind", 3, 3, TR7ARG_PROC, DYNWIND)

   /*------- exception ----------*/
   _PROC___(op_with_exception_handler, "with-exception-handler", 2, 2, TR7ARG_PROC, WITH_EXC_HNDL)
   _PROC___(op_raise, "raise", 1, 1, NULL, RAISE)
   _PROC___(op_raise_continuable, "raise-continuable", 1, 1, NULL, RAISECON)
   ___OPER_(op_reraise, NULL, 0, 0, NULL, RERAISE)
   _PROC___(op_error, "error", 1, INF_ARG, TR7ARG_STRING TR7ARG_ANY, ERROR)
   _PROC___(op_is_error_object, "error-object?", 1, 1, NULL, ERROROBJP)
   _PROC___(op_error_msg, "error-object-message", 1, 1, TR7ARG_ERROBJ, ERRORMSG)
   _PROC___(op_error_irritants, "error-object-irritants", 1, 1, TR7ARG_ERROBJ, ERRORIRRIT)

   _PROC___(op_is_read_error, "read-error?", 1, 1, NULL, READERRP)
   _PROC___(op_is_file_error, "file-error?", 1, 1, NULL, FILEERRP)

   /*------- port ----------*/
   _PROC___(op_call_with_port, "call-with-port", 2, 2, TR7ARG_PORT TR7ARG_PROC, CALLWPORT)

   _PROC___(op_is_input_port, "input-port?", 1, 1, TR7ARG_ANY, INPORTP)
   _PROC___(op_is_output_port, "output-port?", 1, 1, TR7ARG_ANY, OUTPORTP)
   _PROC___(op_is_textual_port, "textual-port?", 1, 1, TR7ARG_ANY, TXTPORTP)
   _PROC___(op_is_binary_port, "binary-port?", 1, 1, TR7ARG_ANY, BINPORTP)
   _PROC___(op_is_port, "port?", 1, 1, TR7ARG_ANY, PORTP)

   _PROC___(op_is_input_port, "input-port-open?", 1, 1, TR7ARG_PORT, INPORTOPENP) /* ALIAS */
   _PROC___(op_is_output_port, "output-port-open?", 1, 1, TR7ARG_PORT, OUTPORTOPENP) /* ALIAS */

   _PROC___(op_close_port, "close-port", 1, 1, TR7ARG_PORT, CLOSE_PORT)
   _PROC___(op_close_input_port, "close-input-port", 1, 1, TR7ARG_INPORT, CLOSE_INPORT)
   _PROC___(op_close_output_port, "close-output-port", 1, 1, TR7ARG_OUTPORT, CLOSE_OUTPORT)

   _PROC___(op_open_input_string, "open-input-string", 1, 1, TR7ARG_STRING, OPEN_INSTRING)
   _PROC___(op_open_output_string, "open-output-string", 0, 0, NULL, OPEN_OUTSTRING)
   _PROC___(op_get_output_string, "get-output-string", 1, 1, TR7ARG_OUTPORT, GET_OUTSTRING)

   _PROC___(op_open_input_bytevector, "open-input-bytevector", 1, 1, TR7ARG_BYTEVEC, OPEN_INBYTEVEC)
   _PROC___(op_open_output_bytevector, "open-output-bytevector", 0, 0, NULL, OPEN_OUTBYTEVEC)
   _PROC___(op_get_output_bytevector, "get-output-bytevector", 1, 1, TR7ARG_OUTPORT, GET_OUTBYTEVEC)

   /*------- input ----------*/
   _PROC___(op_read_char, "read-char", 0, 1, TR7ARG_INPORT, READ_CHAR)
   _PROC___(op_peek_char, "peek-char", 0, 1, TR7ARG_INPORT, PEEK_CHAR)
   _PROC___(op_read_line, "read-line", 0, 1, TR7ARG_INPORT, READ_LINE)
   _PROC___(op_is_eof_object, "eof-object?", 1, 1, NULL, EOFOBJP)
   _PROC___(op_eof_object, "eof-object", 0, 0, NULL, EOFOBJ)
   _PROC___(op_is_char_ready, "char-ready?", 0, 1, TR7ARG_INPORT, CHAR_READY)
   _PROC___(op_read_string, "read-string", 1, 2, TR7ARG_NATURAL TR7ARG_INPORT, READ_STRING)
   _PROC___(op_read_u8, "read-u8", 0, 1, TR7ARG_INPORT, READ_BYTE)
   _PROC___(op_peek_u8, "peek-u8", 0, 1, TR7ARG_INPORT, PEEK_BYTE)
   _PROC___(op_is_u8_ready, "u8-ready?", 0, 1, TR7ARG_INPORT, BYTE_READY)
   _PROC___(op_read_bytevector, "read-bytevector", 1, 2, TR7ARG_NATURAL TR7ARG_INPORT, READ_BYTEVEC)
   _PROC___(op_read_bytevector_to, "read-bytevector!", 1, 4, TR7ARG_BYTEVEC TR7ARG_INPORT TR7ARG_NATURAL, READ_BYTEVEC_SET)

   /*------- output ----------*/
   _PROC___(op_write_char, "write-char", 1, 2, TR7ARG_CHAR TR7ARG_OUTPORT, WRITE_CHAR)
   _PROC___(op_write_newline, "newline", 0, 1, TR7ARG_OUTPORT, NEWLINE)
   _PROC___(op_write_string, "write-string", 1, 4, TR7ARG_STRING TR7ARG_OUTPORT TR7ARG_NATURAL, WRITE_STRING)
   _PROC___(op_write_u8, "write-u8", 1, 2, TR7ARG_NATURAL TR7ARG_OUTPORT, WRITE_U8)
   _PROC___(op_write_bytevector, "write-bytevector", 1, 4, TR7ARG_BYTEVEC TR7ARG_OUTPORT TR7ARG_NATURAL, WRITE_BYTEVEC)
   _PROC___(op_flush_output_port, "flush-output-port", 0, 1, TR7ARG_OUTPORT, FLUSH_OUTPORT)

   /*------- feature ----------*/
   _PROC___(op_features, "features", 0, 0, NULL, FEATURES)
_END_LIBRARY_(base)

/*================= (scheme case-lambda) ====================*/
#if USE_SCHEME_CASE_LAMBDA
_BEGIN_LIBRARY_(case_lambda, "scheme/case-lambda")
   _SYNTAX_(op_case_lambda, "case-lambda", 0, 0, NULL, CASE_LAMBDA)
   ___OPER_(op_case_lambda_next, NULL, 0, 0, NULL, CASE_LAMBDA_NEXT)
_END_LIBRARY_(case_lambda)
#endif

/*================= (scheme char) ====================*/
#if USE_SCHEME_CHAR
_BEGIN_LIBRARY_(char, "scheme/char")
   /* char */
   _PROC___(op_char_eq_ci, "char-ci=?", 2, INF_ARG, TR7ARG_CHAR, CHARCIEQP)
   _PROC___(op_char_lt_ci, "char-ci<?", 2, INF_ARG, TR7ARG_CHAR, CHARCILTP)
   _PROC___(op_char_gt_ci, "char-ci>?", 2, INF_ARG, TR7ARG_CHAR, CHARCIGTP)
   _PROC___(op_char_le_ci, "char-ci<=?", 2, INF_ARG, TR7ARG_CHAR, CHARCILEP)
   _PROC___(op_char_ge_ci, "char-ci>=?", 2, INF_ARG, TR7ARG_CHAR, CHARCIGEP)
   _PROC___(op_char_is_alpha, "char-alphabetic?", 1, 1, TR7ARG_CHAR, CHARALPHAP)
   _PROC___(op_char_is_num, "char-numeric?", 1, 1, TR7ARG_CHAR, CHARNUMP)
   _PROC___(op_char_is_space, "char-whitespace?", 1, 1, TR7ARG_CHAR, CHARWHITEP)
   _PROC___(op_char_is_upper, "char-upper-case?", 1, 1, TR7ARG_CHAR, CHARUPPERP)
   _PROC___(op_char_is_lower, "char-lower-case?", 1, 1, TR7ARG_CHAR, CHARLOWERP)
   _PROC___(op_char_digit_value, "digit-value", 1, 1, TR7ARG_CHAR, DIGVAL)
   _PROC___(op_char_upcase, "char-upcase", 1, 1, TR7ARG_CHAR, CHARUP)
   _PROC___(op_char_downcase, "char-downcase", 1, 1, TR7ARG_CHAR, CHARDOWN)
   _PROC___(op_char_downcase, "char-foldcase", 1, 1, TR7ARG_CHAR, CHARFOLD)
   /* string */
   _PROC___(op_string_eq_ci, "string-ci=?", 2, INF_ARG, TR7ARG_STRING, STRINGCI_EQP)
   _PROC___(op_string_lt_ci, "string-ci<?", 2, INF_ARG, TR7ARG_STRING, STRINGCI_LTP)
   _PROC___(op_string_gt_ci, "string-ci>?", 2, INF_ARG, TR7ARG_STRING, STRINGCI_GTP)
   _PROC___(op_string_le_ci, "string-ci<=?", 2, INF_ARG, TR7ARG_STRING, STRINGCI_LEP)
   _PROC___(op_string_ge_ci, "string-ci>=?", 2, INF_ARG, TR7ARG_STRING, STRINGCI_GEP)
   _PROC___(op_string_upcase, "string-upcase", 1, 1, TR7ARG_STRING, STRUP)
   _PROC___(op_string_downcase, "string-downcase", 1, 1, TR7ARG_STRING, STRDOWN)
   _PROC___(op_string_downcase, "string-foldcase", 1, 1, TR7ARG_STRING, STRFOLD)
_END_LIBRARY_(char)
#endif

/*================= (scheme complex) ====================*/
#if USE_SCHEME_COMPLEX
_BEGIN_LIBRARY_(complex, "scheme/complex")
/*   _PROC___(op_make_rectangular, "make-rectangular", 2, 2, TR7ARG_NUMBER, CPLX_MKREC) */
/*   _PROC___(op_make_polar, "make-polar", 2, 2, TR7ARG_NUMBER, CPLX_MKPOL) */
/*   _PROC___(op_real_part, "real-part", 1, 1, TR7ARG_NUMBER, CPLX_REAL) */
/*   _PROC___(op_imag_part, "imag-part", 1, 1, TR7ARG_NUMBER, CPLX_IMAG) */
/*   _PROC___(op_magnitude, "magnitude", 1, 1, TR7ARG_NUMBER, CPLX_MAGN) */
/*   _PROC___(op_angle, "angle", 1, 1, TR7ARG_NUMBER, CPLX_ANGLE) */
_END_LIBRARY_(complex)
#endif

/*================= (scheme cxr) ====================*/
#if USE_SCHEME_CXR
_BEGIN_LIBRARY_(cxr, "scheme/cxr")
   _PROC___(op_caaar, "caaar", 1, 1, TR7ARG_PAIR, CAAAR)
   _PROC___(op_caadr, "caadr", 1, 1, TR7ARG_PAIR, CAADR)
   _PROC___(op_cadar, "cadar", 1, 1, TR7ARG_PAIR, CADAR)
   _PROC___(op_caddr, "caddr", 1, 1, TR7ARG_PAIR, CADDR)
   _PROC___(op_cdaar, "cdaar", 1, 1, TR7ARG_PAIR, CDAAR)
   _PROC___(op_cdadr, "cdadr", 1, 1, TR7ARG_PAIR, CDADR)
   _PROC___(op_cddar, "cddar", 1, 1, TR7ARG_PAIR, CDDAR)
   _PROC___(op_cdddr, "cdddr", 1, 1, TR7ARG_PAIR, CDDDR)
   _PROC___(op_caaaar, "caaaar", 1, 1, TR7ARG_PAIR, CAAAAR)
   _PROC___(op_caaadr, "caaadr", 1, 1, TR7ARG_PAIR, CAAADR)
   _PROC___(op_caadar, "caadar", 1, 1, TR7ARG_PAIR, CAADAR)
   _PROC___(op_caaddr, "caaddr", 1, 1, TR7ARG_PAIR, CAADDR)
   _PROC___(op_cadaar, "cadaar", 1, 1, TR7ARG_PAIR, CADAAR)
   _PROC___(op_cadadr, "cadadr", 1, 1, TR7ARG_PAIR, CADADR)
   _PROC___(op_caddar, "caddar", 1, 1, TR7ARG_PAIR, CADDAR)
   _PROC___(op_cadddr, "cadddr", 1, 1, TR7ARG_PAIR, CADDDR)
   _PROC___(op_cdaaar, "cdaaar", 1, 1, TR7ARG_PAIR, CDAAAR)
   _PROC___(op_cdaadr, "cdaadr", 1, 1, TR7ARG_PAIR, CDAADR)
   _PROC___(op_cdadar, "cdadar", 1, 1, TR7ARG_PAIR, CDADAR)
   _PROC___(op_cdaddr, "cdaddr", 1, 1, TR7ARG_PAIR, CDADDR)
   _PROC___(op_cddaar, "cddaar", 1, 1, TR7ARG_PAIR, CDDAAR)
   _PROC___(op_cddadr, "cddadr", 1, 1, TR7ARG_PAIR, CDDADR)
   _PROC___(op_cdddar, "cdddar", 1, 1, TR7ARG_PAIR, CDDDAR)
   _PROC___(op_cddddr, "cddddr", 1, 1, TR7ARG_PAIR, CDDDDR)
_END_LIBRARY_(cxr)
#endif

/*================= (scheme eval) ====================*/
#if USE_SCHEME_EVAL
_BEGIN_LIBRARY_(eval, "scheme/eval")
   _PROC___(op_environment, "environment", 0, INF_ARG, NULL, ENV)
   _PROC___(op_eval, "eval", 1, 2, TR7ARG_ANY TR7ARG_ENVIRONMENT, EVAL)
_END_LIBRARY_(eval)
#endif

/*================= (scheme file) ====================*/
#if USE_SCHEME_FILE
_BEGIN_LIBRARY_(file, "scheme/file")
   _PROC___(op_call_with_input_file, "call-with-input-file", 2, 2, TR7ARG_STRING TR7ARG_PROC, CALLWINFILE)
   _PROC___(op_call_with_output_file, "call-with-output-file", 2, 2, TR7ARG_STRING TR7ARG_PROC, CALLWOUTFILE)
   _PROC___(op_delete_file, "delete-file", 1, 1, TR7ARG_STRING, DELETE_FILE)
   _PROC___(op_file_exists, "file-exists?", 1, 1, TR7ARG_STRING, FILE_EXISTS_P)

   _PROC___(op_open_binary_input_file, "open-binary-input-file", 1, 1, TR7ARG_STRING, OPEN_BININFILE)
   _PROC___(op_open_binary_output_file, "open-binary-output-file", 1, 1, TR7ARG_STRING, OPEN_BINOUTFILE)
   _PROC___(op_open_input_file, "open-input-file", 1, 1, TR7ARG_STRING, OPEN_INFILE)
   _PROC___(op_open_output_file, "open-output-file", 1, 1, TR7ARG_STRING, OPEN_OUTFILE)

   _PROC___(op_with_input_file, "with-input-from-file", 2, 2, TR7ARG_STRING TR7ARG_PROC, WITHINFILE)
   _PROC___(op_with_output_file, "with-output-to-file", 2, 2, TR7ARG_STRING TR7ARG_PROC, WITHOUTFILE)
_END_LIBRARY_(file)
#endif

/*================= (scheme inexact) ====================*/
#if USE_SCHEME_INEXACT
_BEGIN_LIBRARY_(inexact, "scheme/inexact")
   _PROC___(op_is_finite, "finite?", 1, 1, TR7ARG_NUMBER, FINITEP)
   _PROC___(op_is_infinite, "infinite?", 1, 1, TR7ARG_NUMBER, INFINITEP)
   _PROC___(op_is_nan, "nan?", 1, 1, TR7ARG_NUMBER, NANP)

#if USE_MATH
   _PROC___(op_exp, "exp", 1, 1, TR7ARG_NUMBER, EXP)
   _PROC___(op_log, "log", 1, 1, TR7ARG_NUMBER, LOG) /* 1, 2!! */
   _PROC___(op_sin, "sin", 1, 1, TR7ARG_NUMBER, SIN)
   _PROC___(op_cos, "cos", 1, 1, TR7ARG_NUMBER, COS)
   _PROC___(op_tan, "tan", 1, 1, TR7ARG_NUMBER, TAN)
   _PROC___(op_asin, "asin", 1, 1, TR7ARG_NUMBER, ASIN)
   _PROC___(op_acos, "acos", 1, 1, TR7ARG_NUMBER, ACOS)
   _PROC___(op_atan, "atan", 1, 2, TR7ARG_NUMBER, ATAN)
   _PROC___(op_sqrt, "sqrt", 1, 1, TR7ARG_NUMBER, SQRT)
#endif
_END_LIBRARY_(inexact)
#endif

/*================= (scheme lazy) ====================*/
#if USE_SCHEME_LAZY
_BEGIN_LIBRARY_(lazy, "scheme/lazy")
   _SYNTAX_(op_delay, "delay", 0, 0, NULL, DELAY)
   _PROC___(op_is_promise, "promise?", 1, 1, TR7ARG_ANY, PROMISEP)
   _PROC___(op_force, "force", 1, 1, TR7ARG_ANY, FORCE)
   _SYNTAX_(op_delay_force, "delay-force", 1, 1, TR7ARG_ANY, DELAYFORCE)
   ___OPER_(op_save_forced, NULL, 0, 0, NULL, SAVE_FORCED)
   ___OPER_(op_force_delayed, NULL, 0, 0, NULL, FORCE_DELAYED)
   _PROC___(op_make_promise, "make-promise", 1, 1, TR7ARG_ANY, MKPROMISE)
_END_LIBRARY_(lazy)
#endif

/*================= (scheme load) ====================*/
#if USE_SCHEME_LOAD
_BEGIN_LIBRARY_(load, "scheme/load")
   _PROC___(op_load, "load", 1, 2, TR7ARG_STRING TR7ARG_ENVIRONMENT, LOAD)
_END_LIBRARY_(load)
#endif

/*================= (scheme process-context) ====================*/
#if USE_SCHEME_PROCESS_CONTEXT
_BEGIN_LIBRARY_(process_context, "scheme/process-context")
   _PROC___(op_command_line, "command-line", 0, 0, NULL, CMDLINE)
   _PROC___(op_exit, "exit", 0, 1, NULL, EXIT)
   _PROC___(op_emergency_exit, "emergency-exit", 0, 1, NULL, EMERGEXIT)
   _PROC___(op_get_env_var, "get-environment-variable", 1, 1, TR7ARG_STRING, GETENVVAR)
   _PROC___(op_get_env_vars, "get-environment-variables", 0, 0, NULL, GETENVVARS)
_END_LIBRARY_(process_context)
#endif

/*================= (scheme read) ====================*/
#if USE_SCHEME_READ
_BEGIN_LIBRARY_(read, "scheme/read")
   _PROC___(op_read, "read", 0, 1, TR7ARG_INPORT, READ)
_END_LIBRARY_(read)
#endif

/*================= (scheme repl) ====================*/
#if USE_SCHEME_REPL
_BEGIN_LIBRARY_(repl, "scheme/repl")
   _PROC___(op_interaction_environment, "interaction-environment", 0, 0, NULL, INT_ENV)
_END_LIBRARY_(repl)
#endif

/*================= (scheme time) ====================*/
#if USE_SCHEME_TIME
_BEGIN_LIBRARY_(time, "scheme/time")
   _PROC___(op_current_second, "current-second", 0, 0, NULL, CURRENT_SECOND)
   _PROC___(op_current_jiffy, "current-jiffy", 0, 0, NULL, CURRENT_JIFFY)
   _PROC___(op_jiffies_per_second, "jiffies-per-second", 0, 0, NULL, JIFFIES_PER_SECOND)
_END_LIBRARY_(time)
#endif

/*================= (scheme write) ====================*/
#if USE_SCHEME_WRITE
_BEGIN_LIBRARY_(write, "scheme/write")
   _PROC___(op_write, "write", 1, 2, TR7ARG_ANY TR7ARG_OUTPORT, WRITE)
   _PROC___(op_write_shared, "write-shared", 1, 2, TR7ARG_ANY TR7ARG_OUTPORT, WRITE_SHARED)
   _PROC___(op_write_simple, "write-simple", 1, 2, TR7ARG_ANY TR7ARG_OUTPORT, WRITE_SIMPLE)
   _PROC___(op_display, "display", 1, 2, TR7ARG_ANY TR7ARG_OUTPORT, DISPLAY)
_END_LIBRARY_(write)
#endif

/*================= (srfi 136) ====================*/
#if USE_SRFI_136
_BEGIN_LIBRARY_(srfi_136, "srfi/136")
   _PROC___(op_is_record, "record?", 1, 1, TR7ARG_ANY, RECORDP)
   _PROC___(op_is_record_desc, "record-type-descriptor?", 1, 1, TR7ARG_ANY, RECDESCP)
   _PROC___(op_record_desc, "record-type-descriptor", 1, 1, TR7ARG_RECORD, RECDESC)
   _PROC___(op_record_desc_pred, "record-type-predicate", 1, 1, TR7ARG_RECORD_DESC, RECDESCPRED)
   _PROC___(op_record_desc_name, "record-type-name", 1, 1, TR7ARG_RECORD_DESC, RECDESCNAME)
   _PROC___(op_record_desc_parent, "record-type-parent", 1, 1, TR7ARG_RECORD_DESC, RECDESCPARENT)
   _PROC___(op_record_desc_fields, "record-type-fields", 1, 1, TR7ARG_RECORD_DESC, RECDESCFIELDS)
   _PROC___(op_make_record_desc, "make-record-type-descriptor", 2, 3, TR7ARG_SYMBOL TR7ARG_LIST TR7ARG_ANY, MKRECDESC)
   _PROC___(op_make_record, "make-record", 2, 2, TR7ARG_RECORD_DESC TR7ARG_VECTOR, MKRECORD)
_END_LIBRARY_(srfi_136)
#endif

/*================= not standard ====================*/
_BEGIN_LIBRARY_(tr7_environment, "tr7/environment")
   _PROC___(op_is_environment, "environment?", 1, 1, TR7ARG_ANY, ENVP)
   _PROC___(op_is_defined, "defined?", 1, 2, TR7ARG_SYMBOL TR7ARG_ENVIRONMENT, DEFP)
   _PROC___(op_oblist, "oblist", 0, 0, NULL, OBLIST)
   _PROC___(op_current_environment, "current-environment", 0, 0, NULL, CURR_ENV)
   _PROC___(op_environment_list, "tr7-environment->list", 0, 1, TR7ARG_ENVIRONMENT, ENVIRONMENT_LIST)
#if USE_SCHEME_R5RS
/*   _PROC___(op_schreportenv, "scheme-report-environment", 1, 1, TR7ARG_NATURAL, SCH_ENV) */
/*   _PROC___(op_nullenv, "null-environment", 1, 1, TR7ARG_NATURAL, NUL_ENV) */
#endif
_END_LIBRARY_(tr7_environment)

#if USE_DL
_BEGIN_LIBRARY_(tr7_extension, "tr7/extension")
   _PROC___(op_load_extension, "load-extension", 1, 1, TR7ARG_STRING, LOADEXT)
_END_LIBRARY_(tr7_extension)
#endif

_BEGIN_LIBRARY_(tr7_sharp, "tr7/sharp")
   ___OPER_(op_sharp, NULL, 0, 0, NULL, SHARP)
   ___OPER_(op_read_internal, NULL, 0, 0, NULL, READ_INTERNAL)
_END_LIBRARY_(tr7_sharp)

_BEGIN_LIBRARY_(tr7_gc, "tr7/gc")
   _PROC___(op_gc, "tr7-gc", 0, 1, NULL, GC)
   _PROC___(op_gc_verbose, "tr7-gc-verbose", 0, 1, NULL, GCVERB)
   _PROC___(op_new_segment, "new-segment", 0, 1, TR7ARG_NUMBER, NEWSEGMENT)
_END_LIBRARY_(tr7_gc)

_BEGIN_LIBRARY_(tr7_trace, "tr7/trace")
   _PROC___(op_tracing, "tr7-tracing", 1, 1, TR7ARG_NATURAL, TRACING)
   _PROC___(op_show_prompt, "tr7-show-prompt", 0, 1, NULL, SHOW_PROMPT)
   _PROC___(op_show_eval, "tr7-show-eval", 0, 1, NULL, SHOW_EVAL)
   _PROC___(op_show_result, "tr7-show-result", 0, 1, NULL, SHOW_RESULT)
_END_LIBRARY_(tr7_trace)

_BEGIN_LIBRARY_(tr7_repl, "tr7/repl")
   ___OPER_(op_repl_read, NULL, 0, 0, NULL, REPL_READ)
   ___OPER_(op_repl_eval, NULL, 0, 0, NULL, REPL_EVAL)
   ___OPER_(op_repl_print, NULL, 0, 0, NULL, REPL_PRINT)
   ___OPER_(op_repl_guard, NULL, 0, 0, NULL, REPL_GUARD)
_END_LIBRARY_(tr7_repl)

/*================= tuning ====================*/
#ifdef TR7_EXTRA_CODE
#include TR7_EXTRA_CODE
#endif
/*
**************************************************************************
*/
#undef ___OPER_
#undef _PROC___
#undef _SYNTAX_
#undef _SYMBOL_
#undef _BEGIN_LIBRARY_
#undef _END_LIBRARY_

#endif /* ___OPER_ */
/*
Local variables:
c-file-style: "k&r"
indent: -kr -nut -i3 -l200 -br -nce
End:
vim: noai ts=3 sw=3 expandtab
*/
